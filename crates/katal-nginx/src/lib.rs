use compact_str::CompactString;
use core::fmt;

pub struct RawFragment<S = CompactString> {
  pub text: S,
}

pub struct LocationDirective<S = CompactString> {
  pub pattern: LocationPattern<S>,
  pub body: RawFragment<S>,
  pub indent: Option<u32>,
}

impl<S> fmt::Display for LocationDirective<S>
where
  S: AsRef<str>,
{
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    write!(f, "location ")?;
    match &self.pattern {
      LocationPattern::Exact(pat) => write!(f, "= {}", pat.as_ref())?,
      LocationPattern::WeakPrefix(pat) => f.write_str(pat.as_ref())?,
      LocationPattern::StrongPrefix(pat) => write!(f, "^~ {}", pat.as_ref())?,
      LocationPattern::Regex {
        text,
        case_sensitive: true,
      } => write!(f, "~ {}", text.as_ref())?,
      LocationPattern::Regex {
        text,
        case_sensitive: false,
      } => write!(f, "~* {}", text.as_ref())?,
      LocationPattern::Named(name) => write!(f, "@{}", name.as_ref())?,
    }
    writeln!(f, " {{")?;
    f.write_str(self.body.text.as_ref())?;
    writeln!(f, "}}")?;
    Ok(())
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum LocationPattern<S = CompactString> {
  /// Exact path match
  Exact(S),
  /// Path prefix, matched if there is no matching regex or longer prefix
  WeakPrefix(S),
  /// Path prefix, matched if there is no longer prefix
  StrongPrefix(S),
  /// Regular expression
  Regex { text: S, case_sensitive: bool },
  /// Named route reference
  Named(S),
}
