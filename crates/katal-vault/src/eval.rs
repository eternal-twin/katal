use crate::{decrypt, DecryptError};
use base64::Engine;
use serde_json::{Map as JsonMap, Value as JsonValue};
use sha3::Shake256;
use std::collections::HashMap;
use std::str::FromStr;
use thiserror::Error;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum VaultInput {
  // Encrypted value
  Aead(String),
  // Seed for generated secret
  Generate(String),
  // Same as other password
  Get(Vec<String>),
  // UTF-8 text
  Text(String),
}

#[derive(Debug, Error)]
pub enum VaultInputParseError {
  /// Invalid scheme
  #[error("invalid scheme")]
  Scheme,
}

impl FromStr for VaultInput {
  type Err = VaultInputParseError;

  fn from_str(s: &str) -> Result<Self, Self::Err> {
    if let Some(encrypted) = s.strip_prefix("aead:") {
      Ok(Self::Aead(encrypted.to_string()))
    } else if let Some(seed) = s.strip_prefix("generate:") {
      Ok(Self::Generate(seed.to_string()))
    } else if let Some(path) = s.strip_prefix("get:") {
      Ok(Self::Get(path.split('.').map(String::from).collect()))
    } else if let Some(secret) = s.strip_prefix("text:") {
      Ok(Self::Text(secret.to_string()))
    } else {
      Err(Self::Err::Scheme)
    }
  }
}

#[derive(Debug, Error)]
pub enum EvalVaultError {
  /// Invalid vault input format
  #[error("invalid vault input format at {0:?}")]
  Format(Vec<String>, #[source] VaultInputParseError),
  /// Invalid input type (must be a string)
  #[error("invalid vault input type at {0:?}")]
  Type(Vec<String>),
  /// Failed to decrypt aead
  #[error("aead decryption error at {0:?}")]
  Decrypt(Vec<String>, #[source] DecryptError),
  /// Detected cycle between `get` inputs
  #[error("detected cycle between `get` inputs`")]
  GetCycle,
}

pub fn eval_vault(
  key: &[u8],
  mut input: JsonMap<String, JsonValue>,
) -> Result<JsonMap<String, JsonValue>, EvalVaultError> {
  let mut flat_input = {
    let mut flat_input: HashMap<Vec<String>, VaultInput> = HashMap::new();
    let path: Vec<String> = Vec::new();
    flatten_input(&input, &path, &mut flat_input)?;
    flat_input
  };
  let mut flat_output: HashMap<Vec<String>, String> = HashMap::new();
  let mut remaining = flat_input.len();
  while remaining > 0 {
    let mut new_flat_input: HashMap<Vec<String>, VaultInput> = HashMap::new();
    for (path, value_input) in flat_input {
      match value_input {
        VaultInput::Generate(seed) => {
          let secret = generate(key, &path, seed.as_bytes());
          flat_output.insert(path, secret);
        }
        VaultInput::Aead(katal_cipher) => match decrypt(&path, &katal_cipher, key) {
          Ok(secret) => {
            let secret = String::from_utf8(secret).unwrap();
            flat_output.insert(path, secret);
          }
          Err(e) => {
            return Err(EvalVaultError::Decrypt(path, e));
          }
        },
        VaultInput::Get(target) => {
          if let Some(resolved) = flat_output.get(&target) {
            flat_output.insert(path, resolved.clone());
          } else {
            new_flat_input.insert(path, VaultInput::Get(target));
          }
        }
        VaultInput::Text(secret) => {
          flat_output.insert(path, secret);
        }
      }
    }
    flat_input = new_flat_input;

    let new_remaining = flat_input.len();
    if new_remaining < remaining {
      remaining = new_remaining;
    } else {
      return Err(EvalVaultError::GetCycle);
    }
  }

  unflatten_output(&mut input, &[], &mut flat_output);

  Ok(input)
}

fn flatten_input(
  input: &JsonMap<String, JsonValue>,
  path: &[String],
  out: &mut HashMap<Vec<String>, VaultInput>,
) -> Result<(), EvalVaultError> {
  for (key, value) in input {
    let mut child_path = path.to_vec();
    child_path.push(key.clone());
    match value {
      JsonValue::Object(child) => {
        flatten_input(child, &child_path, out)?;
      }
      JsonValue::String(s) => match VaultInput::from_str(s) {
        Err(e) => return Err(EvalVaultError::Format(child_path, e)),
        Ok(val) => {
          out.insert(child_path, val);
        }
      },
      _ => return Err(EvalVaultError::Type(child_path)),
    }
  }
  Ok(())
}

fn unflatten_output(target: &mut JsonMap<String, JsonValue>, path: &[String], flat: &mut HashMap<Vec<String>, String>) {
  for (key, value) in target {
    let mut child_path = path.to_vec();
    child_path.push(key.clone());
    match value {
      JsonValue::Object(child) => {
        unflatten_output(child, &child_path, flat);
      }
      target_val @ JsonValue::String(_) => match flat.remove(&child_path) {
        None => unreachable!("path for string should be in flat output at this stage"),
        Some(val) => {
          *target_val = JsonValue::String(val);
        }
      },
      _ => unreachable!("only string and objects should be left at this stage"),
    }
  }
}

fn generate(key: &[u8], path: &[String], seed: &[u8]) -> String {
  use digest::{ExtendableOutput, Update, XofReader};

  let path = serde_json::to_string(&path).unwrap();
  let path = path.as_bytes();

  // Use `0u8` to escape inner values

  let mut hasher = Shake256::default();
  for (i, field) in [key, path, seed].into_iter().enumerate() {
    let i: u8 = i.try_into().unwrap();
    hasher.update(&[i + 1, 0u8, i + 1]);
    for (i, chunk) in <[u8]>::split(field, |x| *x == 0u8).enumerate() {
      if i > 0 {
        hasher.update(&[0u8, 0u8]);
      }
      hasher.update(chunk);
    }
    hasher.update(&[i + 1, 0u8, i + 1]);
  }

  let mut reader = hasher.finalize_xof();
  // Use a multiple of 3 for the length to avoid padding
  // Multiple of 3 bytes => multiple of 24 bits => evenly divided by
  // the 6-bits of base64.
  let mut secret: [u8; 15] = [0u8; 15];
  reader.read(&mut secret);
  base64::engine::general_purpose::URL_SAFE_NO_PAD.encode(secret)
}
