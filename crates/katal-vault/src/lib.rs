pub mod eval;

use aead::{Aead as _, KeyInit as _, Payload};
use aes_gcm_siv::{Aes256GcmSiv, Key, KeySizeUser, Nonce};
use base64::Engine;
use digest::{ExtendableOutput, Update, XofReader};
use generic_array014::GenericArray;
use rand::rngs::StdRng;
use rand::{RngCore, SeedableRng};
use sha3::Shake256;
use static_assertions::assert_type_eq_all;
use thiserror::Error;
use typenum::Unsigned;

type Aes256KeySize = <Aes256GcmSiv as KeySizeUser>::KeySize;
assert_type_eq_all!(Aes256KeySize, typenum::U32);
const AES_256_KEY_SIZE: usize = Aes256KeySize::USIZE;
type Aes256NonceSize = typenum::U12;
const AES_256_NONCE_SIZE: usize = Aes256NonceSize::USIZE;

assert_type_eq_all!(GenericArray<u8, Aes256NonceSize>, Nonce);

pub fn encrypt(path: Vec<String>, clear_value: Vec<u8>, clear_password: &[u8]) -> String {
  let additional_associated_data = serde_json::to_string(&path).unwrap();
  let clear_payload = Payload {
    msg: clear_value.as_slice(),
    aad: additional_associated_data.as_bytes(),
  };
  let (nonce, cipher_payload) = encrypt_payload(clear_payload, clear_password);
  to_katal_string((nonce, cipher_payload))
}

fn encrypt_payload(clear_payload: Payload, clear_password: &[u8]) -> ([u8; AES_256_NONCE_SIZE], Vec<u8>) {
  let raw_key: [u8; AES_256_KEY_SIZE] = password_to_key(clear_password);
  let raw_nonce: [u8; AES_256_NONCE_SIZE] = generate_nonce();
  let key: Key<Aes256GcmSiv> = Key::<Aes256GcmSiv>::from(raw_key);
  let nonce: Nonce = Nonce::from(raw_nonce);
  let cipher: Aes256GcmSiv = Aes256GcmSiv::new(&key);
  // let payload = Payload::from(clear_value.as_slice());
  let cipher_payload = cipher.encrypt(&nonce, clear_payload).unwrap();
  (raw_nonce, cipher_payload.to_vec())
}

#[derive(Debug, Error)]
pub enum DecryptError {
  /// The decryption failed, most likely because of a wrong vault password or value path
  #[error("decryption error")]
  Decrypt,
}

pub fn decrypt(path: &[String], katal_str: &str, clear_password: &[u8]) -> Result<Vec<u8>, DecryptError> {
  let (nonce, cipher_payload) = from_katal_string(katal_str);

  let additional_associated_data = serde_json::to_string(&path).unwrap();
  let cipher_payload = Payload {
    msg: cipher_payload.as_slice(),
    aad: additional_associated_data.as_bytes(),
  };
  decrypt_payload(nonce, cipher_payload, clear_password)
}

fn decrypt_payload(
  nonce: [u8; AES_256_NONCE_SIZE],
  cipher_payload: Payload,
  clear_password: &[u8],
) -> Result<Vec<u8>, DecryptError> {
  let key: [u8; AES_256_KEY_SIZE] = password_to_key(clear_password);
  let key: &Key<Aes256GcmSiv> = Key::<Aes256GcmSiv>::from_slice(&key);
  let cipher = Aes256GcmSiv::new(key);
  let clear_value = cipher
    .decrypt(Nonce::from_slice(&nonce), cipher_payload)
    .map_err(|_e: aead::Error| DecryptError::Decrypt)?;
  Ok(clear_value.to_vec())
}

// TODO: Use `concat-kdf` crate?
fn password_to_key(clear_password: &[u8]) -> [u8; AES_256_KEY_SIZE] {
  let mut hasher = Shake256::default();
  hasher.update(clear_password);
  let mut reader = hasher.finalize_xof();
  let mut key: [u8; AES_256_KEY_SIZE] = [0; AES_256_KEY_SIZE];
  reader.read(&mut key);
  key
}

fn generate_nonce() -> [u8; AES_256_NONCE_SIZE] {
  let mut rng = StdRng::from_entropy();
  let mut nonce = [0; AES_256_NONCE_SIZE];
  rng.fill_bytes(&mut nonce);
  nonce
}

/// Inspired by the PHC format
/// <https://docs.rs/password-hash/latest/password_hash/struct.PasswordHash.html?
fn to_katal_string((nonce, payload): ([u8; AES_256_NONCE_SIZE], Vec<u8>)) -> String {
  let nonce = base64::engine::general_purpose::STANDARD.encode(nonce);
  let payload = base64::engine::general_purpose::STANDARD.encode(payload);
  format!("$katal$v=1$${nonce}${payload}")
}

fn from_katal_string(katal_str: &str) -> ([u8; AES_256_NONCE_SIZE], Vec<u8>) {
  let fields: [&str; 6] = katal_str.split('$').collect::<Vec<_>>().try_into().unwrap();
  assert_eq!(fields[0], "");
  assert_eq!(fields[1], "katal");
  assert_eq!(fields[2], "v=1");
  assert_eq!(fields[3], "");
  let nonce = base64::engine::general_purpose::STANDARD.decode(fields[4]).unwrap();
  let payload = base64::engine::general_purpose::STANDARD.decode(fields[5]).unwrap();
  (nonce.try_into().unwrap(), payload)
}
