use crate::Host;
use serde::{Deserialize, Serialize};
use std::fmt::Debug;
use std::path::{Path, PathBuf};
use thiserror::Error;

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, Error)]
pub enum CreateDirError {
  #[error("Search permission denied on ancestor or write denied on parent")]
  Access,
  #[error("Path already exists")]
  AlreadyExists,
  #[error("Symbolic link loop")]
  Loop,
  #[error("Parent link count limit")]
  LinkLimit,
  #[error("Path too long")]
  NameLimit,
  #[error("Missing ancestor")]
  NotFound,
  #[error("Not enough space on the FS")]
  SpaceLimit,
  #[error("An ancestor is not a directory")]
  NotDir,
  #[error("Readonly file system")]
  RoFs,
  #[error("Unexpected error")]
  Other(String),
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, Error)]
pub enum ReadDirError {
  #[error("a component of `path` does not exist or is a dangling symbolic link.")]
  NotFound,
  #[error("unexpected error")]
  Other(String),
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, Error)]
pub enum ReadFileError {
  #[error("A component of `path` does not exist or is a dangling symbolic link.")]
  NotFound,
  #[error("unexpected error")]
  Other(String),
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, Error)]
pub enum MetadataError {
  #[error("search permission is denied for one of the directories in the path prefix.")]
  Access,
  #[error("too many symbolic links encountered while traversing the path.")]
  Loop,
  #[error("`path` is too long")]
  NameLimit,
  #[error("a component of `path` does not exist or is a dangling symbolic link.")]
  NotFound,
  #[error("out of kernel memory.")]
  NoMem,
  #[error("a component of the path prefix of `path` is not a directory.")]
  NotDir,
  #[error("`path` refers to a file whose size, inode number, or number of blocks cannot be represented")]
  Overflow,
  #[error("unexpected error: {0}")]
  Other(String),
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, Error)]
pub enum CreateFileError<P: Debug + AsRef<Path> = PathBuf> {
  #[error("path already exists: {}", .0.as_ref().display())]
  AlreadyExists(P),
  #[error("permission denied for file creation at path: {}", .0.as_ref().display())]
  PermissionDenied(P),
  #[error("unexpected error on file creation: {path}: {1}", path = .0.as_ref().display())]
  Other(P, String),
}

impl<P: Debug + AsRef<Path>> CreateFileError<P> {
  pub fn to_owned(self) -> CreateFileError<PathBuf> {
    match self {
      Self::AlreadyExists(p) => CreateFileError::AlreadyExists(p.as_ref().to_path_buf()),
      Self::PermissionDenied(p) => CreateFileError::PermissionDenied(p.as_ref().to_path_buf()),
      Self::Other(p, c) => CreateFileError::Other(p.as_ref().to_path_buf(), c),
    }
  }
}

pub trait Metadata: Debug {
  fn is_dir(&self) -> bool;
  fn is_file(&self) -> bool;
  fn is_symlink(&self) -> bool;
  /// UNIX timestamp in POSIX seconds
  fn creation_time(&self) -> i64;
  /// UNIX timestamp in POSIX seconds
  fn access_time(&self) -> i64;
  /// UNIX timestamp in POSIX seconds
  fn modification_time(&self) -> i64;
}

pub trait DirEntry {
  fn path(&self) -> PathBuf;
}

pub trait FsHost: Host {
  /// Metadata for file-system entries
  type Metadata: Metadata;
  /// Directory entry when reading a directory
  type DirEntry: DirEntry;

  /// Create a new directory at the provided path.
  fn create_dir<P: AsRef<Path>>(&self, path: P) -> Result<(), CreateDirError>;

  /// Returns a snapshot of a directory's content.
  fn read_dir<P: AsRef<Path>>(&self, path: P) -> Result<Vec<Self::DirEntry>, ReadDirError>;

  /// Returns a snapshot of a file's content.
  fn read_file<P: AsRef<Path>>(&self, path: P) -> Result<Vec<u8>, ReadFileError>;

  /// Create a new file at the provided path.
  fn create_file<P: AsRef<Path>, C: AsRef<[u8]>>(&self, path: P, content: C) -> Result<(), CreateFileError>;

  /// Query filesystem metadata about the entry at the provided path, following symlinks.
  fn metadata<P: AsRef<Path>>(&self, path: P) -> Result<Self::Metadata, MetadataError>;

  /// Query filesystem metadata about the entry at the provided path, without following symlinks.
  fn symlink_metadata<P: AsRef<Path>>(&self, path: P) -> Result<Self::Metadata, MetadataError>;
}
