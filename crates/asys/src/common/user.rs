use crate::Host;

/// User management support
pub trait UserHost: Host {}
