use crate::{Command, CommandOutput, ExecHost, Host, NixHost, TryExecError};
use crossbeam::thread::{Scope, ScopedJoinHandle};
use std::io::Write;
use std::process::Stdio;

pub mod fs;
pub mod user;

/// Local Linux Host.
pub struct LocalLinux;

impl Host for LocalLinux {}

impl ExecHost for LocalLinux {
  fn try_exec(&self, command: &Command) -> Result<CommandOutput, TryExecError> {
    // TODO: tokio::process
    let mut cmd = std::process::Command::new(command.get_program());
    cmd.args(command.get_args());
    if let Some(d) = command.get_current_dir() {
      cmd.current_dir(d);
    }
    let env = command.get_env();
    if !env.inherit {
      cmd.env_clear();
    }
    let mut has_set_path = false;
    let mut has_set_ld_library_path = false;
    for (k, v) in env.vars.iter() {
      if let Some(v) = v.as_deref() {
        match k.as_str() {
          "PATH" if !has_set_path => {
            has_set_path = true;
            if env.path_extra.is_empty() {
              cmd.env(k, v);
            } else {
              // todo: use the `intersperse` method once it's stable
              let mut value = String::new();
              for entry in env.path_extra.iter().rev() {
                value.push_str(entry);
                value.push(':');
              }
              value.push_str(v);
              cmd.env(k, value);
            }
          }
          "LD_LIBRARY_PATH" if !has_set_ld_library_path => {
            has_set_ld_library_path = true;
            if env.ld_library_path_extra.is_empty() {
              cmd.env(k, v);
            } else {
              // todo: use the `intersperse` method once it's stable
              let mut value = String::new();
              for entry in env.ld_library_path_extra.iter().rev() {
                value.push_str(entry);
                value.push(':');
              }
              value.push_str(v);
              cmd.env(k, value);
            }
          }
          _ => {
            cmd.env(k, v);
          }
        }
      } else {
        cmd.env_remove(k);
      }
    }
    if !has_set_path && !env.path_extra.is_empty() {
      let mut value = String::new();
      for (i, entry) in env.path_extra.iter().rev().enumerate() {
        if i > 0 {
          value.push(':');
        }
        value.push_str(entry);
      }
      if env.inherit {
        if let Ok(old_path) = std::env::var("PATH") {
          value.push(':');
          value.push_str(old_path.as_str());
        }
      }
      cmd.env("PATH", value);
    }
    if !has_set_ld_library_path && !env.ld_library_path_extra.is_empty() {
      let mut value = String::new();
      for (i, entry) in env.ld_library_path_extra.iter().rev().enumerate() {
        if i > 0 {
          value.push(':');
        }
        value.push_str(entry);
      }
      if env.inherit {
        if let Ok(old_ld_library_path) = std::env::var("LD_LIBRARY_PATH") {
          value.push(':');
          value.push_str(old_ld_library_path.as_str());
        }
      }
      cmd.env("LD_LIBRARY_PATH", value);
    }

    let input = command.get_stdin();
    cmd.stdin(if input.is_empty() {
      Stdio::null()
    } else {
      Stdio::piped()
    });
    cmd.stdout(Stdio::piped());
    cmd.stderr(Stdio::piped());
    let mut child = cmd
      .spawn()
      .map_err(|e| TryExecError::Spawn(command.clone(), e.to_string()))?;
    crossbeam::thread::scope(move |s: &Scope| -> Result<CommandOutput, TryExecError> {
      let thread: Option<ScopedJoinHandle<Result<(), TryExecError>>> = if input.is_empty() {
        None
      } else {
        let stdin = child.stdin.take();
        let mut stdin = match stdin {
          Some(stdin) => stdin,
          None => return Err(TryExecError::StdinMissing),
        };
        let thread = s.spawn(move |_| match stdin.write_all(input) {
          Ok(()) => {
            drop(stdin);
            Ok(())
          }
          Err(e) => Err(TryExecError::StdinWrite(e.to_string())),
        });
        Some(thread)
      };
      let out = child
        .wait_with_output()
        .map_err(|e| TryExecError::Wait(e.to_string()))?;
      if let Some(thread) = thread {
        thread.join().map_err(|_| TryExecError::StdinJoinThread)??;
      }
      Ok(CommandOutput::from(out))
    })
    .map_err(|_| TryExecError::ThreadScope)?
  }
}

impl NixHost for LocalLinux {}
