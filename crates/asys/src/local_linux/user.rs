use crate::common::user::UserHost;
use crate::linux::user::{
  CreateGroupError, GetUserGroupsError, Gid, LinuxGroup, LinuxUser, LinuxUserHost, TryGroupFromIdError,
  TryGroupFromNameError, TryUserFromNameError, Uid, UpdateGroupError,
};
use crate::local_linux::LocalLinux;
use crate::{Command, ExecHost};
use serde::{Deserialize, Serialize};
use std::ffi::CString;
use std::path::PathBuf;

impl UserHost for LocalLinux {}

impl LinuxUserHost for LocalLinux {
  type Group = LocalLinuxGroup;
  type User = LocalLinuxUser;

  fn create_group(&self, name: &str, gid: Option<Gid>) -> Result<(), CreateGroupError> {
    let mut cmd = Command::new("groupadd");
    if let Some(gid) = gid {
      cmd = cmd.arg("--gid").arg(gid.to_string());
    }
    cmd = cmd.arg(name);
    match self.exec(&cmd).map(drop) {
      Ok(()) => Ok(()),
      Err(e) => Err(CreateGroupError::Other(e.to_string())),
    }
  }

  fn update_group(&self, name: &str, gid: Option<Gid>) -> Result<(), UpdateGroupError> {
    let mut cmd = Command::new("groupmod");
    if let Some(gid) = gid {
      cmd = cmd.arg("--gid").arg(gid.to_string());
    }
    cmd = cmd.arg(name);
    match self.exec(&cmd).map(drop) {
      Ok(()) => Ok(()),
      Err(e) => Err(UpdateGroupError::Other(e.to_string())),
    }
  }

  fn try_group_from_name(&self, name: &str) -> Result<Option<Self::Group>, TryGroupFromNameError> {
    match nix::unistd::Group::from_name(name) {
      Ok(Some(nix)) => Ok(Some(LocalLinuxGroup { nix })),
      Ok(None) => Ok(None),
      Err(e) => Err(TryGroupFromNameError::Other(e.to_string())),
    }
  }

  fn try_group_from_id(&self, gid: Gid) -> Result<Option<Self::Group>, TryGroupFromIdError> {
    match nix::unistd::Group::from_gid(gid.into()) {
      Ok(Some(nix)) => Ok(Some(LocalLinuxGroup { nix })),
      Ok(None) => Ok(None),
      Err(e) => Err(TryGroupFromIdError::Other(e.to_string())),
    }
  }

  fn try_user_from_name(&self, name: &str) -> Result<Option<Self::User>, TryUserFromNameError> {
    match nix::unistd::User::from_name(name) {
      Ok(Some(nix)) => Ok(Some(LocalLinuxUser::from_nix(nix))),
      Ok(None) => Ok(None),
      Err(e) => Err(TryUserFromNameError::Other(e.to_string())),
    }
  }

  fn get_user_groups(&self, user_name: &str, extra_group: Gid) -> Result<Vec<Gid>, GetUserGroupsError> {
    let user_name = match CString::new(user_name) {
      Ok(user_name) => user_name,
      Err(e) => return Err(GetUserGroupsError::Other(e.to_string())),
    };
    match nix::unistd::getgrouplist(user_name.as_c_str(), extra_group.into()) {
      Ok(groups) => Ok(groups.into_iter().map(Gid::from).collect()),
      Err(e) => Err(GetUserGroupsError::Other(e.to_string())),
    }
  }
}

#[derive(Debug, Clone, PartialEq)]
pub struct LocalLinuxGroup {
  nix: nix::unistd::Group,
}

impl LinuxGroup for LocalLinuxGroup {
  fn gid(&self) -> Gid {
    Gid::from(self.nix.gid)
  }

  fn name(&self) -> &str {
    &self.nix.name
  }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct LocalLinuxUser {
  /// Username
  pub name: String,
  /// User password (probably hashed)
  pub passwd: CString,
  /// User ID
  pub uid: Uid,
  /// Group ID
  pub gid: Gid,
  /// User information
  pub gecos: CString,
  /// Home directory
  pub dir: PathBuf,
  /// Path to shell
  pub shell: PathBuf,
  /// Login class
  pub class: Option<CString>,
  /// Last password change
  pub change: Option<libc::time_t>,
  /// Expiration time of account
  pub expire: Option<libc::time_t>,
}

impl LocalLinuxUser {
  pub fn from_nix(nix_user: nix::unistd::User) -> Self {
    Self {
      name: nix_user.name,
      passwd: nix_user.passwd,
      uid: Uid::from_nix(nix_user.uid),
      gid: Gid::from_nix(nix_user.gid),
      gecos: nix_user.gecos,
      dir: nix_user.dir,
      shell: nix_user.shell,
      class: None,
      change: None,
      expire: None,
    }
  }

  pub fn into_nix(self) -> nix::unistd::User {
    nix::unistd::User {
      name: self.name,
      passwd: self.passwd,
      uid: nix::unistd::Uid::from(self.uid),
      gid: nix::unistd::Gid::from(self.gid),
      gecos: self.gecos,
      dir: self.dir,
      shell: self.shell,
      // class: None,
      // change: None,
      // expire: None,
    }
  }
}

impl LinuxUser for LocalLinuxUser {
  fn name(&self) -> &str {
    &self.name
  }

  fn uid(&self) -> Uid {
    self.uid
  }

  fn gid(&self) -> Gid {
    self.gid
  }

  fn shell(&self) -> PathBuf {
    self.shell.clone()
  }

  fn home(&self) -> PathBuf {
    self.dir.clone()
  }
}
