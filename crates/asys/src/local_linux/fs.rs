use crate::common::fs::{
  CreateDirError, CreateFileError, DirEntry, FsHost, Metadata, MetadataError, ReadDirError, ReadFileError,
};
use crate::linux::fs::{
  CreateSymlinkError, FileMode, LinuxFileType, LinuxFsHost, LinuxMetadata, ResolveError, SetFileModeError,
  SetFileOwnerError,
};
use crate::linux::user::{Gid, Uid};
use crate::local_linux::LocalLinux;
use nix::NixPath;
use std::io;
use std::path::{Path, PathBuf};

fn map_stat_err(e: nix::Error) -> MetadataError {
  match e {
    nix::Error::EACCES => MetadataError::Access,
    nix::Error::ELOOP => MetadataError::Loop,
    nix::Error::ENAMETOOLONG => MetadataError::NameLimit,
    nix::Error::ENOENT => MetadataError::NotFound,
    nix::Error::ENOMEM => MetadataError::NoMem,
    nix::Error::ENOTDIR => MetadataError::NotDir,
    nix::Error::EOVERFLOW => MetadataError::Overflow,
    e => MetadataError::Other(e.to_string()),
  }
}

impl FsHost for LocalLinux {
  type Metadata = LocalLinuxMetadata;
  type DirEntry = LocalLinuxDirEntry;

  fn read_dir<P: AsRef<Path>>(&self, path: P) -> Result<Vec<Self::DirEntry>, ReadDirError> {
    let path = path.as_ref();
    match std::fs::read_dir(path) {
      Ok(content) => {
        let mut entries: Vec<LocalLinuxDirEntry> = Vec::new();
        for entry in content {
          let entry = match entry {
            Ok(entry) => entry,
            Err(e) => return Err(ReadDirError::Other(e.to_string())),
          };
          entries.push(LocalLinuxDirEntry { inner: entry })
        }
        Ok(entries)
      }
      Err(e) => Err(match e.kind() {
        io::ErrorKind::NotFound => ReadDirError::NotFound,
        _ => ReadDirError::Other(e.to_string()),
      }),
    }
  }

  fn read_file<P: AsRef<Path>>(&self, path: P) -> Result<Vec<u8>, ReadFileError> {
    std::fs::read(path.as_ref()).map_err(|e| match e.kind() {
      io::ErrorKind::NotFound => ReadFileError::NotFound,
      _ => ReadFileError::Other(e.to_string()),
    })
  }

  fn create_dir<P: AsRef<Path>>(&self, path: P) -> Result<(), CreateDirError> {
    self.create_dir_with_mode(path, FileMode::ALL_PERM)
  }

  fn create_file<P: AsRef<Path>, C: AsRef<[u8]>>(&self, path: P, content: C) -> Result<(), CreateFileError> {
    let path = path.as_ref();
    match std::fs::write(path, content) {
      Ok(()) => Ok(()),
      Err(e) => match e.kind() {
        io::ErrorKind::AlreadyExists => Err(CreateFileError::AlreadyExists(path.to_path_buf())),
        io::ErrorKind::PermissionDenied => Err(CreateFileError::PermissionDenied(path.to_path_buf())),
        _ => Err(CreateFileError::Other(path.to_path_buf(), e.to_string())),
      },
    }
  }

  fn metadata<P: AsRef<Path>>(&self, path: P) -> Result<Self::Metadata, MetadataError> {
    match nix::sys::stat::stat(path.as_ref()) {
      Ok(nix) => Ok(LocalLinuxMetadata { nix }),
      Err(e) => Err(map_stat_err(e)),
    }
  }

  fn symlink_metadata<P: AsRef<Path>>(&self, path: P) -> Result<Self::Metadata, MetadataError> {
    match nix::sys::stat::lstat(path.as_ref()) {
      Ok(nix) => Ok(LocalLinuxMetadata { nix }),
      Err(e) => Err(map_stat_err(e)),
    }
  }
}

impl LinuxFsHost for LocalLinux {
  fn create_dir_with_mode<P: AsRef<Path>>(&self, path: P, mode: FileMode) -> Result<(), CreateDirError> {
    let e = match nix::unistd::mkdir(path.as_ref(), mode.into()) {
      Ok(()) => return Ok(()),
      Err(e) => e,
    };
    let e = match e {
      nix::Error::EACCES => CreateDirError::Access,
      nix::Error::EEXIST => CreateDirError::AlreadyExists,
      nix::Error::ELOOP => CreateDirError::Loop,
      nix::Error::EMLINK => CreateDirError::LinkLimit,
      nix::Error::ENAMETOOLONG => CreateDirError::NameLimit,
      nix::Error::ENOENT => CreateDirError::NotFound,
      nix::Error::ENOSPC => CreateDirError::SpaceLimit,
      nix::Error::EROFS => CreateDirError::RoFs,
      e => CreateDirError::Other(e.to_string()),
    };
    Err(e)
  }

  fn set_file_mode<P: AsRef<Path>>(&self, path: P, mode: FileMode) -> Result<(), SetFileModeError> {
    let path = path.as_ref();
    let res = path
      .with_nix_path(|cstr| unsafe { libc::chmod(cstr.as_ptr(), libc::mode_t::from(mode)) })
      .map_err(|e| SetFileModeError::Other(e.to_string()))?;
    let res = nix::errno::Errno::result(res).map(drop);
    let e = match res {
      Ok(()) => return Ok(()),
      Err(e) => e,
    };
    let e = match e {
      nix::Error::EACCES => SetFileModeError::Access,
      nix::Error::EIO => SetFileModeError::Io,
      nix::Error::ELOOP => SetFileModeError::Loop,
      nix::Error::ENAMETOOLONG => SetFileModeError::NameLimit,
      nix::Error::ENOENT => SetFileModeError::NotFound,
      nix::Error::ENOMEM => SetFileModeError::NoMem,
      nix::Error::EPERM => SetFileModeError::Perm,
      nix::Error::ENOTDIR => SetFileModeError::NotDir,
      nix::Error::EROFS => SetFileModeError::RoFs,
      e => SetFileModeError::Other(e.to_string()),
    };
    Err(e)
  }

  fn set_file_owner<P: AsRef<Path>>(
    &self,
    path: P,
    uid: Option<Uid>,
    gid: Option<Gid>,
  ) -> Result<(), SetFileOwnerError> {
    let e = match nix::unistd::chown(path.as_ref(), uid.map(Into::into), gid.map(Into::into)) {
      Ok(()) => return Ok(()),
      Err(e) => e,
    };
    let e = match e {
      nix::Error::EACCES => SetFileOwnerError::Access,
      nix::Error::EIO => SetFileOwnerError::Io,
      nix::Error::ELOOP => SetFileOwnerError::Loop,
      nix::Error::ENAMETOOLONG => SetFileOwnerError::NameLimit,
      nix::Error::ENOENT => SetFileOwnerError::NotFound,
      nix::Error::ENOMEM => SetFileOwnerError::NoMem,
      nix::Error::EPERM => SetFileOwnerError::Perm,
      nix::Error::ENOTDIR => SetFileOwnerError::NotDir,
      nix::Error::EROFS => SetFileOwnerError::RoFs,
      e => SetFileOwnerError::Other(e.to_string()),
    };
    Err(e)
  }

  fn resolve<P: AsRef<Path>>(&self, path: P) -> Result<PathBuf, ResolveError> {
    let e = match std::fs::canonicalize(path) {
      Ok(p) => return Ok(p),
      Err(e) => e,
    };
    let e = match e.kind() {
      std::io::ErrorKind::NotFound => ResolveError::NotFound,
      _ => ResolveError::Other(e.to_string()),
    };
    Err(e)
  }

  fn create_symlink(&self, link_path: impl AsRef<Path>, pointee: impl AsRef<Path>) -> Result<(), CreateSymlinkError> {
    let link_path = link_path.as_ref();
    let pointee = pointee.as_ref();
    assert!(pointee.is_absolute());
    let e = match nix::unistd::symlinkat(pointee, None, link_path) {
      Ok(()) => return Ok(()),
      Err(e) => e,
    };
    let e = match e {
      nix::Error::EEXIST => CreateSymlinkError::AlreadyExists,
      e => CreateSymlinkError::Other(e.to_string()),
    };
    Err(e)
  }
}

#[derive(Debug)]
pub struct LocalLinuxMetadata {
  nix: nix::sys::stat::FileStat,
}

impl Metadata for LocalLinuxMetadata {
  fn is_dir(&self) -> bool {
    self.try_file_type().map(|t| t.is_dir()).unwrap_or(false)
  }
  fn is_file(&self) -> bool {
    self.try_file_type().map(|t| t.is_file()).unwrap_or(false)
  }
  fn is_symlink(&self) -> bool {
    self.try_file_type().map(|t| t.is_symlink()).unwrap_or(false)
  }

  fn creation_time(&self) -> i64 {
    self.nix.st_ctime
  }
  fn access_time(&self) -> i64 {
    self.nix.st_atime
  }
  fn modification_time(&self) -> i64 {
    self.nix.st_mtime
  }
}

impl LinuxMetadata for LocalLinuxMetadata {
  fn try_file_type(&self) -> Option<LinuxFileType> {
    LinuxFileType::from_st_mode(self.nix.st_mode)
  }

  fn mode(&self) -> FileMode {
    FileMode::from_st_mode(self.nix.st_mode)
  }

  fn uid(&self) -> Uid {
    Uid::from_raw(self.nix.st_uid)
  }

  fn gid(&self) -> Gid {
    Gid::from_raw(self.nix.st_gid)
  }
}

pub struct LocalLinuxDirEntry {
  inner: std::fs::DirEntry,
}

impl DirEntry for LocalLinuxDirEntry {
  fn path(&self) -> PathBuf {
    self.inner.path()
  }
}
