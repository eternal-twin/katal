use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;
use std::fmt;
use std::process::Output;
use thiserror::Error;

/// Host Abstraction layer
pub mod common;
pub mod linux;
pub mod local_linux;

pub trait Host: Send + Sync {}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct CommandEnv {
  inherit: bool,
  vars: BTreeMap<String, Option<String>>,
  /// Extra entries to add to `LD_LIBRARY_PATH`
  ///
  /// The entries are added in reverse order: values are prepended to th
  /// start of the env var.
  ld_library_path_extra: Vec<String>,
  /// Extra entries to add to `PATH`
  ///
  /// The entries are added in reverse order: values are prepended to th
  /// start of the env var.
  path_extra: Vec<String>,
}

impl CommandEnv {
  fn new() -> Self {
    Self {
      inherit: true,
      vars: BTreeMap::new(),
      ld_library_path_extra: Vec::new(),
      path_extra: Vec::new(),
    }
  }
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct Command<CurrentDir = String> {
  program: String,
  current_dir: Option<CurrentDir>,
  args: Vec<String>,
  env: CommandEnv,
  stdin: Vec<u8>,
  /// Boolean indicating if this command may cause mutations on the host
  ///
  /// This information can be set once the command is built. Any further
  /// change will reset it to the default value (`true`).
  may_mutate_host: bool,
}

#[derive(Debug)]
pub struct CommandArgs<'a> {
  inner: std::slice::Iter<'a, String>,
}

impl<'a> Iterator for CommandArgs<'a> {
  type Item = &'a str;

  fn next(&mut self) -> Option<Self::Item> {
    self.inner.next().map(String::as_str)
  }

  fn size_hint(&self) -> (usize, Option<usize>) {
    self.inner.size_hint()
  }
}

impl<'a> ExactSizeIterator for CommandArgs<'a> {
  fn len(&self) -> usize {
    self.inner.len()
  }
}

/// Abstract `std::process::ExitStatus`, for Linux
#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct LinuxCommandTermination {
  pub status: Option<i32>,
}

impl LinuxCommandTermination {
  pub fn success(&self) -> bool {
    self.status == Some(0)
  }
}

// TODO: Don't default to Linux
#[derive(Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct CommandOutput<Termination = LinuxCommandTermination> {
  pub termination: Termination,
  pub stdout: Vec<u8>,
  pub stderr: Vec<u8>,
}

// If either stderr or stdout are valid utf8 strings it prints the valid
// strings, otherwise it prints the byte sequence instead
impl fmt::Debug for CommandOutput {
  fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
    let stdout_utf8 = std::str::from_utf8(&self.stdout);
    let stdout_debug: &dyn fmt::Debug = match stdout_utf8 {
      Ok(ref str) => str,
      Err(_) => &self.stdout,
    };

    let stderr_utf8 = std::str::from_utf8(&self.stderr);
    let stderr_debug: &dyn fmt::Debug = match stderr_utf8 {
      Ok(ref str) => str,
      Err(_) => &self.stderr,
    };

    fmt
      .debug_struct("CommandOutput")
      .field("termination", &self.termination)
      .field("stdout", stdout_debug)
      .field("stderr", stderr_debug)
      .finish()
  }
}

impl From<Output> for CommandOutput {
  fn from(old: Output) -> Self {
    Self {
      termination: LinuxCommandTermination {
        status: old.status.code(),
      },
      stdout: old.stdout,
      stderr: old.stderr,
    }
  }
}

impl<CurrentDir> Command<CurrentDir> {
  pub fn new(program: impl AsRef<str>) -> Self {
    Self {
      program: program.as_ref().to_string(),
      current_dir: None,
      args: Vec::new(),
      env: CommandEnv::new(),
      stdin: Vec::new(),
      may_mutate_host: true,
    }
  }

  pub fn arg(mut self, a: impl AsRef<str>) -> Self {
    self.args.push(a.as_ref().to_string());
    self.may_mutate_host = true;
    self
  }

  pub fn env(mut self, k: impl AsRef<str>, v: impl AsRef<str>) -> Self {
    self
      .env
      .vars
      .insert(k.as_ref().to_string(), Some(v.as_ref().to_string()));
    self.may_mutate_host = true;
    self
  }

  pub fn prepend_ld_library_path(mut self, lib_dir: impl AsRef<str>) -> Self {
    self.env.ld_library_path_extra.push(lib_dir.as_ref().to_string());
    self.may_mutate_host = true;
    self
  }

  pub fn prepend_path(mut self, path_dir: impl AsRef<str>) -> Self {
    self.env.path_extra.push(path_dir.as_ref().to_string());
    self.may_mutate_host = true;
    self
  }

  pub fn current_dir(mut self, d: impl Into<CurrentDir>) -> Self {
    self.current_dir = Some(d.into());
    self.may_mutate_host = true;
    self
  }

  pub fn stdin(mut self, stdin: impl AsRef<[u8]>) -> Self {
    self.stdin = stdin.as_ref().to_vec();
    self.may_mutate_host = true;
    self
  }

  pub fn hint_no_host_mutation(mut self) -> Self {
    self.may_mutate_host = false;
    self
  }

  pub fn get_program(&self) -> &str {
    &self.program
  }

  pub fn get_current_dir(&self) -> Option<&CurrentDir> {
    self.current_dir.as_ref()
  }

  pub fn get_args(&self) -> CommandArgs {
    CommandArgs {
      inner: self.args.iter(),
    }
  }

  pub fn get_env(&self) -> &CommandEnv {
    &self.env
  }

  pub fn get_stdin(&self) -> &[u8] {
    &self.stdin
  }

  pub fn may_mutate_host(&self) -> bool {
    self.may_mutate_host
  }
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, Error)]
pub enum TryExecError {
  #[error("missing stdin pipe")]
  StdinMissing,
  #[error("failed to write to stdin: {0}")]
  StdinWrite(String),
  #[error("failed to join stdin writer thread")]
  StdinJoinThread,
  #[error("failed to spawn child process: command={0:?}: {1:?}")]
  Spawn(Command, String),
  #[error("failed to wait on child process: {0}")]
  Wait(String),
  #[error("unexpected thread scope error")]
  ThreadScope,
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, Error)]
pub enum ExecError {
  #[error("command {0:?} terminated with failure: {1:?}")]
  FailureTermination(Command, CommandOutput),
  #[error("missing stdin pipe")]
  StdinMissing,
  #[error("failed to write to stdin: {0}")]
  StdinWrite(String),
  #[error("failed to join stdin writer thread")]
  StdinJoinThread,
  #[error("failed to spawn child process: command={0:?}: {1:?}")]
  Spawn(Command, String),
  #[error("failed to wait on child process: {0}")]
  Wait(String),
  #[error("unexpected thread scope error")]
  ThreadScope,
  #[error("unexpected error: {0}")]
  Other(String),
}

impl From<TryExecError> for ExecError {
  fn from(value: TryExecError) -> Self {
    match value {
      TryExecError::StdinMissing => Self::StdinMissing,
      TryExecError::StdinWrite(e) => Self::StdinWrite(e),
      TryExecError::StdinJoinThread => Self::StdinJoinThread,
      TryExecError::Spawn(c, e) => Self::Spawn(c, e),
      TryExecError::Wait(e) => Self::Wait(e),
      TryExecError::ThreadScope => Self::ThreadScope,
    }
  }
}

/// This trait indicates that the host can execute arbitrary commands.
pub trait ExecHost: Host {
  fn try_exec(&self, command: &Command) -> Result<CommandOutput, TryExecError>;

  fn exec(&self, command: &Command) -> Result<CommandOutput, ExecError> {
    let out = self.try_exec(command)?;
    if !out.termination.success() {
      Err(ExecError::FailureTermination(Command::clone(command), out))
    } else {
      Ok(out)
    }
  }
}

pub trait NixHost: ExecHost {}
