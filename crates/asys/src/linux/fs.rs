use crate::common::fs::{CreateDirError, FsHost, Metadata};
use crate::linux::user::{Gid, Uid};
use serde::{Deserialize, Serialize};
use std::fmt;
use std::ops;
use std::path::{Path, PathBuf};
use thiserror::Error;

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, Error)]
pub enum SetFileModeError {
  #[error("Search permission is denied on a component of the path prefix.")]
  Access,
  #[error("A low-level I/O error occurred while modifying the inode.")]
  Io,
  #[error("Too many symbolic links were encountered in resolving `path`.")]
  Loop,
  #[error("`path` is too long.")]
  NameLimit,
  #[error("The entry does not exist")]
  NotFound,
  #[error("Insufficient kernel memory was available")]
  NoMem,
  #[error("The effective UID does not match the owner of the file, and the process is not privileged (Linux: it does not have the CAP_FOWNER capability).")]
  Perm,
  #[error("A component of the path prefix is not a directory.")]
  NotDir,
  #[error("The named file resides on a read-only filesystem.")]
  RoFs,
  #[error("Unexpected error: {0}")]
  Other(String),
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, Error)]
pub enum SetFileOwnerError {
  #[error("Search permission is denied on a component of the path prefix.")]
  Access,
  #[error("A low-level I/O error occurred while modifying the inode.")]
  Io,
  #[error("Too many symbolic links were encountered in resolving `path`.")]
  Loop,
  #[error("`path` is too long.")]
  NameLimit,
  #[error("The entry does not exist")]
  NotFound,
  #[error("Insufficient kernel memory was available")]
  NoMem,
  #[error("The calling process did not have the required permissions to change owner and/or group.")]
  Perm,
  #[error("A component of the path prefix is not a directory.")]
  NotDir,
  #[error("The named file resides on a read-only filesystem.")]
  RoFs,
  #[error("Unexpected error: {0}")]
  Other(String),
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, Error)]
pub enum ResolveError {
  #[error("A component in the input path does not exist")]
  NotFound,
  #[error("Unexpected error: {0}")]
  Other(String),
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, Error)]
pub enum CreateSymlinkError {
  #[error("Symlink already exists")]
  AlreadyExists,
  #[error("Unexpected error: {0}")]
  Other(String),
}

pub trait LinuxMetadata: Metadata {
  fn try_file_type(&self) -> Option<LinuxFileType>;

  fn file_type(&self) -> LinuxFileType {
    self.try_file_type().expect("unknown file type")
  }

  fn mode(&self) -> FileMode;

  fn uid(&self) -> Uid;

  fn gid(&self) -> Gid;
}

pub trait LinuxFsHost: FsHost
where
  <Self as FsHost>::Metadata: LinuxMetadata,
{
  fn create_dir_with_mode<P: AsRef<Path>>(&self, path: P, mode: FileMode) -> Result<(), CreateDirError>;

  fn set_file_mode<P: AsRef<Path>>(&self, path: P, mode: FileMode) -> Result<(), SetFileModeError>;

  fn set_file_owner<P: AsRef<Path>>(
    &self,
    path: P,
    uid: Option<Uid>,
    gid: Option<Gid>,
  ) -> Result<(), SetFileOwnerError>;

  /// Resolves the path into a canonical absolute path, where intermediate symlinks are fully resolved.
  fn resolve<P: AsRef<Path>>(&self, path: P) -> Result<PathBuf, ResolveError>;

  /// Creates a symlink at `link_path` pointing to `pointee`.
  fn create_symlink(&self, link_path: impl AsRef<Path>, pointee: impl AsRef<Path>) -> Result<(), CreateSymlinkError>;
}

/// Wrapper for the _file mode_ bitfield from [`::libc::stat::st_mode`].
///
/// This corresponds to the 12 least significant bits.
///
/// # Invariant
///
/// ```
/// fn check(fm: asys::linux::fs::FileMode) {
///   assert_eq!(fm.to_raw(), fm.to_raw() & 0o7777)
/// }
/// ```
#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash, Serialize)]
pub struct FileMode(u32);

impl FileMode {
  // The names come from https://en.cppreference.com/w/cpp/filesystem/perms
  pub const OTHERS_EXEC: Self = FileMode(0o0001);
  pub const OTHERS_WRITE: Self = FileMode(0o0002);
  pub const OTHERS_READ: Self = FileMode(0o0004);
  pub const GROUP_EXEC: Self = FileMode(0o0010);
  pub const GROUP_WRITE: Self = FileMode(0o0020);
  pub const GROUP_READ: Self = FileMode(0o0040);
  pub const OWNER_EXEC: Self = FileMode(0o0100);
  pub const OWNER_WRITE: Self = FileMode(0o0200);
  pub const OWNER_READ: Self = FileMode(0o0400);
  pub const STICKY_BIT: Self = FileMode(0o1000);
  pub const SET_GID: Self = FileMode(0o2000);
  pub const SET_UID: Self = FileMode(0o4000);

  pub const NONE: Self = FileMode(0o0000);
  pub const OTHERS_ALL: Self = FileMode(0o0007);
  pub const GROUP_ALL: Self = FileMode(0o0070);
  pub const OWNER_ALL: Self = FileMode(0o0700);
  pub const ALL_EXEC: Self = FileMode(0o0111);
  pub const ALL_WRITE: Self = FileMode(0o0222);
  pub const ALL_READ: Self = FileMode(0o0444);
  pub const ALL_PERM: Self = FileMode(0o0777);
  pub const ALL_BITS: Self = FileMode(0o7777);

  /// Extract the file mode from the [`::libc::stat::st_mode`] _file mode_ bitfield.
  ///
  /// The other bits will be ignored.
  pub const fn from_st_mode(raw: u32) -> Self {
    Self(raw & 0o7777)
  }

  pub const fn to_raw(self) -> u32 {
    self.0
  }

  pub const fn is_none(self) -> bool {
    self.0 == 0
  }
}

impl From<FileMode> for u32 {
  fn from(mode: FileMode) -> Self {
    mode.to_raw()
  }
}

impl From<FileMode> for nix::sys::stat::Mode {
  fn from(mode: FileMode) -> Self {
    Self::from_bits_truncate(mode.to_raw())
  }
}

impl From<nix::sys::stat::Mode> for FileMode {
  fn from(mode: nix::sys::stat::Mode) -> Self {
    Self::from_st_mode(mode.bits())
  }
}

impl ops::BitAnd<FileMode> for FileMode {
  type Output = FileMode;

  fn bitand(self, rhs: FileMode) -> Self::Output {
    Self(self.0 & rhs.0)
  }
}

impl ops::BitAndAssign<FileMode> for FileMode {
  fn bitand_assign(&mut self, rhs: FileMode) {
    self.0 &= rhs.0
  }
}

impl ops::BitOr<FileMode> for FileMode {
  type Output = FileMode;

  fn bitor(self, rhs: FileMode) -> Self::Output {
    Self(self.0 | rhs.0)
  }
}

impl ops::BitOrAssign<FileMode> for FileMode {
  fn bitor_assign(&mut self, rhs: FileMode) {
    self.0 |= rhs.0
  }
}

impl fmt::Debug for FileMode {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    write!(f, "FileMode({:04o})", self.0)
  }
}

/// Wrapper for the _file permission_ bitfield from [`::libc::stat::st_mode`].
///
/// This corresponds to the 9 least significant bits.
///
/// # Invariant
///
/// ```
/// fn check(fp: asys::linux::fs::FilePerm) {
///   assert_eq!(fp.to_raw(), fp.to_raw() & 0o777)
/// }
/// ```
#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash, Serialize)]
pub struct FilePerm(u32);

impl FilePerm {
  // The names come from https://en.cppreference.com/w/cpp/filesystem/perms
  pub const OTHERS_EXEC: Self = FilePerm(0o001);
  pub const OTHERS_WRITE: Self = FilePerm(0o002);
  pub const OTHERS_READ: Self = FilePerm(0o004);
  pub const GROUP_EXEC: Self = FilePerm(0o010);
  pub const GROUP_WRITE: Self = FilePerm(0o020);
  pub const GROUP_READ: Self = FilePerm(0o040);
  pub const OWNER_EXEC: Self = FilePerm(0o100);
  pub const OWNER_WRITE: Self = FilePerm(0o200);
  pub const OWNER_READ: Self = FilePerm(0o400);

  pub const NONE: Self = FilePerm(0o000);
  pub const OTHERS_ALL: Self = FilePerm(0o007);
  pub const GROUP_ALL: Self = FilePerm(0o070);
  pub const OWNER_ALL: Self = FilePerm(0o700);
  pub const ALL_EXEC: Self = FilePerm(0o111);
  pub const ALL_WRITE: Self = FilePerm(0o222);
  pub const ALL_READ: Self = FilePerm(0o444);
  pub const ALL: Self = FilePerm(0o777);

  /// Extract the file mode from the [`::libc::stat::st_mode`] _file mode_ bitfield.
  ///
  /// The other bits will be ignored.
  pub const fn from_st_mode(raw: u32) -> Self {
    Self(raw & 0o777)
  }

  pub const fn to_raw(self) -> u32 {
    self.0
  }
}

impl From<FilePerm> for u32 {
  fn from(perm: FilePerm) -> Self {
    perm.to_raw()
  }
}

impl From<FilePerm> for nix::sys::stat::Mode {
  fn from(perm: FilePerm) -> Self {
    Self::from_bits_truncate(perm.to_raw())
  }
}

impl From<FileMode> for FilePerm {
  fn from(mode: FileMode) -> Self {
    Self::from_st_mode(mode.to_raw())
  }
}

impl From<nix::sys::stat::Mode> for FilePerm {
  fn from(mode: nix::sys::stat::Mode) -> Self {
    Self::from_st_mode(mode.bits())
  }
}

impl ops::BitAnd<FilePerm> for FilePerm {
  type Output = FilePerm;

  fn bitand(self, rhs: FilePerm) -> Self::Output {
    Self(self.0 & rhs.0)
  }
}

impl ops::BitAndAssign<FilePerm> for FilePerm {
  fn bitand_assign(&mut self, rhs: FilePerm) {
    self.0 &= rhs.0
  }
}

impl ops::BitOr<FilePerm> for FilePerm {
  type Output = FilePerm;

  fn bitor(self, rhs: FilePerm) -> Self::Output {
    Self(self.0 | rhs.0)
  }
}

impl ops::BitOrAssign<FilePerm> for FilePerm {
  fn bitor_assign(&mut self, rhs: FilePerm) {
    self.0 |= rhs.0
  }
}

impl fmt::Debug for FilePerm {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    write!(f, "FilePerm({:03o})", self.0)
  }
}

/// Linux file type.
pub enum LinuxFileType {
  /// Regular file
  File,
  Dir,
  Symlink,
  BlockDevice,
  CharDevice,
  Fifo,
  Socket,
}

impl LinuxFileType {
  /// Extract the file type from the [`::libc::stat::st_mode`] _file type_ bitfield.
  ///
  /// The other bits will be ignored.
  /// Returns `None` when file type is unknown.
  pub const fn from_st_mode(raw: u32) -> Option<Self> {
    let t = match raw & 0o170000 {
      0o010000 => Self::Fifo,
      0o020000 => Self::CharDevice,
      0o040000 => Self::Dir,
      0o060000 => Self::BlockDevice,
      0o100000 => Self::File,
      0o120000 => Self::Symlink,
      0o140000 => Self::Socket,
      _ => return None,
    };
    Some(t)
  }

  pub fn is_dir(&self) -> bool {
    matches!(self, Self::Dir)
  }

  pub fn is_file(&self) -> bool {
    matches!(self, Self::File)
  }

  pub fn is_symlink(&self) -> bool {
    matches!(self, Self::Symlink)
  }
}
