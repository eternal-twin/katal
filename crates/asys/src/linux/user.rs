use crate::common::user::UserHost;
use libc::{gid_t, uid_t};
use serde::{Deserialize, Serialize};
use std::fmt;
use std::path::PathBuf;
use thiserror::Error;

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, Error)]
pub enum CreateGroupError {
  #[error("unexpected error: {0}")]
  Other(String),
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, Error)]
pub enum UpdateGroupError {
  #[error("unexpected error: {0}")]
  Other(String),
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, Error)]
pub enum TryGroupFromNameError {
  #[error("unexpected error: {0}")]
  Other(String),
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, Error)]
pub enum TryGroupFromIdError {
  #[error("unexpected error: {0}")]
  Other(String),
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, Error)]
pub enum GroupFromNameError {
  #[error("group not found")]
  NotFound,
  #[error("unexpected error: {0}")]
  Other(String),
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, Error)]
pub enum GroupFromIdError {
  #[error("group not found")]
  NotFound,
  #[error("unexpected error: {0}")]
  Other(String),
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, Error)]
pub enum TryUserFromNameError {
  #[error("unexpected error: {0}")]
  Other(String),
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, Error)]
pub enum UserFromNameError {
  #[error("user not found")]
  NotFound,
  #[error("unexpected error: {0}")]
  Other(String),
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, Error)]
pub enum GetUserGroupsError {
  #[error("unexpected error: {0}")]
  Other(String),
}

pub trait LinuxGroup: PartialEq {
  fn gid(&self) -> Gid;
  fn name(&self) -> &str;
}

pub trait LinuxUser: PartialEq {
  /// Get the user id
  fn uid(&self) -> Uid;
  /// Get the id of this user's primary group
  fn gid(&self) -> Gid;
  fn name(&self) -> &str;
  fn shell(&self) -> PathBuf;
  fn home(&self) -> PathBuf;
}

pub trait LinuxUserHost: UserHost {
  type Group: LinuxGroup + Send;
  type User: LinuxUser + Send;

  fn create_group(&self, name: &str, gid: Option<Gid>) -> Result<(), CreateGroupError>;

  fn update_group(&self, name: &str, gid: Option<Gid>) -> Result<(), UpdateGroupError>;

  fn try_group_from_name(&self, name: &str) -> Result<Option<Self::Group>, TryGroupFromNameError>;

  fn try_group_from_id(&self, gid: Gid) -> Result<Option<Self::Group>, TryGroupFromIdError>;

  fn try_user_from_name(&self, name: &str) -> Result<Option<Self::User>, TryUserFromNameError>;

  fn group_from_name(&self, name: &str) -> Result<Self::Group, GroupFromNameError> {
    match self.try_group_from_name(name) {
      Ok(Some(g)) => Ok(g),
      Ok(None) => Err(GroupFromNameError::NotFound),
      Err(TryGroupFromNameError::Other(e)) => Err(GroupFromNameError::Other(e)),
    }
  }

  fn group_from_id(&self, gid: Gid) -> Result<Self::Group, GroupFromIdError> {
    match self.try_group_from_id(gid) {
      Ok(Some(g)) => Ok(g),
      Ok(None) => Err(GroupFromIdError::NotFound),
      Err(TryGroupFromIdError::Other(e)) => Err(GroupFromIdError::Other(e)),
    }
  }

  fn user_from_name(&self, name: &str) -> Result<Self::User, UserFromNameError> {
    match self.try_user_from_name(name) {
      Ok(Some(u)) => Ok(u),
      Ok(None) => Err(UserFromNameError::NotFound),
      Err(TryUserFromNameError::Other(e)) => Err(UserFromNameError::Other(e.to_string())),
    }
  }

  fn get_user_groups(&self, user_name: &str, extra_group: Gid) -> Result<Vec<Gid>, GetUserGroupsError>;
}

#[derive(Debug, Copy, Clone, Hash, Ord, PartialOrd, Eq, PartialEq, Serialize, Deserialize)]
pub struct Gid(gid_t);

impl Gid {
  /// Creates `Gid` from raw `gid_t`.
  pub const fn from_raw(gid: gid_t) -> Self {
    Self(gid)
  }

  /// Creates `Gid` from `nix::unistd::Gid`.
  pub const fn from_nix(gid: nix::unistd::Gid) -> Self {
    Self::from_raw(gid.as_raw())
  }

  /// Get the raw `gid_t` wrapped by `self`.
  pub const fn into_raw(self) -> gid_t {
    self.0
  }
}

impl From<Gid> for gid_t {
  fn from(gid: Gid) -> Self {
    gid.0
  }
}

impl From<nix::unistd::Gid> for Gid {
  fn from(gid: nix::unistd::Gid) -> Self {
    Self(gid.as_raw())
  }
}

impl From<Gid> for nix::unistd::Gid {
  fn from(gid: Gid) -> Self {
    Self::from_raw(gid.into_raw())
  }
}

impl fmt::Display for Gid {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    fmt::Display::fmt(&self.0, f)
  }
}

#[derive(Debug, Copy, Clone, Hash, Ord, PartialOrd, Eq, PartialEq, Serialize, Deserialize)]
pub struct Uid(uid_t);

impl Uid {
  pub const ROOT: Self = Self(0);

  /// Creates `Uid` from raw `uid_t`.
  pub const fn from_raw(uid: uid_t) -> Self {
    Self(uid)
  }

  /// Creates `Uid` from `nix::unistd::Uid`.
  pub const fn from_nix(uid: nix::unistd::Uid) -> Self {
    Self::from_raw(uid.as_raw())
  }

  /// Get the raw `uid_t` wrapped by `self`.
  pub const fn into_raw(self) -> uid_t {
    self.0
  }

  // TODO: cfg(nix only)
  pub fn real() -> Uid {
    Self(nix::unistd::Uid::current().as_raw())
  }

  // TODO: cfg(nix only)
  pub fn effective() -> Uid {
    Self(nix::unistd::Uid::effective().as_raw())
  }
}

impl From<Uid> for uid_t {
  fn from(uid: Uid) -> Self {
    uid.0
  }
}

impl From<nix::unistd::Uid> for Uid {
  fn from(uid: nix::unistd::Uid) -> Self {
    Self::from_nix(uid)
  }
}

impl From<Uid> for nix::unistd::Uid {
  fn from(uid: Uid) -> Self {
    Self::from_raw(uid.into_raw())
  }
}

impl fmt::Display for Uid {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    fmt::Display::fmt(&self.0, f)
  }
}

#[derive(Debug, Clone, Hash, Ord, PartialOrd, Eq, PartialEq, Serialize, Deserialize)]
pub enum GroupRef {
  Id(Gid),
  Name(String),
}

impl From<Gid> for GroupRef {
  fn from(gid: Gid) -> Self {
    Self::Id(gid)
  }
}

#[derive(Debug, Clone, Hash, Ord, PartialOrd, Eq, PartialEq, Serialize, Deserialize)]
pub enum UserRef {
  Id(Uid),
  Name(String),
}

impl UserRef {
  pub const ROOT: Self = Self::Id(Uid::ROOT);
}

impl From<Uid> for UserRef {
  fn from(uid: Uid) -> Self {
    Self::Id(uid)
  }
}

impl fmt::Display for GroupRef {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    match self {
      GroupRef::Id(g) => fmt::Display::fmt(&g, f),
      GroupRef::Name(g) => fmt::Display::fmt(&g, f),
    }
  }
}
