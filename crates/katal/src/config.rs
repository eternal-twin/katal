use serde::{Deserialize, Serialize};
use std::collections::{BTreeMap, BTreeSet, HashMap};
use std::fs;
use std::io;
use std::path::{Path, PathBuf};
use thiserror::Error;
use url::Url;

#[derive(Clone, Debug, PartialEq, Eq, Deserialize)]
pub struct KatalConfig {
  pub loader: LoaderConfig,
  pub state: SqliteStateConfig,
  pub daemon: DaemonConfig,
  pub config: Option<RawConfig>,
}

#[derive(Clone, Debug, PartialEq, Eq, Deserialize)]
#[serde(tag = "type")]
pub enum LoaderConfig {
  Git(GitLoaderConfig),
  Dir(DirLoaderConfig),
}

#[derive(Clone, Debug, PartialEq, Eq, Deserialize)]
pub struct GitLoaderConfig {
  pub repo: String,
  pub dir: Option<String>,
  pub r#ref: String,
}

#[derive(Clone, Debug, PartialEq, Eq, Deserialize)]
pub struct DirLoaderConfig {
  pub dir: String,
}

#[derive(Clone, Debug, PartialEq, Eq, Deserialize)]
pub struct SqliteStateConfig {
  pub path: String,
}

#[derive(Clone, Debug, PartialEq, Eq, Deserialize)]
pub struct DaemonConfig {
  pub key: String,
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct RawConfig {
  pub vault: Option<RawVaultConfig>,
  pub apps: Option<RawAppsConfig>,
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct RawAppsConfig {
  pub brute: Option<RawAppConfig<RawBruteChannelConfig>>,
  pub dinorpg: Option<RawAppConfig<RawDinorpgChannelConfig>>,
  pub dinocard: Option<RawAppConfig<RawAutoChannelConfig>>,
  pub emush: Option<RawAppConfig<RawEmushChannelConfig>>,
  pub epopotamo: Option<RawAppConfig<RawEpopotamoChannelConfig>>,
  pub eternalfest: Option<RawAppConfig<RawEternalfestChannelConfig>>,
  pub eternaltwin: Option<RawAppConfig<RawEternaltwinChannelConfig>>,
  pub frutibandas: Option<RawAppConfig<RawFrutibandasChannelConfig>>,
  pub kadokadeo: Option<RawAppConfig<RawAutoChannelConfig>>,
  pub kingdom: Option<RawAppConfig<RawKingdomChannelConfig>>,
  pub neoparc: Option<RawAppConfig<RawNeoparcChannelConfig>>,
  pub uptrace: Option<RawAppConfig<RawUptraceChannelConfig>>,
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct RawVaultConfig {
  pub sudo_password: Option<String>,
  /// Borgbase API key (to manage _all_ repositories)
  pub borgbase: Option<String>,
  /// Clickhouse system user password
  pub ch_sys_password: Option<String>,
  #[serde(default)]
  pub brute: BTreeMap<String, RawBruteVault>,
  #[serde(default)]
  pub dinocard: BTreeMap<String, RawAutoVault>,
  #[serde(default)]
  pub dinorpg: BTreeMap<String, RawDinorpgVault>,
  #[serde(default)]
  pub emush: BTreeMap<String, RawEmushVault>,
  #[serde(default)]
  pub epopotamo: BTreeMap<String, RawEpopotamoVault>,
  #[serde(default)]
  pub eternalfest: BTreeMap<String, RawEternalfestVault>,
  #[serde(default)]
  pub eternaltwin: BTreeMap<String, RawEternaltwinVault>,
  #[serde(default)]
  pub kadokadeo: BTreeMap<String, RawAutoVault>,
  #[serde(default)]
  pub kingdom: BTreeMap<String, RawKingdomVault>,
  #[serde(default)]
  pub neoparc: BTreeMap<String, RawNeoparcVault>,
  #[serde(default)]
  pub uptrace: BTreeMap<String, RawUptraceVault>,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct RawAutoVault {
  pub borgbase_repo: String,
  pub db_admin_password: String,
  pub db_main_password: String,
  pub db_read_password: String,
  pub secret: String,
  pub etwin_client_secret: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct RawBruteVault {
  pub borgbase_repo: String,
  pub db_admin_password: String,
  pub db_main_password: String,
  pub db_read_password: String,
  pub etwin_client_secret: String,
  pub discord_webhook_token: Option<String>,
  pub logs_webhook_token: Option<String>,
  pub rankup_webhook_token: Option<String>,
  pub release_webhook_token: Option<String>,
  pub known_issues_webhook_token: Option<String>,
  pub cookie_secret: String,
  pub csrf_secret: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct RawDinorpgVault {
  pub borgbase_repo: String,
  pub db_admin_password: String,
  pub db_main_password: String,
  pub db_read_password: String,
  pub secret_key: String,
  pub etwin_client_secret: String,
  pub discord_webhook_token: Option<String>,
  pub logs_webhook_token: Option<String>,
  pub salt: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct RawEternaltwinVault {
  pub borgbase_repo: String,
  pub db_admin_password: String,
  pub db_main_password: String,
  pub db_read_password: String,
  pub secret_key: String,
  pub mailer: String,
  pub brute_production_secret: String,
  pub brute_staging_secret: String,
  pub dinocard_production_secret: String,
  pub dinocard_staging_secret: String,
  pub dinorpg_production_secret: String,
  pub dinorpg_staging_secret: String,
  pub directquiz_secret: String,
  pub emush_production_secret: String,
  pub emush_staging_secret: String,
  pub epopotamo_production_secret: String,
  pub epopotamo_staging_secret: String,
  pub eternalfest_production_secret: String,
  pub eternalfest_staging_secret: String,
  pub kadokadeo_production_secret: String,
  pub kadokadeo_staging_secret: String,
  pub kingdom_production_secret: String,
  pub kingdom_staging_secret: String,
  pub mjrt_secret: String,
  pub myhordes_secret: String,
  pub neoparc_production_secret: String,
  pub neoparc_staging_secret: String,
  pub wiki_secret: String,
  pub twinoid_client_id: String,
  pub twinoid_client_key: String,
  pub uptrace_dsn: Option<String>,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct RawEternalfestVault {
  pub borgbase_repo: String,
  pub db_admin_password: String,
  pub db_main_password: String,
  pub db_read_password: String,
  pub secret_key: String,
  pub cookie_key: String,
  pub etwin_client_secret: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct RawEmushVault {
  pub borgbase_repo: String,
  pub db_admin_password: String,
  pub db_main_password: String,
  pub db_read_password: String,
  pub app_secret: String,
  pub access_passphrase: String,
  pub etwin_client_secret: String,
  pub discord_webhook: Option<String>,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct RawEpopotamoVault {
  pub borgbase_repo: String,
  pub db_admin_password: String,
  pub db_main_password: String,
  pub db_read_password: String,
  pub etwin_client_secret: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct RawKingdomVault {
  pub borgbase_repo: String,
  pub db_admin_password: String,
  pub db_main_password: String,
  pub db_read_password: String,
  pub app_secret: String,
  pub etwin_client_secret: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct RawNeoparcVault {
  pub borgbase_repo: String,
  pub db_admin_password: String,
  pub db_main_password: String,
  pub db_read_password: String,
  pub secret: Option<String>,
  pub discord_token: Option<String>,
  pub salt: String,
  pub seed: String,
  pub test: String,
  pub etwin_client_secret: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct RawUptraceVault {
  pub pg_admin_password: String,
  pub pg_main_password: String,
  pub pg_read_password: String,
  pub ch_main_password: String,
  pub ch_read_password: String,
  pub secret: String,
  pub uptrace_system_project_token: String,
  pub uptrace_project_token: BTreeMap<String, String>,
  pub uptrace_user_password: BTreeMap<String, String>,
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct RawAppConfig<ChanConfig: ChannelConfig> {
  pub user: Option<String>,
  pub group: Option<String>,
  pub repository: String,
  pub channels: HashMap<String, ChanConfig>,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct RawAutoChannelConfig {
  pub state: Option<RawStateConfig>,
  pub domain: String,
  pub release: String,
  pub https: Option<RawHttpsConfig>,
  pub main_port: u16,
  pub fallback_port: u16,
  pub etwin_uri: String,
  pub etwin_client_id: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct RawBruteChannelConfig {
  pub state: Option<RawStateConfig>,
  pub domain: String,
  pub release: String,
  pub https: Option<RawHttpsConfig>,
  pub main_port: u16,
  pub fallback_port: u16,
  pub etwin_uri: String,
  pub etwin_client_id: String,
  pub discord_webhook_id: Option<String>,
  pub logs_webhook_id: Option<String>,
  pub rankup_webhook_id: Option<String>,
  pub release_webhook_id: Option<String>,
  pub known_issues_webhook_id: Option<String>,
  pub cors_regex: String,
  #[serde(default)]
  pub subdomains: BTreeSet<String>,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct RawDinorpgChannelConfig {
  pub state: Option<RawStateConfig>,
  pub domain: String,
  pub release: String,
  pub https: Option<RawHttpsConfig>,
  pub main_port: u16,
  pub fallback_port: u16,
  pub etwin_uri: String,
  pub etwin_client_id: String,
  pub discord_webhook_id: Option<String>,
  pub logs_webhook_id: Option<String>,
  pub administrator: String,
  pub wss_port: u16,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct RawEmushChannelConfig {
  pub state: Option<RawStateConfig>,
  pub domain: String,
  pub release: String,
  pub https: Option<RawHttpsConfig>,
  pub main_port: u16,
  pub fallback_port: u16,
  pub etwin_uri: String,
  pub etwin_client_id: String,
  pub admin_user_id: Option<String>,
  pub app_name: String,
  pub otel_endpoint: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct RawEpopotamoChannelConfig {
  pub state: Option<RawStateConfig>,
  pub domain: String,
  pub release: String,
  pub https: Option<RawHttpsConfig>,
  pub main_port: u16,
  pub fallback_port: u16,
  pub etwin_uri: String,
  pub etwin_client_id: String,
  pub permanent_admin: String,
  pub socket_port: u16,
  pub profiling: bool,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct RawFrutibandasChannelConfig {
  pub state: Option<RawStateConfig>,
  pub domain: String,
  pub release: String,
  pub https: Option<RawHttpsConfig>,
  pub main_port: u16,
  pub fallback_port: u16,
  // pub etwin_uri: String,
  // pub etwin_client_id: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct RawEternalfestChannelConfig {
  pub state: Option<RawStateConfig>,
  pub domain: String,
  pub release: String,
  pub https: Option<RawHttpsConfig>,
  pub main_port: u16,
  pub backend_port: u16,
  pub fallback_port: u16,
  pub etwin_uri: Url,
  pub etwin_client_id: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct RawEternaltwinChannelConfig {
  pub state: Option<RawStateConfig>,
  pub domain: String,
  pub release: String,
  pub https: Option<RawHttpsConfig>,
  pub main_port: u16,
  pub backend_port: u16,
  pub fallback_port: u16,
  pub opentelemetry_endpoint: Option<String>,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct RawKingdomChannelConfig {
  pub state: Option<RawStateConfig>,
  pub domain: String,
  pub release: String,
  pub https: Option<RawHttpsConfig>,
  pub main_port: u16,
  pub fallback_port: u16,
  pub etwin_uri: String,
  pub etwin_client_id: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct RawNeoparcChannelConfig {
  pub state: Option<RawStateConfig>,
  pub domain: String,
  pub release: String,
  pub https: Option<RawHttpsConfig>,
  pub main_port: u16,
  pub fallback_port: u16,
  pub etwin_uri: String,
  pub etwin_client_id: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct RawUptraceChannelConfig {
  pub state: Option<RawStateConfig>,
  pub domain: String,
  pub version: String,
  pub sha256_digest: String,
  pub https: Option<RawHttpsConfig>,
  pub main_port: u16,
  pub grpc_port: u16,
  pub fallback_port: u16,
  #[serde(default)]
  pub projects: BTreeMap<String, RawUptraceChannelProjectConfig>,
  #[serde(default)]
  pub users: BTreeMap<String, RawUptraceChannelUserConfig>,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct RawUptraceChannelUserConfig {
  pub name: String,
  pub email: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct RawUptraceChannelProjectConfig {
  pub name: String,
}

pub trait ChannelConfig {
  fn state(&self) -> Option<&RawStateConfig>;
}

macro_rules! impl_channel_config {
  ($config:ty) => {
    impl ChannelConfig for $config {
      fn state(&self) -> Option<&RawStateConfig> {
        self.state.as_ref()
      }
    }
  };
}

impl_channel_config!(RawAutoChannelConfig);
impl_channel_config!(RawBruteChannelConfig);
impl_channel_config!(RawDinorpgChannelConfig);
impl_channel_config!(RawEmushChannelConfig);
impl_channel_config!(RawEpopotamoChannelConfig);
impl_channel_config!(RawEternalfestChannelConfig);
impl_channel_config!(RawEternaltwinChannelConfig);
impl_channel_config!(RawFrutibandasChannelConfig);
impl_channel_config!(RawKingdomChannelConfig);
impl_channel_config!(RawNeoparcChannelConfig);
impl_channel_config!(RawUptraceChannelConfig);

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct RawHttpsConfig {
  pub email: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct RawStateConfig {
  pub token: String,
  pub action: StateAction,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub enum StateAction {
  /// Reset the state
  Reset,
  /// Restore from a backup
  Restore,
}

#[derive(Debug, Error)]
pub enum FindConfigFileError {
  #[error("No configuration file `katal.toml` found from: {:?}", .0)]
  NotFound(PathBuf),
  #[error("Unexpected I/O error when resolving configuration `katal.toml` from: {:?}: {:?}", .0, .1)]
  Other(PathBuf, io::Error),
}

#[derive(Debug, Error)]
pub enum FindConfigError {
  #[error("No configuration file found from: {:?}", .0)]
  NotFound(PathBuf),
  #[error("Failed to parse configuration file at: {:?}: {:?}", .0, .1)]
  ParseError(PathBuf, toml::de::Error),
  #[error("Unexpected I/O error when resolving configuration from: {:?}: {:?}", .0, .1)]
  Other(PathBuf, io::Error),
}

fn find_config_file(dir: PathBuf) -> Result<(PathBuf, String), FindConfigFileError> {
  for d in dir.ancestors() {
    let config_path = d.join("katal.toml");
    match fs::read_to_string(&config_path) {
      Ok(toml) => return Ok((config_path, toml)),
      Err(e) if e.kind() == io::ErrorKind::NotFound => continue,
      Err(e) => return Err(FindConfigFileError::Other(dir, e)),
    }
  }
  Err(FindConfigFileError::NotFound(dir))
}

pub fn parse_config(_file: &Path, config_toml: &str) -> Result<RawConfig, toml::de::Error> {
  let raw: RawConfig = toml::from_str(config_toml)?;
  Ok(raw)
}

pub fn find_config(dir: PathBuf) -> Result<RawConfig, FindConfigError> {
  match find_config_file(dir) {
    Ok((file, config_toml)) => match parse_config(&file, &config_toml) {
      Ok(config) => Ok(config),
      Err(e) => Err(FindConfigError::ParseError(file, e)),
    },
    Err(FindConfigFileError::NotFound(dir)) => Err(FindConfigError::NotFound(dir)),
    Err(FindConfigFileError::Other(dir, cause)) => Err(FindConfigError::Other(dir, cause)),
  }
}

pub fn parse_new_config(_file: &Path, config_toml: &str) -> Result<KatalConfig, toml::de::Error> {
  let raw: KatalConfig = toml::from_str(config_toml)?;
  Ok(raw)
}

pub fn find_new_config(dir: PathBuf) -> Result<KatalConfig, FindConfigError> {
  match find_config_file(dir) {
    Ok((file, config_toml)) => match parse_new_config(&file, &config_toml) {
      Ok(config) => Ok(config),
      Err(e) => Err(FindConfigError::ParseError(file, e)),
    },
    Err(FindConfigFileError::NotFound(dir)) => Err(FindConfigError::NotFound(dir)),
    Err(FindConfigFileError::Other(dir, cause)) => Err(FindConfigError::Other(dir, cause)),
  }
}
