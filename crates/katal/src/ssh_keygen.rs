use crate::task::{AsyncFn, TaskResult, TaskSuccess};
use async_trait::async_trait;
use asys::common::fs::FsHost;
use asys::linux::fs::{LinuxFsHost, LinuxMetadata};
use asys::{Command, ExecHost};
use std::path::PathBuf;

pub struct SshKeyEd25519 {
  /// Path to the private key file
  pub key: PathBuf,
  pub comment: String,
  pub password: Option<String>,
}

impl SshKeyEd25519 {
  pub fn priv_key_path(&self) -> PathBuf {
    self.key.clone()
  }

  pub fn pub_key_path(&self) -> PathBuf {
    let mut priv_key = self.key.clone().into_os_string();
    priv_key.push(".pub");
    priv_key.into()
  }
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for SshKeyEd25519
where
  H: ExecHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<(), anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let mut changed = false;
    let priv_key = self.priv_key_path();
    let pub_key = self.pub_key_path();
    match (priv_key.exists(), pub_key.exists()) {
      (true, true) => return Ok(TaskSuccess { changed, output: () }),
      (true, false) => std::fs::remove_file(&priv_key)?,
      (false, true) => std::fs::remove_file(&pub_key)?,
      (false, false) => {}
    }
    changed = true;

    let mut cmd = Command::new("ssh-keygen");
    cmd = cmd.arg("-t").arg("ed25519");
    cmd = cmd.arg("-C").arg(&self.comment);
    cmd = cmd.arg("-f").arg(self.key.to_str().unwrap());
    cmd = cmd.arg("-N").arg(self.password.as_deref().unwrap_or(""));

    host.exec(&cmd)?;

    Ok(TaskSuccess { changed, output: () })
  }
}
