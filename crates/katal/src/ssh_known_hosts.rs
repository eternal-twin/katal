use crate::task::fs::EnsureFile;
use crate::task::AsyncFn;
use asys::local_linux::LocalLinux;
use asys::{Command, ExecHost};
use once_cell::sync::Lazy;
use regex::Regex;
use serde::{Deserialize, Serialize};
use std::fmt;
use std::io::ErrorKind;
use std::path::{Path, PathBuf};
use vec1::Vec1;

static KNOWN_HOST_PATTERN: Lazy<Regex> = Lazy::new(|| Regex::new(r"^([^ ]+) ([^ ]+) ([^ ]+)(?: (.*))?$").unwrap());
static FINGERPRINT_PATTERN: Lazy<Regex> = Lazy::new(|| Regex::new(r"^(\d+) ([^ ]+)(?: ([^\n]*))?\n?$").unwrap());

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct SshUrl {
  // pub credentials: Option<String>,
  pub host: String,
  pub path: String,
}

impl std::str::FromStr for SshUrl {
  type Err = anyhow::Error;

  fn from_str(input: &str) -> Result<Self, Self::Err> {
    let url = url::Url::parse(input).map_err(|e| anyhow::Error::msg(format!("InvalidSshUrl: {input}: {e}")))?;
    if url.scheme() != "ssh" {
      return Err(anyhow::Error::msg(format!("NotAnSshUrl: {}", input)));
    };
    let host = url
      .host_str()
      .map(|s| s.to_string())
      .ok_or_else(|| anyhow::Error::msg(format!("MissingHost: {}", input)))?;
    let path = url.path().to_string();
    Ok(Self {
      // credentials,
      host,
      path,
    })
  }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct KeyTypes {
  dsa: bool,
  ecdsa: bool,
  ed25519: bool,
  rsa: bool,
}

impl Default for KeyTypes {
  fn default() -> Self {
    Self {
      dsa: false,
      ecdsa: true,
      ed25519: true,
      rsa: true,
    }
  }
}

impl KeyTypes {
  pub fn ed25519() -> Self {
    Self {
      dsa: false,
      ecdsa: false,
      ed25519: true,
      rsa: false,
    }
  }

  pub fn to_keyscan(&self) -> String {
    let types = [
      self.dsa.then_some("dsa"),
      self.ecdsa.then_some("ecdsa"),
      self.ed25519.then_some("ed25519"),
      self.rsa.then_some("rsa"),
    ];
    let types: Vec<&'static str> = types.iter().filter_map(|t| *t).collect();
    types.join(",")
  }
}

pub fn keyscan<H: ExecHost>(host: &H, types: KeyTypes, target: &str) -> Result<Vec<KnownHostEntry>, anyhow::Error> {
  let mut cmd = Command::new("ssh-keyscan");
  cmd = cmd.arg("-t").arg(types.to_keyscan());
  cmd = cmd.arg(target);
  let output = host.exec(&cmd)?;
  parse_known_hosts(&output.stdout)
}

pub fn parse_known_hosts(mut input: &[u8]) -> Result<Vec<KnownHostEntry>, anyhow::Error> {
  let mut entries: Vec<KnownHostEntry> = Vec::new();
  while !input.is_empty() {
    match input[0] {
      b'#' => {
        while !input.is_empty() && input[0] != b'\n' {
          input = &input[1..];
        }
        continue;
      }
      b'\n' | b'\t' => {
        input = &input[1..];
        continue;
      }
      _ => {}
    }
    let line = {
      let old_input = input;
      while !input.is_empty() && input[0] != b'\n' {
        input = &input[1..];
      }
      &old_input[..(old_input.len() - input.len())]
    };
    let line = std::str::from_utf8(line)?;
    let line = match KNOWN_HOST_PATTERN.captures(line) {
      None => return Err(anyhow::Error::msg(format!("InvalidRow: {}", line))),
      Some(l) => l,
    };
    let raw_spec = line.get(1).unwrap().as_str();
    let spec = if raw_spec.starts_with('|') {
      KnownHostSpec::Hash(raw_spec.to_string())
    } else {
      let mut source_patterns = raw_spec.split(',').map(str::to_string).map(HostnamePattern);
      let mut patterns = Vec1::new(source_patterns.next().unwrap());
      patterns.extend(source_patterns);
      KnownHostSpec::Patterns(patterns)
    };
    let key_type = line.get(2).unwrap().as_str().to_string();
    let pub_key = line.get(3).unwrap().as_str().to_string();

    entries.push(KnownHostEntry {
      marker: None,
      spec,
      key_type,
      pub_key,
    });
  }
  Ok(entries)
}

pub fn ssh_fingerprint<H: ExecHost>(host: &H, keytype: &str, pub_key: &str) -> Result<String, anyhow::Error> {
  let mut cmd = Command::new("ssh-keygen").arg("-l");
  cmd = cmd.arg("-f").arg("-");
  cmd = cmd.stdin(format!("{} {}", keytype, pub_key).as_bytes());
  let output = host.exec(&cmd)?;
  parse_fingerprint(&output.stdout)
}

pub fn parse_fingerprint(input: &[u8]) -> Result<String, anyhow::Error> {
  let line = std::str::from_utf8(input)?;
  let line = match FINGERPRINT_PATTERN.captures(line) {
    None => return Err(anyhow::Error::msg(format!("InvalidRow: {}", line))),
    Some(l) => l,
  };
  let fingerprint = line.get(2).unwrap().as_str().to_string();
  Ok(fingerprint)
}

/// See <https://man.archlinux.org/man/sshd.8#SSH_KNOWN_HOSTS_FILE_FORMAT>.
#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct KnownHostEntry {
  pub marker: Option<KnownHostMarker>,
  pub spec: KnownHostSpec,
  pub key_type: String,
  pub pub_key: String,
}

impl fmt::Display for KnownHostEntry {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    match self.marker {
      Some(KnownHostMarker::CertAuthority) => f.write_str("@cert-authority ")?,
      Some(KnownHostMarker::Revoked) => f.write_str("@revoked ")?,
      _ => {}
    }
    match &self.spec {
      KnownHostSpec::Hash(h) => f.write_str(h)?,
      KnownHostSpec::Patterns(pats) => {
        for (i, pat) in pats.iter().enumerate() {
          if i > 0 {
            f.write_str(",")?;
          }
          f.write_str(&pat.0)?;
        }
      }
    }
    f.write_str(" ")?;
    f.write_str(&self.key_type)?;
    f.write_str(" ")?;
    f.write_str(&self.pub_key)?;
    Ok(())
  }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub enum KnownHostMarker {
  CertAuthority,
  Revoked,
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub enum KnownHostSpec {
  Hash(String),
  Patterns(Vec1<HostnamePattern>),
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct HostnamePattern(pub String);

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct SshKnownHosts {
  pub path: PathBuf,
  pub comment: String,
  pub password: Option<String>,
}

pub fn read_known_hosts(path: impl AsRef<Path>) -> Result<Vec<KnownHostEntry>, anyhow::Error> {
  let input = match std::fs::read(path) {
    Ok(input) => input,
    Err(e) => match e.kind() {
      ErrorKind::NotFound => return Ok(Vec::new()),
      _ => return Err(e.into()),
    },
  };
  parse_known_hosts(&input)
}

pub async fn append_known_hosts(path: impl AsRef<Path>, entry: &KnownHostEntry) -> Result<(), anyhow::Error> {
  let path = path.as_ref();
  let mut content = match std::fs::read(path) {
    Ok(input) => input,
    Err(e) => match e.kind() {
      ErrorKind::NotFound => Vec::new(),
      _ => return Err(e.into()),
    },
  };
  match content.last() {
    Some(c) if *c != b'\n' => content.push(b'\n'),
    _ => {}
  }
  content.extend(entry.to_string().as_bytes());
  content.push(b'\n');
  EnsureFile::new(path).content(content).run(&LocalLinux).await?;
  Ok(())
}

#[cfg(test)]
mod test {
  use super::{parse_known_hosts, HostnamePattern, KnownHostEntry, KnownHostSpec};
  use crate::ssh_known_hosts::{parse_fingerprint, SshUrl};
  use std::str::FromStr;
  use vec1::Vec1;

  #[test]
  fn test_parse_known_hosts() {
    let input = r"# github.com:22 SSH-2.0-babeld-22beb20a
# github.com:22 SSH-2.0-babeld-22beb20a
github.com ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAq2A7hRGmdnm9tUDbO9IDSwBK6TbQa+PXYPCPy6rbTrTtw7PHkccKrpp0yVhp5HdEIcKr6pLlVDBfOLX9QUsyCOV0wzfjIJNlGEYsdlLJizHhbn2mUjvSAHQqZETYP81eFzLQNnPHt4EVVUh7VfDESU84KezmD5QlWpXLmvU31/yMf+Se8xhHTvKSCZIFImWwoG6mbUoWf9nzpIoaSjB+weqqUUmpaaasXVal72J+UX2B+2RPW3RcT0eOzQgqlJL3RKrTJvdsjE3JEAvGq3lGHSZXy28G3skua2SmVi/w4yCE6gbODqnTWlg7+wC604ydGXA8VJiS5ap43JXiUFFAaQ==
# github.com:22 SSH-2.0-babeld-22beb20a
# github.com:22 SSH-2.0-babeld-22beb20a
# github.com:22 SSH-2.0-babeld-22beb20a
";
    let actual = parse_known_hosts(input.as_bytes()).unwrap();
    let expected = vec![KnownHostEntry {
      marker: None,
      spec: KnownHostSpec::Patterns(Vec1::new(HostnamePattern("github.com".to_string()))),
      key_type: "ssh-rsa".to_string(),
      pub_key: {
        let k = "AAAAB3NzaC1yc2EAAAABIwAAAQEAq2A7hRGmdnm9tUDbO9IDSwBK6TbQa+PXYPCPy6rbTrTtw7PHkccKrpp0yVhp5HdEIcKr6pLlVDBfOLX9QUsyCOV0wzfjIJNlGEYsdlLJizHhbn2mUjvSAHQqZETYP81eFzLQNnPHt4EVVUh7VfDESU84KezmD5QlWpXLmvU31/yMf+Se8xhHTvKSCZIFImWwoG6mbUoWf9nzpIoaSjB+weqqUUmpaaasXVal72J+UX2B+2RPW3RcT0eOzQgqlJL3RKrTJvdsjE3JEAvGq3lGHSZXy28G3skua2SmVi/w4yCE6gbODqnTWlg7+wC604ydGXA8VJiS5ap43JXiUFFAaQ==";
        k.to_string()
      },
    }];
    assert_eq!(actual, expected)
  }

  #[test]
  fn test_parse_fingerprint() {
    let input = "256 SHA256:yufQ7QKrcSOCj43GdTD8vfNmJwJH6RcTgpLQQKjXc5Y no comment (ED25519)\n";
    let actual = parse_fingerprint(input.as_bytes()).unwrap();
    let expected = "SHA256:yufQ7QKrcSOCj43GdTD8vfNmJwJH6RcTgpLQQKjXc5Y";
    assert_eq!(actual.as_str(), expected)
  }

  #[test]
  fn test_parse_ssh_uri() {
    let input = "ssh://foobar28z.repo.borgbase.com/./repo";
    let actual = SshUrl::from_str(input).unwrap();
    let expected = SshUrl {
      host: "foobar28z.repo.borgbase.com".to_string(),
      path: "/repo".to_string(),
    };
    assert_eq!(actual, expected)
  }
}
