use crate::pg_sql::{PgRoleName, PgSqlQuotedId, RoleSpec, SequenceSpec, TableSpec};
use core::fmt;
use vec1::Vec1;

pub struct Grant;

impl Grant {
  pub fn to<S: AsRef<str>>(role_spec: RoleSpec<S>) -> GrantToRoles<S> {
    GrantToRoles {
      roles: Vec1::new(role_spec),
    }
  }

  pub fn to_role<S: AsRef<str>>(role: PgRoleName<S>) -> GrantToRoles<S> {
    Self::to(RoleSpec::Role(role))
  }

  pub fn to_public<S: AsRef<str>>() -> GrantToRoles<S> {
    Self::to(RoleSpec::Public)
  }
}

pub struct GrantToRoles<S: AsRef<str> = String> {
  roles: Vec1<RoleSpec<S>>,
}

impl<S: AsRef<str>> GrantToRoles<S> {
  pub fn to(mut self, role_spec: RoleSpec<S>) -> Self {
    self.roles.push(role_spec);
    self
  }

  pub fn to_role(self, role: PgRoleName<S>) -> Self {
    self.to(RoleSpec::Role(role))
  }

  pub fn to_public(self) -> Self {
    self.to(RoleSpec::Public)
  }

  pub fn on_database(self, database: S) -> GrantOnDatabase<S, false> {
    GrantOnDatabase {
      roles: self.roles,
      database,
      create: false,
      connect: false,
      temporary: false,
      with_grant: false,
    }
  }

  pub fn on_schema(self, schema: S) -> GrantOnSchema<S, false> {
    GrantOnSchema {
      roles: self.roles,
      schema,
      create: false,
      usage: false,
      with_grant: false,
    }
  }

  pub fn on_table(self, table: S) -> GrantOnTables<S, false> {
    GrantOnTables {
      roles: self.roles,
      tables: Vec1::new(TableSpec::Table(table)),
      with_grant: false,
      select: false,
      insert: false,
      update: false,
      delete: false,
      truncate: false,
      references: false,
      trigger: false,
    }
  }

  pub fn on_all_tables_in(self, schema: S) -> GrantOnTables<S, false> {
    GrantOnTables {
      roles: self.roles,
      tables: Vec1::new(TableSpec::AllInSchema(schema)),
      with_grant: false,
      select: false,
      insert: false,
      update: false,
      delete: false,
      truncate: false,
      references: false,
      trigger: false,
    }
  }

  pub fn on_all_sequences_in(self, schema: S) -> GrantOnSequences<S, false> {
    GrantOnSequences {
      roles: self.roles,
      sequences: Vec1::new(SequenceSpec::AllInSchema(schema)),
      with_grant: false,
      usage: false,
      select: false,
      update: false,
    }
  }
}

pub struct GrantOnDatabase<S: AsRef<str>, const HAS_GRANTS: bool> {
  roles: Vec1<RoleSpec<S>>,
  database: S,
  with_grant: bool,
  create: bool,
  connect: bool,
  temporary: bool,
}

impl<S: AsRef<str>, const HAS_GRANTS: bool> GrantOnDatabase<S, HAS_GRANTS> {
  pub fn with_grant(mut self) -> Self {
    self.with_grant = true;
    self
  }

  pub fn create(self) -> GrantOnDatabase<S, true> {
    GrantOnDatabase {
      roles: self.roles,
      database: self.database,
      create: true,
      connect: self.connect,
      temporary: self.temporary,
      with_grant: self.with_grant,
    }
  }

  pub fn connect(self) -> GrantOnDatabase<S, true> {
    GrantOnDatabase {
      roles: self.roles,
      database: self.database,
      create: self.create,
      connect: true,
      temporary: self.temporary,
      with_grant: self.with_grant,
    }
  }

  pub fn temporary(self) -> GrantOnDatabase<S, true> {
    GrantOnDatabase {
      roles: self.roles,
      database: self.database,
      create: self.create,
      connect: self.connect,
      temporary: true,
      with_grant: self.with_grant,
    }
  }

  pub fn all(self) -> GrantOnDatabase<S, true> {
    GrantOnDatabase {
      roles: self.roles,
      database: self.database,
      create: true,
      connect: true,
      temporary: true,
      with_grant: self.with_grant,
    }
  }
}

impl<S: AsRef<str>> fmt::Display for GrantOnDatabase<S, true> {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    let mut query: String = "GRANT".to_string();
    let mut first = true;
    if self.create {
      first = false;
      query.push_str(" CREATE");
    }
    if self.connect {
      if !first {
        query.push(',');
      }
      first = false;
      query.push_str(" CONNECT");
    }
    if self.temporary {
      if !first {
        query.push(',');
      }
      query.push_str(" TEMPORARY");
    }
    query.push_str(" ON DATABASE ");
    query.push_str(&PgSqlQuotedId(self.database.as_ref()).to_string());
    query.push_str(" TO ");
    let mut first = true;
    for role in self.roles.iter() {
      if first {
        first = false;
      } else {
        query.push_str(", ");
      }
      match role {
        RoleSpec::Role(r) => query.push_str(&PgSqlQuotedId(r.as_str()).to_string()),
        RoleSpec::Public => query.push_str("PUBLIC"),
      }
    }
    query.push(';');
    f.write_str(&query)
  }
}

pub struct GrantOnSchema<S: AsRef<str>, const HAS_GRANTS: bool> {
  roles: Vec1<RoleSpec<S>>,
  schema: S,
  with_grant: bool,
  create: bool,
  usage: bool,
}

impl<S: AsRef<str>, const HAS_GRANTS: bool> GrantOnSchema<S, HAS_GRANTS> {
  pub fn with_grant(mut self) -> Self {
    self.with_grant = true;
    self
  }

  pub fn create(self) -> GrantOnSchema<S, true> {
    GrantOnSchema {
      roles: self.roles,
      schema: self.schema,
      create: true,
      usage: self.usage,
      with_grant: self.with_grant,
    }
  }

  pub fn usage(self) -> GrantOnSchema<S, true> {
    GrantOnSchema {
      roles: self.roles,
      schema: self.schema,
      create: self.create,
      usage: true,
      with_grant: self.with_grant,
    }
  }

  pub fn all(self) -> GrantOnSchema<S, true> {
    GrantOnSchema {
      roles: self.roles,
      schema: self.schema,
      create: true,
      usage: true,
      with_grant: self.with_grant,
    }
  }
}

impl<S: AsRef<str>> fmt::Display for GrantOnSchema<S, true> {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    let mut query: String = "GRANT".to_string();
    let mut first = true;
    if self.create {
      first = false;
      query.push_str(" CREATE");
    }
    if self.usage {
      if !first {
        query.push(',');
      }
      query.push_str(" USAGE");
    }
    query.push_str(" ON SCHEMA ");
    query.push_str(&PgSqlQuotedId(self.schema.as_ref()).to_string());
    query.push_str(" TO ");
    let mut first = true;
    for role in self.roles.iter() {
      if first {
        first = false;
      } else {
        query.push_str(", ");
      }
      match role {
        RoleSpec::Role(r) => query.push_str(&PgSqlQuotedId(r.as_str()).to_string()),
        RoleSpec::Public => query.push_str("PUBLIC"),
      }
    }
    query.push(';');
    f.write_str(&query)
  }
}

pub struct GrantOnTables<S: AsRef<str>, const HAS_GRANTS: bool> {
  roles: Vec1<RoleSpec<S>>,
  tables: Vec1<TableSpec<S>>,
  with_grant: bool,
  select: bool,
  insert: bool,
  update: bool,
  delete: bool,
  truncate: bool,
  references: bool,
  trigger: bool,
}

impl<S: AsRef<str>, const HAS_GRANTS: bool> GrantOnTables<S, HAS_GRANTS> {
  pub fn with_grant(mut self) -> Self {
    self.with_grant = true;
    self
  }

  pub fn select(self) -> GrantOnTables<S, true> {
    GrantOnTables {
      roles: self.roles,
      tables: self.tables,
      with_grant: self.with_grant,
      select: true,
      insert: self.insert,
      update: self.update,
      delete: self.delete,
      truncate: self.truncate,
      references: self.references,
      trigger: self.trigger,
    }
  }

  pub fn insert(self) -> GrantOnTables<S, true> {
    GrantOnTables {
      roles: self.roles,
      tables: self.tables,
      with_grant: self.with_grant,
      select: self.select,
      insert: true,
      update: self.update,
      delete: self.delete,
      truncate: self.truncate,
      references: self.references,
      trigger: self.trigger,
    }
  }

  pub fn update(self) -> GrantOnTables<S, true> {
    GrantOnTables {
      roles: self.roles,
      tables: self.tables,
      with_grant: self.with_grant,
      select: self.select,
      insert: self.insert,
      update: true,
      delete: self.delete,
      truncate: self.truncate,
      references: self.references,
      trigger: self.trigger,
    }
  }

  pub fn delete(self) -> GrantOnTables<S, true> {
    GrantOnTables {
      roles: self.roles,
      tables: self.tables,
      with_grant: self.with_grant,
      select: self.select,
      insert: self.insert,
      update: self.update,
      delete: true,
      truncate: self.truncate,
      references: self.references,
      trigger: self.trigger,
    }
  }

  pub fn truncate(self) -> GrantOnTables<S, true> {
    GrantOnTables {
      roles: self.roles,
      tables: self.tables,
      with_grant: self.with_grant,
      select: self.select,
      insert: self.insert,
      update: self.update,
      delete: self.delete,
      truncate: true,
      references: self.references,
      trigger: self.trigger,
    }
  }

  pub fn references(self) -> GrantOnTables<S, true> {
    GrantOnTables {
      roles: self.roles,
      tables: self.tables,
      with_grant: self.with_grant,
      select: self.select,
      insert: self.insert,
      update: self.update,
      delete: self.delete,
      truncate: self.truncate,
      references: true,
      trigger: self.trigger,
    }
  }

  pub fn trigger(self) -> GrantOnTables<S, true> {
    GrantOnTables {
      roles: self.roles,
      tables: self.tables,
      with_grant: self.with_grant,
      select: self.select,
      insert: self.insert,
      update: self.update,
      delete: self.delete,
      truncate: self.truncate,
      references: self.references,
      trigger: true,
    }
  }

  pub fn all(self) -> GrantOnTables<S, true> {
    GrantOnTables {
      roles: self.roles,
      tables: self.tables,
      with_grant: self.with_grant,
      select: true,
      insert: true,
      update: true,
      delete: true,
      truncate: true,
      references: true,
      trigger: true,
    }
  }
}

impl<S: AsRef<str>> fmt::Display for GrantOnTables<S, true> {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    let mut query: String = "GRANT".to_string();
    let mut first = true;
    if self.select {
      first = false;
      query.push_str(" SELECT");
    }
    if self.insert {
      if !first {
        query.push(',');
      }
      first = false;
      query.push_str(" INSERT");
    }
    if self.update {
      if !first {
        query.push(',');
      }
      first = false;
      query.push_str(" UPDATE");
    }
    if self.delete {
      if !first {
        query.push(',');
      }
      first = false;
      query.push_str(" DELETE");
    }
    if self.truncate {
      if !first {
        query.push(',');
      }
      first = false;
      query.push_str(" TRUNCATE");
    }
    if self.references {
      if !first {
        query.push(',');
      }
      first = false;
      query.push_str(" REFERENCES");
    }
    if self.trigger {
      if !first {
        query.push(',');
      }
      query.push_str(" TRIGGER");
    }
    query.push_str(" ON");
    let mut first = true;
    for table in self.tables.iter() {
      if first {
        first = false;
      } else {
        query.push(',');
      }
      match table {
        TableSpec::Table(t) => {
          query.push_str(" TABLE ");
          query.push_str(&PgSqlQuotedId(t.as_ref()).to_string())
        }
        TableSpec::AllInSchema(s) => {
          query.push_str(" ALL TABLES IN SCHEMA ");
          query.push_str(&PgSqlQuotedId(s.as_ref()).to_string())
        }
      }
    }
    query.push_str(" TO ");
    let mut first = true;
    for role in self.roles.iter() {
      if first {
        first = false;
      } else {
        query.push_str(", ");
      }
      match role {
        RoleSpec::Role(r) => query.push_str(&PgSqlQuotedId(r.as_str()).to_string()),
        RoleSpec::Public => query.push_str("PUBLIC"),
      }
    }
    query.push(';');
    f.write_str(&query)
  }
}

pub struct GrantOnSequences<S: AsRef<str>, const HAS_GRANTS: bool> {
  roles: Vec1<RoleSpec<S>>,
  sequences: Vec1<SequenceSpec<S>>,
  with_grant: bool,
  usage: bool,
  select: bool,
  update: bool,
}

impl<S: AsRef<str>, const HAS_GRANTS: bool> GrantOnSequences<S, HAS_GRANTS> {
  pub fn with_grant(mut self) -> Self {
    self.with_grant = true;
    self
  }

  pub fn usage(self) -> GrantOnSequences<S, true> {
    GrantOnSequences {
      roles: self.roles,
      sequences: self.sequences,
      with_grant: self.with_grant,
      usage: true,
      select: self.select,
      update: self.update,
    }
  }

  pub fn select(self) -> GrantOnSequences<S, true> {
    GrantOnSequences {
      roles: self.roles,
      sequences: self.sequences,
      with_grant: self.with_grant,
      usage: self.usage,
      select: true,
      update: self.update,
    }
  }

  pub fn update(self) -> GrantOnSequences<S, true> {
    GrantOnSequences {
      roles: self.roles,
      sequences: self.sequences,
      with_grant: self.with_grant,
      usage: self.usage,
      select: self.select,
      update: true,
    }
  }

  pub fn all(self) -> GrantOnSequences<S, true> {
    GrantOnSequences {
      roles: self.roles,
      sequences: self.sequences,
      with_grant: self.with_grant,
      usage: true,
      select: true,
      update: true,
    }
  }
}

impl<S: AsRef<str>> fmt::Display for GrantOnSequences<S, true> {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    let mut query: String = "GRANT".to_string();
    let mut first = true;
    if self.usage {
      first = false;
      query.push_str(" USAGE");
    }
    if self.select {
      if !first {
        query.push(',');
      }
      first = false;
      query.push_str(" SELECT");
    }
    if self.update {
      if !first {
        query.push(',');
      }
      query.push_str(" UPDATE");
    }
    query.push_str(" ON");
    let mut first = true;
    for seq in self.sequences.iter() {
      if first {
        first = false;
      } else {
        query.push(',');
      }
      match seq {
        SequenceSpec::Sequence(s) => {
          query.push_str(" SEQUENCE ");
          query.push_str(&PgSqlQuotedId(s.as_ref()).to_string())
        }
        SequenceSpec::AllInSchema(s) => {
          query.push_str(" ALL SEQUENCES IN SCHEMA ");
          query.push_str(&PgSqlQuotedId(s.as_ref()).to_string())
        }
      }
    }
    query.push_str(" TO ");
    let mut first = true;
    for role in self.roles.iter() {
      if first {
        first = false;
      } else {
        query.push_str(", ");
      }
      match role {
        RoleSpec::Role(r) => query.push_str(&PgSqlQuotedId(r.as_str()).to_string()),
        RoleSpec::Public => query.push_str("PUBLIC"),
      }
    }
    query.push(';');
    f.write_str(&query)
  }
}
