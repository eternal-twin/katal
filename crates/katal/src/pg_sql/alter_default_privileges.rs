use crate::pg_sql::{PgRoleName, PgSqlQuotedId, RoleSpec};
use core::fmt;
use vec1::Vec1;

pub struct AlterDefaultPrivileges;

impl AlterDefaultPrivileges {
  pub fn for_current_user<S: AsRef<str>>() -> AlterDefaultPrivilegesForGranters<S> {
    AlterDefaultPrivilegesForGranters { granters: Vec::new() }
  }

  pub fn for_role<S: AsRef<str>>(granter: PgRoleName<S>) -> AlterDefaultPrivilegesForGranters<S> {
    AlterDefaultPrivilegesForGranters {
      granters: vec![granter],
    }
  }
}

pub struct AlterDefaultPrivilegesForGranters<S: AsRef<str> = String> {
  /// Default privileges for objects created by the following roles.
  /// (Empty means "current user")
  granters: Vec<PgRoleName<S>>,
}

impl<S: AsRef<str>> AlterDefaultPrivilegesForGranters<S> {
  pub fn for_role(mut self, granter: PgRoleName<S>) -> Self {
    self.granters.push(granter);
    self
  }

  pub fn in_schema(self, schema: S) -> AlterDefaultPrivilegesInSchemas<S> {
    AlterDefaultPrivilegesInSchemas {
      granters: self.granters,
      in_schemas: vec![schema],
    }
  }

  pub fn in_all_schemas(self) -> AlterDefaultPrivilegesInSchemas<S> {
    AlterDefaultPrivilegesInSchemas {
      granters: self.granters,
      in_schemas: Vec::new(),
    }
  }
}

pub struct AlterDefaultPrivilegesInSchemas<S: AsRef<str> = String> {
  granters: Vec<PgRoleName<S>>,
  /// Default privileges for objects created in the following schemas.
  /// (Empty means all the schemas)
  in_schemas: Vec<S>,
}

impl<S: AsRef<str>> AlterDefaultPrivilegesInSchemas<S> {
  pub fn grant_on_tables_to(self, role_spec: RoleSpec<S>) -> AlterDefaultPrivilegesGrantOnTables<S, false> {
    AlterDefaultPrivilegesGrantOnTables {
      granters: self.granters,
      in_schemas: self.in_schemas,
      grantees: Vec1::new(role_spec),
      with_grant: false,
      select: false,
      insert: false,
      update: false,
      delete: false,
      truncate: false,
      references: false,
      trigger: false,
    }
  }

  pub fn grant_on_tables_to_role(self, role: PgRoleName<S>) -> AlterDefaultPrivilegesGrantOnTables<S, false> {
    self.grant_on_tables_to(RoleSpec::Role(role))
  }

  pub fn grant_on_sequences_to(self, role_spec: RoleSpec<S>) -> AlterDefaultPrivilegesGrantOnSequences<S, false> {
    AlterDefaultPrivilegesGrantOnSequences {
      granters: self.granters,
      in_schemas: self.in_schemas,
      grantees: Vec1::new(role_spec),
      with_grant: false,
      usage: false,
      select: false,
      update: false,
    }
  }

  pub fn grant_on_sequences_to_role(self, role: PgRoleName<S>) -> AlterDefaultPrivilegesGrantOnSequences<S, false> {
    self.grant_on_sequences_to(RoleSpec::Role(role))
  }
}

pub struct AlterDefaultPrivilegesGrantOnTables<S: AsRef<str>, const HAS_GRANTS: bool> {
  /// Default privileges for tables created by...
  /// - `Some(role)` => ...the role `role`.
  /// - `None` => ...the current user.
  granters: Vec<PgRoleName<S>>,
  /// Default privileges for tables created in...
  /// - `Some(schema)` => ...the schema `schema` of the current database.
  /// - `None` => ...any schema of the current database.
  in_schemas: Vec<S>,
  grantees: Vec1<RoleSpec<S>>,
  with_grant: bool,
  select: bool,
  insert: bool,
  update: bool,
  delete: bool,
  truncate: bool,
  references: bool,
  trigger: bool,
}

impl<S: AsRef<str>, const HAS_GRANTS: bool> AlterDefaultPrivilegesGrantOnTables<S, HAS_GRANTS> {
  pub fn with_grant(mut self) -> Self {
    self.with_grant = true;
    self
  }

  pub fn select(self) -> AlterDefaultPrivilegesGrantOnTables<S, true> {
    AlterDefaultPrivilegesGrantOnTables {
      granters: self.granters,
      in_schemas: self.in_schemas,
      grantees: self.grantees,
      with_grant: self.with_grant,
      select: true,
      insert: self.insert,
      update: self.update,
      delete: self.delete,
      truncate: self.truncate,
      references: self.references,
      trigger: self.trigger,
    }
  }

  pub fn insert(self) -> AlterDefaultPrivilegesGrantOnTables<S, true> {
    AlterDefaultPrivilegesGrantOnTables {
      granters: self.granters,
      in_schemas: self.in_schemas,
      grantees: self.grantees,
      with_grant: self.with_grant,
      select: self.select,
      insert: true,
      update: self.update,
      delete: self.delete,
      truncate: self.truncate,
      references: self.references,
      trigger: self.trigger,
    }
  }

  pub fn update(self) -> AlterDefaultPrivilegesGrantOnTables<S, true> {
    AlterDefaultPrivilegesGrantOnTables {
      granters: self.granters,
      in_schemas: self.in_schemas,
      grantees: self.grantees,
      with_grant: self.with_grant,
      select: self.select,
      insert: self.insert,
      update: true,
      delete: self.delete,
      truncate: self.truncate,
      references: self.references,
      trigger: self.trigger,
    }
  }

  pub fn delete(self) -> AlterDefaultPrivilegesGrantOnTables<S, true> {
    AlterDefaultPrivilegesGrantOnTables {
      granters: self.granters,
      in_schemas: self.in_schemas,
      grantees: self.grantees,
      with_grant: self.with_grant,
      select: self.select,
      insert: self.insert,
      update: self.update,
      delete: true,
      truncate: self.truncate,
      references: self.references,
      trigger: self.trigger,
    }
  }

  pub fn truncate(self) -> AlterDefaultPrivilegesGrantOnTables<S, true> {
    AlterDefaultPrivilegesGrantOnTables {
      granters: self.granters,
      in_schemas: self.in_schemas,
      grantees: self.grantees,
      with_grant: self.with_grant,
      select: self.select,
      insert: self.insert,
      update: self.update,
      delete: self.delete,
      truncate: true,
      references: self.references,
      trigger: self.trigger,
    }
  }

  pub fn references(self) -> AlterDefaultPrivilegesGrantOnTables<S, true> {
    AlterDefaultPrivilegesGrantOnTables {
      granters: self.granters,
      in_schemas: self.in_schemas,
      grantees: self.grantees,
      with_grant: self.with_grant,
      select: self.select,
      insert: self.insert,
      update: self.update,
      delete: self.delete,
      truncate: self.truncate,
      references: true,
      trigger: self.trigger,
    }
  }

  pub fn trigger(self) -> AlterDefaultPrivilegesGrantOnTables<S, true> {
    AlterDefaultPrivilegesGrantOnTables {
      granters: self.granters,
      in_schemas: self.in_schemas,
      grantees: self.grantees,
      with_grant: self.with_grant,
      select: self.select,
      insert: self.insert,
      update: self.update,
      delete: self.delete,
      truncate: self.truncate,
      references: self.references,
      trigger: true,
    }
  }

  pub fn all(self) -> AlterDefaultPrivilegesGrantOnTables<S, true> {
    AlterDefaultPrivilegesGrantOnTables {
      granters: self.granters,
      in_schemas: self.in_schemas,
      grantees: self.grantees,
      with_grant: self.with_grant,
      select: true,
      insert: true,
      update: true,
      delete: true,
      truncate: true,
      references: true,
      trigger: true,
    }
  }
}

impl<S: AsRef<str>> fmt::Display for AlterDefaultPrivilegesGrantOnTables<S, true> {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    let mut query: String = "ALTER DEFAULT PRIVILEGES".to_string();
    if !self.granters.is_empty() {
      query.push_str(" FOR ");
      let mut first = true;
      for granter in self.granters.iter() {
        if first {
          first = false;
        } else {
          query.push_str(", ");
        }
        query.push_str("ROLE ");
        query.push_str(&PgSqlQuotedId(granter.as_str()).to_string());
      }
    }
    if !self.in_schemas.is_empty() {
      query.push_str(" IN SCHEMA ");
      let mut first = true;
      for schema in self.in_schemas.iter() {
        if first {
          first = false;
        } else {
          query.push_str(", ");
        }
        query.push_str(&PgSqlQuotedId(schema.as_ref()).to_string());
      }
    }
    query.push_str(" GRANT");
    let mut first = true;
    if self.select {
      first = false;
      query.push_str(" SELECT");
    }
    if self.insert {
      if !first {
        query.push(',');
      }
      first = false;
      query.push_str(" INSERT");
    }
    if self.update {
      if !first {
        query.push(',');
      }
      first = false;
      query.push_str(" UPDATE");
    }
    if self.delete {
      if !first {
        query.push(',');
      }
      first = false;
      query.push_str(" DELETE");
    }
    if self.truncate {
      if !first {
        query.push(',');
      }
      first = false;
      query.push_str(" TRUNCATE");
    }
    if self.references {
      if !first {
        query.push(',');
      }
      first = false;
      query.push_str(" REFERENCES");
    }
    if self.trigger {
      if !first {
        query.push(',');
      }
      query.push_str(" TRIGGER");
    }
    query.push_str(" ON TABLES TO ");
    let mut first = true;
    for grantee in self.grantees.iter() {
      if first {
        first = false;
      } else {
        query.push_str(", ");
      }
      match grantee {
        RoleSpec::Role(r) => query.push_str(&PgSqlQuotedId(r.as_str()).to_string()),
        RoleSpec::Public => query.push_str("PUBLIC"),
      }
    }
    query.push(';');
    f.write_str(&query)
  }
}

pub struct AlterDefaultPrivilegesGrantOnSequences<S: AsRef<str>, const HAS_GRANTS: bool> {
  /// Default privileges for sequences created by...
  /// - `Some(role)` => ...the role `role`.
  /// - `None` => ...the current user.
  granters: Vec<PgRoleName<S>>,
  /// Default privileges for sequences created in...
  /// - `Some(schema)` => ...the schema `schema` of the current database.
  /// - `None` => ...any schema of the current database.
  in_schemas: Vec<S>,
  grantees: Vec1<RoleSpec<S>>,
  with_grant: bool,
  usage: bool,
  select: bool,
  update: bool,
}

impl<S: AsRef<str>, const HAS_GRANTS: bool> AlterDefaultPrivilegesGrantOnSequences<S, HAS_GRANTS> {
  pub fn with_grant(mut self) -> Self {
    self.with_grant = true;
    self
  }

  pub fn usage(self) -> AlterDefaultPrivilegesGrantOnSequences<S, true> {
    AlterDefaultPrivilegesGrantOnSequences {
      granters: self.granters,
      in_schemas: self.in_schemas,
      grantees: self.grantees,
      with_grant: self.with_grant,
      usage: true,
      select: self.usage,
      update: self.update,
    }
  }

  pub fn select(self) -> AlterDefaultPrivilegesGrantOnSequences<S, true> {
    AlterDefaultPrivilegesGrantOnSequences {
      granters: self.granters,
      in_schemas: self.in_schemas,
      grantees: self.grantees,
      with_grant: self.with_grant,
      usage: self.usage,
      select: true,
      update: self.update,
    }
  }

  pub fn update(self) -> AlterDefaultPrivilegesGrantOnSequences<S, true> {
    AlterDefaultPrivilegesGrantOnSequences {
      granters: self.granters,
      in_schemas: self.in_schemas,
      grantees: self.grantees,
      with_grant: self.with_grant,
      usage: self.usage,
      select: self.select,
      update: true,
    }
  }

  pub fn all(self) -> AlterDefaultPrivilegesGrantOnSequences<S, true> {
    AlterDefaultPrivilegesGrantOnSequences {
      granters: self.granters,
      in_schemas: self.in_schemas,
      grantees: self.grantees,
      with_grant: self.with_grant,
      usage: true,
      select: true,
      update: true,
    }
  }
}

impl<S: AsRef<str>> fmt::Display for AlterDefaultPrivilegesGrantOnSequences<S, true> {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    let mut query: String = "ALTER DEFAULT PRIVILEGES".to_string();
    if !self.granters.is_empty() {
      query.push_str(" FOR ");
      let mut first = true;
      for granter in self.granters.iter() {
        if first {
          first = false;
        } else {
          query.push_str(", ");
        }
        query.push_str("ROLE ");
        query.push_str(&PgSqlQuotedId(granter.as_str()).to_string());
      }
    }
    if !self.in_schemas.is_empty() {
      query.push_str(" IN SCHEMA ");
      let mut first = true;
      for schema in self.in_schemas.iter() {
        if first {
          first = false;
        } else {
          query.push_str(", ");
        }
        query.push_str(&PgSqlQuotedId(schema.as_ref()).to_string());
      }
    }
    query.push_str(" GRANT");
    let mut first = true;
    if self.usage {
      first = false;
      query.push_str(" USAGE");
    }
    if self.select {
      if !first {
        query.push(',');
      }
      first = false;
      query.push_str(" SELECT");
    }
    if self.update {
      if !first {
        query.push(',');
      }
      query.push_str(" UPDATE");
    }
    query.push_str(" ON SEQUENCES TO ");
    let mut first = true;
    for grantee in self.grantees.iter() {
      if first {
        first = false;
      } else {
        query.push_str(", ");
      }
      match grantee {
        RoleSpec::Role(r) => query.push_str(&PgSqlQuotedId(r.as_str()).to_string()),
        RoleSpec::Public => query.push_str("PUBLIC"),
      }
    }
    query.push(';');
    f.write_str(&query)
  }
}
