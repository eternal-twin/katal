use md5::{Digest, Md5};
use serde::{Deserialize, Serialize};
use sqlx::postgres::{PgDatabaseError, PgSeverity};
use sqlx::Postgres;
use std::convert::TryFrom;
use std::fmt;
use std::fmt::Write;
use thiserror::Error;

pub mod alter_default_privileges;
pub mod grant;
pub mod revoke;

#[derive(Debug, Clone, PartialEq, Eq, Error)]
#[error("[{code} - {severity:?}] {message} (detail = {detail:?}, hint = {hint:?}, where = {where:?}, schema = {schema:?}, table = {table:?}, data_type = {data_type:?}, constraint = {constraint:?}, file = {file:?}, line = {line:?}, routine = {routine:?})")]
pub struct PgError {
  pub severity: PgSeverity,
  pub code: String,
  pub message: String,
  pub detail: Option<String>,
  pub hint: Option<String>,
  // pub position: Option<PgErrorPosition>,
  pub r#where: Option<String>,
  pub schema: Option<String>,
  pub table: Option<String>,
  pub data_type: Option<String>,
  pub constraint: Option<String>,
  pub file: Option<String>,
  pub line: Option<usize>,
  pub routine: Option<String>,
}

impl PgError {
  pub fn from_database_error(err: PgDatabaseError) -> Self {
    Self {
      severity: err.severity(),
      code: err.code().to_string(),
      message: err.message().to_string(),
      detail: err.detail().map(|s| s.to_string()),
      hint: err.hint().map(|s| s.to_string()),
      // position: err.position().map(todo!()),
      r#where: err.r#where().map(|s| s.to_string()),
      schema: err.schema().map(|s| s.to_string()),
      table: err.table().map(|s| s.to_string()),
      data_type: err.data_type().map(|s| s.to_string()),
      constraint: err.constraint().map(|s| s.to_string()),
      file: err.file().map(|s| s.to_string()),
      line: err.line(),
      routine: err.routine().map(|s| s.to_string()),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, Error)]
pub enum CreateRoleError {
  #[error("The role already exists.")]
  AlreadyExists,
  #[error("unexpected database error: {0:?}")]
  Database(Box<PgError>),
  #[error("unexpected error: {0}")]
  Other(String),
}

pub struct CreateRoleQuery<S: AsRef<str> = String> {
  name: PgRoleName<S>,
  is_superuser: Option<bool>,
  can_create_db: Option<bool>,
  can_create_role: Option<bool>,
  inherit_privileges: Option<bool>,
  can_login: Option<bool>,
  is_replication: Option<bool>,
  bypass_rls: Option<bool>,
  // Invariant: `max_connection >= -1`
  max_connections: Option<i32>,
  password: Option<String>,
}

impl<S: AsRef<str>> CreateRoleQuery<S> {
  pub fn new(name: PgRoleName<S>) -> Self {
    Self {
      name,
      is_superuser: None,
      can_create_db: None,
      can_create_role: None,
      inherit_privileges: None,
      can_login: None,
      is_replication: None,
      bypass_rls: None,
      max_connections: None,
      password: None,
    }
  }

  pub fn superuser(mut self, is_superuser: bool) -> Self {
    self.is_superuser = Some(is_superuser);
    self
  }

  pub fn can_login(mut self, can_login: bool) -> Self {
    self.can_login = Some(can_login);
    self
  }

  pub fn raw_password(mut self, pass: &str) -> Self {
    self.password = Some(pass.to_string());
    self
  }

  pub fn md5_password(mut self, pass: &str) -> Self {
    let mut hasher = Md5::new();
    hasher.update(pass);
    hasher.update(self.name.as_str());
    let digest = hasher.finalize();
    self.password = Some(format!("md5{:x}", digest));
    self
  }

  pub fn scram_sha256_password(self, _pass: &str) -> Self {
    todo!()
  }

  // See <https://doxygen.postgresql.org/createuser_8c_source.html>
  pub async fn execute(self, executor: impl sqlx::Executor<'_, Database = Postgres>) -> Result<(), CreateRoleError> {
    let mut query: String = format!("CREATE ROLE {}", PgSqlQuotedId(self.name.as_str()));
    if let Some(pass) = self.password {
      write!(query, " PASSWORD {}", PgSqlStrLit(&pass)).unwrap();
    }
    if let Some(opt) = self.is_superuser {
      query.push_str(if opt { " SUPERUSER" } else { " NOSUPERUSER" });
    }
    if let Some(opt) = self.can_create_db {
      query.push_str(if opt { " CREATEDB" } else { " NOCREATEDB" });
    }
    if let Some(opt) = self.can_create_role {
      query.push_str(if opt { " CREATEROLE" } else { " NOCREATEROLE" });
    }
    if let Some(opt) = self.inherit_privileges {
      query.push_str(if opt { " INHERIT" } else { " NOINHERIT" });
    }
    if let Some(opt) = self.can_login {
      query.push_str(if opt { " LOGIN" } else { " NOLOGIN" });
    }
    if let Some(opt) = self.is_replication {
      query.push_str(if opt { " REPLICATION" } else { " NOREPLICATION" });
    }
    if let Some(opt) = self.bypass_rls {
      query.push_str(if opt { " BYPASSRLS" } else { " NOBYPASSRLS" });
    }
    if let Some(mc) = self.max_connections {
      write!(query, " CONNECTION LIMIT {}", mc).unwrap();
    }
    query.push(';');

    let res = sqlx::query(&query).execute(executor).await;

    match res {
      Ok(res) => {
        debug_assert_eq!(res.rows_affected(), 0);
        Ok(())
      }
      Err(sqlx::Error::Database(e)) => match e.try_downcast::<PgDatabaseError>() {
        Ok(e) => match e.code() {
          "42710" => Err(CreateRoleError::AlreadyExists),
          _ => Err(CreateRoleError::Database(Box::new(PgError::from_database_error(*e)))),
        },
        Err(e) => Err(CreateRoleError::Other(e.to_string())),
      },
      Err(e) => Err(CreateRoleError::Other(e.to_string())),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, Error)]
pub enum AlterRoleError {
  #[error("The role was not found.")]
  NotFound,
  #[error("unexpected database error: {0:?}")]
  Database(Box<PgError>),
  #[error("unexpected error: {0}")]
  Other(String),
}

pub struct AlterRoleQuery<'q> {
  name: &'q str,
  is_superuser: Option<bool>,
  can_create_db: Option<bool>,
  can_create_role: Option<bool>,
  inherit_privileges: Option<bool>,
  can_login: Option<bool>,
  is_replication: Option<bool>,
  bypass_rls: Option<bool>,
  // Invariant: `max_connection >= -1`
  max_connections: Option<i32>,
  password: Option<Option<String>>,
}

impl<'q> AlterRoleQuery<'q> {
  pub fn new(name: &'q str) -> Self {
    Self {
      name,
      is_superuser: None,
      can_create_db: None,
      can_create_role: None,
      inherit_privileges: None,
      can_login: None,
      is_replication: None,
      bypass_rls: None,
      max_connections: None,
      password: None,
    }
  }

  pub fn superuser(mut self, is_superuser: bool) -> Self {
    self.is_superuser = Some(is_superuser);
    self
  }

  pub fn can_login(mut self, can_login: bool) -> Self {
    self.can_login = Some(can_login);
    self
  }

  pub fn no_password(mut self) -> Self {
    self.password = Some(None);
    self
  }

  pub fn raw_password(mut self, pass: &str) -> Self {
    self.password = Some(Some(pass.to_string()));
    self
  }

  pub fn md5_password(mut self, pass: &str) -> Self {
    let mut hasher = Md5::new();
    let input = format!("{}{}", pass, self.name);
    hasher.update(&input);
    let digest = hasher.finalize();
    self.password = Some(Some(format!("md5{:x}", digest)));
    self
  }

  pub fn scram_sha256_password(self, _pass: &str) -> Self {
    todo!()
  }

  // See <https://doxygen.postgresql.org/createuser_8c_source.html>
  pub async fn execute(self, executor: impl sqlx::Executor<'_, Database = Postgres>) -> Result<(), AlterRoleError> {
    let mut query: String = format!("ALTER ROLE {}", PgSqlQuotedId(self.name));
    if let Some(pass) = self.password {
      query.push_str(" PASSWORD ");
      if let Some(raw) = pass {
        write!(query, "{}", PgSqlStrLit(&raw)).unwrap();
      } else {
        query.push_str("NULL");
      }
    }
    if let Some(opt) = self.is_superuser {
      query.push_str(if opt { " SUPERUSER" } else { " NOSUPERUSER" });
    }
    if let Some(opt) = self.can_create_db {
      query.push_str(if opt { " CREATEDB" } else { " NOCREATEDB" });
    }
    if let Some(opt) = self.can_create_role {
      query.push_str(if opt { " CREATEROLE" } else { " NOCREATEROLE" });
    }
    if let Some(opt) = self.inherit_privileges {
      query.push_str(if opt { " INHERIT" } else { " NOINHERIT" });
    }
    if let Some(opt) = self.can_login {
      query.push_str(if opt { " LOGIN" } else { " NOLOGIN" });
    }
    if let Some(opt) = self.is_replication {
      query.push_str(if opt { " REPLICATION" } else { " NOREPLICATION" });
    }
    if let Some(opt) = self.bypass_rls {
      query.push_str(if opt { " BYPASSRLS" } else { " NOBYPASSRLS" });
    }
    if let Some(mc) = self.max_connections {
      write!(query, " CONNECTION LIMIT {}", mc).unwrap();
    }
    query.push(';');

    let res = sqlx::query(&query).execute(executor).await;

    match res {
      Ok(res) => {
        debug_assert_eq!(res.rows_affected(), 0);
        Ok(())
      }
      Err(sqlx::Error::Database(e)) => match e.try_downcast::<PgDatabaseError>() {
        Ok(e) => match e.code() {
          "42704" => Err(AlterRoleError::NotFound),
          _ => Err(AlterRoleError::Database(Box::new(PgError::from_database_error(*e)))),
        },
        Err(e) => Err(AlterRoleError::Other(e.to_string())),
      },
      Err(e) => Err(AlterRoleError::Other(e.to_string())),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, Error)]
pub enum CreateDatabaseError {
  #[error("the database already exists.")]
  AlreadyExists,
  #[error("cannot run database creation inside a transaction block.")]
  InTransaction,
  #[error("unexpected database error: {0:?}")]
  Database(Box<PgError>),
  #[error("unexpected error: {0}")]
  Other(String),
}

/// <https://www.postgresql.org/docs/current/sql-createdatabase.html>
#[derive(Debug, Copy, Clone)]
pub struct CreateDatabaseQuery<'q> {
  name: &'q str,
  owner: Option<&'q str>,
  template: Option<&'q str>,
  encoding: Option<&'q str>,
  lc_collate: Option<&'q str>,
  lc_ctype: Option<&'q str>,
  tablespace: Option<&'q str>,
  allow_connections: Option<bool>,
  max_connections: Option<i32>,
  is_template: Option<bool>,
}

impl<'q> CreateDatabaseQuery<'q> {
  pub fn new(name: &'q str) -> Self {
    Self {
      name,
      owner: None,
      template: None,
      encoding: None,
      lc_collate: None,
      lc_ctype: None,
      tablespace: None,
      allow_connections: None,
      max_connections: None,
      is_template: None,
    }
  }

  pub fn owner(mut self, owner: &'q str) -> Self {
    self.owner = Some(owner);
    self
  }
  pub fn template(mut self, template: &'q str) -> Self {
    self.template = Some(template);
    self
  }
  pub fn encoding(mut self, encoding: &'q str) -> Self {
    self.encoding = Some(encoding);
    self
  }
  pub fn lc_collate(mut self, lc_collate: &'q str) -> Self {
    self.lc_collate = Some(lc_collate);
    self
  }
  pub fn lc_ctype(mut self, lc_ctype: &'q str) -> Self {
    self.lc_ctype = Some(lc_ctype);
    self
  }
  pub fn locale(mut self, locale: &'q str) -> Self {
    self.lc_collate = Some(locale);
    self.lc_ctype = Some(locale);
    self
  }
  pub fn tablespace(mut self, tablespace: &'q str) -> Self {
    self.tablespace = Some(tablespace);
    self
  }
  pub fn allow_connections(mut self, allow: bool) -> Self {
    self.allow_connections = Some(allow);
    self
  }
  pub fn max_connections(mut self, max_connections: Option<u32>) -> Self {
    let limit = max_connections.map(|l| i32::try_from(l).unwrap()).unwrap_or(-1);
    self.max_connections = Some(limit);
    self
  }
  pub fn is_template(mut self, is_template: bool) -> Self {
    self.is_template = Some(is_template);
    self
  }

  // See <https://www.postgresql.org/docs/current/sql-createdatabase.html>
  pub async fn execute(
    self,
    executor: impl sqlx::Executor<'_, Database = Postgres>,
  ) -> Result<(), CreateDatabaseError> {
    let mut query: String = format!("CREATE DATABASE {}", PgSqlQuotedId(self.name));
    if let Some(owner) = self.owner {
      query.push_str(" OWNER ");
      query.push_str(&PgSqlQuotedId(owner).to_string());
    }
    if let Some(template) = self.template {
      query.push_str(" TEMPLATE ");
      query.push_str(&PgSqlQuotedId(template).to_string());
    }
    if let Some(encoding) = self.encoding {
      query.push_str(" ENCODING ");
      query.push_str(&PgSqlStrLit(encoding).to_string());
    }
    if let Some(lc_collate) = self.lc_collate {
      query.push_str(" LC_COLLATE ");
      query.push_str(&PgSqlStrLit(lc_collate).to_string());
    }
    if let Some(lc_ctype) = self.lc_ctype {
      query.push_str(" LC_CTYPE ");
      query.push_str(&PgSqlStrLit(lc_ctype).to_string());
    }
    if let Some(tablespace) = self.tablespace {
      query.push_str(" TABLESPACE ");
      query.push_str(&PgSqlQuotedId(tablespace).to_string());
    }
    if self.allow_connections.is_some() {
      todo!("Support allow_connections");
    }
    if let Some(max_connections) = self.max_connections {
      query.push_str(" CONNECTION LIMIT ");
      query.push_str(&max_connections.to_string());
    }
    if self.is_template.is_some() {
      todo!("Support is_template");
    }
    query.push(';');

    let res = sqlx::query(&query).execute(executor).await;

    match res {
      Ok(res) => {
        debug_assert_eq!(res.rows_affected(), 0);
        Ok(())
      }
      Err(sqlx::Error::Database(e)) => match e.try_downcast::<PgDatabaseError>() {
        Ok(e) => match e.code() {
          "25001" => Err(CreateDatabaseError::InTransaction),
          "42P04" => Err(CreateDatabaseError::AlreadyExists),
          _ => Err(CreateDatabaseError::Database(Box::new(PgError::from_database_error(
            *e,
          )))),
        },
        Err(e) => Err(CreateDatabaseError::Other(e.to_string())),
      },
      Err(e) => Err(CreateDatabaseError::Other(e.to_string())),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, Error)]
pub enum DropDatabaseError {
  #[error("unexpected database error: {0:?}")]
  Database(Box<PgError>),
  #[error("unexpected error: {0}")]
  Other(String),
}

/// <https://www.postgresql.org/docs/current/sql-dropdatabase.html>
#[derive(Debug, Copy, Clone)]
pub struct DropDatabaseQuery<'q> {
  name: &'q str,
  if_exists: bool,
  force: bool,
}

impl<'q> DropDatabaseQuery<'q> {
  pub fn new(name: &'q str) -> Self {
    Self {
      name,
      if_exists: false,
      force: false,
    }
  }

  pub fn if_exists(mut self, if_exists: bool) -> Self {
    self.if_exists = if_exists;
    self
  }

  pub fn force(mut self, force: bool) -> Self {
    self.force = force;
    self
  }

  pub async fn execute(self, executor: impl sqlx::Executor<'_, Database = Postgres>) -> Result<(), DropDatabaseError> {
    let if_exists = if self.if_exists { " IF EXISTS" } else { "" };
    let mut query: String = format!("DROP DATABASE{} {}", if_exists, PgSqlQuotedId(self.name));
    if self.force {
      query.push_str(" WITH (FORCE)");
    }
    query.push(';');

    let res = sqlx::query(&query).execute(executor).await;

    match res {
      Ok(_) => Ok(()),
      Err(e) => Err(DropDatabaseError::Other(e.to_string())),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, Error)]
pub enum AlterDatabaseOwnerError {
  #[error("The database was not found.")]
  DbNotFound,
  #[error("The owner role was not found.")]
  RoleNotFound,
  #[error("unexpected database error: {0:?}")]
  Database(Box<PgError>),
  #[error("unexpected error: {0}")]
  Other(String),
}

pub struct AlterDatabase<'q> {
  name: &'q str,
}

impl<'q> AlterDatabase<'q> {
  pub fn new(name: &'q str) -> Self {
    Self { name }
  }

  pub fn owner(self, owner: &'q str) -> AlterDatabaseOwner<'q> {
    AlterDatabaseOwner { name: self.name, owner }
  }
}

pub struct AlterDatabaseOwner<'q> {
  name: &'q str,
  owner: &'q str,
}

impl<'q> AlterDatabaseOwner<'q> {
  // See <https://www.postgresql.org/docs/current/sql-alterdatabase.html>
  pub async fn execute(
    self,
    executor: impl sqlx::Executor<'_, Database = Postgres>,
  ) -> Result<(), AlterDatabaseOwnerError> {
    let query: String = format!(
      "ALTER DATABASE {name} OWNER TO {new_owner};",
      name = PgSqlQuotedId(self.name),
      new_owner = PgSqlQuotedId(self.owner)
    );

    let res = sqlx::query(&query).execute(executor).await;

    match res {
      Ok(res) => {
        debug_assert_eq!(res.rows_affected(), 0);
        Ok(())
      }
      Err(sqlx::Error::Database(e)) => match e.try_downcast::<PgDatabaseError>() {
        Ok(e) => match e.code() {
          "3D000" => Err(AlterDatabaseOwnerError::DbNotFound),
          "42704" => Err(AlterDatabaseOwnerError::RoleNotFound),
          _ => Err(AlterDatabaseOwnerError::Database(Box::new(
            PgError::from_database_error(*e),
          ))),
        },
        Err(e) => Err(AlterDatabaseOwnerError::Other(e.to_string())),
      },
      Err(e) => Err(AlterDatabaseOwnerError::Other(e.to_string())),
    }
  }
}

/// A wrapper to quote identifiers
///
/// Based on [`fmtId`](https://doxygen.postgresql.org/string__utils_8c.html#a674e056d35d907d91da10d2bf96d411e).
#[derive(Copy, Clone, Debug)]
pub struct PgSqlQuotedId<'a>(pub &'a str);

impl<'a> fmt::Display for PgSqlQuotedId<'a> {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    f.write_char('"')?;
    for c in self.0.chars() {
      if c == '"' {
        f.write_str("\"\"")?;
      } else {
        f.write_char(c)?;
      }
    }
    f.write_char('"')?;
    Ok(())
  }
}

/// A wrapper to write string literals
///
/// Based on [`fmtId`](https://doxygen.postgresql.org/string__utils_8c.html#a674e056d35d907d91da10d2bf96d411e).
#[derive(Copy, Clone, Debug)]
pub struct PgSqlStrLit<'a>(pub &'a str);

impl<'a> fmt::Display for PgSqlStrLit<'a> {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    f.write_char('\'')?;
    for c in self.0.chars() {
      if c == '\'' {
        f.write_str("\'\'")?;
      } else {
        f.write_char(c)?;
      }
    }
    f.write_char('\'')?;
    Ok(())
  }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct PgRoleName<S: AsRef<str> = String>(S);

impl<S: AsRef<str>> PgRoleName<S> {
  pub fn new(value: S) -> Self {
    Self::try_new(value).expect("InvalidRoleNameValue")
  }

  pub fn try_new(value: S) -> Option<Self> {
    if value.as_ref().is_empty() {
      return None;
    }
    Some(Self(value))
  }

  pub fn as_str(&self) -> &str {
    self.0.as_ref()
  }

  pub fn as_ref(&self) -> PgRoleName<&str> {
    PgRoleName(self.as_str())
  }

  pub fn to_owned(&self) -> PgRoleName<String> {
    PgRoleName(self.as_str().to_owned())
  }

  pub fn inner(self) -> S {
    self.0
  }
}

impl<S: AsRef<str>> fmt::Display for PgRoleName<S> {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    self.0.as_ref().fmt(f)
  }
}

impl<S: AsRef<str>> AsRef<str> for PgRoleName<S> {
  fn as_ref(&self) -> &str {
    self.as_str()
  }
}

pub struct AclItem<S: AsRef<str> = String> {
  /// Who is the recipient of the privileges
  /// None: Public
  /// Some: Specific Role
  pub grantee: Option<PgRoleName<S>>,
  /// Who granted the privileges
  pub grantor: PgRoleName<S>,
  pub privileges: PrivilegeMap,
}

pub struct PrivilegeMap {
  pub select: PgPrivilegeState,
  pub insert: PgPrivilegeState,
  pub update: PgPrivilegeState,
  pub delete: PgPrivilegeState,
  pub truncate: PgPrivilegeState,
  pub references: PgPrivilegeState,
  pub trigger: PgPrivilegeState,
  pub create: PgPrivilegeState,
  pub connect: PgPrivilegeState,
  pub temporary: PgPrivilegeState,
  pub execute: PgPrivilegeState,
  pub usage: PgPrivilegeState,
}

#[derive(Copy, Clone, Debug, Ord, PartialOrd, Eq, PartialEq, Serialize, Deserialize)]
pub enum PgPrivilegeState {
  Default,
  /// The boolean indicates if the recipient of this privilege may grant it
  /// further to other roles.
  Granted(bool),
}

impl Default for PgPrivilegeState {
  fn default() -> Self {
    Self::Default
  }
}

pub enum RoleSpec<S: AsRef<str> = String> {
  Role(PgRoleName<S>),
  Public,
}

pub enum TableSpec<S: AsRef<str> = String> {
  Table(S),
  AllInSchema(S),
}

pub enum SequenceSpec<S: AsRef<str> = String> {
  Sequence(S),
  AllInSchema(S),
}
