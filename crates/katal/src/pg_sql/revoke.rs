use crate::pg_sql::{PgRoleName, PgSqlQuotedId, RoleSpec};
use core::fmt;
use vec1::Vec1;

#[derive(Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq)]
pub enum OnDependentGrant {
  Cascade,
  Restrict,
}

pub struct Revoke;

impl Revoke {
  pub fn from<S: AsRef<str>>(role_spec: RoleSpec<S>) -> RevokFromRoles<S> {
    RevokFromRoles {
      roles: Vec1::new(role_spec),
      on_dependent_grant: OnDependentGrant::Restrict,
      grant_option: false,
    }
  }

  pub fn from_role<S: AsRef<str>>(role: PgRoleName<S>) -> RevokFromRoles<S> {
    Self::from(RoleSpec::Role(role))
  }

  pub fn from_public<S: AsRef<str>>() -> RevokFromRoles<S> {
    Self::from(RoleSpec::Public)
  }
}

pub struct RevokFromRoles<S: AsRef<str> = String> {
  roles: Vec1<RoleSpec<S>>,
  on_dependent_grant: OnDependentGrant,
  /// Only remove the grant option
  grant_option: bool,
}

impl<S: AsRef<str>> RevokFromRoles<S> {
  pub fn from(mut self, role_spec: RoleSpec<S>) -> Self {
    self.roles.push(role_spec);
    self
  }

  pub fn from_role(self, role: PgRoleName<S>) -> Self {
    self.from(RoleSpec::Role(role))
  }

  pub fn from_public(self) -> Self {
    self.from(RoleSpec::Public)
  }

  pub fn grant_option(mut self) -> Self {
    self.grant_option = true;
    self
  }

  pub fn on_dependent_cascade(mut self) -> Self {
    self.on_dependent_grant = OnDependentGrant::Cascade;
    self
  }

  pub fn on_schema(self, schema: S) -> RevokeOnSchema<S, false> {
    RevokeOnSchema {
      roles: self.roles,
      on_dependent_grant: self.on_dependent_grant,
      grant_option: self.grant_option,
      schema,
      create: false,
      usage: false,
    }
  }
}

pub struct RevokeOnSchema<S: AsRef<str>, const HAS_GRANTS: bool> {
  roles: Vec1<RoleSpec<S>>,
  on_dependent_grant: OnDependentGrant,
  grant_option: bool,
  schema: S,
  create: bool,
  usage: bool,
}

impl<S: AsRef<str>, const HAS_GRANTS: bool> RevokeOnSchema<S, HAS_GRANTS> {
  pub fn create(self) -> RevokeOnSchema<S, true> {
    RevokeOnSchema {
      roles: self.roles,
      on_dependent_grant: self.on_dependent_grant,
      grant_option: self.grant_option,
      schema: self.schema,
      create: true,
      usage: self.usage,
    }
  }

  pub fn usage(self) -> RevokeOnSchema<S, true> {
    RevokeOnSchema {
      roles: self.roles,
      on_dependent_grant: self.on_dependent_grant,
      grant_option: self.grant_option,
      schema: self.schema,
      create: self.create,
      usage: true,
    }
  }

  pub fn all(self) -> RevokeOnSchema<S, true> {
    RevokeOnSchema {
      roles: self.roles,
      on_dependent_grant: self.on_dependent_grant,
      grant_option: self.grant_option,
      schema: self.schema,
      create: true,
      usage: true,
    }
  }
}

impl<S: AsRef<str>> fmt::Display for RevokeOnSchema<S, true> {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    let mut query: String = "REVOKE".to_string();
    if self.grant_option {
      query.push_str(" GRANT OPTION FOR");
    }

    let mut first = true;
    if self.create {
      first = false;
      query.push_str(" CREATE");
    }
    if self.usage {
      if !first {
        query.push(',');
      }
      query.push_str(" USAGE");
    }
    query.push_str(" ON SCHEMA ");
    query.push_str(&PgSqlQuotedId(self.schema.as_ref()).to_string());
    query.push_str(" FROM ");
    let mut first = true;
    for role in self.roles.iter() {
      if first {
        first = false;
      } else {
        query.push(',');
      }
      match role {
        RoleSpec::Role(r) => query.push_str(&PgSqlQuotedId(r.as_str()).to_string()),
        RoleSpec::Public => query.push_str("PUBLIC"),
      }
    }
    match self.on_dependent_grant {
      OnDependentGrant::Cascade => query.push_str(" CASCADE"),
      OnDependentGrant::Restrict => query.push_str(" RESTRICT"),
    }
    query.push(';');
    f.write_str(&query)
  }
}
