use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct DiscordWebhook<S = String>
where
  S: AsRef<str>,
{
  pub id: S,
  pub token: S,
}

impl<S> DiscordWebhook<S>
where
  S: AsRef<str>,
{
  pub fn to_owned(&self) -> DiscordWebhook<String> {
    DiscordWebhook {
      id: self.id.as_ref().to_string(),
      token: self.id.as_ref().to_string(),
    }
  }
}
