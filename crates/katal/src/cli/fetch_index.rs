use crate::task::postgres::PostgresIndex;
use clap::Parser;
use std::path::PathBuf;
use tokio::fs;

/// Arguments to the `fetch-index` command.
#[derive(Debug, Parser)]
pub struct Args {}

pub async fn run(_args: &Args) -> Result<(), anyhow::Error> {
  let crate_root = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
  let pg_index = PostgresIndex::from_ftp().await?;
  let pg_index_path = crate_root.join("./files/pg/index.json");
  fs::write(
    pg_index_path,
    serde_json::to_string_pretty(&pg_index).expect("failed to serialize pg index"),
  )
  .await
  .expect("failed to serialize pg index");
  Ok(())
}
