use crate::etwin::auto::{BuildAutoRelease, DeployAutoAsRoot, UpgradeAutoDatabase};
use crate::etwin::brute::{BuildBruteRelease, DeployBruteAsRoot, UpgradeBruteDatabase};
use crate::etwin::dinorpg::{BuildDinorpgRelease, DeployDinorpgAsRoot, UpgradeDinorpgDatabase};
use crate::etwin::emush::{BuildEmushRelease, DeployEmushAsRoot, UpgradeEmushDatabase};
use crate::etwin::epopotamo::{BuildEpopotamoRelease, DeployEpopotamoAsRoot, UpgradeEpopotamoDatabase};
use crate::etwin::eternalfest::{BuildEternalfestRelease, DeployEternalfestAsRoot, UpgradeEternalfestDatabase};
use crate::etwin::etwin::{BuildEtwinRelease, DeployEternaltwinAsRoot, UpgradeEtwinDatabase};
use crate::etwin::frutibandas::{BuildFrutibandasRelease, DeployFrutibandasAsRoot};
use crate::etwin::kingdom::{BuildKingdomRelease, DeployKingdomAsRoot, UpgradeKingdomDatabase};
use crate::etwin::neoparc::{BuildNeoparcRelease, DeployNeoparcAsRoot, UpgradeNeoparcDatabase};
use crate::etwin::system::SyncSystemAsRoot;
use crate::job_runner::opaque_data::BincodeBuf;
use crate::job_runner::{
  JobClientMessage, JobClientReadError, JobClientReader, JobClientWriter, StdinJobClientReader, StdoutJobClientWriter,
};
use crate::task::borg::{CreateBorgArchive, InitBorgRepo};
use crate::task::fused::{RemoteStateStore, StateAndHost};
use crate::task::git::GitCheckout;
use crate::task::nvm::NvmCommand;
use crate::task::php::WantedPhpExtensionBuild;
use crate::task::postgres::{PostgresBuild, PostgresClusterCommand};
use crate::task::rust::RustupCommand;
use crate::task::uptrace::{BuildUptraceRelease, DeployUptraceAsRoot};
use crate::task::TaskRunnerRegistry;
use asys::local_linux::LocalLinux;
use clap::Parser;
use serde::{Deserialize, Serialize};
use thiserror::Error;

/// Arguments to the `stdio-agent` command.
#[derive(Debug, Parser)]
pub struct StdioAgentArgs {}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize, Error)]
pub enum StdioAgentError {
  #[error("stdio agent failed to read next message")]
  ClientRead(#[from] JobClientReadError),
  #[error("missing job start message")]
  MissingJobStart,
  #[error("received job start for unknown task {0:?}")]
  UnknownTask(String),
}

pub async fn run(args: &StdioAgentArgs) -> Result<(), anyhow::Error> {
  let pid = std::process::id();
  eprintln!("StdioAgent({}): start", pid);
  match inner(args).await {
    Ok(()) => {
      eprintln!("StdioAgent({}): success", pid);
      Ok(())
    }
    Err(e) => {
      eprintln!("StdioAgent({}): error {:?}", pid, &e);
      Err(e.into())
    }
  }
}

pub async fn inner(_args: &StdioAgentArgs) -> Result<(), StdioAgentError> {
  let mut rx = StdinJobClientReader::new();
  let tx = StdoutJobClientWriter::new();

  let job = rx.next_client_message().await?;
  let job = match job {
    Some(JobClientMessage::RequestStart(job)) => job,
    _ => return Err(StdioAgentError::MissingJobStart),
  };

  let registry = TaskRunnerRegistry::new();

  let host = LocalLinux;
  let host = StateAndHost::new(host, RemoteStateStore::new(rx, tx, registry));

  let registry: TaskRunnerRegistry<BincodeBuf, &'_ StateAndHost<LocalLinux, RemoteStateStore>, BincodeBuf> = {
    let mut r = TaskRunnerRegistry::new();
    r.register::<GitCheckout>();
    r.register::<DeployAutoAsRoot>();
    r.register::<BuildAutoRelease>();
    r.register::<UpgradeAutoDatabase>();
    r.register::<DeployBruteAsRoot>();
    r.register::<BuildBruteRelease>();
    r.register::<UpgradeBruteDatabase>();
    r.register::<DeployDinorpgAsRoot>();
    r.register::<BuildDinorpgRelease>();
    r.register::<UpgradeDinorpgDatabase>();
    r.register::<DeployEternalfestAsRoot>();
    r.register::<BuildEternalfestRelease>();
    r.register::<UpgradeEternalfestDatabase>();
    r.register::<DeployEpopotamoAsRoot>();
    r.register::<BuildEpopotamoRelease>();
    r.register::<UpgradeEpopotamoDatabase>();
    r.register::<DeployFrutibandasAsRoot>();
    r.register::<BuildFrutibandasRelease>();
    r.register::<DeployEternaltwinAsRoot>();
    r.register::<PostgresBuild>();
    r.register::<PostgresClusterCommand>();
    r.register::<BuildEtwinRelease>();
    r.register::<NvmCommand>();
    r.register::<RustupCommand>();
    r.register::<UpgradeEtwinDatabase>();
    r.register::<InitBorgRepo>();
    r.register::<CreateBorgArchive>();
    r.register::<DeployEmushAsRoot>();
    r.register::<BuildEmushRelease>();
    r.register::<UpgradeEmushDatabase>();
    r.register::<DeployKingdomAsRoot>();
    r.register::<BuildKingdomRelease>();
    r.register::<UpgradeKingdomDatabase>();
    r.register::<DeployNeoparcAsRoot>();
    r.register::<BuildNeoparcRelease>();
    r.register::<UpgradeNeoparcDatabase>();
    r.register::<DeployUptraceAsRoot>();
    r.register::<BuildUptraceRelease>();
    r.register::<SyncSystemAsRoot>();
    r.register::<WantedPhpExtensionBuild>();
    r
  };

  let runner = match registry.get(job.task.as_str()) {
    Some(r) => r,
    None => return Err(StdioAgentError::UnknownTask(job.task)),
  };

  let out = runner.erased_run(&job.input, &host).await;

  drop(registry);

  let (_rx, mut tx) = host.state.into_parts();

  tx.job_complete(job.job_id, out).await;

  Ok(())
}
