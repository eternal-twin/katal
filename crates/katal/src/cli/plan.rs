use crate::config::{find_new_config, FindConfigError, KatalConfig, LoaderConfig, RawConfig};
use crate::etwin::system::SyncSystem;
use crate::task::fused::StateAndHost;
use asys::local_linux::LocalLinux;
use clap::Parser;
use katal_loader::dir::DirLoader;
use katal_loader::git::GitLoader;
use katal_loader::resolve_vault;
use katal_state::sqlite::SqliteStore;

/// Arguments to the `plan` command.
#[derive(Debug, Parser)]
pub struct PlanArgs {}

pub async fn run(_args: &PlanArgs) -> Result<(), anyhow::Error> {
  let path = std::env::current_dir().unwrap();
  // let old_config = find_config(path.clone()).expect("old config not found");
  let config: KatalConfig = match find_new_config(path) {
    Ok(config) => config,
    Err(e @ FindConfigError::NotFound(_)) => {
      panic!("config file not found: {e}");
    }
    Err(e) => return Err(anyhow::Error::from(e)),
  };

  let raw_config = match config.config {
    Some(raw_config) => raw_config,
    None => {
      let target_public = match &config.loader {
        LoaderConfig::Git(loader) => {
          let target_loader = GitLoader::new(loader.repo.clone(), loader.dir.clone(), loader.r#ref.clone());
          target_loader.load().await.expect("failed to load config from Git repo")
        }
        LoaderConfig::Dir(loader) => {
          let target_loader = DirLoader::new(loader.dir.clone());
          target_loader
            .load()
            .await
            .expect("failed to load config from local directory")
        }
      };

      eprintln!("{}\n", serde_json::to_string_pretty(&target_public).unwrap());

      // let password = Password::new().with_prompt("Vault super-key?").interact()?;
      let password = config.daemon.key;

      let target_secret = resolve_vault(password.as_bytes(), target_public).expect("failed to resolve vault");
      // eprintln!("{}\n", serde_json::to_string_pretty(&target_secret).unwrap());

      let raw_config: RawConfig = serde_json::from_value(target_secret).expect("malformed config");
      raw_config
    }
  };

  let task = SyncSystem { config: raw_config };

  let state = SqliteStore::new(config.state.path, false).await;
  let _host = StateAndHost::new(LocalLinux, state);

  dbg!(task);

  eprintln!("aborting (use `sync` to really apply changes)");

  Ok(())
}
