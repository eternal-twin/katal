// use std::ffi::CString;
// use libc::uid_t;
//
// #[derive(Copy, Clone, Debug, Ord, PartialOrd, Eq, PartialEq)]
// pub struct UserId(uid_t);
//
// impl UserId {
//   pub const fn from_raw(uid: uid_t) -> Self {
//     Self(uid)
//   }
//
//   pub const fn to_raw(&self) -> uid_t {
//     self.0
//   }
// }
//
// impl From<UserId> for uid_t {
//   fn from(uid: UserId) -> Self {
//     uid.to_raw()
//   }
// }
//
// impl From<uid_t> for UserId {
//   fn from(uid: uid_t) -> Self {
//     Self::from_raw(uid)
//   }
// }
//
// struct UserName(CString);
//
// struct User {
//   uid: UserId,
// }
//
// fn get_by_uid(uid: UserId) -> Option<User> {
//   from_uid();
//
//   unsafe {
//     let pwd: *mut libc::passwd;
//     libc::getpwuid_r(uid.to_raw(), );
//   }
//   None
// }
