use crate::config::GitLoaderConfig;

#[allow(unused)]
pub struct DaemonConfig {
  key: String,
  target: GitLoaderConfig,
}
