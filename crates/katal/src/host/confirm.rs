use crate::host::{Command, ExecHost, Host, NixHost};
use dialoguer::{theme::ColorfulTheme, Confirm};

pub struct ConfirmHost<H: Host> {
  inner: H,
}

impl<H: Host> ConfirmHost<H> {
  pub fn new(inner: H) -> Self {
    Self { inner }
  }
}

impl<H: Host> Host for ConfirmHost<H> {}

impl<H: ExecHost> ExecHost for ConfirmHost<H> {
  fn try_exec(&self, command: &Command) -> Result<std::process::Output, anyhow::Error> {
    if command.may_mutate_host {
      let mut prompt = String::from("The following command may mutate the host. Do you want to run it?\n");
      prompt += command.get_program();
      prompt.push('\n');
      for arg in command.get_args() {
        prompt.push_str("  ");
        prompt.push_str(arg);
        prompt.push('\n');
      }

      let proceed = Confirm::with_theme(&ColorfulTheme::default())
        .with_prompt(&prompt)
        .interact()
        .unwrap();

      if !proceed {
        panic!("Abort: rejected mutating command");
      }
    }
    self.inner.exec(command)
  }
}

impl<H: NixHost> NixHost for ConfirmHost<H> {}
