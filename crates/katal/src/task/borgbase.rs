use chrono::{DateTime, Utc};
use reqwest::Client;
use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};
use std::convert::identity;
use std::time::Duration;

const USER_AGENT: &str = "Katal";
const TIMEOUT: Duration = Duration::from_millis(5000);

pub struct HttpBorgbaseClient {
  client: Client,
}

pub struct BoundHttpBorgbaseClient {
  client: HttpBorgbaseClient,
  auth_token: String,
}

#[derive(Debug, Clone, Eq, PartialEq, PartialOrd, Ord, Serialize, Deserialize)]
pub struct GraphqlId(String);

impl GraphqlId {
  pub fn inner(self) -> String {
    self.0
  }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RepoType {
  pub id: String,
  pub name: String,
  pub server: Option<ServerType>,
  pub quota: i32,
  pub quota_enabled: bool,
  pub alert_days: i32,
  pub region: String,
  pub borg_version: RepoBorgVersion,
  pub restic_version: String,
  pub htpasswd: Option<String>,
  pub append_only: bool,
  pub append_only_keys: Option<Vec<String>>,
  pub full_access_keys: Option<Vec<String>>,
  pub rsync_keys: Option<Vec<String>>,
  pub access_mode: String,
  pub encryption: String,
  pub created_at: Option<DateTime<Utc>>,
  pub last_modified: Option<DateTime<Utc>>,
  pub compaction_enabled: bool,
  pub compaction_interval: i32,
  pub compaction_interval_unit: String,
  pub compaction_hour: i32,
  pub compaction_hour_timezone: String,
  pub repo_path: Option<String>,
  /// Current usage
  ///
  /// Range: from 0 to 100 (it's a percentage)
  pub current_usage: Option<f64>,
}

impl PartialEq for RepoType {
  fn eq(&self, other: &Self) -> bool {
    [
      self.id == other.id,
      self.name == other.name,
      self.server == other.server,
      self.quota == other.quota,
      self.quota_enabled == other.quota_enabled,
      self.alert_days == other.alert_days,
      self.region == other.region,
      self.borg_version == other.borg_version,
      self.restic_version == other.restic_version,
      self.htpasswd == other.htpasswd,
      self.append_only == other.append_only,
      self.append_only_keys == other.append_only_keys,
      self.full_access_keys == other.full_access_keys,
      self.rsync_keys == other.rsync_keys,
      self.access_mode == other.access_mode,
      self.encryption == other.encryption,
      self.created_at == other.created_at,
      self.last_modified == other.last_modified,
      self.compaction_enabled == other.compaction_enabled,
      self.compaction_interval == other.compaction_interval,
      self.compaction_interval_unit == other.compaction_interval_unit,
      self.compaction_hour == other.compaction_hour,
      self.compaction_hour_timezone == other.compaction_hour_timezone,
      self.current_usage.map(|x| x.to_ne_bytes()) == other.current_usage.map(|x| x.to_ne_bytes()),
    ]
    .into_iter()
    .all(identity)
  }
}

impl Eq for RepoType {}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ServerType {
  pub id: GraphqlId,
  pub hostname: String,
  pub region: String,
  pub public: bool,
  pub fingerprint_rsa: String,
  pub fingerprint_ecdsa: String,
  pub fingerprint_ed25519: String,
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub enum RepoBorgVersion {
  #[serde(rename = "LATEST")]
  Latest,
  #[serde(rename = "V_1_1_X")]
  V1_1,
  #[serde(rename = "V_1_2_X")]
  V1_2,
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RepoListArgs {
  pub name: Option<String>,
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RepoAddArgs {
  pub name: String,
  pub region: String,
  pub borg_version: RepoBorgVersion,
  pub full_access_keys: Option<Vec<String>>,
  pub append_only_keys: Option<Vec<String>>,
  pub rsync_keys: Option<Vec<String>>,
  pub quota: i32,
  pub quota_enabled: bool,
  pub alert_days: i32,
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RepoAdd {
  repo_added: RepoType,
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RepoEditArgs {
  pub id: String,
  pub name: String,
  pub region: String,
  pub borg_version: RepoBorgVersion,
  pub full_access_keys: Option<Vec<String>>,
  pub append_only_keys: Option<Vec<String>>,
  pub rsync_keys: Option<Vec<String>>,
  pub quota: i32,
  pub quota_enabled: bool,
  pub alert_days: i32,
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RepoEdit {
  repo_edited: RepoType,
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PublicSshKey {
  pub id: GraphqlId,
  pub name: String,
  pub key_data: String,
  pub key_type: String,
  pub bits: i32,
  pub comment: Option<String>,
  pub hash_md5: String,
  pub hash_sha256: String,
  pub added_at: Option<DateTime<Utc>>,
  pub last_used_at: Option<DateTime<Utc>>,
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SshAddArgs {
  pub name: String,
  pub key_data: String,
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SshAdd {
  key_added: PublicSshKey,
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SshDeleteArgs {
  pub id: GraphqlId,
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SshDelete {
  ok: bool,
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct GetSshKeysResponse {
  ssh_list: Vec<PublicSshKey>,
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct GetReposResponse {
  repo_list: Vec<RepoType>,
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AddSshKeyResponse {
  ssh_add: SshAdd,
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AddRepoResponse {
  repo_add: RepoAdd,
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct EditRepoResponse {
  repo_edit: RepoEdit,
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct DeleteSshKeyResponse {
  ssh_delete: SshDelete,
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
struct GraphqlQuery {
  query: String,
}

impl GraphqlQuery {
  pub fn new(query: impl ToString) -> Self {
    Self {
      query: query.to_string(),
    }
  }
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
struct ParametrizedGraphqlQuery<Var> {
  query: String,
  variables: Var,
}

impl<Var> ParametrizedGraphqlQuery<Var> {
  pub fn new(query: impl ToString, variables: Var) -> Self {
    Self {
      query: query.to_string(),
      variables,
    }
  }
}

/// There is no support for partial failure: either there are errors or all
/// queries succeed
#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
#[serde(untagged)]
enum GraphqlResponse<T> {
  Err(GraphqlResponseErr),
  Ok(GraphqlResponseOk<T>),
}

impl<T> GraphqlResponse<T> {
  pub fn unwrap(self) -> GraphqlResponseOk<T> {
    match self {
      GraphqlResponse::Ok(ok) => ok,
      GraphqlResponse::Err(err) => {
        panic!("Unwrap with Graphql errors: {:?}", err);
      }
    }
  }
}

/// Response without any single error
#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct GraphqlResponseOk<T> {
  data: T,
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct GraphqlResponseErr {
  errors: Vec<GraphqlError>,
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct GraphqlError {
  message: String,
  locations: Vec<GraphqlLocation>,
  path: Vec<String>,
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct GraphqlLocation {
  line: u32,
  column: u32,
}

impl HttpBorgbaseClient {
  pub fn new() -> Result<Self, anyhow::Error> {
    Ok(Self {
      client: Client::builder()
        .user_agent(USER_AGENT)
        .timeout(TIMEOUT)
        .redirect(reqwest::redirect::Policy::none())
        .build()?,
    })
  }

  pub fn with_auth(self, auth_token: String) -> BoundHttpBorgbaseClient {
    BoundHttpBorgbaseClient {
      client: self,
      auth_token,
    }
  }

  pub async fn get_repos(&self, auth_token: &str, name: Option<String>) -> Result<Vec<RepoType>, anyhow::Error> {
    let res = self
      .client
      .get("https://api.borgbase.com/graphql")
      .bearer_auth(auth_token)
      .json(&ParametrizedGraphqlQuery::new(
        r"
        query repoList (
          $name: String
        ) {
          repoList(
            name: $name
          ) {
            id, name,
            server { id, hostname, region, public, fingerprintRsa, fingerprintEcdsa, fingerprintEd25519 },
            quota, quotaEnabled,
            alertDays, region, format, borgVersion, resticVersion,
            htpasswd, appendOnly, appendOnlyKeys, fullAccessKeys, rsyncKeys,
            accessMode, encryption, createdAt, lastModified, compactionEnabled,
            compactionInterval, compactionIntervalUnit, compactionHour, compactionHourTimezone,
            repoPath, currentUsage
          }
        }",
        RepoListArgs { name },
      ))
      .send()
      .await?;

    let res: GraphqlResponse<GetReposResponse> = checked_json(res).await?;

    Ok(res.unwrap().data.repo_list)
  }

  pub async fn add_repo(&self, auth_token: &str, options: RepoAddArgs) -> Result<RepoType, anyhow::Error> {
    let res = self
      .client
      .post("https://api.borgbase.com/graphql")
      .bearer_auth(auth_token)
      .json(&ParametrizedGraphqlQuery::new(
        r"
        mutation repoAdd (
          $alertDays: Int
          $appendOnlyKeys: [String]
          $borgVersion: String
          $fullAccessKeys: [String]
          $name: String
          $quota: Int
          $quotaEnabled: Boolean
          $region: String
          $rsyncKeys: [String]
        ) {
          repoAdd(
            alertDays: $alertDays
            appendOnlyKeys: $appendOnlyKeys
            borgVersion: $borgVersion
            fullAccessKeys: $fullAccessKeys
            name: $name
            quota: $quota
            quotaEnabled: $quotaEnabled
            region: $region
            rsyncKeys: $rsyncKeys
          ) {
            repoAdded {
              id, name,
              server { id, hostname, region, public, fingerprintRsa, fingerprintEcdsa, fingerprintEd25519 },
              quota, quotaEnabled,
              alertDays, region, format, borgVersion, resticVersion,
              htpasswd, appendOnly, appendOnlyKeys, fullAccessKeys, rsyncKeys,
              accessMode, encryption, createdAt, lastModified, compactionEnabled,
              compactionInterval, compactionIntervalUnit, compactionHour, compactionHourTimezone,
              repoPath, currentUsage
            }
          }
        }",
        options,
      ))
      .send()
      .await?;

    let res: GraphqlResponse<AddRepoResponse> = checked_json(res).await?;

    Ok(res.unwrap().data.repo_add.repo_added)
  }

  pub async fn edit_repo(&self, auth_token: &str, options: RepoEditArgs) -> Result<RepoType, anyhow::Error> {
    let res = self
      .client
      .post("https://api.borgbase.com/graphql")
      .bearer_auth(auth_token)
      .json(&ParametrizedGraphqlQuery::new(
        r"
        mutation repoEdit (
          $id: String!
          $alertDays: Int
          $appendOnlyKeys: [String]
          $borgVersion: String
          $fullAccessKeys: [String]
          $name: String
          $quota: Int
          $quotaEnabled: Boolean
          $region: String
          $rsyncKeys: [String]
        ) {
          repoEdit(
            id: $id
            alertDays: $alertDays
            appendOnlyKeys: $appendOnlyKeys
            borgVersion: $borgVersion
            fullAccessKeys: $fullAccessKeys
            name: $name
            quota: $quota
            quotaEnabled: $quotaEnabled
            region: $region
            rsyncKeys: $rsyncKeys
          ) {
            repoEdited {
              id, name,
              server { id, hostname, region, public, fingerprintRsa, fingerprintEcdsa, fingerprintEd25519 },
              quota, quotaEnabled,
              alertDays, region, format, borgVersion, resticVersion,
              htpasswd, appendOnly, appendOnlyKeys, fullAccessKeys, rsyncKeys,
              accessMode, encryption, createdAt, lastModified, compactionEnabled,
              compactionInterval, compactionIntervalUnit, compactionHour, compactionHourTimezone,
              repoPath, currentUsage
            }
          }
        }",
        options,
      ))
      .send()
      .await?;

    let res: GraphqlResponse<EditRepoResponse> = checked_json(res).await?;

    Ok(res.unwrap().data.repo_edit.repo_edited)
  }

  pub async fn get_ssh_keys(&self, auth_token: &str) -> Result<Vec<PublicSshKey>, anyhow::Error> {
    let res = self
      .client
      .get("https://api.borgbase.com/graphql")
      .bearer_auth(auth_token)
      .json(&GraphqlQuery::new(
        "query { sshList { id, name, keyData, keyType, bits, comment, hashMd5, hashSha256, addedAt, lastUsedAt }}",
      ))
      .send()
      .await?;

    let res: GraphqlResponse<GetSshKeysResponse> = checked_json(res).await?;

    Ok(res.unwrap().data.ssh_list)
  }

  pub async fn add_ssh_key(&self, auth_token: &str, options: SshAddArgs) -> Result<PublicSshKey, anyhow::Error> {
    let res = self
      .client
      .post("https://api.borgbase.com/graphql")
      .bearer_auth(auth_token)
      .json(&ParametrizedGraphqlQuery::new(
        r"
        mutation sshAdd (
          $name: String!
          $keyData: String!
        ) {
          sshAdd(
            name: $name
            keyData: $keyData
          ) {
            keyAdded {
              id, name, keyData, keyType, bits, comment,
              hashMd5, hashSha256, addedAt, lastUsedAt
            }
          }
        }",
        options,
      ))
      .send()
      .await?;

    let res: GraphqlResponse<AddSshKeyResponse> = checked_json(res).await?;

    Ok(res.unwrap().data.ssh_add.key_added)
  }

  pub async fn delete_ssh_key(&self, auth_token: &str, options: SshDeleteArgs) -> Result<(), anyhow::Error> {
    let res = self
      .client
      .post("https://api.borgbase.com/graphql")
      .bearer_auth(auth_token)
      .json(&ParametrizedGraphqlQuery::new(
        r"
        mutation sshDelete (
          $id: Int!
        ) {
          sshDelete(
            id: $id
          ) {
            ok
          }
        }",
        options,
      ))
      .send()
      .await?;

    let res: GraphqlResponse<DeleteSshKeyResponse> = checked_json(res).await?;

    if !res.unwrap().data.ssh_delete.ok {
      return Err(anyhow::Error::msg("Failed to delete key: probably in use"));
    }

    Ok(())
  }
}

impl BoundHttpBorgbaseClient {
  pub async fn get_repos(&self, name: Option<String>) -> Result<Vec<RepoType>, anyhow::Error> {
    self.client.get_repos(&self.auth_token, name).await
  }

  pub async fn add_repo(&self, options: RepoAddArgs) -> Result<RepoType, anyhow::Error> {
    self.client.add_repo(&self.auth_token, options).await
  }

  pub async fn edit_repo(&self, options: RepoEditArgs) -> Result<RepoType, anyhow::Error> {
    self.client.edit_repo(&self.auth_token, options).await
  }

  pub async fn get_ssh_keys(&self) -> Result<Vec<PublicSshKey>, anyhow::Error> {
    self.client.get_ssh_keys(&self.auth_token).await
  }

  pub async fn add_ssh_key(&self, options: SshAddArgs) -> Result<PublicSshKey, anyhow::Error> {
    self.client.add_ssh_key(&self.auth_token, options).await
  }

  pub async fn delete_ssh_key(&self, options: SshDeleteArgs) -> Result<(), anyhow::Error> {
    self.client.delete_ssh_key(&self.auth_token, options).await
  }
}

async fn checked_json<T>(res: reqwest::Response) -> Result<T, anyhow::Error>
where
  T: DeserializeOwned,
{
  let full = res.bytes().await?;

  serde_json::from_slice(&full).map_err(|e| anyhow::Error::from(e).context(String::from_utf8_lossy(&full).to_string()))
}
