use crate::task::builder::{ExecAsBuilder, KatalBuilderError, WantedKatalBuilder};
use crate::task::fs::{EnsureDir, EnsureDirError, EnsureFile, EnsureFileSymlink, EnsureTree, EnsureTreeError};
use crate::task::pacman::EnsurePacmanPackages;
use crate::task::systemd::{SystemdState, SystemdUnit};
use crate::task::{AsyncFn, NamedTask, TaskName, TaskResult, TaskSuccess};
use async_trait::async_trait;
use asys::common::fs::{FsHost, ReadFileError};
use asys::linux::fs::{FileMode, LinuxFsHost, LinuxMetadata};
use asys::linux::user::{GroupRef, LinuxUserHost, Uid, UserRef};
use asys::local_linux::LocalLinux;
use asys::{Command, ExecError, ExecHost};
use hyper_tls::HttpsConnector;
use katal_loader::tree::{SubTree, TarGz};
use pear_client::client::http::HttpPearClient;
use pear_client::common::release::Release;
use pear_client::compact_str::CompactString;
use pear_client::query::get_release::GetReleaseQuery;
use pear_client::tower_service::Service;
use serde::{Deserialize, Serialize};
use std::collections::BTreeSet;
use std::fmt;
use std::fmt::Debug;
use std::net::{SocketAddrV4, SocketAddrV6};
use std::path::PathBuf;
use url::Url;

pub const PHP_INI: &str = include_str!("../../files/php/php.ini");

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Default, Serialize, Deserialize)]
pub struct PhpAvailable {
  pub ext_curl: bool,
  pub ext_ctype: bool,
  pub ext_iconv: bool,
  pub ext_intl: bool,
  pub ext_opentelemetry: bool,
  pub ext_pdo_pgsql: bool,
  pub ext_pgsql: bool,
  pub ext_protobuf: bool,
  pub ext_sodium: bool,
  pub ext_zip: bool,
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for PhpAvailable
where
  H: ExecHost + LinuxFsHost + LinuxUserHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::Group: Send + Sync,
  H::User: Send + Sync + Debug,
{
  type Output = TaskResult<(), anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let mut changed = false;
    {
      let mut pacman = EnsurePacmanPackages::new().present("composer").present("php-fpm");
      if self.ext_intl {
        pacman = pacman.present("php-intl");
      }
      if self.ext_pdo_pgsql || self.ext_pgsql {
        pacman = pacman.present("php-pgsql");
      }
      if self.ext_sodium {
        pacman = pacman.present("php-sodium");
      }
      changed = pacman.run(host).await?.changed || changed;
    }
    if self.ext_protobuf {
      let task = WantedPhpProtobuf {
        php_version: String::from("8.3.8"),
        version: String::from("4.27.1"),
      };
      let out = task.run(host).await.expect("failed to install php-protobuf");
      changed = out.changed || changed;
      let out: PhpProtobuf = out.output;
      EnsureFileSymlink::new(PathBuf::from("/usr/lib/php/modules/protobuf.so"))
        .points_to(out.module)
        .mode(FileMode::ALL_READ)
        .run(host)
        .await
        .expect("failed to link protobuf extension");
    }
    if self.ext_opentelemetry {
      let task = WantedPhpOpentelemetry {
        php_version: String::from("8.3.8"),
        version: String::from("1.0.3"),
      };
      let out = task.run(host).await.expect("failed to install php-opentelemetry");
      changed = out.changed || changed;
      let out: PhpOpentelemetry = out.output;
      EnsureFileSymlink::new(PathBuf::from("/usr/lib/php/modules/opentelemetry.so"))
        .points_to(out.module)
        .mode(FileMode::ALL_READ)
        .run(host)
        .await
        .expect("failed to link opentelemetry extension");
    }
    changed = EnsureFile::new("/etc/php/php.ini")
      .content(PHP_INI)
      .owner(Uid::ROOT)
      .mode(FileMode::ALL_READ)
      .run(host)
      .await?
      .changed
      || changed;
    let mut extensions: BTreeSet<&'static str> = BTreeSet::new();
    // if self.ext_curl {
    //   extensions.insert("curl");
    // }
    // if self.ext_ctype {
    //   extensions.insert("ctype");
    // }
    if self.ext_iconv {
      extensions.insert("iconv");
    }
    if self.ext_intl {
      extensions.insert("intl");
    }
    if self.ext_opentelemetry {
      extensions.insert("opentelemetry");
    }
    if self.ext_pdo_pgsql {
      extensions.insert("pdo_pgsql");
    }
    if self.ext_protobuf {
      extensions.insert("protobuf");
    }
    if self.ext_sodium {
      extensions.insert("sodium");
    }
    // if self.ext_zip {
    //   extensions.insert("zip");
    // }
    for ext in extensions.into_iter() {
      let path = PathBuf::from("/etc/php/conf.d").join(format!("ext_{}.ini", ext));
      let content = format!("extension={}\n", ext);
      changed = EnsureFile::new(path)
        .content(content)
        .owner(Uid::ROOT)
        .mode(FileMode::ALL_READ)
        .run(host)
        .await?
        .changed
        || changed;
    }

    changed = SystemdUnit::new("php-fpm.service")
      .enabled(true)
      .state(SystemdState::Active)
      .run(host)
      .await?
      .changed
      || changed;
    Ok(TaskSuccess { changed, output: () })
  }
}

/// https://www.php.net/manual/en/install.fpm.configuration.php
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct NamedPhpFpmPoolConfig {
  pub name: String,
  pub config: PhpFpmPoolConfig,
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct PhpFpmPoolConfig {
  pub user: String,
  pub group: PhpFpmUnixGroup,
  pub listen: PhpFpmSocketAddr,
  pub listen_owner: Option<String>,
  pub listen_group: Option<String>,
  pub pm: Option<String>,
  pub pm_max_children: Option<u16>,
  pub pm_start_servers: Option<u16>,
  pub pm_min_spare_servers: Option<u16>,
  pub pm_max_spare_servers: Option<u16>,
  pub php_log_error: Option<String>,
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum PhpFpmSocketAddr {
  V4(SocketAddrV4),
  V6(SocketAddrV6),
  All(u16),
  Unix(PathBuf),
}

impl fmt::Display for PhpFpmSocketAddr {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    match self {
      PhpFpmSocketAddr::V4(ref addr) => fmt::Display::fmt(addr, f),
      PhpFpmSocketAddr::V6(ref addr) => fmt::Display::fmt(addr, f),
      PhpFpmSocketAddr::All(port) => fmt::Display::fmt(port, f),
      PhpFpmSocketAddr::Unix(ref sock) => fmt::Display::fmt(&sock.display(), f),
    }
  }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum PhpFpmUnixGroup {
  FromUser,
  Named(String),
}

impl fmt::Display for NamedPhpFpmPoolConfig {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    writeln!(f, "[{}]", self.name.as_str())?;
    writeln!(f, "user = {}", self.config.user.as_str())?;
    match &self.config.group {
      PhpFpmUnixGroup::FromUser => {}
      PhpFpmUnixGroup::Named(ref group) => writeln!(f, "group = {}", group.as_str())?,
    }
    writeln!(f, "listen = {}", &self.config.listen)?;
    if let Some(listen_owner) = self.config.listen_owner.as_ref() {
      writeln!(f, "listen.owner = {}", listen_owner.as_str())?;
    }
    if let Some(listen_group) = self.config.listen_group.as_ref() {
      writeln!(f, "listen.group = {}", listen_group.as_str())?;
    }
    if let Some(pm) = self.config.pm.as_ref() {
      writeln!(f, "pm = {}", pm.as_str())?;
    }
    if let Some(pm_max_children) = self.config.pm_max_children {
      writeln!(f, "pm.max_children = {}", pm_max_children)?;
    }
    if let Some(pm_start_servers) = self.config.pm_start_servers {
      writeln!(f, "pm.start_servers = {}", pm_start_servers)?;
    }
    if let Some(pm_min_spare_servers) = self.config.pm_min_spare_servers {
      writeln!(f, "pm.min_spare_servers = {}", pm_min_spare_servers)?;
    }
    if let Some(pm_max_spare_servers) = self.config.pm_max_spare_servers {
      writeln!(f, "pm.max_spare_servers = {}", pm_max_spare_servers)?;
    }
    if let Some(php_log_error) = self.config.php_log_error.as_ref() {
      writeln!(f, "php_admin_value[error_log] = {}", php_log_error.as_str())?;
    }
    Ok(())
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Default, Serialize, Deserialize)]
pub struct WantedPhpProtobuf {
  pub php_version: String,
  pub version: String,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Default, Serialize, Deserialize)]
pub struct PhpProtobuf {
  pub version: String,
  /// Absolute path to the PHP module.
  /// On Linux, it will be the path to the `.so` file.
  pub module: PathBuf,
}

#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
pub enum WantedPhpProtobufError {
  #[error("failed to acquire katal builder user")]
  KatalBuilder(#[from] KatalBuilderError),
  #[error("failed to create fs dir for source")]
  EnsureDir(#[from] EnsureDirError),
  #[error("failed to create fs tree for source")]
  EnsureArchiveTree(#[from] EnsureTreeError),
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for WantedPhpProtobuf
where
  H: ExecHost + LinuxFsHost + LinuxUserHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::Group: Send + Sync,
  H::User: Send + Sync + Debug,
  H::Group: Send + Sync,
  H::User: Send + Sync + Debug,
{
  type Output = TaskResult<PhpProtobuf, WantedPhpProtobufError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let mut changed = false;
    let pecl_dir = PathBuf::from(format!("/opt/pecl-php.{}", self.php_version));
    let protobuf_dir = pecl_dir.join("protobuf");
    let base_dir = protobuf_dir.join(&self.version);
    let dist_dir = base_dir.join("dist");
    let dist_witness = base_dir.join("dist_ok");
    let module = dist_dir.join("protobuf.so");

    let witness_exists = match host.read_file(dist_witness.as_path()) {
      Ok(_) => true,
      Err(ReadFileError::NotFound) => false,
      Err(e) => {
        panic!("failed to check witness file: {e:?}")
      }
    };
    let mut result = TaskSuccess {
      changed: false,
      output: PhpProtobuf {
        version: self.version.clone(),
        module,
      },
    };

    if witness_exists {
      eprintln!("php_protobuf: witness exists, bailint out");
      return Ok(result);
    }

    eprintln!("php_protobuf: performing full install");
    let src_dir = base_dir.join("src");
    let release = {
      let connector = HttpsConnector::new();
      let client = hyper_util::client::legacy::Client::builder(hyper_util::rt::TokioExecutor::new()).build(connector);
      let mut client = HttpPearClient::new(client);
      let context = pear_client::context::Context::new().set_pear_url(pear_client::context::PearUrl(
        Url::parse("https://pecl.php.net/").unwrap(),
      ));
      let query = GetReleaseQuery::<_>::new(CompactString::new("protobuf"), CompactString::new(&self.version))
        .set_context(context);
      let release: Release = client.call(&query).await.unwrap();
      eprintln!("php_protobuf: fetched release metadata");
      release
    };
    let archive_link = release.archive.link;
    let archive: Vec<u8> = {
      let client = reqwest::Client::builder()
        .user_agent("katal_php")
        .build()
        .expect("building the reqwest client succeeds");
      let res = client
        .get(archive_link.as_str())
        .send()
        .await
        .expect("failed to download archive");
      eprintln!("sent archive request, downloading");
      let archive = res.bytes().await.expect("failed to download archive content");
      archive.to_vec()
    };
    eprintln!("archive downloaded");
    let archive: TarGz = TarGz::new(&archive).expect("invalid archive format");
    eprintln!("archive ready");
    let builder = WantedKatalBuilder::new().run(host).await?;
    changed = builder.changed || changed;
    for dir in [&pecl_dir, &protobuf_dir, &base_dir] {
      eprintln!("create dir {dir:?}");
      changed = EnsureDir::new(dir.clone()).run(host).await?.changed || changed;
    }
    eprintln!("create dir {src_dir:?}");
    changed = EnsureDir::new(src_dir.clone())
      .group(GroupRef::Id(builder.output.gid))
      .owner(UserRef::Id(builder.output.uid))
      .mode(FileMode::OWNER_ALL)
      .run(host)
      .await?
      .changed
      || changed;
    eprintln!("unpacking");
    changed = EnsureTree::new(
      src_dir.clone(),
      SubTree {
        tree: archive,
        prefix: vec![format!("protobuf-{}", &self.version)],
      },
    )
    .group(builder.output.gid)
    .owner(builder.output.uid)
    .run(host)
    .await
    .map_err(WantedPhpProtobufError::EnsureArchiveTree)?
    .changed
      || changed;

    let exec_out = ExecAsBuilder::<LocalLinux, _>::new(WantedPhpExtensionBuild {
      src_dir,
      phpize: PathBuf::from("phpize"),
      make: PathBuf::from("make"),
      basename: String::from("protobuf"),
    })
    .run(host)
    .await
    .expect("failed build");
    changed = exec_out.changed || changed;
    let build_out = exec_out.output.expect("php extension build failed");
    changed = build_out.changed || changed;
    let build_out: PhpExtensionBuild = build_out.output;

    eprintln!("create dir {dist_dir:?}");
    changed = EnsureDir::new(dist_dir.clone())
      .mode(FileMode::ALL_READ | FileMode::ALL_EXEC)
      .run(host)
      .await?
      .changed
      || changed;

    let control = host.read_file(build_out.control).expect("failed to read control file");
    let library = host.read_file(build_out.library).expect("failed to read library file");

    changed = EnsureFile::new(dist_dir.join("protobuf.la"))
      .mode(FileMode::ALL_READ)
      .content(control)
      .run(host)
      .await
      .expect("failed to write control file")
      .changed
      || changed;
    changed = EnsureFile::new(dist_dir.join("protobuf.so"))
      .mode(FileMode::ALL_READ)
      .content(library)
      .run(host)
      .await
      .expect("failed to write library file")
      .changed
      || changed;

    changed = EnsureFile::new(dist_witness)
      .content([])
      .run(host)
      .await
      .unwrap()
      .changed
      || changed;

    result.changed = changed;

    Ok(result)
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Default, Serialize, Deserialize)]
pub struct WantedPhpOpentelemetry {
  pub php_version: String,
  pub version: String,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Default, Serialize, Deserialize)]
pub struct PhpOpentelemetry {
  pub version: String,
  /// Absolute path to the PHP module.
  /// On Linux, it will be the path to the `.so` file.
  pub module: PathBuf,
}

#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
pub enum WantedPhpOpentelemetryError {
  #[error("failed to acquire katal builder user")]
  KatalBuilder(#[from] KatalBuilderError),
  #[error("failed to create fs dir for source")]
  EnsureDir(#[from] EnsureDirError),
  #[error("failed to create fs tree for source")]
  EnsureArchiveTree(#[from] EnsureTreeError),
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for WantedPhpOpentelemetry
where
  H: ExecHost + LinuxFsHost + LinuxUserHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::Group: Send + Sync,
  H::User: Send + Sync + Debug,
  H::Group: Send + Sync,
  H::User: Send + Sync + Debug,
{
  type Output = TaskResult<PhpOpentelemetry, WantedPhpOpentelemetryError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let mut changed = false;
    let pecl_dir = PathBuf::from(format!("/opt/pecl-php.{}", self.php_version));
    let protobuf_dir = pecl_dir.join("opentelemetry");
    let base_dir = protobuf_dir.join(&self.version);
    let dist_dir = base_dir.join("dist");
    let dist_witness = base_dir.join("dist_ok");
    let module = dist_dir.join("opentelemetry.so");

    let witness_exists = match host.read_file(dist_witness.as_path()) {
      Ok(_) => true,
      Err(ReadFileError::NotFound) => false,
      Err(e) => {
        panic!("failed to check witness file: {e:?}")
      }
    };
    let mut result = TaskSuccess {
      changed: false,
      output: PhpOpentelemetry {
        version: self.version.clone(),
        module,
      },
    };

    if witness_exists {
      eprintln!("php_opentelemetry: witness exists, bailint out");
      return Ok(result);
    }

    eprintln!("php_opentelemetry: performing full install");
    let src_dir = base_dir.join("src");
    let release = {
      let connector = HttpsConnector::new();
      let client = hyper_util::client::legacy::Client::builder(hyper_util::rt::TokioExecutor::new()).build(connector);
      let mut client = HttpPearClient::new(client);
      let context = pear_client::context::Context::new().set_pear_url(pear_client::context::PearUrl(
        Url::parse("https://pecl.php.net/").unwrap(),
      ));
      let query = GetReleaseQuery::<_>::new(CompactString::new("opentelemetry"), CompactString::new(&self.version))
        .set_context(context);
      let release: Release = client.call(&query).await.unwrap();
      eprintln!("php_opentelemetry: fetched release metadata");
      release
    };
    let archive_link = release.archive.link;
    let archive: Vec<u8> = {
      let client = reqwest::Client::builder()
        .user_agent("katal_php")
        .build()
        .expect("building the reqwest client succeeds");
      let res = client
        .get(archive_link.as_str())
        .send()
        .await
        .expect("failed to download archive");
      eprintln!("sent archive request, downloading");
      let archive = res.bytes().await.expect("failed to download archive content");
      archive.to_vec()
    };
    eprintln!("archive downloaded");
    let archive: TarGz = TarGz::new(&archive).expect("invalid archive format");
    eprintln!("archive ready");
    let builder = WantedKatalBuilder::new().run(host).await?;
    changed = builder.changed || changed;
    for dir in [&pecl_dir, &protobuf_dir, &base_dir] {
      eprintln!("create dir {dir:?}");
      changed = EnsureDir::new(dir.clone()).run(host).await?.changed || changed;
    }
    eprintln!("create dir {src_dir:?}");
    changed = EnsureDir::new(src_dir.clone())
      .group(GroupRef::Id(builder.output.gid))
      .owner(UserRef::Id(builder.output.uid))
      .mode(FileMode::OWNER_ALL)
      .run(host)
      .await?
      .changed
      || changed;
    eprintln!("unpacking");
    changed = EnsureTree::new(
      src_dir.clone(),
      SubTree {
        tree: archive,
        prefix: vec![format!("opentelemetry-{}", &self.version)],
      },
    )
    .group(builder.output.gid)
    .owner(builder.output.uid)
    .run(host)
    .await
    .map_err(WantedPhpOpentelemetryError::EnsureArchiveTree)?
    .changed
      || changed;
    eprintln!("unpacked");

    let exec_out = ExecAsBuilder::<LocalLinux, _>::new(WantedPhpExtensionBuild {
      src_dir,
      phpize: PathBuf::from("phpize"),
      make: PathBuf::from("make"),
      basename: String::from("opentelemetry"),
    })
    .run(host)
    .await
    .expect("failed build");
    changed = exec_out.changed || changed;
    let build_out = exec_out.output.expect("php extension build failed");
    changed = build_out.changed || changed;
    let build_out: PhpExtensionBuild = build_out.output;

    eprintln!("create dir {dist_dir:?}");
    changed = EnsureDir::new(dist_dir.clone())
      .mode(FileMode::ALL_READ | FileMode::ALL_EXEC)
      .run(host)
      .await?
      .changed
      || changed;

    let control = host.read_file(build_out.control).expect("failed to read control file");
    let library = host.read_file(build_out.library).expect("failed to read library file");

    changed = EnsureFile::new(dist_dir.join("opentelemetry.la"))
      .mode(FileMode::ALL_READ)
      .content(control)
      .run(host)
      .await
      .expect("failed to write control file")
      .changed
      || changed;
    changed = EnsureFile::new(dist_dir.join("opentelemetry.so"))
      .mode(FileMode::ALL_READ)
      .content(library)
      .run(host)
      .await
      .expect("failed to write library file")
      .changed
      || changed;

    changed = EnsureFile::new(dist_witness)
      .content([])
      .run(host)
      .await
      .unwrap()
      .changed
      || changed;

    result.changed = changed;

    Ok(result)
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Default, Serialize, Deserialize)]
pub struct WantedPhpExtensionBuild {
  pub src_dir: PathBuf,
  pub phpize: PathBuf,
  pub make: PathBuf,
  /// Basename of the output files. The build will return `./modules/{basename}.{la,so}`.
  pub basename: String,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Default, Serialize, Deserialize)]
pub struct PhpExtensionBuild {
  /// libtool control file (.la)
  pub control: PathBuf,
  /// Shared object library (.so)
  pub library: PathBuf,
}

#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error, Serialize, Deserialize)]
pub enum PhpExtensionBuildError {
  #[error("failed when running `phpize`")]
  Phpize(#[source] ExecError),
  #[error("failed when running `configure`")]
  Configure(#[source] ExecError),
  #[error("failed when running `make`")]
  Make(#[source] ExecError),
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for WantedPhpExtensionBuild
where
  H: ExecHost + LinuxFsHost + LinuxUserHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::Group: Send + Sync,
  H::User: Send + Sync + Debug,
  H::Group: Send + Sync,
  H::User: Send + Sync + Debug,
{
  type Output = TaskResult<PhpExtensionBuild, PhpExtensionBuildError>;

  async fn run(&self, _host: &'h H) -> Self::Output {
    let changed = true;
    {
      let cmd = Command::new(self.phpize.as_path().display().to_string())
        .current_dir(self.src_dir.as_path().display().to_string());
      LocalLinux.exec(&cmd).map_err(PhpExtensionBuildError::Phpize)?;
    }
    {
      let cmd = Command::new(self.src_dir.join("configure").as_path().display().to_string())
        .current_dir(self.src_dir.as_path().display().to_string());
      LocalLinux.exec(&cmd).map_err(PhpExtensionBuildError::Configure)?;
    }
    {
      let cmd = Command::new(self.make.as_path().display().to_string())
        .current_dir(self.src_dir.as_path().display().to_string());
      LocalLinux.exec(&cmd).map_err(PhpExtensionBuildError::Make)?;
    }
    // todo: don't hardcode lib name
    let output = PhpExtensionBuild {
      control: self.src_dir.join(format!("modules/{}.la", self.basename)),
      library: self.src_dir.join(format!("modules/{}.so", self.basename)),
    };
    Ok(TaskSuccess { changed, output })
  }
}

impl<'h, H> NamedTask<&'h H> for WantedPhpExtensionBuild
where
  H: ExecHost + LinuxFsHost + LinuxUserHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::Group: Send + Sync,
  H::User: Send + Sync + Debug,
  H::Group: Send + Sync,
  H::User: Send + Sync + Debug,
{
  const NAME: TaskName = TaskName::new("php::WantedPhpExtensionBuild");
}
