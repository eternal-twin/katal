use crate::task::fs::EnsureFile;
use crate::task::pacman::EnsurePacmanPackages;
use crate::task::systemd::{SystemdState, SystemdUnit};
use crate::task::{AsyncFn, TaskResult, TaskSuccess};
use async_trait::async_trait;
use asys::common::fs::FsHost;
use asys::linux::fs::{FileMode, LinuxFsHost, LinuxMetadata};
use asys::linux::user::Uid;
/// Certbot configuration tasks
///
/// <https://certbot.eff.org/docs/man/certbot.html>
use asys::{Command, ExecHost};
use std::path::PathBuf;

pub const RENEW_CERTBOT_SERVICE: &str = include_str!("../../files/certbot/renew_certbot.service");
pub const RENEW_CERTBOT_TIMER: &str = include_str!("../../files/certbot/renew_certbot.timer");

pub struct CertbotReady;

#[async_trait]
impl<'h, H: ExecHost + LinuxFsHost> AsyncFn<&'h H> for CertbotReady
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<(), anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let mut changed = false;
    changed = EnsurePacmanPackages::new().present("certbot").run(host).await?.changed || changed;
    changed = EnsureFile::new("/etc/systemd/system/renew_certbot.service")
      .content(RENEW_CERTBOT_SERVICE)
      .owner(Uid::ROOT)
      .mode(FileMode::ALL_READ)
      .run(host)
      .await?
      .changed
      || changed;
    changed = EnsureFile::new("/etc/systemd/system/renew_certbot.timer")
      .content(RENEW_CERTBOT_TIMER)
      .owner(Uid::ROOT)
      .mode(FileMode::ALL_READ)
      .run(host)
      .await?
      .changed
      || changed;
    changed = SystemdUnit::new("renew_certbot.timer")
      .enabled(true)
      .state(SystemdState::Active)
      .run(host)
      .await?
      .changed
      || changed;
    Ok(TaskSuccess { changed, output: () })
  }
}

pub struct CertbotCert {
  email: String,
  cert_name: String,
  domain: String,
  secondary_domains: Vec<String>,
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for CertbotCert
where
  H: ExecHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<PathBuf, anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let mut changed = false;
    changed = CertbotReady.run(host).await?.changed || changed;

    let mut cmd = Command::new("certbot").arg("certonly");
    cmd = cmd.arg("--standalone").arg("--noninteractive");
    cmd = cmd.arg("--pre-hook").arg("systemctl stop nginx");
    cmd = cmd.arg("--post-hook").arg("systemctl start nginx");
    cmd = cmd.arg("--expand").arg("--agree-tos");
    cmd = cmd.arg("--email").arg(&self.email);
    cmd = cmd.arg("--cert-name").arg(&self.cert_name);
    let groups = std::iter::once(&self.domain)
      .chain(self.secondary_domains.iter())
      .map(String::as_str)
      .collect::<Vec<&str>>()
      .as_slice()
      .join(",");
    cmd = cmd.arg("--domains").arg(groups);

    host.exec(&cmd)?;

    Ok(TaskSuccess {
      changed,
      output: self.get_path(),
    })
  }
}

impl CertbotCert {
  pub fn new(email: impl ToString, cert_name: impl ToString, domain: impl ToString) -> Self {
    Self {
      email: email.to_string(),
      cert_name: cert_name.to_string(),
      domain: domain.to_string(),
      secondary_domains: Vec::new(),
    }
  }

  pub fn domain(mut self, domain: impl ToString) -> Self {
    self.secondary_domains.push(domain.to_string());
    self
  }

  pub fn get_path(&self) -> PathBuf {
    let mut output = PathBuf::from("/etc/letsencrypt/live");
    output.push(&self.cert_name);
    output.push("cert.pem");
    output
  }

  pub fn get_fullchain(&self) -> PathBuf {
    let mut output = PathBuf::from("/etc/letsencrypt/live");
    output.push(&self.cert_name);
    output.push("fullchain.pem");
    output
  }

  pub fn get_priv(&self) -> PathBuf {
    let mut output = PathBuf::from("/etc/letsencrypt/live");
    output.push(&self.cert_name);
    output.push("privkey.pem");
    output
  }
}
