use crate::task::fs::EnsureFile;
use crate::task::{AsyncFn, TaskResult, TaskSuccess};
use async_trait::async_trait;
use asys::common::fs::FsHost;
use asys::linux::fs::{FileMode, LinuxFsHost, LinuxMetadata};
use asys::linux::user::{Gid, LinuxUser, Uid};
use asys::ExecHost;
use std::path::PathBuf;

const BASH_RC: &str = r#"if [ -f ~/.profile ];
then
    .  ~/.profile;
fi

[[ $- != *i* ]] && return
"#;

pub struct BashRc {
  pub home: PathBuf,
  pub uid: Uid,
  pub gid: Gid,
}

impl BashRc {
  pub fn for_user<LU: LinuxUser>(user: &LU) -> Self {
    Self {
      home: user.home(),
      uid: user.uid(),
      gid: user.gid(),
    }
  }
}

#[async_trait]
impl<'h, H: ExecHost + LinuxFsHost> AsyncFn<&'h H> for BashRc
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<(), anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let mut changed = false;
    changed = EnsureFile::new(self.home.join(".bashrc"))
      .content(BASH_RC)
      .owner(self.uid)
      .group(self.gid)
      .mode(FileMode::OWNER_READ)
      .run(host)
      .await?
      .changed
      || changed;
    Ok(TaskSuccess { changed, output: () })
  }
}
