use crate::task::fs::{EnsureFile, EnsureFileError};
use crate::task::systemd::config::Config;
use crate::task::{AsyncFn, NamedTask, TaskName, TaskResult, TaskSuccess};
use async_trait::async_trait;
use asys::common::fs::FsHost;
use asys::linux::fs::{LinuxFsHost, LinuxMetadata};
use asys::linux::user::LinuxUserHost;
use asys::local_linux::LocalLinux;
use asys::{Command, ExecError, ExecHost};
use core::fmt;
use serde::{Deserialize, Serialize};
use std::path::PathBuf;
use thiserror::Error;

pub mod config {
  use core::fmt;

  #[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
  pub struct Config<S: AsRef<str>> {
    pub unit: Option<Unit<S>>,
    pub service: Option<Service<S>>,
    pub timer: Option<Timer<S>>,
    pub install: Option<Install<S>>,
  }

  impl<S> fmt::Display for Config<S>
  where
    S: AsRef<str>,
  {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
      if let Some(unit) = self.unit.as_ref() {
        writeln!(f, "[Unit]")?;
        writeln!(f, "{unit}")?;
      }
      if let Some(service) = self.service.as_ref() {
        writeln!(f, "[Service]")?;
        writeln!(f, "{service}")?;
      }
      if let Some(timer) = self.timer.as_ref() {
        writeln!(f, "[Timer]")?;
        writeln!(f, "{timer}")?;
      }
      if let Some(install) = self.install.as_ref() {
        writeln!(f, "[Install]")?;
        writeln!(f, "{install}")?;
      }
      Ok(())
    }
  }

  /// \[Unit] Section Options
  ///
  /// The unit file may include a \[Unit] section, which carries generic information about the unit
  /// that is not dependent on the type of unit.
  ///
  /// See <https://www.freedesktop.org/software/systemd/man/latest/systemd.unit.html#%5BUnit%5D%20Section%20Options>
  #[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
  pub struct Unit<S: AsRef<str>> {
    /// A short human readable title of the unit.
    ///
    /// Added in version 201.
    pub description: Option<S>,
    /// A list of URIs referencing documentation for this unit or its configuration.
    ///
    /// Added in version 201.
    pub documentation: Vec<S>,
    /// Configures (weak) requirement dependencies on other units.
    ///
    /// Added in version 201.
    pub wants: Vec<S>,
    /// Similar to `wants`, but declares a stronger requirement dependency.
    ///
    /// Added in version 201.
    pub requires: Vec<S>,
    /// Similar to `requires`. However, if the units listed here are not started already, they will
    /// not be started and the starting of this unit will fail immediately.
    ///
    /// Added in version 201.
    pub requisite: Vec<S>,
    /// Configures requirement dependencies, very similar in style to `requires`. However, this
    /// dependency type is stronger: in addition to the effect of `requires` it declares that if
    /// the unit bound to is stopped, this unit will be stopped too.
    ///
    /// Added in version 201.
    pub binds_to: Vec<S>,
    /// Configures dependencies similar to `requires`, but limited to stopping and restarting of
    /// units.
    ///
    /// Added in version 201.
    pub part_of: Vec<S>,
    /// Configures dependencies similar to `wants`, but as long as this unit is up, all units listed
    /// in `upholds` are started whenever found to be inactive or failed, and no job is queued for
    /// them.
    ///
    /// Added in version 249.
    pub upholds: Vec<S>,
    /// A list of unit names. Configures negative requirement dependencies.
    ///
    /// Added in version 201.
    pub conflicts: Vec<S>,
    /// This setting configures ordering dependencies between units.
    ///
    /// Added in version 201.
    pub before: Vec<S>,
    /// This setting configures ordering dependencies between units.
    ///
    /// Added in version 201.
    pub after: Vec<S>,
    /// A list of one or more units that are activated when this unit enters the `failed` state.
    ///
    /// Added in version 201.
    pub on_failure: Vec<S>,
    /// A list of one or more units that are activated when this unit enters the `inactive` state.
    ///
    /// Added in version 201.
    pub on_success: Vec<S>,
    pub propagates_reload_to: Vec<S>,
    pub reload_propagated_from: Vec<S>,
    pub propagates_stop_to: Vec<S>,
    pub stop_propagated_from: Vec<S>,
    pub joins_namespace_of: Vec<S>,
    pub requires_mounts_for: Vec<S>,
    pub on_success_job_mode: Option<JobMode>,
    pub on_failure_job_mode: Option<JobMode>,
    pub ignore_on_isolate: Option<bool>,
    pub stop_when_unneeded: Option<bool>,
    pub refuse_manual_start: Option<bool>,
    pub refuse_manual_stop: Option<bool>,
    pub allow_isolate: Option<bool>,
    pub default_dependencies: Option<bool>,
    pub survive_final_kill_signal: Option<bool>,
    pub collect_mode: Option<S>,
    pub failure_action: Option<S>,
    pub success_action: Option<S>,
    pub failure_action_exit_status: Option<S>,
    pub success_action_exit_status: Option<S>,
    pub job_timeout_sec: Option<S>,
    pub job_running_timeout_sec: Option<S>,
    pub job_timeout_action: Option<S>,
    pub job_timeout_reboot_argument: Option<S>,
    pub start_limit_interval_sec: Option<S>,
    pub start_limit_burst: Option<S>,
    pub start_limit_action: Option<S>,
    pub reboot_arguments: Option<S>,
    pub source_path: Option<S>,
  }

  impl<S> fmt::Display for Unit<S>
  where
    S: AsRef<str>,
  {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
      if let Some(v) = self.description.as_ref() {
        writeln!(f, "Description={}", v.as_ref())?;
      }
      if !self.documentation.is_empty() {
        todo!()
      }
      for v in &self.wants {
        writeln!(f, "Wants={}", v.as_ref())?;
      }
      for v in &self.requires {
        writeln!(f, "Requires={}", v.as_ref())?;
      }
      if !self.requisite.is_empty() {
        todo!()
      }
      if !self.binds_to.is_empty() {
        todo!()
      }
      if !self.part_of.is_empty() {
        todo!()
      }
      if !self.upholds.is_empty() {
        todo!()
      }
      if !self.conflicts.is_empty() {
        todo!()
      }
      if !self.before.is_empty() {
        todo!()
      }
      for v in &self.after {
        writeln!(f, "After={}", v.as_ref())?;
      }
      if !self.on_failure.is_empty() {
        todo!()
      }
      if !self.on_success.is_empty() {
        todo!()
      }
      if !self.propagates_reload_to.is_empty() {
        todo!()
      }
      if !self.reload_propagated_from.is_empty() {
        todo!()
      }
      if !self.propagates_stop_to.is_empty() {
        todo!()
      }
      if !self.stop_propagated_from.is_empty() {
        todo!()
      }
      if !self.joins_namespace_of.is_empty() {
        todo!()
      }
      if !self.requires_mounts_for.is_empty() {
        todo!()
      }
      if let Some(_v) = self.on_success_job_mode.as_ref() {
        todo!()
      }
      if let Some(_v) = self.on_failure_job_mode.as_ref() {
        todo!()
      }
      if let Some(_v) = self.ignore_on_isolate.as_ref() {
        todo!()
      }
      if let Some(_v) = self.stop_when_unneeded.as_ref() {
        todo!()
      }
      if let Some(_v) = self.refuse_manual_start.as_ref() {
        todo!()
      }
      if let Some(_v) = self.refuse_manual_stop.as_ref() {
        todo!()
      }
      if let Some(_v) = self.allow_isolate.as_ref() {
        todo!()
      }
      if let Some(_v) = self.default_dependencies.as_ref() {
        todo!()
      }
      if let Some(_v) = self.survive_final_kill_signal.as_ref() {
        todo!()
      }
      if let Some(_v) = self.collect_mode.as_ref() {
        todo!()
      }
      if let Some(_v) = self.failure_action.as_ref() {
        todo!()
      }
      if let Some(_v) = self.success_action.as_ref() {
        todo!()
      }
      if let Some(_v) = self.failure_action_exit_status.as_ref() {
        todo!()
      }
      if let Some(_v) = self.success_action_exit_status.as_ref() {
        todo!()
      }
      if let Some(_v) = self.job_timeout_sec.as_ref() {
        todo!()
      }
      if let Some(_v) = self.job_running_timeout_sec.as_ref() {
        todo!()
      }
      if let Some(_v) = self.job_timeout_action.as_ref() {
        todo!()
      }
      if let Some(_v) = self.job_timeout_reboot_argument.as_ref() {
        todo!()
      }
      if let Some(_v) = self.start_limit_interval_sec.as_ref() {
        todo!()
      }
      if let Some(_v) = self.start_limit_burst.as_ref() {
        todo!()
      }
      if let Some(_v) = self.start_limit_action.as_ref() {
        todo!()
      }
      if let Some(_v) = self.reboot_arguments.as_ref() {
        todo!()
      }
      if let Some(_v) = self.source_path.as_ref() {
        todo!()
      }
      Ok(())
    }
  }

  #[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
  pub struct Install<S: AsRef<str>> {
    pub alias: Vec<S>,
    pub wanted_by: Vec<S>,
    pub required_by: Vec<S>,
    pub upheld_by: Vec<S>,
    pub also: Vec<S>,
    pub default_instance: Option<S>,
  }

  impl<S> fmt::Display for Install<S>
  where
    S: AsRef<str>,
  {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
      if !self.alias.is_empty() {
        todo!()
      }
      for v in &self.wanted_by {
        writeln!(f, "WantedBy={}", v.as_ref())?;
      }
      if !self.required_by.is_empty() {
        todo!()
      }
      if !self.upheld_by.is_empty() {
        todo!()
      }
      if !self.also.is_empty() {
        todo!()
      }
      if let Some(_v) = self.default_instance.as_ref() {
        todo!()
      }
      Ok(())
    }
  }

  #[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
  pub enum JobMode {
    Fail,
    Replace,
    ReplaceIrreversibly,
    Isolate,
    Flush,
    IgnoreDependencies,
    IgnoreRequirements,
  }

  /// See <https://www.freedesktop.org/software/systemd/man/latest/systemd.service.html#Options>
  #[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
  pub struct Service<S: AsRef<str>> {
    pub r#type: Option<ServiceType>,
    pub exit_type: Option<ExitType>,
    pub remain_after_exit: Option<bool>,
    pub guess_main_pid: Option<bool>,
    pub pid_file: Option<S>,
    pub bus_name: Option<S>,
    pub exec_start: Option<CommandLine<S>>,
    pub exec_start_pre: Option<CommandLine<S>>,
    pub exec_start_post: Option<CommandLine<S>>,
    pub exec_condition: Option<CommandLine<S>>,
    pub exec_reload: Option<CommandLine<S>>,
    pub exec_stop: Option<CommandLine<S>>,
    pub restart_sec: Option<S>,
    pub restart_steps: Option<S>,
    pub restart_max_delay_sec: Option<S>,
    pub timeout_start_sec: Option<S>,
    pub timeout_stop_sec: Option<S>,
    pub timeout_abort_sec: Option<S>,
    pub timeout_sec: Option<S>,
    pub timeout_start_failure_mode: Option<S>,
    pub timeout_stop_failure_mode: Option<S>,
    pub runtime_max_sec: Option<S>,
    pub runtime_randomize_extra_sec: Option<S>,
    pub watchdog_sec: Option<S>,
    pub restart: Option<S>,
    pub restart_mode: Option<S>,
    pub success_exit_status: Option<S>,
    pub restart_prevent_exit_status: Option<S>,
    pub restart_force_exit_status: Option<S>,
    pub root_directory_start_only: Option<S>,
    pub non_blocking: Option<S>,
    pub notify_access: Option<S>,
    pub sockets: Option<S>,
    pub file_descriptor_store_max: Option<S>,
    pub file_descriptor_store_preserve: Option<S>,
    pub usb_function_descriptors: Option<S>,
    pub usb_function_strings: Option<S>,
    pub oom_policy: Option<S>,
    pub open_file: Option<S>,
    pub reload_signal: Option<S>,
    pub exec: Exec<S>,
    pub kill: Kill<S>,
    pub resource_control: ResourceControl<S>,
  }

  impl<S> fmt::Display for Service<S>
  where
    S: AsRef<str>,
  {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
      if let Some(v) = self.r#type {
        writeln!(f, "Type={}", v)?;
      }
      if let Some(_v) = self.exit_type.as_ref() {
        todo!()
      }
      if let Some(_v) = self.remain_after_exit.as_ref() {
        todo!()
      }
      if let Some(_v) = self.guess_main_pid.as_ref() {
        todo!()
      }
      if let Some(v) = self.pid_file.as_ref() {
        writeln!(f, "PIDFile={}", v.as_ref())?;
      }
      if let Some(_v) = self.bus_name.as_ref() {
        todo!()
      }
      if let Some(v) = self.exec_start.as_ref() {
        writeln!(f, "ExecStart={}", v.line.as_ref())?;
      }
      if let Some(v) = self.exec_start_pre.as_ref() {
        writeln!(f, "ExecStartPre={}", v.line.as_ref())?;
      }
      if let Some(_v) = self.exec_start_post.as_ref() {
        todo!()
      }
      if let Some(_v) = self.exec_condition.as_ref() {
        todo!()
      }
      if let Some(v) = self.exec_reload.as_ref() {
        writeln!(f, "ExecReload={}", v.line.as_ref())?;
      }
      if let Some(_v) = self.exec_stop.as_ref() {
        todo!()
      }
      if let Some(v) = self.restart_sec.as_ref() {
        writeln!(f, "RestartSec={}", v.as_ref())?;
      }
      if let Some(_v) = self.restart_steps.as_ref() {
        todo!()
      }
      if let Some(_v) = self.restart_max_delay_sec.as_ref() {
        todo!()
      }
      if let Some(v) = self.timeout_start_sec.as_ref() {
        writeln!(f, "TimeoutStartSec={}", v.as_ref())?;
      }
      if let Some(v) = self.timeout_stop_sec.as_ref() {
        writeln!(f, "TimeoutStopSec={}", v.as_ref())?;
      }
      if let Some(_v) = self.timeout_abort_sec.as_ref() {
        todo!()
      }
      if let Some(v) = self.timeout_sec.as_ref() {
        writeln!(f, "TimeoutSec={}", v.as_ref())?;
      }
      if let Some(_v) = self.timeout_start_failure_mode.as_ref() {
        todo!()
      }
      if let Some(_v) = self.timeout_stop_failure_mode.as_ref() {
        todo!()
      }
      if let Some(_v) = self.runtime_max_sec.as_ref() {
        todo!()
      }
      if let Some(_v) = self.runtime_randomize_extra_sec.as_ref() {
        todo!()
      }
      if let Some(_v) = self.watchdog_sec.as_ref() {
        todo!()
      }
      if let Some(v) = self.restart.as_ref() {
        writeln!(f, "Restart={}", v.as_ref())?;
      }
      if let Some(_v) = self.restart_mode.as_ref() {
        todo!()
      }
      if let Some(_v) = self.success_exit_status.as_ref() {
        todo!()
      }
      if let Some(_v) = self.restart_prevent_exit_status.as_ref() {
        todo!()
      }
      if let Some(_v) = self.restart_force_exit_status.as_ref() {
        todo!()
      }
      if let Some(_v) = self.root_directory_start_only.as_ref() {
        todo!()
      }
      if let Some(_v) = self.non_blocking.as_ref() {
        todo!()
      }
      if let Some(_v) = self.notify_access.as_ref() {
        todo!()
      }
      if let Some(_v) = self.sockets.as_ref() {
        todo!()
      }
      if let Some(_v) = self.file_descriptor_store_max.as_ref() {
        todo!()
      }
      if let Some(_v) = self.file_descriptor_store_preserve.as_ref() {
        todo!()
      }
      if let Some(_v) = self.usb_function_descriptors.as_ref() {
        todo!()
      }
      if let Some(_v) = self.usb_function_strings.as_ref() {
        todo!()
      }
      if let Some(_v) = self.oom_policy.as_ref() {
        todo!()
      }
      if let Some(_v) = self.open_file.as_ref() {
        todo!()
      }
      if let Some(_v) = self.reload_signal.as_ref() {
        todo!()
      }
      self.exec.fmt(f)?;
      Ok(())
    }
  }

  /// Mechanism used to notify the service manager that start-up has finished.
  ///
  /// <https://www.freedesktop.org/software/systemd/man/latest/systemd.service.html#Type=>
  #[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
  pub enum ServiceType {
    Simple,
    Exec,
    Forking,
    /// The service manager will consider the unit up after the main process exits.
    Oneshot,
    Dbus,
    /// The service sends a `READY=1` notification message via [`sd_notify(3)`](https://www.freedesktop.org/software/systemd/man/latest/sd_notify.html#)
    /// or an equivalent call when it has finished starting up.
    Notify,
    NotifyReload,
    Idle,
  }

  impl ServiceType {
    pub fn as_str(self) -> &'static str {
      match self {
        Self::Simple => "simple",
        Self::Exec => "exec",
        Self::Forking => "forking",
        Self::Oneshot => "oneshot",
        Self::Dbus => "dbus",
        Self::Notify => "notify",
        Self::NotifyReload => "notify-reload",
        Self::Idle => "idle",
      }
    }
  }

  impl fmt::Display for ServiceType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
      f.write_str(self.as_str())
    }
  }

  #[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
  pub enum ExitType {
    Main,
    Cgroup,
  }

  /// See <https://www.freedesktop.org/software/systemd/man/latest/systemd.slice.html#Options>
  #[derive(Default, Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
  pub struct Slice<S: AsRef<str>> {
    pub resource_control: Option<ResourceControl<S>>,
  }

  impl<S> fmt::Display for Slice<S>
  where
    S: AsRef<str>,
  {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
      if let Some(v) = self.resource_control.as_ref() {
        v.fmt(f)?;
      }
      Ok(())
    }
  }

  /// See <https://www.freedesktop.org/software/systemd/man/latest/systemd.exec.html>
  #[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
  pub struct Exec<S: AsRef<str>> {
    pub exec_search_path: Option<S>,
    pub working_directory: Option<S>,
    pub root_directory: Option<S>,
    pub root_image: Option<S>,
    pub root_image_options: Option<S>,
    pub root_ephemeral: Option<S>,
    pub root_hash: Option<S>,
    pub root_hash_signature: Option<S>,
    pub root_verity: Option<S>,
    pub root_image_policy: Option<S>,
    pub mount_image_policy: Option<S>,
    pub extension_image_policy: Option<S>,
    pub mount_api_vfs: Option<S>,
    pub protect_proc: Option<S>,
    pub proc_subset: Option<S>,
    pub bind_paths: Option<S>,
    pub bind_read_only_paths: Option<S>,
    pub mount_images: Option<S>,
    pub extension_images: Option<S>,
    pub extension_directories: Option<S>,
    pub user: Option<S>,
    pub group: Option<S>,
    pub dynamic_user: Option<S>,
    pub supplementary_groups: Option<S>,
    pub set_login_environment: Option<S>,
    pub pam_name: Option<S>,
    pub capability_bounding_set: Option<S>,
    pub ambient_capabilities: Option<S>,
    pub no_new_privileges: Option<S>,
    pub secure_bits: Option<S>,
    pub se_linux_context: Option<S>,
    pub app_armor_profile: Option<S>,
    pub smack_process_label: Option<S>,
    pub limit_cpu: Option<S>,
    pub limit_fsize: Option<S>,
    pub limit_data: Option<S>,
    pub limit_stack: Option<S>,
    pub limit_core: Option<S>,
    pub limit_rss: Option<S>,
    pub limit_no_file: Option<S>,
    pub limit_as: Option<S>,
    pub limit_nproc: Option<S>,
    pub limit_mem_lock: Option<S>,
    pub limit_locks: Option<S>,
    pub limit_sig_pending: Option<S>,
    pub limit_msg_queue: Option<S>,
    pub limit_nice: Option<S>,
    pub limit_rt_prio: Option<S>,
    pub limit_rt_time: Option<S>,
    pub umask: Option<S>,
    pub coredump_filter: Option<S>,
    pub keyrin_mode: Option<S>,
    pub oom_score_adjust: Option<S>,
    pub timer_slack_nsec: Option<S>,
    pub personality: Option<S>,
    pub ignore_sigpipe: Option<S>,
    pub nice: Option<S>,
    pub cpu_scheduling_policy: Option<S>,
    pub cpu_scheduling_priority: Option<S>,
    pub cpu_scheduling_reset_on_fork: Option<S>,
    pub cpu_affinity: Option<S>,
    pub numa_policy: Option<S>,
    pub numa_mask: Option<S>,
    pub io_scheduling_class: Option<S>,
    pub io_scheduling_priority: Option<S>,
    pub protect_system: Option<S>,
    pub protect_home: Option<S>,
    pub runtime_directory: Option<S>,
    pub state_directory: Option<S>,
    pub cache_directory: Option<S>,
    pub logs_directory: Option<S>,
    pub configuration_directory: Option<S>,
    pub runtime_directory_mode: Option<S>,
    pub state_directory_mode: Option<S>,
    pub cache_directory_mode: Option<S>,
    pub logs_directory_mode: Option<S>,
    pub configuration_directory_mode: Option<S>,
    pub runtime_directory_preserve: Option<S>,
    pub timeout_clean_sec: Option<S>,
    pub read_write_paths: Option<S>,
    pub read_only_paths: Option<S>,
    pub inaccessible_paths: Option<S>,
    pub exec_paths: Option<S>,
    pub no_exec_paths: Option<S>,
    pub temporary_file_system: Option<S>,
    pub private_tmp: Option<S>,
    pub private_devices: Option<S>,
    pub private_network: Option<S>,
    pub network_namespace_path: Option<S>,
    pub private_ipc: Option<S>,
    pub ipc_namespace_path: Option<S>,
    pub memory_ksm: Option<S>,
    pub private_users: Option<S>,
    pub protect_hostname: Option<S>,
    pub protect_clock: Option<S>,
    pub protect_kernel_tunables: Option<S>,
    pub protect_kernel_modules: Option<S>,
    pub protect_kernel_logs: Option<S>,
    pub protect_control_groups: Option<S>,
    pub restrict_address_families: Option<S>,
    pub restrict_file_systems: Option<S>,
    pub restrict_namespaces: Option<S>,
    pub lock_personality: Option<S>,
    pub memory_deny_write_execute: Option<S>,
    pub restrict_realtime: Option<S>,
    pub restrict_suid_sgid: Option<S>,
    pub remove_ipc: Option<S>,
    pub private_mounts: Option<S>,
    pub mount_flags: Option<S>,
    pub system_call_filter: Option<S>,
    pub system_call_error_number: Option<S>,
    pub system_call_architectures: Option<S>,
    pub system_call_log: Option<S>,
    pub environment: Vec<S>,
    pub environment_file: Option<S>,
    pub pass_environment: Option<S>,
    pub unset_environment: Option<S>,
    pub standard_input: Option<S>,
    pub standard_output: Option<S>,
    pub standard_error: Option<S>,
    pub standard_input_text: Option<S>,
    pub standard_input_data: Option<S>,
    pub log_level_max: Option<S>,
    pub log_extra_fields: Option<S>,
    pub log_rate_limit_interval_sec: Option<S>,
    pub log_rate_limit_burst: Option<S>,
    pub log_filter_patterns: Option<S>,
    pub log_namespace: Option<S>,
    pub syslog_identifier: Option<S>,
    pub syslog_facility: Option<S>,
    pub syslog_level: Option<S>,
    pub syslog_level_prefix: Option<S>,
    pub tty_path: Option<S>,
    pub tty_reset: Option<S>,
    pub tty_vhangup: Option<S>,
    pub tty_rows: Option<S>,
    pub tty_columns: Option<S>,
    pub tty_disallocate: Option<S>,
    pub load_credentials: Option<S>,
    pub load_credentials_encrypted: Option<S>,
    pub import_credentials: Option<S>,
    pub set_credentials: Option<S>,
    pub set_credentials_encrypted: Option<S>,
    pub utmp_identifier: Option<S>,
    pub utmp_mode: Option<S>,
  }

  impl<S> fmt::Display for Exec<S>
  where
    S: AsRef<str>,
  {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
      if let Some(v) = self.exec_search_path.as_ref() {
        writeln!(f, "ExecSearchPath={}", v.as_ref())?;
      }
      if let Some(v) = self.working_directory.as_ref() {
        writeln!(f, "WorkingDirectory={}", v.as_ref())?;
      }
      if let Some(_v) = self.root_directory.as_ref() {
        todo!()
      }
      if let Some(_v) = self.root_image.as_ref() {
        todo!()
      }
      if let Some(_v) = self.root_image_options.as_ref() {
        todo!()
      }
      if let Some(_v) = self.root_ephemeral.as_ref() {
        todo!()
      }
      if let Some(_v) = self.root_hash.as_ref() {
        todo!()
      }
      if let Some(_v) = self.root_hash_signature.as_ref() {
        todo!()
      }
      if let Some(_v) = self.root_verity.as_ref() {
        todo!()
      }
      if let Some(_v) = self.root_image_policy.as_ref() {
        todo!()
      }
      if let Some(_v) = self.mount_image_policy.as_ref() {
        todo!()
      }
      if let Some(_v) = self.extension_image_policy.as_ref() {
        todo!()
      }
      if let Some(_v) = self.mount_api_vfs.as_ref() {
        todo!()
      }
      if let Some(_v) = self.protect_proc.as_ref() {
        todo!()
      }
      if let Some(_v) = self.proc_subset.as_ref() {
        todo!()
      }
      if let Some(_v) = self.bind_paths.as_ref() {
        todo!()
      }
      if let Some(_v) = self.bind_read_only_paths.as_ref() {
        todo!()
      }
      if let Some(_v) = self.mount_images.as_ref() {
        todo!()
      }
      if let Some(_v) = self.extension_images.as_ref() {
        todo!()
      }
      if let Some(_v) = self.extension_directories.as_ref() {
        todo!()
      }
      if let Some(v) = self.user.as_ref() {
        writeln!(f, "User={}", v.as_ref())?;
      }
      if let Some(v) = self.group.as_ref() {
        writeln!(f, "Group={}", v.as_ref())?;
      }
      if let Some(_v) = self.dynamic_user.as_ref() {
        todo!()
      }
      if let Some(_v) = self.supplementary_groups.as_ref() {
        todo!()
      }
      if let Some(_v) = self.set_login_environment.as_ref() {
        todo!()
      }
      if let Some(_v) = self.pam_name.as_ref() {
        todo!()
      }
      if let Some(_v) = self.capability_bounding_set.as_ref() {
        todo!()
      }
      if let Some(_v) = self.ambient_capabilities.as_ref() {
        todo!()
      }
      if let Some(v) = self.no_new_privileges.as_ref() {
        writeln!(f, "NoNewPrivileges={}", v.as_ref())?;
      }
      if let Some(_v) = self.secure_bits.as_ref() {
        todo!()
      }
      if let Some(_v) = self.se_linux_context.as_ref() {
        todo!()
      }
      if let Some(_v) = self.app_armor_profile.as_ref() {
        todo!()
      }
      if let Some(_v) = self.smack_process_label.as_ref() {
        todo!()
      }
      if let Some(_v) = self.limit_cpu.as_ref() {
        todo!()
      }
      if let Some(_v) = self.limit_fsize.as_ref() {
        todo!()
      }
      if let Some(_v) = self.limit_data.as_ref() {
        todo!()
      }
      if let Some(_v) = self.limit_stack.as_ref() {
        todo!()
      }
      if let Some(v) = self.limit_core.as_ref() {
        writeln!(f, "LimitCORE={}", v.as_ref())?;
      }
      if let Some(v) = self.limit_rss.as_ref() {
        writeln!(f, "LimitRSS={}", v.as_ref())?;
      }
      if let Some(v) = self.limit_no_file.as_ref() {
        writeln!(f, "LimitNOFILE={}", v.as_ref())?;
      }
      if let Some(_v) = self.limit_as.as_ref() {
        todo!()
      }
      if let Some(v) = self.limit_nproc.as_ref() {
        writeln!(f, "LimitNPROC={}", v.as_ref())?;
      }
      if let Some(_v) = self.limit_mem_lock.as_ref() {
        todo!()
      }
      if let Some(_v) = self.limit_locks.as_ref() {
        todo!()
      }
      if let Some(_v) = self.limit_sig_pending.as_ref() {
        todo!()
      }
      if let Some(_v) = self.limit_msg_queue.as_ref() {
        todo!()
      }
      if let Some(_v) = self.limit_nice.as_ref() {
        todo!()
      }
      if let Some(_v) = self.limit_rt_prio.as_ref() {
        todo!()
      }
      if let Some(_v) = self.limit_rt_time.as_ref() {
        todo!()
      }
      if let Some(_v) = self.umask.as_ref() {
        todo!()
      }
      if let Some(_v) = self.coredump_filter.as_ref() {
        todo!()
      }
      if let Some(_v) = self.keyrin_mode.as_ref() {
        todo!()
      }
      if let Some(v) = self.oom_score_adjust.as_ref() {
        writeln!(f, "OOMScoreAdjust={}", v.as_ref())?;
      }
      if let Some(_v) = self.timer_slack_nsec.as_ref() {
        todo!()
      }
      if let Some(_v) = self.personality.as_ref() {
        todo!()
      }
      if let Some(_v) = self.ignore_sigpipe.as_ref() {
        todo!()
      }
      if let Some(_v) = self.nice.as_ref() {
        todo!()
      }
      if let Some(_v) = self.cpu_scheduling_policy.as_ref() {
        todo!()
      }
      if let Some(_v) = self.cpu_scheduling_priority.as_ref() {
        todo!()
      }
      if let Some(_v) = self.cpu_scheduling_reset_on_fork.as_ref() {
        todo!()
      }
      if let Some(_v) = self.cpu_affinity.as_ref() {
        todo!()
      }
      if let Some(_v) = self.numa_policy.as_ref() {
        todo!()
      }
      if let Some(_v) = self.numa_mask.as_ref() {
        todo!()
      }
      if let Some(_v) = self.io_scheduling_class.as_ref() {
        todo!()
      }
      if let Some(_v) = self.io_scheduling_priority.as_ref() {
        todo!()
      }
      if let Some(v) = self.protect_system.as_ref() {
        writeln!(f, "ProtectSystem={}", v.as_ref())?;
      }
      if let Some(v) = self.protect_home.as_ref() {
        writeln!(f, "ProtectHome={}", v.as_ref())?;
      }
      if let Some(v) = self.runtime_directory.as_ref() {
        writeln!(f, "RuntimeDirectory={}", v.as_ref())?;
      }
      if let Some(_v) = self.state_directory.as_ref() {
        todo!()
      }
      if let Some(_v) = self.cache_directory.as_ref() {
        todo!()
      }
      if let Some(_v) = self.logs_directory.as_ref() {
        todo!()
      }
      if let Some(_v) = self.configuration_directory.as_ref() {
        todo!()
      }
      if let Some(v) = self.runtime_directory_mode.as_ref() {
        writeln!(f, "RuntimeDirectoryMode={}", v.as_ref())?;
      }
      if let Some(_v) = self.state_directory_mode.as_ref() {
        todo!()
      }
      if let Some(_v) = self.cache_directory_mode.as_ref() {
        todo!()
      }
      if let Some(_v) = self.logs_directory_mode.as_ref() {
        todo!()
      }
      if let Some(_v) = self.configuration_directory_mode.as_ref() {
        todo!()
      }
      if let Some(_v) = self.runtime_directory_preserve.as_ref() {
        todo!()
      }
      if let Some(_v) = self.timeout_clean_sec.as_ref() {
        todo!()
      }
      if let Some(_v) = self.read_write_paths.as_ref() {
        todo!()
      }
      if let Some(_v) = self.read_only_paths.as_ref() {
        todo!()
      }
      if let Some(_v) = self.inaccessible_paths.as_ref() {
        todo!()
      }
      if let Some(_v) = self.exec_paths.as_ref() {
        todo!()
      }
      if let Some(_v) = self.no_exec_paths.as_ref() {
        todo!()
      }
      if let Some(_v) = self.temporary_file_system.as_ref() {
        todo!()
      }
      if let Some(v) = self.private_tmp.as_ref() {
        writeln!(f, "PrivateTmp={}", v.as_ref())?;
      }
      if let Some(v) = self.private_devices.as_ref() {
        writeln!(f, "PrivateDevices={}", v.as_ref())?;
      }
      if let Some(_v) = self.private_network.as_ref() {
        todo!()
      }
      if let Some(_v) = self.network_namespace_path.as_ref() {
        todo!()
      }
      if let Some(_v) = self.private_ipc.as_ref() {
        todo!()
      }
      if let Some(_v) = self.ipc_namespace_path.as_ref() {
        todo!()
      }
      if let Some(_v) = self.memory_ksm.as_ref() {
        todo!()
      }
      if let Some(_v) = self.private_users.as_ref() {
        todo!()
      }
      if let Some(_v) = self.protect_hostname.as_ref() {
        todo!()
      }
      if let Some(_v) = self.protect_clock.as_ref() {
        todo!()
      }
      if let Some(v) = self.protect_kernel_tunables.as_ref() {
        writeln!(f, "ProtectKernelTunables={}", v.as_ref())?;
      }
      if let Some(v) = self.protect_kernel_modules.as_ref() {
        writeln!(f, "ProtectKernelModules={}", v.as_ref())?;
      }
      if let Some(_v) = self.protect_kernel_logs.as_ref() {
        todo!()
      }
      if let Some(v) = self.protect_control_groups.as_ref() {
        writeln!(f, "ProtectControlGroups={}", v.as_ref())?;
      }
      if let Some(v) = self.restrict_address_families.as_ref() {
        writeln!(f, "RestrictAddressFamilies={}", v.as_ref())?;
      }
      if let Some(_v) = self.restrict_file_systems.as_ref() {
        todo!()
      }
      if let Some(v) = self.restrict_namespaces.as_ref() {
        writeln!(f, "RestrictNamespaces={}", v.as_ref())?;
      }
      if let Some(_v) = self.lock_personality.as_ref() {
        todo!()
      }
      if let Some(_v) = self.memory_deny_write_execute.as_ref() {
        todo!()
      }
      if let Some(v) = self.restrict_realtime.as_ref() {
        writeln!(f, "RestrictRealtime={}", v.as_ref())?;
      }
      if let Some(_v) = self.restrict_suid_sgid.as_ref() {
        todo!()
      }
      if let Some(_v) = self.remove_ipc.as_ref() {
        todo!()
      }
      if let Some(_v) = self.private_mounts.as_ref() {
        todo!()
      }
      if let Some(_v) = self.mount_flags.as_ref() {
        todo!()
      }
      if let Some(_v) = self.system_call_filter.as_ref() {
        todo!()
      }
      if let Some(_v) = self.system_call_error_number.as_ref() {
        todo!()
      }
      if let Some(v) = self.system_call_architectures.as_ref() {
        writeln!(f, "SystemCallArchitectures={}", v.as_ref())?;
      }
      if let Some(_v) = self.system_call_log.as_ref() {
        todo!()
      }
      for v in self.environment.iter() {
        writeln!(f, "Environment={}", v.as_ref())?;
      }
      if let Some(_v) = self.environment_file.as_ref() {
        todo!()
      }
      if let Some(_v) = self.pass_environment.as_ref() {
        todo!()
      }
      if let Some(_v) = self.unset_environment.as_ref() {
        todo!()
      }
      if let Some(_v) = self.standard_input.as_ref() {
        todo!()
      }
      if let Some(_v) = self.standard_output.as_ref() {
        todo!()
      }
      if let Some(_v) = self.standard_error.as_ref() {
        todo!()
      }
      if let Some(_v) = self.standard_input_text.as_ref() {
        todo!()
      }
      if let Some(_v) = self.standard_input_data.as_ref() {
        todo!()
      }
      if let Some(_v) = self.log_level_max.as_ref() {
        todo!()
      }
      if let Some(_v) = self.log_extra_fields.as_ref() {
        todo!()
      }
      if let Some(_v) = self.log_rate_limit_interval_sec.as_ref() {
        todo!()
      }
      if let Some(_v) = self.log_rate_limit_burst.as_ref() {
        todo!()
      }
      if let Some(_v) = self.log_filter_patterns.as_ref() {
        todo!()
      }
      if let Some(_v) = self.log_namespace.as_ref() {
        todo!()
      }
      if let Some(v) = self.syslog_identifier.as_ref() {
        writeln!(f, "SyslogIdentifier={}", v.as_ref())?;
      }
      if let Some(_v) = self.syslog_facility.as_ref() {
        todo!()
      }
      if let Some(_v) = self.syslog_level.as_ref() {
        todo!()
      }
      if let Some(_v) = self.syslog_level_prefix.as_ref() {
        todo!()
      }
      if let Some(_v) = self.tty_path.as_ref() {
        todo!()
      }
      if let Some(_v) = self.tty_reset.as_ref() {
        todo!()
      }
      if let Some(_v) = self.tty_vhangup.as_ref() {
        todo!()
      }
      if let Some(_v) = self.tty_rows.as_ref() {
        todo!()
      }
      if let Some(_v) = self.tty_columns.as_ref() {
        todo!()
      }
      if let Some(_v) = self.tty_disallocate.as_ref() {
        todo!()
      }
      if let Some(_v) = self.load_credentials.as_ref() {
        todo!()
      }
      if let Some(_v) = self.load_credentials_encrypted.as_ref() {
        todo!()
      }
      if let Some(_v) = self.import_credentials.as_ref() {
        todo!()
      }
      if let Some(_v) = self.set_credentials.as_ref() {
        todo!()
      }
      if let Some(_v) = self.set_credentials_encrypted.as_ref() {
        todo!()
      }
      if let Some(_v) = self.utmp_identifier.as_ref() {
        todo!()
      }
      if let Some(_v) = self.utmp_mode.as_ref() {
        todo!()
      }
      Ok(())
    }
  }

  /// See <https://www.freedesktop.org/software/systemd/man/latest/systemd.kill.html>
  #[derive(Debug, Default, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
  pub struct Kill<S: AsRef<str>> {
    pub kill_mode: Option<S>,
    pub kill_signal: Option<S>,
    pub restart_kill_signal: Option<S>,
    pub send_sig_hup: Option<S>,
    pub send_sig_kill: Option<S>,
    pub final_kill_signal: Option<S>,
    pub watchdog_signal: Option<S>,
  }

  impl<S> fmt::Display for Kill<S>
  where
    S: AsRef<str>,
  {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
      if let Some(v) = self.kill_mode.as_ref() {
        writeln!(f, "KillMode={}", v.as_ref())?;
      }
      if let Some(v) = self.kill_signal.as_ref() {
        writeln!(f, "KillSignal={}", v.as_ref())?;
      }
      if let Some(v) = self.restart_kill_signal.as_ref() {
        writeln!(f, "RestartKillSignal={}", v.as_ref())?;
      }
      if let Some(v) = self.send_sig_hup.as_ref() {
        writeln!(f, "SendSIGHUP={}", v.as_ref())?;
      }
      if let Some(v) = self.send_sig_kill.as_ref() {
        writeln!(f, "SendSIGKILL={}", v.as_ref())?;
      }
      if let Some(v) = self.final_kill_signal.as_ref() {
        writeln!(f, "FinalKillSignal={}", v.as_ref())?;
      }
      if let Some(v) = self.watchdog_signal.as_ref() {
        writeln!(f, "WatchdogSignal={}", v.as_ref())?;
      }
      Ok(())
    }
  }

  /// See <https://www.freedesktop.org/software/systemd/man/latest/systemd.resource-control.html>
  #[derive(Debug, Default, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
  pub struct ResourceControl<S: AsRef<str>> {
    pub cpu_accounting: Option<S>,
    pub cpu_weight: Option<S>,
    pub startup_cpu_weight: Option<S>,
    pub cpu_quota: Option<S>,
    pub cpu_quota_period_sec: Option<S>,
    pub allowed_cpus: Option<S>,
    pub startup_allowed_cpus: Option<S>,
    pub memory_accounting: Option<S>,
    pub memory_min: Option<S>,
    pub memory_low: Option<S>,
    pub startup_memory_low: Option<S>,
    pub default_startup_memory_low: Option<S>,
    pub memory_high: Option<S>,
    pub startup_memory_high: Option<S>,
    pub memory_max: Option<S>,
    pub startup_memory_max: Option<S>,
    pub memory_swap_max: Option<S>,
    pub startup_memory_swap_max: Option<S>,
    pub memory_z_swap_max: Option<S>,
    pub startup_memory_z_swap_max: Option<S>,
    pub memory_z_swap_writeback: Option<S>,
    pub allowed_memory_nodes: Option<S>,
    pub startup_allowed_memory_nodes: Option<S>,
    pub task_accounting: Option<S>,
    pub task_max: Option<S>,
    pub io_accounting: Option<S>,
    pub io_weight: Option<S>,
    pub startup_io_weight: Option<S>,
    pub io_device_weight: Option<S>,
    pub io_read_bandwidth_max: Option<S>,
    pub io_write_bandwidth_max: Option<S>,
    pub io_read_iops_max: Option<S>,
    pub io_write_iops_max: Option<S>,
    pub io_device_latency_target_sec: Option<S>,
    pub ip_accounting: Option<S>,
    pub ip_address_allow: Option<S>,
    pub ip_address_deny: Option<S>,
    pub socket_bind_allow: Option<S>,
    pub socket_bind_deny: Option<S>,
    pub restrict_network_interfaces: Option<S>,
    pub nft_set: Option<S>,
    pub ip_ingress_filter_path: Option<S>,
    pub ip_egress_filter_path: Option<S>,
    pub bpf_program: Option<S>,
    pub device_allow: Option<S>,
    pub device_policy: Option<S>,
    pub slice: Option<S>,
    pub delegate: Option<S>,
    pub delegate_subgroup: Option<S>,
    pub disable_controller: Option<S>,
    pub managed_oom_swap: Option<S>,
    pub managed_oom_memory_pressure: Option<S>,
    pub managed_oom_memory_pressure_limit: Option<S>,
    pub managed_oom_memory_pressure_duration_sec: Option<S>,
    pub memory_pressure_watch: Option<S>,
    pub memory_pressure_threshold_sec: Option<S>,
    pub coredump_receive: Option<S>,
  }

  impl<S> fmt::Display for ResourceControl<S>
  where
    S: AsRef<str>,
  {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
      if let Some(_v) = self.cpu_accounting.as_ref() {
        todo!()
      }
      if let Some(_v) = self.cpu_weight.as_ref() {
        todo!()
      }
      if let Some(_v) = self.startup_cpu_weight.as_ref() {
        todo!()
      }
      if let Some(v) = self.cpu_quota.as_ref() {
        writeln!(f, "CPUQuota={}", v.as_ref())?;
      }
      if let Some(_v) = self.cpu_quota_period_sec.as_ref() {
        todo!()
      }
      if let Some(_v) = self.allowed_cpus.as_ref() {
        todo!()
      }
      if let Some(_v) = self.startup_allowed_cpus.as_ref() {
        todo!()
      }
      if let Some(_v) = self.memory_accounting.as_ref() {
        todo!()
      }
      if let Some(_v) = self.memory_min.as_ref() {
        todo!()
      }
      if let Some(_v) = self.memory_low.as_ref() {
        todo!()
      }
      if let Some(_v) = self.startup_memory_low.as_ref() {
        todo!()
      }
      if let Some(_v) = self.default_startup_memory_low.as_ref() {
        todo!()
      }
      if let Some(v) = self.memory_high.as_ref() {
        writeln!(f, "MemoryHigh={}", v.as_ref())?;
      }
      if let Some(_v) = self.startup_memory_high.as_ref() {
        todo!()
      }
      if let Some(v) = self.memory_max.as_ref() {
        writeln!(f, "MemoryMax={}", v.as_ref())?;
      }
      if let Some(_v) = self.startup_memory_max.as_ref() {
        todo!()
      }
      if let Some(_v) = self.memory_swap_max.as_ref() {
        todo!()
      }
      if let Some(_v) = self.startup_memory_swap_max.as_ref() {
        todo!()
      }
      if let Some(_v) = self.memory_z_swap_max.as_ref() {
        todo!()
      }
      if let Some(_v) = self.startup_memory_z_swap_max.as_ref() {
        todo!()
      }
      if let Some(_v) = self.memory_z_swap_writeback.as_ref() {
        todo!()
      }
      if let Some(_v) = self.allowed_memory_nodes.as_ref() {
        todo!()
      }
      if let Some(_v) = self.startup_allowed_memory_nodes.as_ref() {
        todo!()
      }
      if let Some(_v) = self.task_accounting.as_ref() {
        todo!()
      }
      if let Some(_v) = self.task_max.as_ref() {
        todo!()
      }
      if let Some(_v) = self.io_accounting.as_ref() {
        todo!()
      }
      if let Some(_v) = self.io_weight.as_ref() {
        todo!()
      }
      if let Some(_v) = self.startup_io_weight.as_ref() {
        todo!()
      }
      if let Some(_v) = self.io_device_weight.as_ref() {
        todo!()
      }
      if let Some(_v) = self.io_read_bandwidth_max.as_ref() {
        todo!()
      }
      if let Some(_v) = self.io_write_bandwidth_max.as_ref() {
        todo!()
      }
      if let Some(_v) = self.io_read_iops_max.as_ref() {
        todo!()
      }
      if let Some(_v) = self.io_write_iops_max.as_ref() {
        todo!()
      }
      if let Some(_v) = self.io_device_latency_target_sec.as_ref() {
        todo!()
      }
      if let Some(_v) = self.ip_accounting.as_ref() {
        todo!()
      }
      if let Some(_v) = self.ip_address_allow.as_ref() {
        todo!()
      }
      if let Some(_v) = self.ip_address_deny.as_ref() {
        todo!()
      }
      if let Some(_v) = self.socket_bind_allow.as_ref() {
        todo!()
      }
      if let Some(_v) = self.socket_bind_deny.as_ref() {
        todo!()
      }
      if let Some(_v) = self.restrict_network_interfaces.as_ref() {
        todo!()
      }
      if let Some(_v) = self.nft_set.as_ref() {
        todo!()
      }
      if let Some(_v) = self.ip_ingress_filter_path.as_ref() {
        todo!()
      }
      if let Some(_v) = self.ip_egress_filter_path.as_ref() {
        todo!()
      }
      if let Some(_v) = self.bpf_program.as_ref() {
        todo!()
      }
      if let Some(_v) = self.device_allow.as_ref() {
        todo!()
      }
      if let Some(_v) = self.device_policy.as_ref() {
        todo!()
      }
      if let Some(_v) = self.slice.as_ref() {
        todo!()
      }
      if let Some(_v) = self.delegate.as_ref() {
        todo!()
      }
      if let Some(_v) = self.delegate_subgroup.as_ref() {
        todo!()
      }
      if let Some(_v) = self.disable_controller.as_ref() {
        todo!()
      }
      if let Some(_v) = self.managed_oom_swap.as_ref() {
        todo!()
      }
      if let Some(_v) = self.managed_oom_memory_pressure.as_ref() {
        todo!()
      }
      if let Some(_v) = self.managed_oom_memory_pressure_limit.as_ref() {
        todo!()
      }
      if let Some(_v) = self.managed_oom_memory_pressure_duration_sec.as_ref() {
        todo!()
      }
      if let Some(_v) = self.memory_pressure_watch.as_ref() {
        todo!()
      }
      if let Some(_v) = self.memory_pressure_threshold_sec.as_ref() {
        todo!()
      }
      if let Some(_v) = self.coredump_receive.as_ref() {
        todo!()
      }
      Ok(())
    }
  }

  #[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
  pub struct CommandLine<S: AsRef<str>> {
    pub line: S,
  }

  /// See <https://www.freedesktop.org/software/systemd/man/latest/systemd.timer.html#Options>
  #[derive(Debug, Default, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
  pub struct Timer<S: AsRef<str>> {
    pub on_active_sec: Option<S>,
    pub on_boot_sec: Option<S>,
    pub on_startup_sec: Option<S>,
    pub on_unit_active_sec: Option<S>,
    pub on_unit_inactive_sec: Option<S>,
    pub on_calendar: Option<S>,
    pub accuracy_sec: Option<S>,
    pub randomize_delay_sec: Option<S>,
    pub fixed_random_delay: Option<S>,
    pub on_clock_change: Option<S>,
    pub on_timezone_change: Option<S>,
    pub unit: Option<S>,
    pub persistent: Option<bool>,
    pub wake_system: Option<S>,
    pub remain_after_elapse: Option<S>,
  }

  impl<S> fmt::Display for Timer<S>
  where
    S: AsRef<str>,
  {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
      if let Some(_v) = self.on_active_sec.as_ref() {
        todo!()
      }
      if let Some(_v) = self.on_boot_sec.as_ref() {
        todo!()
      }
      if let Some(_v) = self.on_startup_sec.as_ref() {
        todo!()
      }
      if let Some(_v) = self.on_unit_active_sec.as_ref() {
        todo!()
      }
      if let Some(_v) = self.on_unit_inactive_sec.as_ref() {
        todo!()
      }
      if let Some(v) = self.on_calendar.as_ref() {
        writeln!(f, "OnCalendar={}", v.as_ref())?;
      }
      if let Some(_v) = self.accuracy_sec.as_ref() {
        todo!()
      }
      if let Some(v) = self.randomize_delay_sec.as_ref() {
        writeln!(f, "RandomizedDelaySec={}", v.as_ref())?;
      }
      if let Some(_v) = self.fixed_random_delay.as_ref() {
        todo!()
      }
      if let Some(_v) = self.on_clock_change.as_ref() {
        todo!()
      }
      if let Some(_v) = self.on_timezone_change.as_ref() {
        todo!()
      }
      if let Some(_v) = self.unit.as_ref() {
        todo!()
      }
      if let Some(v) = self.persistent {
        writeln!(f, "Persistent={v}")?;
      }
      if let Some(_v) = self.wake_system.as_ref() {
        todo!()
      }
      if let Some(_v) = self.remain_after_elapse.as_ref() {
        todo!()
      }
      Ok(())
    }
  }
}

/// Ensure the slice is present in `/etc/systemd/system`
#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct EnsureSystemdServiceConfig<S: AsRef<str>> {
  pub name: String,
  pub config: Config<S>,
}

impl<'h, H, S> NamedTask<&'h H> for EnsureSystemdServiceConfig<S>
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  S: AsRef<str>,
  Self: Send + Sync,
{
  const NAME: TaskName = TaskName::new("EnsureSystemdServiceConfig");
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, Error)]
pub enum EnsureSystemdServiceConfigError {
  #[error("failed to ensure service file is present")]
  EnsureFile(#[source] EnsureFileError),
}

#[async_trait]
impl<'h, H, S> AsyncFn<&'h H> for EnsureSystemdServiceConfig<S>
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  S: AsRef<str>,
  Self: Send + Sync,
{
  type Output = TaskResult<(), EnsureSystemdServiceConfigError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let path = PathBuf::from("/etc/systemd/system/").join(format!("{}.service", self.name));
    EnsureFile::new(path)
      .content(self.config.to_string())
      .run(host)
      .await
      .map_err(EnsureSystemdServiceConfigError::EnsureFile)
  }
}

/// Ensure the slice is present in `/etc/systemd/system`
#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct EnsureSystemdSliceConfig {
  pub name: String,
  pub config: SystemdSliceConfig,
}

/// https://www.freedesktop.org/software/systemd/man/latest/systemd.resource-control.html#
#[derive(Default, Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct SystemdSliceConfig {
  pub unit: Option<config::Unit<String>>,
  pub slice: Option<config::Slice<String>>,
}

impl fmt::Display for SystemdSliceConfig {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    if let Some(unit) = self.unit.as_ref() {
      writeln!(f, "[Unit]")?;
      writeln!(f, "{unit}")?;
    }
    if let Some(slice) = self.slice.as_ref() {
      writeln!(f, "[Slice]")?;
      writeln!(f, "{slice}")?;
    }
    Ok(())
  }
}

impl<'h, H: ExecHost + LinuxUserHost + LinuxFsHost> NamedTask<&'h H> for EnsureSystemdSliceConfig
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  const NAME: TaskName = TaskName::new("EnsureSystemdSliceConfig");
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, Error)]
pub enum EnsureSystemdSliceConfigError {
  #[error("failed to ensure slice file is present")]
  EnsureFile(#[source] EnsureFileError),
}

#[async_trait]
impl<'h, H: ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for EnsureSystemdSliceConfig
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<(), EnsureSystemdSliceConfigError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let path = PathBuf::from("/etc/systemd/system/").join(format!("{}.slice", self.name));
    EnsureFile::new(path)
      .content(self.config.to_string())
      .run(host)
      .await
      .map_err(EnsureSystemdSliceConfigError::EnsureFile)
  }
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct SystemdUnit {
  name: String,
  target: SystemdTarget,
}

#[derive(Debug, Clone, Copy, Eq, PartialEq, Serialize, Deserialize)]
pub struct SystemdTarget {
  enabled: Option<bool>,
  state: Option<SystemdState>,
}

#[derive(Debug, Clone, Copy, Eq, PartialEq, Serialize, Deserialize)]
pub enum SystemdState {
  Inactive,
  Active,
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, Error)]
pub enum SystemdUnitError {
  #[error("failed to read system status")]
  Status(#[from] SystemdGetStatusError),
  #[error("failed to set `enabled` status")]
  Enabled(String),
  #[error("failed to set active state")]
  State(String),
}

#[async_trait]
impl<'h, H: ExecHost> AsyncFn<&'h H> for SystemdUnit {
  type Output = TaskResult<(), SystemdUnitError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let mut changed = false;
    let status = get_status(&self.name).await?;
    if status.load_state == LoadState::NotFound && self.target.state == Some(SystemdState::Inactive) {
      return Ok(TaskSuccess { changed, output: () });
    }

    if let Some(enabled) = self.target.enabled {
      let mut cmd = Command::new("systemctl");
      cmd = cmd.arg(if enabled { "enable" } else { "disable" });
      cmd = cmd.arg(&self.name);
      host.exec(&cmd).map_err(|e| SystemdUnitError::Enabled(e.to_string()))?;
      changed = true;
    }
    if let Some(state) = self.target.state {
      let mut cmd = Command::new("systemctl");
      cmd = cmd.arg(match state {
        SystemdState::Active => "start",
        SystemdState::Inactive => "stop",
      });
      cmd = cmd.arg(&self.name);
      host.exec(&cmd).map_err(|e| SystemdUnitError::State(e.to_string()))?;
      changed = true;
    }

    Ok(TaskSuccess { changed, output: () })
  }
}

impl SystemdUnit {
  pub fn new(name: impl ToString) -> Self {
    Self {
      name: name.to_string(),
      target: SystemdTarget {
        enabled: None,
        state: None,
      },
    }
  }

  pub fn enabled(mut self, enabled: bool) -> Self {
    self.target.enabled = Some(enabled);
    self
  }

  pub fn state(mut self, state: SystemdState) -> Self {
    self.target.state = Some(state);
    self
  }
}

#[derive(Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
pub struct SystemdStatus {
  pub restart: Option<Restart>,
  pub active_state: ActiveState,
  pub load_state: LoadState,
}

#[derive(Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
pub enum Restart {
  Yes,
  No,
  Always,
  OnFailure,
}

#[derive(Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
pub enum ActiveState {
  Active,
  Failed,
  Inactive,
}

#[derive(Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
pub enum LoadState {
  NotFound,
  Loaded,
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, Error)]
pub enum SystemdGetStatusError {
  #[error("failed to execute systemctl show {1:?}")]
  Exec(#[source] ExecError, String),
  #[error("failed to read systemctl show {1:?} stdout as UTF-8: {0}")]
  SystemctlShowFormat(String, String),
  #[error("unexpected value for systemd show {0:?}: key {1:?}, value {2:?}")]
  UnexpectedValue(String, String, String),
  #[error("missing value for systemd show {0:?}: key {1:?}")]
  MissingValue(String, String),
}
// LoadError=org.freedesktop.systemd1.NoSuchUnit "Unit foohiudsahu.service not found."
pub async fn get_status(name: &str) -> Result<SystemdStatus, SystemdGetStatusError> {
  let cmd = Command::new("systemctl").arg("show").arg(name);
  let out = LocalLinux
    .exec(&cmd)
    .map_err(|e| SystemdGetStatusError::Exec(e, name.to_string()))?
    .stdout;
  let out =
    String::from_utf8(out).map_err(|e| SystemdGetStatusError::SystemctlShowFormat(e.to_string(), name.to_string()))?;
  let mut restart: Option<Restart> = None;
  let mut active_state: Option<ActiveState> = None;
  let mut load_state: Option<LoadState> = None;
  for l in out.lines() {
    if let Some(eq) = l.find('=') {
      let key = &l[0..eq];
      let val = &l[(eq + 1)..];
      match key {
        "Restart" => {
          restart = Some(match val {
            "yes" => Restart::Yes,
            "no" => Restart::No,
            "always" => Restart::Always,
            "on-failure" => Restart::OnFailure,
            val => {
              return Err(SystemdGetStatusError::UnexpectedValue(
                name.to_string(),
                key.to_string(),
                val.to_string(),
              ))
            }
          })
        }
        "ActiveState" => {
          active_state = Some(match val {
            "active" => ActiveState::Active,
            "failed" => ActiveState::Failed,
            "inactive" => ActiveState::Inactive,
            val => {
              return Err(SystemdGetStatusError::UnexpectedValue(
                name.to_string(),
                key.to_string(),
                val.to_string(),
              ))
            }
          })
        }
        "LoadState" => {
          load_state = Some(match val {
            "not-found" => LoadState::NotFound,
            "loaded" => LoadState::Loaded,
            val => {
              return Err(SystemdGetStatusError::UnexpectedValue(
                name.to_string(),
                key.to_string(),
                val.to_string(),
              ))
            }
          })
        }
        _ => {}
      }
    }
  }
  Ok(SystemdStatus {
    restart,
    active_state: active_state
      .ok_or_else(|| SystemdGetStatusError::MissingValue(name.to_string(), "ActiveState".to_string()))?,
    load_state: load_state
      .ok_or_else(|| SystemdGetStatusError::MissingValue(name.to_string(), "LoadState".to_string()))?,
  })
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, Error)]
pub enum SystemdRestartUnitError {
  #[error("failed to execute systemctl restart {0:?}")]
  Exec(#[source] ExecError, String),
}

pub fn restart_systemd_unit<H>(name: &str, host: &H) -> Result<(), SystemdRestartUnitError>
where
  H: ExecHost,
{
  let cmd = Command::new("systemctl").arg("restart").arg(name);
  host
    .exec(&cmd)
    .map_err(|e| SystemdRestartUnitError::Exec(e, name.to_string()))?;
  Ok(())
}
