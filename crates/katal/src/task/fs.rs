use crate::target::Target;
use crate::task::{AsyncFn, TaskResult, TaskSuccess};
use async_trait::async_trait;
use asys::common::fs::{CreateDirError, CreateFileError, FsHost, Metadata, MetadataError};
use asys::linux::fs::{CreateSymlinkError, FileMode, LinuxFsHost, LinuxMetadata, SetFileModeError, SetFileOwnerError};
use asys::linux::user::{
  Gid, GroupFromNameError, GroupRef, LinuxGroup, LinuxUser, LinuxUserHost, Uid, UserFromNameError, UserRef,
};
use katal_loader::tree::{FileType, FsxDirEntry, ReadDirError, ReadError, ReadFsx, ReadLinkError};
use serde::{Deserialize, Serialize};
use std::path::PathBuf;

// For any file mode x, this represents the target (x & x.and) | x.or
#[derive(Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
pub struct FileModeTarget {
  and: FileMode,
  or: FileMode,
}

impl FileModeTarget {
  pub const ANY: Self = Self {
    and: FileMode::ALL_BITS,
    or: FileMode::NONE,
  };

  pub const fn new(fm: FileMode) -> Self {
    Self { and: fm, or: fm }
  }

  /// Returns a new target where the provided bits can be either set or unset.
  pub const fn any(self, fm: FileMode) -> Self {
    Self {
      and: FileMode::from_st_mode(self.and.to_raw() | fm.to_raw()),
      or: FileMode::from_st_mode(self.or.to_raw() & !fm.to_raw()),
    }
  }

  /// Returns a new target where the provided bits must be set.
  pub const fn set(self, fm: FileMode) -> Self {
    Self {
      and: FileMode::from_st_mode(self.and.to_raw() | fm.to_raw()),
      or: FileMode::from_st_mode(self.or.to_raw() | fm.to_raw()),
    }
  }

  /// Returns a new target where the provided bits must be unset.
  pub const fn unset(self, fm: FileMode) -> Self {
    Self {
      and: FileMode::from_st_mode(self.and.to_raw() & !fm.to_raw()),
      or: FileMode::from_st_mode(self.or.to_raw() & !fm.to_raw()),
    }
  }

  /// Compute what the target file mode should be for the provided input.
  pub fn apply_to(self, old: FileMode) -> FileMode {
    FileMode::from_st_mode((old.to_raw() & self.and.to_raw()) | self.or.to_raw())
  }
}

impl From<FileMode> for FileModeTarget {
  fn from(fm: FileMode) -> Self {
    Self::new(fm)
  }
}

impl<'i> From<&'i FileMode> for FileModeTarget {
  fn from(fm: &'i FileMode) -> Self {
    Self::new(*fm)
  }
}

impl Default for FileModeTarget {
  fn default() -> Self {
    Self::ANY
  }
}

impl Target<FileMode> for FileModeTarget {
  fn test(&self, item: &FileMode) -> bool {
    self.apply_to(*item) == *item
  }
}

#[derive(Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
pub struct UidTarget(Option<Uid>);

impl UidTarget {
  pub const ANY: Self = Self(None);

  pub const fn new(val: Uid) -> Self {
    Self(Some(val))
  }

  pub fn test(self, val: Uid) -> bool {
    self.0.map(|t| t == val).unwrap_or(true)
  }

  pub fn apply_to(self, old: Uid) -> Uid {
    self.0.unwrap_or(old)
  }
}

#[derive(Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
pub struct GidTarget(Option<Gid>);

impl GidTarget {
  pub const ANY: Self = Self(None);

  pub const fn new(val: Gid) -> Self {
    Self(Some(val))
  }

  pub fn test(self, val: Gid) -> bool {
    self.0.map(|t| t == val).unwrap_or(true)
  }

  pub fn apply_to(self, old: Gid) -> Gid {
    self.0.unwrap_or(old)
  }
}

/// Task equivalent to `mkdir`
pub struct EnsureDir {
  /// Absolute path for the directory
  path: PathBuf,
  // /// If the path already exist, allow the final link to be symlink to a directory
  // allow_link: bool,
  // /// If the final link already exists but has the wrong type, allow
  // /// removing/unlinking it in order to create a directory instead.
  // force: bool,
  target: DirTarget,
}

pub struct DirTarget {
  /// Enforced owner of the directory
  owner: Option<UserRef>,
  /// Enforced group of the directory
  group: Option<GroupRef>,
  /// Enforce file mode for the directory
  mode: FileModeTarget,
}

impl DirTarget {
  pub const ANY: Self = Self {
    owner: None,
    group: None,
    mode: FileModeTarget::ANY,
  };
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, thiserror::Error)]
pub enum EnsureDirError {
  #[error("failed to read metadata")]
  Metadata(#[from] MetadataError),
  #[error("failed to create directory")]
  Create(#[source] CreateDirError),
  #[error("failed to set directory mode")]
  Mode(#[from] SetFileModeError),
  #[error("failed to resolve uid from name")]
  UserFromName(#[source] UserFromNameError, String),
  #[error("failed to resolve gid from name")]
  GroupFromName(#[source] GroupFromNameError, String),
  #[error("failed to set directory owner")]
  Owner(#[from] SetFileOwnerError),
}

#[async_trait]
impl<'h, H: LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for EnsureDir
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<(), EnsureDirError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let mut changed = false;
    let old_meta = match host.create_dir(&self.path) {
      Ok(()) => {
        changed = true;
        host.metadata(&self.path).map_err(EnsureDirError::Metadata)?
      }
      Err(e @ CreateDirError::AlreadyExists) => {
        let m = host.metadata(&self.path).map_err(EnsureDirError::Metadata)?;
        if !m.is_dir() {
          return Err(EnsureDirError::Create(e));
        }
        m
      }
      Err(e) => return Err(EnsureDirError::Create(e)),
    };
    {
      let old_mode = old_meta.mode();
      if !self.target.mode.test(&old_mode) {
        host
          .set_file_mode(&self.path, self.target.mode.apply_to(old_mode))
          .map_err(EnsureDirError::Mode)?;
        changed = true;
      }
    }
    {
      let uid_target = match &self.target.owner {
        Some(UserRef::Id(uid)) => UidTarget::new(*uid),
        Some(UserRef::Name(name)) => UidTarget::new(
          host
            .user_from_name(name)
            .map_err(|e| EnsureDirError::UserFromName(e, name.to_string()))?
            .uid(),
        ),
        None => UidTarget::ANY,
      };
      let gid_target = match &self.target.group {
        Some(GroupRef::Id(gid)) => GidTarget::new(*gid),
        Some(GroupRef::Name(name)) => GidTarget::new(
          host
            .group_from_name(name)
            .map_err(|e| EnsureDirError::GroupFromName(e, name.to_string()))?
            .gid(),
        ),
        None => GidTarget::ANY,
      };

      let old_uid = old_meta.uid();
      let old_gid = old_meta.gid();
      let new_uid = if !uid_target.test(old_uid) {
        Some(uid_target.apply_to(old_uid))
      } else {
        None
      };
      let new_gid = if !gid_target.test(old_gid) {
        Some(gid_target.apply_to(old_gid))
      } else {
        None
      };
      if new_uid.is_some() || new_gid.is_some() {
        host
          .set_file_owner(&self.path, new_uid, new_gid)
          .map_err(EnsureDirError::Owner)?;
        changed = true;
      }
    }
    Ok(TaskSuccess { changed, output: () })
  }
}

impl EnsureDir {
  pub fn new(path: PathBuf) -> Self {
    Self {
      path,
      // allow_link: true,
      // force: false,
      target: DirTarget::ANY,
    }
  }

  pub fn owner(mut self, owner: UserRef) -> Self {
    self.target.owner = Some(owner);
    self
  }

  pub fn group(mut self, group: GroupRef) -> Self {
    self.target.group = Some(group);
    self
  }

  pub fn mode(mut self, mode: impl Into<FileModeTarget>) -> Self {
    self.target.mode = mode.into();
    self
  }
}

/// Ensure a file matches a target state
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct EnsureFile {
  /// Absolute path for the file
  path: PathBuf,
  target: FileTarget,
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct FileTarget {
  owner: UidTarget,
  group: GidTarget,
  content: Option<Vec<u8>>,
  mode: FileModeTarget,
}

impl FileTarget {
  const ANY: Self = Self {
    owner: UidTarget::ANY,
    group: GidTarget::ANY,
    content: None,
    mode: FileModeTarget::ANY,
  };
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, thiserror::Error)]
pub enum EnsureFileError {
  #[error("failed to read metadata")]
  Metadata(#[from] MetadataError),
  #[error("failed to create file")]
  Create(#[source] CreateFileError),
  #[error("failed to set file mode")]
  FileMode(#[from] SetFileModeError),
  #[error("failed to set file owner")]
  FileOwner(#[from] SetFileOwnerError),
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for EnsureFile
where
  H: LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<(), EnsureFileError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let mut changed = false;
    let old_meta = match self.target.content.as_deref() {
      Some(content) => match host.create_file(&self.path, content) {
        Ok(()) => {
          changed = true;
          host.metadata(&self.path)?
        }
        Err(e @ CreateFileError::AlreadyExists(_)) => {
          let m = host.metadata(&self.path)?;
          if !m.is_file() {
            return Err(EnsureFileError::Create(e));
          }
          match host.read_file(&self.path) {
            Ok(old_content) if old_content.as_slice() == content => m,
            _ => {
              return Err(EnsureFileError::Create(e));
            }
          }
        }
        Err(e @ CreateFileError::PermissionDenied(_)) => {
          let m = host.metadata(&self.path)?;
          if !m.is_file() {
            return Err(EnsureFileError::Create(e));
          }
          let old_mode = m.mode();
          let privileged_mode = old_mode | FileMode::OWNER_READ | FileMode::OWNER_WRITE;
          if privileged_mode == old_mode {
            return Err(EnsureFileError::Create(e));
          }
          host.set_file_mode(&self.path, privileged_mode)?;
          match host.create_file(&self.path, content) {
            Ok(_) => {
              changed = true;
              host.set_file_mode(&self.path, old_mode)?;
              host.metadata(&self.path)?
            }
            Err(_) => {
              host.set_file_mode(&self.path, old_mode)?;
              return Err(EnsureFileError::Create(e));
            }
          }
        }
        Err(e) => return Err(EnsureFileError::Create(e)),
      },
      None => {
        if !self.path.exists() {
          host.create_file(&self.path, []).map_err(EnsureFileError::Create)?;
        }
        host.metadata(&self.path)?
      }
    };
    {
      let old_mode = old_meta.mode();
      if !self.target.mode.test(&old_mode) {
        host.set_file_mode(&self.path, self.target.mode.apply_to(old_mode))?;
        changed = true;
      }
    }
    {
      let old_uid = old_meta.uid();
      let old_gid = old_meta.gid();
      let new_uid = if !self.target.owner.test(old_uid) {
        Some(self.target.owner.apply_to(old_uid))
      } else {
        None
      };
      let new_gid = if !self.target.group.test(old_gid) {
        Some(self.target.group.apply_to(old_gid))
      } else {
        None
      };
      if new_uid.is_some() || new_gid.is_some() {
        host.set_file_owner(&self.path, new_uid, new_gid)?;
        changed = true;
      }
    }
    Ok(TaskSuccess { changed, output: () })
  }
}

impl EnsureFile {
  pub fn new(path: impl Into<PathBuf>) -> Self {
    Self {
      path: path.into(),
      // allow_link: true,
      // force: false,
      target: FileTarget::ANY,
    }
  }

  pub fn owner(mut self, owner: Uid) -> Self {
    self.target.owner = UidTarget::new(owner);
    self
  }

  pub fn group(mut self, group: Gid) -> Self {
    self.target.group = GidTarget::new(group);
    self
  }

  pub fn mode(mut self, mode: impl Into<FileModeTarget>) -> Self {
    self.target.mode = mode.into();
    self
  }

  pub fn content(mut self, content: impl AsRef<[u8]>) -> Self {
    self.target.content = Some(content.as_ref().to_vec());
    self
  }
}

/// Task equivalent to `ln -s`, but specific to directories
pub struct EnsureDirSymlink {
  /// Absolute path for the symlink
  path: PathBuf,
  target: DirSymlinkTarget,
}

pub struct DirSymlinkTarget {
  /// Enforced owner of the symlink
  owner: Option<Uid>,
  /// Enforced group of the symlink
  group: Option<Gid>,
  /// Enforced file mode for the symlink
  mode: FileModeTarget,
  /// Directory pointed by the symlink.
  pointee: Option<PathBuf>,
}

impl DirSymlinkTarget {
  pub const ANY: Self = Self {
    owner: None,
    group: None,
    mode: FileModeTarget::ANY,
    pointee: None,
  };
}

impl EnsureDirSymlink {
  pub fn new(path: PathBuf) -> Self {
    Self {
      path,
      target: DirSymlinkTarget::ANY,
    }
  }

  pub fn points_to(mut self, pointee: PathBuf) -> Self {
    self.target.pointee = Some(pointee);
    self
  }

  pub fn owner(mut self, owner: Uid) -> Self {
    self.target.owner = Some(owner);
    self
  }

  pub fn group(mut self, group: Gid) -> Self {
    self.target.group = Some(group);
    self
  }

  pub fn mode(mut self, mode: impl Into<FileModeTarget>) -> Self {
    self.target.mode = mode.into();
    self
  }
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, thiserror::Error)]
pub enum EnsureDirSymlinkError {
  #[error("failed to read symlink metadata")]
  Metadata(#[source] MetadataError),
  #[error("an non-symlink entry already exists")]
  Conflict(#[source] CreateSymlinkError),
  #[error("failed to read old symlink")]
  ReadOld(String),
  #[error("failed to remove old symlink")]
  RemoveOld(String),
  #[error("failed to create symlink")]
  Create(#[source] CreateSymlinkError),
  #[error("failed to set symlink mode")]
  FileMode(#[from] SetFileModeError),
  #[error("failed to set symlink owner")]
  FileOwner(#[source] SetFileOwnerError),
}

#[async_trait]
impl<'h, H: LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for EnsureDirSymlink
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<(), EnsureDirSymlinkError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let mut changed = false;
    let old_meta = match host.create_symlink(&self.path, self.target.pointee.as_ref().unwrap()) {
      Ok(()) => {
        changed = true;
        host
          .symlink_metadata(&self.path)
          .map_err(EnsureDirSymlinkError::Metadata)?
      }
      Err(e @ CreateSymlinkError::AlreadyExists) => {
        let m = host
          .symlink_metadata(&self.path)
          .map_err(EnsureDirSymlinkError::Metadata)?;
        if !m.is_symlink() {
          return Err(EnsureDirSymlinkError::Conflict(e));
        }
        // TODO: Avoid directly using `fs`.
        let old_pointee = std::fs::read_link(&self.path).map_err(|e| EnsureDirSymlinkError::ReadOld(e.to_string()))?;
        if &old_pointee == self.target.pointee.as_ref().unwrap() {
          m
        } else {
          std::fs::remove_file(&self.path).map_err(|e| EnsureDirSymlinkError::RemoveOld(e.to_string()))?;
          host
            .create_symlink(&self.path, self.target.pointee.as_ref().unwrap())
            .map_err(EnsureDirSymlinkError::Create)?;
          host
            .symlink_metadata(&self.path)
            .map_err(EnsureDirSymlinkError::Metadata)?
        }
      }
      Err(e) => return Err(EnsureDirSymlinkError::Create(e)),
    };
    {
      let old_mode = old_meta.mode();
      if !self.target.mode.test(&old_mode) {
        host
          .set_file_mode(&self.path, self.target.mode.apply_to(old_mode))
          .map_err(EnsureDirSymlinkError::FileMode)?;
        changed = true;
      }
    }
    {
      let uid_target = match &self.target.owner {
        Some(uid) => UidTarget::new(*uid),
        None => UidTarget::ANY,
      };
      let gid_target = match &self.target.group {
        Some(gid) => GidTarget::new(*gid),
        None => GidTarget::ANY,
      };

      let old_uid = old_meta.uid();
      let old_gid = old_meta.gid();
      let new_uid = if !uid_target.test(old_uid) {
        Some(uid_target.apply_to(old_uid))
      } else {
        None
      };
      let new_gid = if !gid_target.test(old_gid) {
        Some(gid_target.apply_to(old_gid))
      } else {
        None
      };
      if new_uid.is_some() || new_gid.is_some() {
        host
          .set_file_owner(&self.path, new_uid, new_gid)
          .map_err(EnsureDirSymlinkError::FileOwner)?;
        changed = true;
      }
    }
    Ok(TaskSuccess { changed, output: () })
  }
}

/// Task equivalent to `ln -s`, but specific to files
pub struct EnsureFileSymlink {
  /// Absolute path for the symlink
  path: PathBuf,
  target: FileSymlinkTarget,
}

pub struct FileSymlinkTarget {
  /// Enforced owner of the symlink
  owner: Option<Uid>,
  /// Enforced group of the symlink
  group: Option<Gid>,
  /// Enforced file mode for the symlink
  mode: FileModeTarget,
  /// File pointed by the symlink.
  pointee: Option<PathBuf>,
}

impl FileSymlinkTarget {
  pub const ANY: Self = Self {
    owner: None,
    group: None,
    mode: FileModeTarget::ANY,
    pointee: None,
  };
}

impl EnsureFileSymlink {
  pub fn new(path: PathBuf) -> Self {
    Self {
      path,
      target: FileSymlinkTarget::ANY,
    }
  }

  pub fn points_to(mut self, pointee: PathBuf) -> Self {
    self.target.pointee = Some(pointee);
    self
  }

  pub fn owner(mut self, owner: Uid) -> Self {
    self.target.owner = Some(owner);
    self
  }

  pub fn group(mut self, group: Gid) -> Self {
    self.target.group = Some(group);
    self
  }

  pub fn mode(mut self, mode: impl Into<FileModeTarget>) -> Self {
    self.target.mode = mode.into();
    self
  }
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, thiserror::Error)]
pub enum EnsureFileSymlinkError {
  #[error("failed to read symlink metadata")]
  Metadata(#[from] MetadataError),
  #[error("failed to create symlink")]
  Create(#[source] CreateSymlinkError),
  #[error("failed to read symlink")]
  ReadLink(String),
  #[error("failed to remove symlink")]
  RemoveLink(String),
  #[error("failed to set symlink mode")]
  Mode(#[from] SetFileModeError),
  #[error("failed to set symlink owner")]
  Owner(#[from] SetFileOwnerError),
}

#[async_trait]
impl<'h, H: LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for EnsureFileSymlink
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<(), EnsureFileSymlinkError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let mut changed = false;
    let target_pointee = self.target.pointee.as_ref().expect("non-supported pointee path");
    let target_pointee = target_pointee.as_path();
    let old_meta = match host.create_symlink(&self.path, target_pointee) {
      Ok(()) => {
        changed = true;
        host
          .symlink_metadata(&self.path)
          .map_err(EnsureFileSymlinkError::Metadata)?
      }
      Err(e @ CreateSymlinkError::AlreadyExists) => {
        let m = host
          .symlink_metadata(&self.path)
          .map_err(EnsureFileSymlinkError::Metadata)?;
        if !m.is_symlink() {
          return Err(EnsureFileSymlinkError::Create(e));
        }
        // TODO: Avoid directly using `fs`.
        let old_pointee =
          std::fs::read_link(&self.path).map_err(|e| EnsureFileSymlinkError::ReadLink(e.to_string()))?;
        if old_pointee.as_path() == target_pointee {
          m
        } else {
          // TODO: Avoid directly using `fs`.
          std::fs::remove_file(&self.path).map_err(|e| EnsureFileSymlinkError::RemoveLink(e.to_string()))?;
          host
            .create_symlink(&self.path, target_pointee)
            .map_err(EnsureFileSymlinkError::Create)?;
          host
            .symlink_metadata(&self.path)
            .map_err(EnsureFileSymlinkError::Metadata)?
        }
      }
      Err(e) => return Err(EnsureFileSymlinkError::Create(e)),
    };
    {
      let old_mode = old_meta.mode();
      if !self.target.mode.test(&old_mode) {
        host
          .set_file_mode(&self.path, self.target.mode.apply_to(old_mode))
          .map_err(EnsureFileSymlinkError::Mode)?;
        changed = true;
      }
    }
    {
      let uid_target = match &self.target.owner {
        Some(uid) => UidTarget::new(*uid),
        None => UidTarget::ANY,
      };
      let gid_target = match &self.target.group {
        Some(gid) => GidTarget::new(*gid),
        None => GidTarget::ANY,
      };

      let old_uid = old_meta.uid();
      let old_gid = old_meta.gid();
      let new_uid = if !uid_target.test(old_uid) {
        Some(uid_target.apply_to(old_uid))
      } else {
        None
      };
      let new_gid = if !gid_target.test(old_gid) {
        Some(gid_target.apply_to(old_gid))
      } else {
        None
      };
      if new_uid.is_some() || new_gid.is_some() {
        host
          .set_file_owner(&self.path, new_uid, new_gid)
          .map_err(EnsureFileSymlinkError::Owner)?;
        changed = true;
      }
    }
    Ok(TaskSuccess { changed, output: () })
  }
}

/// Similar to `cp --recursive`
pub struct EnsureTree<Tree>
where
  Tree: ReadFsx<[String]>,
{
  /// Absolute path for the tree root (will be a directory created with `EnsureDir`
  path: PathBuf,
  target: TreeTarget<Tree>,
}

pub struct TreeTarget<Tree>
where
  Tree: ReadFsx<[String]>,
{
  /// Input tree to apply
  tree: Tree,
  /// Enforced owner of the directories and files
  owner: Option<Uid>,
  /// Enforced group of the directories and files
  group: Option<Gid>,
  /// Enforced file mode for directories
  dir_mode: FileModeTarget,
  /// Enforced file mode for regular files
  file_mode: FileModeTarget,
  // TODO: Configure how to deal with existing files
}

impl<Tree> EnsureTree<Tree>
where
  Tree: ReadFsx<[String]>,
{
  pub fn new(path: PathBuf, tree: Tree) -> Self {
    Self {
      path,
      target: TreeTarget {
        tree,
        owner: None,
        group: None,
        dir_mode: FileModeTarget::default(),
        file_mode: FileModeTarget::default(),
      },
    }
  }

  pub fn owner(mut self, owner: Uid) -> Self {
    self.target.owner = Some(owner);
    self
  }

  pub fn group(mut self, group: Gid) -> Self {
    self.target.group = Some(group);
    self
  }

  pub fn dir_mode(mut self, mode: impl Into<FileModeTarget>) -> Self {
    self.target.dir_mode = mode.into();
    self
  }

  pub fn file_mode(mut self, mode: impl Into<FileModeTarget>) -> Self {
    self.target.file_mode = mode.into();
    self
  }
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize, thiserror::Error)]
pub enum EnsureTreeError<PathBuf = Vec<String>> {
  #[error("failed to ensure root dir")]
  EnsureRootDir(#[source] EnsureDirError),
  #[error("failed to read tree root")]
  ReadRootDir(#[source] ReadDirError),
  #[error("failed to read tree dir {1:?}")]
  ReadTreeDir(#[source] ReadDirError, PathBuf),
  #[error("failed to ensure tree dir {1:?}")]
  EnsureTreeDir(#[source] EnsureDirError, PathBuf),
  #[error("failed to read tree file {1:?}")]
  ReadTreeFile(#[source] ReadError, PathBuf),
  #[error("failed to ensure tree file {1:?}")]
  EnsureTreeFile(#[source] EnsureFileError, PathBuf),
  #[error("failed to read tree symlink {1:?}")]
  ReadTreeSymlink(#[source] ReadLinkError, PathBuf),
  #[error("failed to ensure tree symlink {1:?}")]
  EnsureTreeSymlink(#[source] EnsureFileSymlinkError, PathBuf),
  #[error("unsupported file type {1:?} at {0:?}")]
  InvalidFileType(PathBuf, FileType),
}

#[async_trait]
impl<'h, H: LinuxUserHost + LinuxFsHost, Tree> AsyncFn<&'h H> for EnsureTree<Tree>
where
  <H as FsHost>::Metadata: LinuxMetadata,
  Tree: ReadFsx<[String]> + Send + Sync,
  Tree::PathBuf: Clone + Send + Sync,
  Tree::DirEntry: Send + Sync,
{
  type Output = TaskResult<(), EnsureTreeError<Tree::PathBuf>>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let mut changed = EnsureDir::new(self.path.clone())
      .run(host)
      .await
      .map_err(EnsureTreeError::EnsureRootDir)?
      .changed;
    let mut stack: Vec<Tree::DirEntry> = self
      .target
      .tree
      .read_dir([])
      .await
      .map_err(EnsureTreeError::ReadRootDir)?;
    while let Some(entry) = stack.pop() {
      let mut path = self.path.clone();
      for segment in entry.path().as_ref() {
        path.push(segment);
      }
      match entry.file_type() {
        FileType::File => {
          let content = self
            .target
            .tree
            .read(entry.path().as_ref())
            .await
            .map_err(|e| EnsureTreeError::ReadTreeFile(e, entry.path().clone()))?;
          let mut cmd = EnsureFile::new(path).content(content).mode(self.target.file_mode);
          if let Some(owner) = self.target.owner {
            cmd = cmd.owner(owner)
          }
          if let Some(group) = self.target.group {
            cmd = cmd.group(group)
          }
          changed = cmd
            .run(host)
            .await
            .map_err(|e| EnsureTreeError::EnsureTreeFile(e, entry.path().clone()))?
            .changed
            || changed
        }
        FileType::Symlink => {
          let pointee = self
            .target
            .tree
            .read_link(entry.path().as_ref())
            .await
            .map_err(|e| EnsureTreeError::ReadTreeSymlink(e, entry.path().clone()))?;
          let pointee = pointee.as_ref().join("/");
          // ensure symlink is absolute
          // todo: expose an option to control this behavior, absolute by default is somewhat surprising
          let pointee = path.parent().unwrap_or(&path).join(pointee);
          let mut cmd = EnsureFileSymlink::new(path).points_to(pointee);
          if let Some(owner) = self.target.owner {
            cmd = cmd.owner(owner)
          }
          if let Some(group) = self.target.group {
            cmd = cmd.group(group)
          }
          changed = cmd
            .run(host)
            .await
            .map_err(|e| EnsureTreeError::EnsureTreeSymlink(e, entry.path().clone()))?
            .changed
            || changed
        }
        FileType::Dir => {
          let mut cmd = EnsureDir::new(path).mode(self.target.dir_mode);
          if let Some(owner) = self.target.owner {
            cmd = cmd.owner(UserRef::Id(owner))
          }
          if let Some(group) = self.target.group {
            cmd = cmd.group(GroupRef::Id(group))
          }
          changed = cmd
            .run(host)
            .await
            .map_err(|e| EnsureTreeError::EnsureTreeDir(e, entry.path().clone()))?
            .changed
            || changed;
          let children = self
            .target
            .tree
            .read_dir(entry.path().as_ref())
            .await
            .map_err(|e| EnsureTreeError::ReadTreeDir(e, entry.path().clone()))?;
          stack.extend(children);
        }
        ft => return Err(EnsureTreeError::InvalidFileType(entry.path().clone(), ft)),
      }
    }
    Ok(TaskSuccess { changed, output: () })
  }
}
