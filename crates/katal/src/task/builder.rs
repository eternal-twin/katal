use crate::task::user::{Group, UpsertGroupByNameError, UpsertUserByNameError, User};
use crate::task::{exec_as, AsyncFn, ExecAsError, NamedTask, TaskResult, TaskSuccess};
use async_trait::async_trait;
use asys::common::fs::FsHost;
use asys::linux::fs::{LinuxFsHost, LinuxMetadata};
use asys::linux::user::{Gid, GroupRef, LinuxGroup, LinuxUser, LinuxUserHost, Uid, UserRef};
use asys::{ExecHost, Host};
use serde::{Deserialize, Serialize};
use std::fmt::Debug;
use std::marker::PhantomData;
use thiserror::Error;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct ExecAsBuilder<RemoteHost, T>
where
  RemoteHost: 'static + Host + ?Sized,
  T: NamedTask<&'static RemoteHost>,
  T: Serialize + Sync,
  T::Output: for<'out_de> Deserialize<'out_de> + Send,
{
  task: T,
  _phantom: PhantomData<fn(&'static RemoteHost) -> ()>,
}

impl<RemoteHost, T> ExecAsBuilder<RemoteHost, T>
where
  RemoteHost: 'static + Host + ?Sized,
  T: NamedTask<&'static RemoteHost>,
  T: Serialize + Sync,
  T::Output: for<'de> Deserialize<'de> + Send,
{
  pub fn new(task: T) -> Self {
    Self {
      task,
      _phantom: PhantomData,
    }
  }
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, Error)]
pub enum ExecAsBuilderError {
  #[error("failed to upsert `katal_builder`")]
  UpsertUser(#[from] KatalBuilderError),
  #[error("task execution failed")]
  Exec(#[from] ExecAsError),
}

#[async_trait]
impl<'h, H: ExecHost + LinuxUserHost + LinuxFsHost, RemoteHost, T> AsyncFn<&'h H> for ExecAsBuilder<RemoteHost, T>
where
  <H as FsHost>::Metadata: LinuxMetadata,
  H::Group: Send + Sync,
  H::User: Send + Sync + Debug,
  RemoteHost: 'static + Host + ?Sized,
  T: NamedTask<&'static RemoteHost>,
  T: Serialize + Sync,
  T::Output: for<'de> Deserialize<'de> + Send,
{
  type Output = TaskResult<T::Output, ExecAsBuilderError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let user = WantedKatalBuilder::new().run(host).await?;
    let output = exec_as(UserRef::Id(user.output.uid), None, &self.task).await?;
    Ok(TaskSuccess {
      changed: user.changed,
      output,
    })
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[non_exhaustive]
pub struct WantedKatalBuilder {}

impl WantedKatalBuilder {
  pub fn new() -> Self {
    Self {}
  }
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, Error)]
pub enum KatalBuilderError {
  #[error("failed to upsert `katal_builder` group")]
  BuilderGroup(#[from] UpsertGroupByNameError),
  #[error("failed to upsert `katal_builder` user")]
  BuilderUser(#[from] UpsertUserByNameError),
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct KatalBuilder {
  pub uid: Uid,
  pub gid: Gid,
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for WantedKatalBuilder
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::Group: Send + Sync,
  H::User: Send + Sync + Debug,
{
  type Output = TaskResult<KatalBuilder, KatalBuilderError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let mut changed = false;
    let group = Group::upsert_by_name("katal_builder").run(host).await?;
    changed = changed || group.changed;
    let group = group.output;
    let user = User::upsert_by_name("katal_builder")
      .system(true)
      .group(GroupRef::from(group.gid()))
      .run(host)
      .await?;
    changed = changed || user.changed;
    let user = user.output;

    let uid = user.uid();
    let gid = user.gid();

    let output = KatalBuilder { uid, gid };
    Ok(TaskSuccess { changed, output })
  }
}
