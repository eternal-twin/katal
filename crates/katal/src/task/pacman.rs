use crate::task::{AsyncFn, TaskResult, TaskSuccess};
use async_trait::async_trait;
use asys::{Command, CommandOutput, ExecError, ExecHost};
use std::collections::BTreeMap;
use std::fmt::Debug;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct EnsurePacmanPackages {
  packages: BTreeMap<String, PackageTarget>,
}

impl Default for EnsurePacmanPackages {
  fn default() -> Self {
    Self::new()
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub enum PackageTarget {
  /// Ensure that the package is **not** present.
  Absent,
  /// Ensure that the package is present, if it is already installed don't update.
  Present,
  /// Ensure that the package is present and up-to-date.
  Latest,
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize, thiserror::Error)]
pub enum EnsurePacmanPackagesError {
  #[error("failed to executed command to ensure packages are absent")]
  ExecAbsent(#[source] PacmanRemoveError),
  #[error("failed to executed command to ensure packages are present")]
  ExecPresent(#[source] PacmanSyncError),
  #[error("failed to sync latest package list")]
  SyncLatest(#[source] PacmanSyncError),
  #[error("failed to executed command to ensure packages are at their latest version")]
  ExecLatest(#[source] PacmanSyncError),
}

#[async_trait]
impl<'h, H: ExecHost> AsyncFn<&'h H> for EnsurePacmanPackages {
  type Output = TaskResult<(), EnsurePacmanPackagesError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let mut has_absent = false;
    let mut absent_cmd = PacmanRemove::new().recursive(true);
    let mut has_present = false;
    let mut present_cmd = PacmanSync::new().needed(true);
    let mut has_latest = false;
    let mut latest_cmd = PacmanSync::new().needed(false);
    for (pkg_name, target) in self.packages.iter() {
      match target {
        PackageTarget::Absent => {
          has_absent = true;
          absent_cmd = absent_cmd.target(pkg_name);
        }
        PackageTarget::Present => {
          has_present = true;
          present_cmd = present_cmd.target(pkg_name);
        }
        PackageTarget::Latest => {
          has_latest = true;
          latest_cmd = latest_cmd.target(pkg_name);
        }
      }
    }
    if has_absent {
      absent_cmd.exec(host).map_err(EnsurePacmanPackagesError::ExecAbsent)?;
    }
    if has_present {
      present_cmd.exec(host).map_err(EnsurePacmanPackagesError::ExecPresent)?;
    }
    if has_latest {
      PacmanSync::new().refresh(true).sys_upgrade(true).exec(host).map_err(EnsurePacmanPackagesError::SyncLatest)?;
      latest_cmd.exec(host).map_err(EnsurePacmanPackagesError::ExecLatest)?;
    }
    let changed = has_absent || has_present || has_latest;
    Ok(TaskSuccess { changed, output: () })
  }
}

impl EnsurePacmanPackages {
  pub fn new() -> Self {
    Self {
      packages: BTreeMap::new(),
    }
  }

  pub fn present(mut self, pkg_name: impl ToString) -> Self {
    self.packages.insert(pkg_name.to_string(), PackageTarget::Present);
    self
  }
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize, thiserror::Error)]
pub enum PacmanRemoveError {
  #[error("pacman remove execution error")]
  Exec(#[from] ExecError),
  #[error("non-success pacman remove exit code")]
  Exit(CommandOutput),
}

pub struct PacmanRemove {
  recursive: bool,
  targets: Vec<String>,
}

impl Default for PacmanRemove {
  fn default() -> Self {
    Self::new()
  }
}

impl PacmanRemove {
  pub fn new() -> Self {
    Self {
      recursive: false,
      targets: Vec::new(),
    }
  }

  pub fn recursive(mut self, recursive: bool) -> Self {
    self.recursive = recursive;
    self
  }

  pub fn target(mut self, target: impl ToString) -> Self {
    self.targets.push(target.to_string());
    self
  }

  pub fn command(&self) -> Command {
    let mut cmd = Command::new("pacman").arg("--remove").arg("--noconfirm");
    cmd = cmd.arg("--color").arg("never");
    cmd = cmd.arg("--noprogressbar");
    if self.recursive {
      cmd = cmd.arg("--recursive");
    }
    for target in self.targets.iter() {
      cmd = cmd.arg(target);
    }
    cmd
  }

  pub fn exec(&self, host: &impl ExecHost) -> Result<(), PacmanRemoveError> {
    let cmd = self.command();
    host
        .exec(&cmd)
        .map(drop)
        .map_err(PacmanRemoveError::Exec)
  }
}

pub struct PacmanSync {
  needed: bool,
  sys_upgrade: bool,
  refresh: bool,
  targets: Vec<String>,
}

impl Default for PacmanSync {
  fn default() -> Self {
    Self::new()
  }
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize, thiserror::Error)]
pub enum PacmanSyncError {
  #[error("pacman sync execution error")]
  Exec(#[from] ExecError),
  #[error("non-success pacman sync exit code")]
  Exit(CommandOutput),
}

impl PacmanSync {
  pub fn new() -> Self {
    Self {
      needed: false,
      sys_upgrade: false,
      refresh: false,
      targets: Vec::new(),
    }
  }

  pub fn needed(mut self, needed: bool) -> Self {
    self.needed = needed;
    self
  }

  pub fn sys_upgrade(mut self, sys_upgrade: bool) -> Self {
    self.sys_upgrade = sys_upgrade;
    self
  }

  pub fn refresh(mut self, refresh: bool) -> Self {
    self.refresh = refresh;
    self
  }

  pub fn target(mut self, target: impl ToString) -> Self {
    self.targets.push(target.to_string());
    self
  }

  fn command(&self) -> Command {
    let mut cmd = Command::new("pacman").arg("--sync").arg("--noconfirm");
    cmd = cmd.arg("--color").arg("never");
    cmd = cmd.arg("--noprogressbar");
    if self.needed {
      cmd = cmd.arg("--needed");
    }
    if self.sys_upgrade {
      cmd = cmd.arg("--sysupgrade");
    }
    if self.refresh {
      cmd = cmd.arg("--refresh");
    }
    for target in self.targets.iter() {
      cmd = cmd.arg(target);
    }
    cmd
  }

  pub fn exec(&self, host: &impl ExecHost) -> Result<(), PacmanSyncError> {
    let cmd = self.command();
    host
      .exec(&cmd)
      .map(drop)
      .map_err(PacmanSyncError::Exec)
  }
}

#[async_trait]
impl<'h, H: ExecHost> AsyncFn<&'h H> for PacmanSync {
  type Output = TaskResult<(), PacmanSyncError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    self.exec(host)?;
    Ok(TaskSuccess {
      changed: true,
      output: (),
    })
  }
}
