use crate::task::pacman::{EnsurePacmanPackages, EnsurePacmanPackagesError};
use crate::task::{AsyncFn, TaskResult, TaskSuccess};
use async_trait::async_trait;
use asys::common::fs::FsHost;
use asys::linux::fs::{LinuxFsHost, LinuxMetadata};
use asys::linux::user::LinuxUserHost;
use asys::ExecHost;
use serde::{Deserialize, Serialize};
pub use sqlx::postgres::types::Oid;
use std::fmt::Debug;
use std::path::PathBuf;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[non_exhaustive]
pub struct JavaReady {}

impl JavaReady {
  #[allow(clippy::new_without_default)]
  pub fn new() -> Self {
    Self {}
  }
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, thiserror::Error)]
pub enum JavaReadyError {
  #[error("failed to ensure Java package presence")]
  EnsurePackage(#[source] EnsurePacmanPackagesError),
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct JavaEnv {
  pub bin_dir: PathBuf,
  pub java_home: PathBuf,
}

#[async_trait]
impl<'h, H: ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for JavaReady
where
  <H as FsHost>::Metadata: LinuxMetadata,
  H::Group: Send + Sync,
  H::User: Send + Sync + Debug,
{
  type Output = TaskResult<JavaEnv, JavaReadyError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let mut changed = false;
    changed = EnsurePacmanPackages::new()
      .present("jdk21-openjdk")
      .run(host)
      .await
      .map_err(JavaReadyError::EnsurePackage)?
      .changed
      || changed;
    Ok(TaskSuccess {
      changed,
      output: JavaEnv {
        bin_dir: PathBuf::from("/usr/lib/jvm/java-21-openjdk/bin"),
        java_home: PathBuf::from("/usr/lib/jvm/java-21-openjdk"),
      },
    })
  }
}
