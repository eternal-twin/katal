use crate::pg_sql::alter_default_privileges::AlterDefaultPrivileges;
use crate::pg_sql::grant::Grant;
use crate::pg_sql::revoke::Revoke;
use crate::pg_sql::{
  AlterDatabase, AlterRoleQuery, CreateDatabaseError, CreateDatabaseQuery, CreateRoleError, CreateRoleQuery,
  DropDatabaseQuery, PgRoleName, PgSqlQuotedId,
};
use crate::task::fs::{EnsureDir, EnsureDirError, EnsureFile, EnsureFileError, EnsureTree, EnsureTreeError};
use crate::task::pacman::{EnsurePacmanPackages, EnsurePacmanPackagesError};
use crate::task::systemd::{
  EnsureSystemdServiceConfig, EnsureSystemdServiceConfigError, SystemdState, SystemdUnit, SystemdUnitError,
};
use crate::task::user::{Group, Password, UpsertGroupByNameError, UpsertUserByNameError, User};
use crate::task::{exec_as, AsyncFn, ExecAsError, NamedTask, TaskName, TaskResult, TaskSuccess};
use async_trait::async_trait;
use asys::common::fs::{FsHost, ReadFileError};
use asys::linux::fs::{FileMode, LinuxFsHost, LinuxMetadata};
use asys::linux::user::{Gid, GroupRef, LinuxGroup, LinuxUser, LinuxUserHost, Uid, UserFromNameError, UserRef};
use asys::local_linux::LocalLinux;
use asys::{Command, ExecError, ExecHost, Host};
use chrono::{DateTime, Utc};
use core::fmt;
use katal_loader::tree::{FileType, FsxDirEntry, PostgresqlFtp, PostgresqlFtpDirEntry, ReadFsx, TarGz};
use once_cell::sync::Lazy;
use regex::Regex;
use serde::{Deserialize, Serialize};
pub use sqlx::postgres::types::Oid;
use sqlx::{Connection, PgConnection, Postgres, Transaction};
use std::borrow::Cow;
use std::collections::BTreeMap;
use std::convert::TryFrom;
use std::fmt::Debug;
use std::path::{Path, PathBuf};
use std::str::FromStr;
use thiserror::Error;
use url::Url;

// TODO: pg_hba.conf
// TODO: postgresql.conf, with `listen_addresses = '*'`

/// Postgres index of releases
///
/// See:
/// - <https://ftp.postgresql.org/pub/source/>
#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct PostgresIndex {
  by_version: BTreeMap<String, PostgresRelease>,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize, Error)]
pub enum PostgresIndexFromFtpError {
  #[error("failed to parse postgres version {0:?}")]
  VersionFormat(String),
  #[error("failed to find digest for file {0:?}")]
  DigestNotFound(String),
}

// static DIGEST_LINE_MODERN: Lazy<Regex> = Lazy::new(|| Regex::new(r"^((?:[0-9a-f]{2})+) .*$").unwrap());
static DIGEST_LINE_LEGACY: Lazy<Regex> = Lazy::new(|| Regex::new(r"^MD5 *\((.*)\) *= ((?:[0-9a-f]{2})+)$").unwrap());

/// Represent a digest file. Examples:
/// ```ignore
/// MD5(postgresql-6.5.3.tar.gz)= 098d1ee4316500a2d033ced3ebd5b831
/// ```
/// ```ignore
/// f2f861fb99d742cb9c2f8aa46a8a947d  postgresql-15.4.tar.bz2
/// ```
pub struct DigestFile<const N: usize, S: AsRef<str> = String> {
  data: S,
}

impl<S: AsRef<str>, const N: usize> DigestFile<N, S> {
  pub fn new(data: S) -> Self {
    Self { data }
  }

  fn get_digest(&self, file: &str) -> Result<[u8; N], ()> {
    match self.get_digest_modern(file) {
      Ok(h) => Ok(h),
      Err(()) => self.get_digest_legacy(file),
    }
  }

  fn get_digest_modern(&self, file: &str) -> Result<[u8; N], ()> {
    if file != file.trim() {
      // panic!("input file name must be trimmed, received {file}");
      return Err(());
    }
    for line in self.data.as_ref().lines() {
      let line = line.trim();
      if line.is_empty() {
        continue;
      }
      let (digest, cur_file) = line.split_once(' ').expect("invalid digest file format");
      let cur_file = cur_file.trim();
      if cur_file != file {
        continue;
      }
      let digest = digest.trim();
      let mut out: [u8; N] = [0; N];
      hex::decode_to_slice(digest, &mut out).expect("hex decode error");
      return Ok(out);
    }
    // panic!("file digest not found for: {file}")
    Err(())
  }

  fn get_digest_legacy(&self, file: &str) -> Result<[u8; N], ()> {
    if file != file.trim() {
      // panic!("input file name must be trimmed, received {file}");
      return Err(());
    }
    for line in self.data.as_ref().lines() {
      let line = line.trim();
      if line.is_empty() {
        continue;
      }
      let capture = DIGEST_LINE_LEGACY.captures(line).ok_or(())?;
      let cur_file = capture[1].trim();
      if cur_file != file {
        continue;
      }
      let digest = &capture[2];
      let mut out: [u8; N] = [0; N];
      hex::decode_to_slice(digest, &mut out).expect("hex decode error");
      return Ok(out);
    }
    // panic!("file digest not found for: {file}")
    Err(())
  }
}

impl PostgresIndex {
  pub fn embedded() -> Self {
    serde_json::from_str(include_str!("../../files/pg/index.json")).expect("embedded postgres index is valid")
  }

  pub async fn from_ftp() -> Result<Self, PostgresIndexFromFtpError> {
    let mut by_version = BTreeMap::new();

    let psql_tree = PostgresqlFtp::new("katal".to_string());
    let source_entries = psql_tree
      .read_dir(["source".to_string()])
      .await
      .expect("failed to read source index");
    for source_item in source_entries {
      if source_item.file_type() != FileType::Dir {
        continue;
      }
      let version = source_item.path().last().expect("source entry path is not empty");
      let version = version.strip_prefix('v').expect("version entry starts with `v`");

      if version.contains("beta") || version.contains("rc") {
        // TODO: Support `beta` and `rc` versions
        continue;
      }

      let mut version_parts = version.split('.');
      let version_parts = {
        let major = version_parts
          .next()
          .map(u16::from_str)
          .ok_or_else(|| PostgresIndexFromFtpError::VersionFormat(version.to_string()))?
          .map_err(|_| PostgresIndexFromFtpError::VersionFormat(version.to_string()))?;
        let minor = version_parts
          .next()
          .map(u16::from_str)
          .ok_or_else(|| PostgresIndexFromFtpError::VersionFormat(version.to_string()))?
          .map_err(|_| PostgresIndexFromFtpError::VersionFormat(version.to_string()))?;
        let patch = version_parts
          .next()
          .map(u16::from_str)
          .transpose()
          .map_err(|_| PostgresIndexFromFtpError::VersionFormat(version.to_string()))?;
        (major, minor, patch)
      };

      let source_name = match version_parts {
        (1, _, _) => format!("postgres95-{version}.tar.gz"),
        (6, 0, None) => format!("postgresql-v{version}.tar.gz"),
        (6, 1, None) => "postgresql-6.1.1.tar.gz".to_string(),
        (6, 2, None) => "postgresql-6.2.1.tar.gz".to_string(),
        (6, 3, None) => "postgresql-6.3.2.tar.gz".to_string(),
        (6, 4, None) => "postgresql-6.4.2.tar.gz".to_string(),
        (6, 5, None) => "postgresql-6.5.3.tar.gz".to_string(),
        (7, 0, Some(1)) => format!("postgresql.{version}.tar.gz"),
        (8, 0, None) => "postgresql-8.0.0.tar.gz".to_string(),
        _ => format!("postgresql-{version}.tar.gz"),
      };
      let source_md5_name = format!("{source_name}.md5");
      let source_sha256_name = format!("{source_name}.sha256");

      let mut source_tgz: Option<PostgresqlFtpDirEntry> = None;
      let mut source_tgz_md5: Option<PostgresqlFtpDirEntry> = None;
      let mut source_tgz_sha256: Option<PostgresqlFtpDirEntry> = None;

      let version_entries = psql_tree
        .read_dir(source_item.path())
        .await
        .expect("failed to read version entries");
      for version_entry in version_entries {
        let file_name = version_entry.path().last().expect("file name is present");
        if file_name == &source_name {
          source_tgz = Some(version_entry)
        } else if file_name == &source_md5_name {
          source_tgz_md5 = Some(version_entry)
        } else if file_name == &source_sha256_name {
          source_tgz_sha256 = Some(version_entry)
        }
      }
      match source_tgz {
        Some(src) => {
          let source_name = src.path().last().expect("source path is not empty");
          let source_tgz_md5 = match source_tgz_md5 {
            None => None,
            Some(entry) => {
              let digest_file = psql_tree.read(entry.path()).await.expect("failed to read md5");
              let digest_file = String::from_utf8(digest_file).expect("encoding error in md5 digest file");
              let digest_file = DigestFile::<16>::new(digest_file);
              Some(
                digest_file
                  .get_digest(source_name)
                  .map_err(|()| PostgresIndexFromFtpError::DigestNotFound(source_name.clone()))?,
              )
            }
          };
          let source_tgz_sha256 = match source_tgz_sha256 {
            None => None,
            Some(entry) => {
              let digest_file = psql_tree.read(entry.path()).await.expect("failed to read sha256");
              let digest_file = String::from_utf8(digest_file).expect("encoding error in sha256 digest file");
              let digest_file = DigestFile::<32>::new(digest_file);
              Some(
                digest_file
                  .get_digest(source_name)
                  .map_err(|()| PostgresIndexFromFtpError::DigestNotFound(source_name.clone()))?,
              )
            }
          };

          let release = PostgresRelease {
            version: version.to_string(),
            source_tgz: src.path().clone(),
            source_size: src.file_size.expect("source has a size"),
            source_tgz_md5,
            source_tgz_sha256,
          };
          by_version.insert(version.to_string(), release);
        }
        None => {
          panic!("source archive not found: {version}");
        }
      }
    }
    Ok(Self { by_version })
  }

  pub fn resolve(&self, version_req: &str) -> Option<&PostgresRelease> {
    self.by_version.get(version_req)
  }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct PostgresRelease {
  pub version: String,
  /// Path of the source `.tar.gz` archive
  source_tgz: Vec<String>,
  source_size: u64,
  /// MD5 hash digest of the source `.tar.gz` archive
  source_tgz_md5: Option<[u8; 16]>,
  /// SHA256 hash digest of the source `.tar.gz` archive
  source_tgz_sha256: Option<[u8; 32]>,
}

impl PostgresRelease {
  /// Return a string uniquely identifying the data format supported by
  /// this Postgres release.
  ///
  /// This is overly pessimistic:
  /// - the same format string guarantees that the releases are compatible
  /// - even if two releases a compatible, this may still return different values
  ///
  /// This should correspond to the value in the `PG_VERSION` file of a Postgres
  /// cluster.
  pub fn data_format(&self) -> &str {
    let mut components = self.version.split('.');
    let Some(major_str) = components.next() else {
      return self.version.as_str();
    };
    let Ok(major) = u32::from_str(major_str) else {
      return self.version.as_str();
    };
    let Some(Ok(minor)) = components.next().map(u32::from_str) else {
      return self.version.as_str();
    };
    if components.next().is_some() {
      return self.version.as_str();
    }
    let write_back = format!("{major}.{minor}");
    if self.version == write_back && major >= 12 {
      major_str
    } else {
      self.version.as_str()
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub enum PostgresVersionReq {
  PacmanLatest,
  Version(String),
}

pub struct GetPostgresSource {
  release: PostgresRelease,
}

impl GetPostgresSource {
  pub fn release(release: PostgresRelease) -> Self {
    Self { release }
  }
}

#[async_trait]
impl<'h, H: Host> AsyncFn<&'h H> for GetPostgresSource {
  type Output = TaskResult<TarGz, anyhow::Error>;

  async fn run(&self, _host: &'h H) -> Self::Output {
    let psql_tree = PostgresqlFtp::new("katal".to_string());
    let res = psql_tree.read(self.release.source_tgz.as_slice()).await?;
    let tree = TarGz::new(res.as_slice())?;
    Ok(TaskSuccess {
      changed: false,
      output: tree,
    })
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct PostgresReady {
  pub version_req: PostgresVersionReq,
  pub service_name: Option<String>,
  pub user_name: Option<String>,
  pub data_root: Option<PathBuf>,
}

impl PostgresReady {
  pub fn new(version_req: PostgresVersionReq) -> Self {
    Self {
      version_req,
      service_name: None,
      user_name: None,
      data_root: None,
    }
  }

  pub fn service_name(mut self, service_name: Option<String>) -> Self {
    self.service_name = service_name;
    self
  }

  pub fn user_name(mut self, user_name: Option<String>) -> Self {
    self.user_name = user_name;
    self
  }

  pub fn data_root(mut self, data_root: Option<PathBuf>) -> Self {
    self.data_root = data_root;
    self
  }
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, Error)]
pub enum PostgresReadyError {
  #[error("wanted user name is incompatible with pacman package")]
  UserNameConflict,
  #[error("wanted service name is incompatible with pacman package")]
  ServiceNameConflict,
  #[error("failed to ensure `postgresql` package is present")]
  EnsurePacmanPackage(#[source] EnsurePacmanPackagesError),
  #[error("failed to resolve `data_root` parent directory")]
  DataRootParent,
  #[error("failed to upsert `katal_builder` group")]
  BuilderGroup(#[source] UpsertGroupByNameError),
  #[error("failed to upsert `katal_builder` user")]
  BuilderUser(#[source] UpsertUserByNameError),
  #[error("failed to ensure build dependencies")]
  EnsureBuildDeps(#[source] EnsurePacmanPackagesError),
  #[error("failed to ensure build dir exists")]
  EnsureBuildDir(#[source] EnsureDirError),
  #[error("build error")]
  Build(#[from] PostgresBuildError),
  #[error("failed to ensure db check script exists")]
  EnsureCheckScript(#[source] EnsureFileError),
  #[error("failed to upsert service group")]
  ServiceGroup(#[source] UpsertGroupByNameError),
  #[error("failed to upsert service user")]
  ServiceUser(#[source] UpsertUserByNameError),
  #[error("failed to ensure systemd service config exists")]
  EnsureSystemdServiceConfig(#[source] EnsureSystemdServiceConfigError),
  #[error("Postgres user not found for name {1:?}")]
  User(#[source] UserFromNameError, String),
  #[error("failed to ensure Postgres user home directory exist, failed at: {1:?}")]
  EnsureHomeDir(#[source] EnsureDirError, PathBuf),
  #[error("failed to initialize Postgres history file")]
  EnsureHistory(#[source] EnsureFileError),
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct PostgresState {
  pub uid: Uid,
  pub gid: Gid,
  /// Base name of the systemd service
  pub service: String,
  pub bin_dir: PathBuf,
  pub lib_dir: PathBuf,
  pub data_root: PathBuf,
  pub release: Option<PostgresRelease>,
}

#[async_trait]
impl<'h, H: ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for PostgresReady
where
  <H as FsHost>::Metadata: LinuxMetadata,
  H::Group: Send + Sync,
  H::User: Send + Sync + Debug,
{
  type Output = TaskResult<PostgresState, PostgresReadyError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let mut changed = false;
    let bin_dir: PathBuf;
    let lib_dir: PathBuf;
    let user_name: String;
    let service_name: String;
    let pg_root: PathBuf;
    let data_root: PathBuf;
    let release: Option<PostgresRelease>;

    match &self.version_req {
      PostgresVersionReq::PacmanLatest => {
        if let Some(wanted_user_name) = self.user_name.as_deref() {
          if wanted_user_name != "postgres" {
            return Err(PostgresReadyError::UserNameConflict);
          }
        }
        if let Some(wanted_service_name) = self.service_name.as_deref() {
          if wanted_service_name != "postgresql" {
            return Err(PostgresReadyError::ServiceNameConflict);
          }
        }
        if let Some(wanted_data_root) = self.data_root.as_deref() {
          if wanted_data_root != Path::new("/var/lib/postgres/data") {
            return Err(PostgresReadyError::ServiceNameConflict);
          }
        }
        changed = EnsurePacmanPackages::new()
          .present("postgresql")
          .run(host)
          .await
          .map_err(PostgresReadyError::EnsurePacmanPackage)?
          .changed
          || changed;

        bin_dir = PathBuf::from("/usr/bin");
        lib_dir = PathBuf::from("/usr/lib");
        user_name = String::from("postgres");
        service_name = String::from("postgresql");
        pg_root = PathBuf::from("/var/lib/postgres");
        data_root = pg_root.join("data");
        release = None;
      }
      PostgresVersionReq::Version(version) => {
        let pg_index = PostgresIndex::embedded();
        let pg_release = pg_index.resolve(version).expect("version not found");

        let builder_group = Group::upsert_by_name("katal_builder")
          .run(host)
          .await
          .map_err(PostgresReadyError::BuilderGroup)?
          .output;
        let builder_user = User::upsert_by_name("katal_builder")
          .system(true)
          .group(GroupRef::from(builder_group.gid()))
          .run(host)
          .await
          .map_err(PostgresReadyError::BuilderUser)?
          .output;

        eprintln!("ensure build dependencies are present");
        changed = EnsurePacmanPackages::new()
          .present("clang")
          .present("docbook-xml")
          .present("docbook-xsl")
          .present("llvm")
          .present("perl")
          .present("perl-ipc-run")
          .present("python")
          .present("systemd")
          .present("tcl")
          .present("util-linux")
          .run(host)
          .await
          .map_err(PostgresReadyError::EnsureBuildDeps)?
          .changed
          || changed;

        eprintln!("ensure target dir exists");
        changed = EnsureDir::new(PathBuf::from("/katal"))
          .owner(UserRef::ROOT)
          .run(host)
          .await
          .map_err(PostgresReadyError::EnsureBuildDir)?
          .changed
          || changed;

        changed = EnsureDir::new(PathBuf::from("/katal/store"))
          .owner(UserRef::ROOT)
          .run(host)
          .await
          .map_err(PostgresReadyError::EnsureBuildDir)?
          .changed
          || changed;

        let pg_dir = PathBuf::from(format!("/katal/store/postgres-{}", pg_release.version));
        changed = EnsureDir::new(pg_dir.clone())
          .owner(UserRef::from(builder_user.uid()))
          .group(GroupRef::from(builder_group.gid()))
          .run(host)
          .await
          .map_err(PostgresReadyError::EnsureBuildDir)?
          .changed
          || changed;

        let pg_build = exec_as::<LocalLinux, PostgresBuild>(
          UserRef::from(builder_user.uid()),
          None,
          &PostgresBuild::new(pg_release.clone()),
        )
        .await
        .expect("running the agent should succeed")?;
        eprintln!("pg_build complete: {pg_build:?}");

        let check_script_path: PathBuf = pg_dir.join("check-data-root");
        {
          // language=sh
          let check_script = format!(
            r##"#!/bin/sh
# Check that the data root is initialized and has a compatible version

PGDATA="$1"

if [ -z "$PGDATA" ]
then
    echo "Usage: $0 database-path"
    exit 1
fi

EXPECTED_VERSION={expected_version}

# Check for the PGDATA structure
if [ -f "$PGDATA/PG_VERSION" ] && [ -d "$PGDATA/base" ]
then
    # Check version of existing PGDATA
    if [ x`cat "$PGDATA/PG_VERSION"` = x"$EXPECTED_VERSION" ]
    then
        : A-OK
    else
        echo $"An old version of the database format was found. Expected $EXPECTED_VERSION"
        exit 1
    fi
else
    # No existing PGDATA! Warn the user to initdb it.
    echo $"\"$PGDATA\" is missing or empty. Cluster is not initialized"
    exit 1
fi

exit 0

          "##,
            expected_version = pg_release.data_format()
          );

          changed = EnsureFile::new(&check_script_path)
            .content(&check_script)
            .mode(FileMode::ALL_EXEC | FileMode::ALL_READ)
            .run(host)
            .await
            .map_err(PostgresReadyError::EnsureCheckScript)?
            .changed
            || changed;
        }

        user_name = String::from(self.user_name.as_deref().unwrap_or("postgres"));
        match self.data_root.as_deref() {
          Some(wanted_data_root) => {
            data_root = PathBuf::from(wanted_data_root);
            pg_root = PathBuf::from(data_root.parent().ok_or(PostgresReadyError::DataRootParent)?);
          }
          None => {
            pg_root = PathBuf::from("/var/lib/postgres");
            data_root = pg_root.join("data");
          }
        }
        service_name = self
          .service_name
          .clone()
          .unwrap_or_else(|| format!("postgresql{}", pg_release.data_format()));
        release = Some(pg_release.clone());

        let group = Group::upsert_by_name(&user_name)
          .run(host)
          .await
          .map_err(PostgresReadyError::ServiceGroup)?;
        changed = group.changed || changed;
        let user = User::upsert_by_name(&user_name)
          .password(&Password::Locked)
          .system(true)
          .comment("PostgreSQL user")
          .shell("/usr/bin/bash")
          .group(GroupRef::Id(group.output.gid()))
          .home(&pg_root)
          .run(host)
          .await
          .map_err(PostgresReadyError::ServiceUser)?;
        bin_dir = pg_dir.join("build/usr/bin");
        lib_dir = pg_dir.join("build/usr/lib");

        {
          use crate::task::systemd::config::{CommandLine, Config, Exec, Install, Kill, Service, ServiceType, Unit};
          let service_description = format!("PostgreSQL database server ({version})");
          let pg_root_str = pg_root.display().to_string();
          let data_root_str = data_root.display().to_string();
          let environment = format!("PGROOT={pg_root_str}");
          let pid_file_str = data_root.join("postmaster.pid").display().to_string();
          let exec_start_pre = format!("{} {data_root_str}", check_script_path.display());
          let exec_start = format!("{} -D {data_root_str}", bin_dir.join("postgres").display());
          // Based on the arch service:
          // <https://gitlab.archlinux.org/archlinux/packaging/packages/postgresql/-/blob/937999e5c450344f4bf724d9f67f67bafbaa6904/postgresql.service>
          let service: Config<&str> = Config {
            unit: Some(Unit {
              description: Some(service_description.as_str()),
              after: vec!["network.target", "network-online.target"],
              wants: vec!["network-online.target"],
              ..Unit::default()
            }),
            service: Some(Service {
              r#type: Some(ServiceType::Notify),
              timeout_sec: Some("120"),
              pid_file: Some(&pid_file_str),
              exec_start_pre: Some(CommandLine { line: &exec_start_pre }),
              exec_start: Some(CommandLine { line: &exec_start }),
              exec_reload: Some(CommandLine {
                line: "/bin/kill -HUP ${MAINPID}",
              }),
              exec: Exec {
                user: Some(user.output.name()),
                group: Some(group.output.name()),
                working_directory: Some(&pg_root_str),
                environment: vec![&environment],
                syslog_identifier: Some("postgres"),
                runtime_directory: Some(&service_name),
                runtime_directory_mode: Some("755"),
                oom_score_adjust: Some("-200"),
                private_tmp: Some("true"),
                protect_home: Some("true"),
                protect_system: Some("full"),
                no_new_privileges: Some("true"),
                protect_control_groups: Some("true"),
                protect_kernel_modules: Some("true"),
                protect_kernel_tunables: Some("true"),
                private_devices: Some("true"),
                restrict_address_families: Some("AF_UNIX AF_INET AF_INET6"),
                restrict_namespaces: Some("true"),
                restrict_realtime: Some("true"),
                system_call_architectures: Some("native"),
                ..Exec::default()
              },
              kill: Kill {
                kill_mode: Some("mixed"),
                kill_signal: Some("SIGINT"),
                ..Kill::default()
              },
              ..Service::default()
            }),
            install: Some(Install {
              wanted_by: vec!["multi-user.target"],
              ..Install::default()
            }),
            ..Config::default()
          };
          changed = EnsureSystemdServiceConfig {
            name: service_name.clone(),
            config: service,
          }
          .run(host)
          .await
          .map_err(PostgresReadyError::EnsureSystemdServiceConfig)?
          .changed
            || changed;
        }
      }
    };
    let user = host
      .user_from_name(&user_name)
      .map_err(|e| PostgresReadyError::User(e, user_name))?;
    let uid = user.uid();
    let gid = user.gid();

    let user_home = user.home();
    let home_ancestors: Vec<&Path> = user_home.ancestors().collect::<Vec<_>>();
    for d in home_ancestors.into_iter().rev() {
      changed = EnsureDir::new(d.to_path_buf())
        .run(host)
        .await
        .map_err(|e| PostgresReadyError::EnsureHomeDir(e, d.to_path_buf()))?
        .changed
        || changed;
    }

    changed = EnsureFile::new(user.home().join(".psql_history"))
      .owner(uid)
      .group(gid)
      .mode(FileMode::OWNER_READ | FileMode::OWNER_WRITE)
      .run(host)
      .await
      .map_err(PostgresReadyError::EnsureHistory)?
      .changed
      || changed;

    let output = PostgresState {
      uid,
      gid,
      bin_dir,
      lib_dir,
      service: service_name,
      data_root,
      release,
    };
    Ok(TaskSuccess { changed, output })
  }
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct PostgresBuild {
  release: PostgresRelease,
}

impl PostgresBuild {
  pub fn new(release: PostgresRelease) -> Self {
    Self { release }
  }
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, Error)]
pub enum PostgresBuildError {
  #[error("failed to check witness file")]
  CheckWitness(#[source] ReadFileError),
  #[error("failed to ensure witness file")]
  EnsureWitness(#[source] EnsureFileError),
  #[error("failed to ensure src dir exists")]
  EnsureSrcDir(#[source] EnsureDirError),
  #[error("failed to ensure build dir exists: {0}")]
  EnsureBuildDir(#[source] EnsureDirError),
  #[error("failed to unpack source tree")]
  EnsureSourceTree(#[source] EnsureTreeError),
  #[error("failed to make `./configure` executable")]
  EnsureConfigureExec(#[from] EnsureFileError),
  #[error("failed to configure build")]
  Configure(#[source] ExecError),
  #[error("failed to build")]
  Build(#[source] ExecError),
  #[error("failed to install")]
  Install(#[source] ExecError),
  #[error("failed to install contrib")]
  InstallContrib(#[source] ExecError),
}

#[async_trait]
impl<'h, H: ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for PostgresBuild
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<(), PostgresBuildError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let release = &self.release;
    let pg_dir = PathBuf::from(format!("/katal/store/postgres-{}", release.version));
    let src_dir = pg_dir.join("src");
    let build_dir = pg_dir.join("build");
    let witness = pg_dir.join("ok");
    let witness_exists = match host.read_file(witness.as_path()) {
      Ok(_) => true,
      Err(ReadFileError::NotFound) => false,
      Err(e) => {
        return Err(PostgresBuildError::CheckWitness(e));
      }
    };
    if witness_exists {
      eprintln!("witness file found, skipping build");
      return Ok(TaskSuccess {
        changed: false,
        output: (),
      });
    }

    let mut changed = EnsureDir::new(src_dir.clone())
      .run(host)
      .await
      .map_err(PostgresBuildError::EnsureSrcDir)?
      .changed;
    eprintln!("Starting Postgres source download");
    let source = GetPostgresSource::release(release.clone())
      .run(host)
      .await
      .expect("download Postgres source");
    changed = source.changed || changed;
    let source: TarGz = source.output;
    changed = EnsureTree::new(src_dir.clone(), source)
      .run(host)
      .await
      .map_err(PostgresBuildError::EnsureSourceTree)?
      .changed
      || changed;
    eprintln!("Postgres source downloaded and unpacked");
    let configure = src_dir.join("configure");
    changed = EnsureFile::new(configure.as_path())
      .mode(FileMode::OWNER_EXEC | FileMode::OWNER_READ | FileMode::OWNER_WRITE)
      .run(host)
      .await
      .map_err(PostgresBuildError::EnsureConfigureExec)?
      .changed
      || changed;
    // TODO: Make `configure` executable
    // build based on the Arch package
    // <https://gitlab.archlinux.org/archlinux/packaging/packages/postgresql/-/blob/11f42c609ae65203392e059e4b8cc473f197edd3/PKGBUILD#L93>
    eprintln!("Configure build");
    let cmd = Command::new(configure.to_str().expect("configure path should be valid UTF-8"))
      .current_dir(src_dir.clone().to_str().expect("src dir should be valid UTF-8"))
      .arg(format!("--prefix={}", build_dir.join("usr").display()))
      .arg(format!("--sysconfdir={}", build_dir.join("etc").display()))
      .arg(format!("--mandir={}", build_dir.join("usr/share/man").display()))
      .arg(format!(
        "--datadir={}",
        build_dir.join("usr/share/postgresql").display()
      ))
      .arg("--disable-rpath")
      .arg("--enable-nls")
      // .arg("--enable-tap-tests")
      // .arg("--enable-thread-safety")
      .arg("--with-gssapi")
      .arg("--with-icu")
      .arg("--with-ldap")
      .arg("--with-libxml")
      .arg("--with-libxslt")
      .arg("--with-llvm")
      .arg("--with-lz4")
      .arg("--with-openssl")
      .arg("--with-pam")
      .arg("--with-perl")
      .arg("--with-python")
      .arg("--with-readline")
      .arg("--with-system-tzdata=/usr/share/zoneinfo")
      .arg("--with-systemd")
      .arg("--with-tcl")
      .arg("--with-uuid=e2fs")
      .arg("--with-zstd")
      .env("CFLAGS", "-ffat-lto-objects");
    host.exec(&cmd).map_err(PostgresBuildError::Configure)?;
    eprintln!("Build");
    let cmd = Command::new("make")
      .current_dir(src_dir.to_str().expect("src dir should be valid UTF-8"))
      .arg("world")
      .env("CFLAGS", "-ffat-lto-objects");
    host.exec(&cmd).map_err(PostgresBuildError::Build)?;

    eprintln!("Install");
    let cmd = Command::new("make")
      .current_dir(src_dir.to_str().expect("src dir should be valid UTF-8"))
      .arg("install");
    host.exec(&cmd).map_err(PostgresBuildError::Install)?;

    eprintln!("Install contrib");
    let cmd = Command::new("make")
      .current_dir(src_dir.to_str().expect("src dir should be valid UTF-8"))
      .arg("-C")
      .arg("contrib")
      .arg("install");
    host.exec(&cmd).map_err(PostgresBuildError::InstallContrib)?;

    changed = EnsureFile::new(witness)
      .content([])
      .run(host)
      .await
      .map_err(PostgresBuildError::EnsureWitness)?
      .changed
      || changed;
    eprintln!("Postgres build complete");

    Ok(TaskSuccess { changed, output: () })
  }
}

impl<'h, H: 'h + ExecHost + LinuxUserHost + LinuxFsHost> NamedTask<&'h H> for PostgresBuild
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  const NAME: TaskName = TaskName("PostgresBuild");
}

#[derive(Debug, Clone, Eq, PartialEq, serde::Serialize, serde::Deserialize)]
pub struct PostgresCluster {
  cluster_dir: PathBuf,
  target: PostgresClusterTarget,
  service_name: Option<String>,
}

impl PostgresCluster {
  pub fn new(cluster_dir: PathBuf, version: PostgresVersionReq) -> Self {
    Self {
      cluster_dir,
      service_name: None,
      target: PostgresClusterTarget {
        locale: "en_US.UTF-8".to_string(),
        encoding: "UTF8".to_string(),
        version,
      },
    }
  }

  pub fn service_name(mut self, name: Option<String>) -> Self {
    self.service_name = name;
    self
  }
}

#[derive(Debug, Clone, Eq, PartialEq, serde::Serialize, serde::Deserialize)]
pub struct PostgresClusterTarget {
  locale: String,
  encoding: String,
  version: PostgresVersionReq,
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, Error)]
pub enum PostgresClusterError {
  #[error("Postgres package is not ready")]
  Ready(#[from] PostgresReadyError),
  #[error("failed to check cluster initialization")]
  CheckCluster(#[from] ReadFileError),
  #[error("failed to ensure cluster data dir")]
  EnsureClusterDir(#[source] EnsureDirError),
  #[error("existing cluster uses data version {cluster:?} but binary uses version {binary:?}")]
  ClusterVersion { cluster: String, binary: String },
  #[error("failed to run cluster initialization command")]
  Command(#[from] PostgresClusterCommandError),
  #[error("failed to switch execution context")]
  ExecAs(#[from] ExecAsError),
  #[error("failed to enable systemd unit")]
  SystemdUnit(#[from] SystemdUnitError),
}

#[async_trait]
impl<'h, H: ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for PostgresCluster
where
  <H as FsHost>::Metadata: LinuxMetadata,
  H::Group: Send + Sync,
  H::User: Send + Sync + Debug,
{
  type Output = TaskResult<PostgresState, PostgresClusterError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let postgres = PostgresReady::new(self.target.version.clone())
      .data_root(Some(self.cluster_dir.clone()))
      .service_name(self.service_name.clone())
      .run(host)
      .await?;

    eprintln!("Postgres ready: {postgres:?}");

    let mut changed = postgres.changed;
    let postgres: PostgresState = postgres.output;

    changed = EnsureDir::new(postgres.data_root.clone())
      .owner(UserRef::Id(postgres.uid))
      .group(GroupRef::Id(postgres.gid))
      .mode(FileMode::OWNER_ALL | FileMode::GROUP_EXEC | FileMode::GROUP_EXEC)
      .run(host)
      .await
      .map_err(PostgresClusterError::EnsureClusterDir)?
      .changed
      || changed;

    match host.read_file(postgres.data_root.join("PG_VERSION")) {
      Ok(version) => {
        let version: Cow<'_, str> = String::from_utf8_lossy(&version);
        let version: &str = version.trim();
        let binary_version: Option<&str> = postgres.release.as_ref().map(|r| r.data_format());
        if let Some(binary_version) = binary_version {
          if binary_version != version {
            return Err(PostgresClusterError::ClusterVersion {
              cluster: String::from(version),
              binary: String::from(binary_version),
            });
          }
        }
        // binary version is unknown, or matches the cluster version
        eprintln!("postgres cluster directory already initialized at version: {version:?}",)
      }
      Err(ReadFileError::NotFound) => {
        eprintln!("postgres directory needs to be initialized");
        let initdb = exec_as::<LocalLinux, PostgresClusterCommand>(
          UserRef::Id(postgres.uid),
          None,
          &PostgresClusterCommand {
            bin_dir: postgres.bin_dir.clone(),
            lib_dir: postgres.lib_dir.clone(),
            cluster: self.clone(),
          },
        )
        .await;
        eprintln!("PostgresCluster Initialized");
        let initdb = initdb??;

        changed = changed || initdb.changed;
      }
      Err(e) => return Err(PostgresClusterError::CheckCluster(e)),
    }

    eprintln!("enabling postgres service: {}", postgres.service.as_str());
    changed = SystemdUnit::new(&postgres.service)
      .enabled(true)
      .state(SystemdState::Active)
      .run(host)
      .await?
      .changed
      || changed;

    Ok(TaskSuccess {
      changed,
      output: postgres,
    })
  }
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct PostgresClusterCommand {
  pub bin_dir: PathBuf,
  pub lib_dir: PathBuf,
  pub cluster: PostgresCluster,
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, thiserror::Error)]
pub enum PostgresClusterCommandError {
  #[error("command execution failed")]
  Exec(#[source] ExecError),
}

#[async_trait]
impl<'h, H: ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for PostgresClusterCommand
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = Result<TaskSuccess<()>, PostgresClusterCommandError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    eprintln!("PostgresClusterCommand Start");
    let exe_str = self.bin_dir.join("initdb").display().to_string();
    let mut cmd = Command::new(exe_str);
    cmd = cmd.prepend_path(self.bin_dir.display().to_string());
    cmd = cmd.prepend_ld_library_path(self.lib_dir.display().to_string());
    cmd = cmd.arg("--locale").arg(&self.cluster.target.locale);
    cmd = cmd.arg("--encoding").arg(&self.cluster.target.encoding);
    cmd = cmd.arg("--pgdata").arg(self.cluster.cluster_dir.to_str().unwrap());
    let result = host.exec(&cmd);
    match result {
      Ok(_) => Ok(TaskSuccess {
        changed: true,
        output: (),
      }),
      Err(e) => Err(PostgresClusterCommandError::Exec(e)),
    }
  }
}

impl<'h, H: 'h + ExecHost + LinuxUserHost + LinuxFsHost> NamedTask<&'h H> for PostgresClusterCommand
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  const NAME: TaskName = TaskName("PostgresClusterCommand");
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct PostgresRole<S: AsRef<str> = String> {
  name: PgRoleName<S>,
  target: PostgresRoleTarget,
}

impl<S: AsRef<str> + Send + Sync> PostgresRole<S> {
  pub fn new(name: PgRoleName<S>) -> Self {
    Self {
      name,
      target: PostgresRoleTarget {
        is_superuser: None,
        inherit_privileges: None,
        can_create_role: None,
        can_create_db: None,
        can_login: None,
        is_replication: None,
        bypass_rls: None,
        max_connection: None,
        password: None,
        password_valid_until: None,
      },
    }
  }

  pub fn superuser(mut self, is_superuser: bool) -> Self {
    self.target.is_superuser = Some(is_superuser);
    self
  }

  pub fn can_login(mut self, can_login: bool) -> Self {
    self.target.can_login = Some(can_login);
    self
  }

  pub fn md5_password(mut self, pass: impl ToString) -> Self {
    let val = PgPresentPasswordTarget::Md5ForClear(pass.to_string());
    self.target.password = Some(PgPasswordTarget::Present(Some(val)));
    self
  }

  pub async fn run<H: Host>(&self, _host: &H) -> <Self as AsyncFn<&H>>::Output {
    let opt = sqlx::postgres::PgConnectOptions::new().username("postgres");
    let mut conn = sqlx::postgres::PgConnection::connect_with(&opt).await?;

    let mut tx = conn.begin().await?;

    let res = PostgresRoleRunner::with_tx(&mut tx, self).await;

    if res.is_ok() {
      tx.commit().await?;
    } else {
      tx.rollback().await?;
    }

    res
  }
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct PostgresRoleTarget {
  /// Role has superuser privileges
  is_superuser: Option<bool>,
  /// Role automatically inherits privileges of roles it is a member of
  inherit_privileges: Option<bool>,
  /// Role can create more roles
  can_create_role: Option<bool>,
  /// Role can create databases
  can_create_db: Option<bool>,
  /// Role can log in. That is, this role can be given as the initial session authorization identifier.
  can_login: Option<bool>,
  /// Role is a replication role. A replication role can initiate replication connections and create and drop replication slots.
  is_replication: Option<bool>,
  /// Role bypasses every row level security policy, see [Row Security Policies](https://www.postgresql.org/docs/current/ddl-rowsecurity.html) for more information.
  bypass_rls: Option<bool>,
  /// For roles that can log in, this sets maximum number of concurrent connections this role can make. -1 means no limit.
  max_connection: Option<Option<u32>>,
  password: Option<PgPasswordTarget>,
  /// Password expiry time (only used for password authentication); null if no expiration
  password_valid_until: Option<Option<DateTime<Utc>>>,
}

/// TODO: Allow expressing more constraints
#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub enum PgPasswordTarget {
  Absent,
  Present(Option<PgPresentPasswordTarget>),
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub enum PgPresentPasswordTarget {
  Raw(String),
  Md5ForClear(String),
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub enum PgPasswordFormat {
  Clear,
  Md5,
  ScramSha256,
}

#[async_trait]
impl<'h, S: AsRef<str> + Send + Sync, H: Host> AsyncFn<&'h H> for PostgresRole<S> {
  type Output = TaskResult<PgRole, anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    self.run(host).await
  }
}

struct PostgresRoleRunner;

impl PostgresRoleRunner {
  async fn with_tx<S: AsRef<str>>(
    tx: &mut Transaction<'_, Postgres>,
    input: &PostgresRole<S>,
  ) -> TaskResult<PgRole, anyhow::Error> {
    let old: Option<PgRole>;

    let mut create_query = CreateRoleQuery::new(input.name.as_ref());
    if let Some(is_superuser) = input.target.is_superuser {
      create_query = create_query.superuser(is_superuser);
    }
    if let Some(can_login) = input.target.can_login {
      create_query = create_query.can_login(can_login);
    }
    if let Some(pass) = &input.target.password {
      match pass {
        PgPasswordTarget::Absent => {}
        PgPasswordTarget::Present(None) => {
          todo!("Avoid eager user creation and only check for password presence")
        }
        PgPasswordTarget::Present(Some(val)) => match val {
          PgPresentPasswordTarget::Raw(s) => create_query = create_query.raw_password(s),
          PgPresentPasswordTarget::Md5ForClear(s) => create_query = create_query.md5_password(s),
        },
      }
    }

    // Kinda like try/catch for SQL
    let mut create_tx = tx.begin().await?;
    match create_query.execute(create_tx.as_mut()).await {
      Ok(()) => {
        create_tx.commit().await?;
        old = None;
      }
      Err(CreateRoleError::AlreadyExists) => {
        create_tx.rollback().await?;
        old = get_role_by_name(tx.as_mut(), input.name.as_str()).await?;

        let mut alter_query = AlterRoleQuery::new(input.name.as_str());
        if let Some(is_superuser) = input.target.is_superuser {
          alter_query = alter_query.superuser(is_superuser);
        }
        if let Some(can_login) = input.target.can_login {
          alter_query = alter_query.can_login(can_login);
        }
        if let Some(pass) = &input.target.password {
          match pass {
            PgPasswordTarget::Absent => alter_query = alter_query.no_password(),
            PgPasswordTarget::Present(None) => {
              todo!("Check for match before altering")
            }
            PgPasswordTarget::Present(Some(val)) => match val {
              PgPresentPasswordTarget::Raw(s) => alter_query = alter_query.raw_password(s),
              PgPresentPasswordTarget::Md5ForClear(s) => alter_query = alter_query.md5_password(s),
            },
          }
        }
        alter_query.execute(tx.as_mut()).await?;
      }
      Err(e) => {
        create_tx.commit().await?;
        return Err(e.into());
      }
    };

    let new = get_role_by_name(tx.as_mut(), input.name.as_str()).await?;

    let new = match new {
      Some(n) => n,
      None => return Err(anyhow::Error::msg("FailedToCreateRole")),
    };

    Ok(TaskSuccess {
      changed: Some(&new) != old.as_ref(),
      output: new,
    })
  }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct PgRole {
  /// Role name
  name: String,
  /// Role has superuser privileges
  is_superuser: bool,
  /// Role automatically inherits privileges of roles it is a member of
  inherit_privileges: bool,
  /// Role can create more roles
  can_create_role: bool,
  /// Role can create databases
  can_create_db: bool,
  /// Role can log in. That is, this role can be given as the initial session authorization identifier.
  can_login: bool,
  /// Role is a replication role. A replication role can initiate replication connections and create and drop replication slots.
  is_replication: bool,
  /// Role bypasses every row level security policy, see [Row Security Policies](https://www.postgresql.org/docs/current/ddl-rowsecurity.html) for more information.
  bypass_rls: bool,
  /// For roles that can log in, this sets maximum number of concurrent connections this role can make. -1 means no limit.
  max_connection: Option<u32>,
  /// Password (possibly encrypted); null if none. The format depends on the form of encryption used.
  password: Option<String>,
  /// Password expiry time (only used for password authentication); null if no expiration
  password_valid_until: Option<DateTime<Utc>>,
}

pub async fn get_role_by_name(
  executor: impl sqlx::Executor<'_, Database = Postgres>,
  name: &str,
) -> Result<Option<PgRole>, sqlx::Error> {
  /// A row from `pg_authid`
  /// - <https://www.postgresql.org/docs/current/catalog-pg-authid.html>
  #[derive(Debug, sqlx::FromRow)]
  #[allow(unused)]
  struct Row {
    oid: Oid,
    rolname: String,
    rolsuper: bool,
    rolinherit: bool,
    rolcreaterole: bool,
    rolcreatedb: bool,
    rolcanlogin: bool,
    rolreplication: bool,
    rolbypassrls: bool,
    rolconnlimit: i32,
    rolpassword: Option<String>,
    rolvaliduntil: Option<DateTime<Utc>>,
  }

  impl From<Row> for PgRole {
    fn from(row: Row) -> Self {
      Self {
        name: row.rolname,
        is_superuser: row.rolsuper,
        inherit_privileges: row.rolinherit,
        can_create_role: row.rolcreaterole,
        can_create_db: row.rolcreatedb,
        can_login: row.rolcanlogin,
        is_replication: row.rolreplication,
        bypass_rls: row.rolbypassrls,
        max_connection: u32::try_from(row.rolconnlimit).ok(),
        password: row.rolpassword,
        password_valid_until: row.rolvaliduntil,
      }
    }
  }

  let row: Option<Row> = sqlx::query_as::<Postgres, Row>(r"
      SELECT oid, rolname, rolsuper, rolinherit, rolcreaterole, rolcreatedb, rolcanlogin, rolreplication, rolbypassrls, rolconnlimit, rolpassword, rolvaliduntil
      FROM pg_authid
      WHERE rolname = $1::TEXT;
    ")
    .bind(name)
    .fetch_optional(executor)
    .await?;
  Ok(row.map(PgRole::from))
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct PostgresDb {
  name: String,
  target: PostgresDbTarget,
}

impl PostgresDb {
  pub fn new(name: impl ToString) -> Self {
    Self {
      name: name.to_string(),
      target: PostgresDbTarget {
        owner: None,
        encoding: None,
        lc_collate: None,
        lc_ctype: None,
        schema: PostgresSchemaTarget::Any,
      },
    }
  }

  pub fn owner(mut self, owner: PgRoleName) -> Self {
    self.target.owner = Some(owner);
    self
  }

  pub fn encoding(mut self, encoding: impl ToString) -> Self {
    self.target.encoding = Some(encoding.to_string());
    self
  }

  pub fn lc_collate(mut self, lc_collate: impl ToString) -> Self {
    self.target.lc_collate = Some(lc_collate.to_string());
    self
  }

  pub fn lc_ctype(mut self, lc_ctype: impl ToString) -> Self {
    self.target.lc_ctype = Some(lc_ctype.to_string());
    self
  }

  pub fn locale(mut self, locale: impl ToString) -> Self {
    let l = locale.to_string();
    self.target.lc_collate = Some(l.clone());
    self.target.lc_ctype = Some(l);
    self
  }

  pub fn force_empty_schema(mut self) -> Self {
    self.target.schema = PostgresSchemaTarget::Empty;
    self
  }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq, Serialize, Deserialize)]
pub enum PostgresSchemaTarget {
  Any,
  Empty,
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct PostgresDbTarget {
  owner: Option<PgRoleName>,
  encoding: Option<String>,
  lc_collate: Option<String>,
  lc_ctype: Option<String>,
  schema: PostgresSchemaTarget,
}

// #[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
// pub enum PostgresDbError {}
// TODO: Detect and fix collection version errors. It requires running
//       `ALTER DATABASE <dbName> REFRESH COLLATION VERSION;` on all DBs.

#[async_trait]
impl<'h, H: Host> AsyncFn<&'h H> for PostgresDb {
  type Output = TaskResult<PgDb, anyhow::Error>;

  async fn run(&self, _host: &'h H) -> Self::Output {
    // TODO: Create cluster

    let opt = sqlx::postgres::PgConnectOptions::new().username("postgres");
    let mut conn = sqlx::postgres::PgConnection::connect_with(&opt).await?;

    PostgresDbRunner::with_conn(&mut conn, self).await
  }
}

struct PostgresDbRunner;

impl PostgresDbRunner {
  async fn with_conn(cx: &mut PgConnection, input: &PostgresDb) -> TaskResult<PgDb, anyhow::Error> {
    // TODO: Only run the query if actually needed
    if input.target.schema == PostgresSchemaTarget::Empty {
      let drop_query = DropDatabaseQuery::new(&input.name).if_exists(true).force(true);
      drop_query.execute(&mut *cx).await?;
    }

    let old: Option<PgDb>;

    let mut create_query = CreateDatabaseQuery::new(&input.name);
    if let Some(owner) = &input.target.owner {
      create_query = create_query.owner(owner.as_str());
    }
    if let Some(encoding) = &input.target.encoding {
      create_query = create_query.encoding(encoding);
    }
    if let Some(lc_collate) = &input.target.lc_collate {
      create_query = create_query.lc_collate(lc_collate);
    }
    if let Some(lc_ctype) = &input.target.lc_ctype {
      create_query = create_query.lc_ctype(lc_ctype);
    }

    match create_query.execute(&mut *cx).await {
      Ok(()) => old = None,
      Err(CreateDatabaseError::AlreadyExists) => {
        let old_db: PgDb = get_db_by_name(&mut *cx, &input.name)
          .await?
          .expect("Db should already exist");

        let mut was_altered: bool = false;

        // <https://www.postgresql.org/docs/current/locale.html>
        // > Some locale categories must have their values fixed when the database is created.
        // > You can use different settings for different databases, but once a database is created,
        // > you cannot change them for that database anymore. `LC_COLLATE` and `LC_CTYPE` are
        // > these categories.
        if let Some(encoding) = &input.target.encoding {
          if encoding != &old_db.encoding.name {
            panic!("ImmutableDbEncoding");
          }
        }
        if let Some(lc_collate) = &input.target.lc_collate {
          if lc_collate != &old_db.lc_collate {
            panic!("ImmutableDbLcCollate");
          }
        }
        if let Some(lc_ctype) = &input.target.lc_ctype {
          if lc_ctype != &old_db.lc_ctype {
            panic!("ImmutableDbLcCtype");
          }
        }

        if let Some(owner) = &input.target.owner {
          if owner.as_str() != old_db.owner.name.as_str() {
            AlterDatabase::new(&input.name)
              .owner(owner.as_str())
              .execute(&mut *cx)
              .await?;
            was_altered = true;
          }
        }

        if !was_altered {
          return Ok(TaskSuccess {
            changed: false,
            output: old_db,
          });
        }

        old = Some(old_db);
      }
      Err(e) => return Err(e.into()),
    };

    let new = get_db_by_name(&mut *cx, &input.name).await?;

    let new = match new {
      Some(n) => n,
      None => return Err(anyhow::Error::msg("FailedToCreateDb")),
    };

    Ok(TaskSuccess {
      changed: Some(&new) != old.as_ref(),
      output: new,
    })
  }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct PgDb {
  /// Row identifier
  oid: Oid,
  /// Role name
  name: String,
  /// Owner of the database, usually the user who created it
  owner: PgRoleRef,
  encoding: PgEncodingRef,
  lc_collate: String,
  lc_ctype: String,
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct PgRoleRef {
  oid: Oid,
  name: String,
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct PgEncodingRef {
  id: i32,
  name: String,
}

pub async fn get_db_by_name(
  executor: impl sqlx::Executor<'_, Database = Postgres>,
  name: &str,
) -> Result<Option<PgDb>, sqlx::Error> {
  #[derive(Debug, sqlx::FromRow)]
  #[allow(unused)]
  struct Row {
    oid: Oid,
    datname: String,
    datdba: Oid,
    datdba_name: String,
    encoding: i32,
    encoding_name: String,
    datcollate: String,
    datctype: String,
    datistemplate: bool,
    datallowconn: bool,
    datconnlimit: i32,
    // datlastsysoid: u32, (removed in Pg15)
    dattablespace: Oid,
  }

  impl From<Row> for PgDb {
    fn from(row: Row) -> Self {
      Self {
        oid: row.oid,
        name: row.datname,
        owner: PgRoleRef {
          oid: row.datdba,
          name: row.datdba_name,
        },
        encoding: PgEncodingRef {
          id: row.encoding,
          name: row.encoding_name,
        },
        lc_collate: row.datcollate,
        lc_ctype: row.datctype,
      }
    }
  }

  let row: Option<Row> = sqlx::query_as::<Postgres, Row>(
    r"
      SELECT oid, datname,
        datdba, pg_get_userbyid(datdba) AS datdba_name,
        pg_database.encoding, pg_catalog.pg_encoding_to_char(pg_database.encoding) as encoding_name,
        pg_database.datcollate, pg_database.datctype,
        pg_database.datistemplate, pg_database.datallowconn,
        pg_database.datconnlimit,
        pg_database.dattablespace
      FROM pg_database
      WHERE pg_database.datname = $1::TEXT;
    ",
  )
  .bind(name)
  .fetch_optional(executor)
  .await?;
  Ok(row.map(PgDb::from))
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct PgSchema {
  /// Row identifier
  oid: Oid,
  /// Schema name
  name: String,
  /// Owner of the schema, usually the user who created it
  owner: PgRoleRef,
}

pub async fn get_schema_by_name(
  executor: impl sqlx::Executor<'_, Database = Postgres>,
  name: &str,
) -> Result<Option<PgSchema>, sqlx::Error> {
  #[derive(Debug, sqlx::FromRow)]
  struct Row {
    oid: Oid,
    nspname: String,
    nspowner: Oid,
    nspowner_name: String,
    // nspacl: Vec<String>,
  }

  impl From<Row> for PgSchema {
    fn from(row: Row) -> Self {
      Self {
        oid: row.oid,
        name: row.nspname,
        owner: PgRoleRef {
          oid: row.nspowner,
          name: row.nspowner_name,
        },
      }
    }
  }

  let row: Option<Row> = sqlx::query_as::<_, Row>(
    r"
      SELECT oid, nspname,
        nspowner, pg_catalog.pg_get_userbyid(nspowner) AS nspowner_name
      FROM pg_catalog.pg_namespace
      WHERE nspname = $1::TEXT;
    ",
  )
  .bind(name)
  .fetch_optional(executor)
  .await?;
  Ok(row.map(PgSchema::from))
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct PostgresDbPermissions {
  db: String,
  admin_user: PgRoleName,
  write_user: PgRoleName,
  read_user: PgRoleName,
}

impl PostgresDbPermissions {
  pub fn new(db: impl ToString, admin_user: PgRoleName, write_user: PgRoleName, read_user: PgRoleName) -> Self {
    Self {
      db: db.to_string(),
      admin_user,
      write_user,
      read_user,
    }
  }

  async fn with_tx(tx: &mut Transaction<'_, Postgres>, input: &PostgresDbPermissions) -> TaskResult<(), anyhow::Error> {
    let public_schema = get_schema_by_name(tx.as_mut(), "public")
      .await?
      .expect("public schema must exist");
    if public_schema.owner.name != input.admin_user.as_str() {
      let query: String = format!(
        "ALTER SCHEMA public OWNER TO {};",
        PgSqlQuotedId(input.admin_user.as_str())
      );
      sqlx::query(&query).execute(tx.as_mut()).await?;
    }

    let queries = [
      // Disable default grants on `public`
      Revoke::from_public()
        .on_dependent_cascade()
        .on_schema("public")
        .all()
        .to_string(),
      // Configure `write_user`
      AlterDefaultPrivileges::for_role(input.admin_user.as_ref())
        .in_schema("public")
        .grant_on_tables_to_role(input.write_user.as_ref())
        .all()
        .to_string(),
      AlterDefaultPrivileges::for_role(input.admin_user.as_ref())
        .in_schema("public")
        .grant_on_sequences_to_role(input.write_user.as_ref())
        .all()
        .to_string(),
      Grant::to_role(input.write_user.as_ref())
        .on_all_tables_in("public")
        .all()
        .to_string(),
      Grant::to_role(input.write_user.as_ref())
        .on_all_sequences_in("public")
        .all()
        .to_string(),
      Grant::to_role(input.write_user.as_ref())
        .on_schema("public")
        .usage()
        .to_string(),
      Grant::to_role(input.write_user.as_ref())
        .on_database(&input.db)
        .connect()
        .to_string(),
      // Configure `read_user`
      AlterDefaultPrivileges::for_role(input.admin_user.as_ref())
        .in_schema("public")
        .grant_on_tables_to_role(input.read_user.as_ref())
        .select()
        .to_string(),
      AlterDefaultPrivileges::for_role(input.admin_user.as_ref())
        .in_schema("public")
        .grant_on_sequences_to_role(input.read_user.as_ref())
        .select()
        .to_string(),
      Grant::to_role(input.read_user.as_ref())
        .on_all_tables_in("public")
        .select()
        .to_string(),
      Grant::to_role(input.read_user.as_ref())
        .on_all_sequences_in("public")
        .select()
        .to_string(),
      Grant::to_role(input.read_user.as_ref())
        .on_schema("public")
        .usage()
        .to_string(),
      Grant::to_role(input.read_user.as_ref())
        .on_database(&input.db)
        .connect()
        .to_string(),
    ];

    for query in queries.iter() {
      sqlx::query(query).execute(tx.as_mut()).await?;
    }

    Ok(TaskSuccess {
      changed: true,
      output: (),
    })
  }
}

#[async_trait]
impl<'h, H: Host> AsyncFn<&'h H> for PostgresDbPermissions {
  type Output = TaskResult<(), anyhow::Error>;

  async fn run(&self, _host: &'h H) -> <Self as AsyncFn<&'h H>>::Output {
    let opt = sqlx::postgres::PgConnectOptions::new()
      .username("postgres")
      .database(&self.db);
    let mut conn = sqlx::postgres::PgConnection::connect_with(&opt).await?;

    let mut tx = conn.begin().await?;

    let res = PostgresDbPermissions::with_tx(&mut tx, self).await;

    if res.is_ok() {
      tx.commit().await?;
    } else {
      tx.rollback().await?;
    }

    res
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct PostgresUrl(Url);

impl PostgresUrl {
  pub fn new(
    host: impl AsRef<str>,
    port: u16,
    name: impl AsRef<str>,
    user: impl AsRef<str>,
    password: impl AsRef<str>,
    version: Option<u8>,
    connection_limit: Option<u16>,
  ) -> Result<Self, url::ParseError> {
    let mut url = Url::parse("postgresql://localhost").expect("Expected default Postgres URI to be valid");
    url.set_host(Some(host.as_ref()))?;
    url
      .set_port(Some(port))
      .expect("Setting the port should always succeed");
    {
      let mut segments = url
        .path_segments_mut()
        .expect("Getting the path segments should always succeed");
      segments.push(name.as_ref());
    }
    url
      .set_username(user.as_ref())
      .expect("Setting the user should always succeed");
    url
      .set_password(Some(password.as_ref()))
      .expect("Setting the password should always succeed");
    {
      let mut query = url.query_pairs_mut();
      if let Some(version) = version {
        query.append_pair("version", version.to_string().as_str());
      }
      if let Some(connection_limit) = connection_limit {
        query.append_pair("connection_limit", connection_limit.to_string().as_str());
      }
      query.append_pair("charset", "utf8");
    }
    Ok(Self(url))
  }
}

impl fmt::Display for PostgresUrl {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    fmt::Display::fmt(&self.0, f)
  }
}

#[cfg(test)]
mod test {
  use crate::task::postgres::DigestFile;

  #[test]
  fn test_parse_digest_md5() {
    let input = "0086bddfe40106075daf94d042784a83  postgresql-15.1.tar.gz\n".to_string();
    let digest = DigestFile::<16>::new(input)
      .get_digest("postgresql-15.1.tar.gz")
      .expect("digest not found");
    assert_eq!(
      digest,
      [0x00, 0x86, 0xbd, 0xdf, 0xe4, 0x01, 0x06, 0x07, 0x5d, 0xaf, 0x94, 0xd0, 0x42, 0x78, 0x4a, 0x83]
    )
  }

  #[test]
  fn test_parse_digest_md5_legacy() {
    let input = "MD5(postgresql-6.5.3.tar.gz)= 098d1ee4316500a2d033ced3ebd5b831\n".to_string();
    let digest = DigestFile::<16>::new(input)
      .get_digest("postgresql-6.5.3.tar.gz")
      .expect("digest not found");
    assert_eq!(
      digest,
      [0x09, 0x8d, 0x1e, 0xe4, 0x31, 0x65, 0x00, 0xa2, 0xd0, 0x33, 0xce, 0xd3, 0xeb, 0xd5, 0xb8, 0x31]
    )
  }

  #[test]
  fn test_parse_digest_md5_legacy_space() {
    let input = "MD5 (postgresql-7.0.tar.gz) = 7fefc2f9d9430a4012aac6c78497db85\n".to_string();
    let digest = DigestFile::<16>::new(input)
      .get_digest("postgresql-7.0.tar.gz")
      .expect("digest not found");
    assert_eq!(
      digest,
      [0x7f, 0xef, 0xc2, 0xf9, 0xd9, 0x43, 0x0a, 0x40, 0x12, 0xaa, 0xc6, 0xc7, 0x84, 0x97, 0xdb, 0x85,]
    )
  }

  #[test]
  fn test_parse_digest_sha256() {
    let input =
      "f9c7c9bda9d9359bb137ed510ac1cf90e6afa1f739c0616bb6b637b2bc2cb604  postgresql-15.0.tar.gz\n".to_string();
    let digest = DigestFile::<32>::new(input)
      .get_digest("postgresql-15.0.tar.gz")
      .expect("digest not found");
    assert_eq!(
      digest,
      [
        0xf9, 0xc7, 0xc9, 0xbd, 0xa9, 0xd9, 0x35, 0x9b, 0xb1, 0x37, 0xed, 0x51, 0x0a, 0xc1, 0xcf, 0x90, 0xe6, 0xaf,
        0xa1, 0xf7, 0x39, 0xc0, 0x61, 0x6b, 0xb6, 0xb6, 0x37, 0xb2, 0xbc, 0x2c, 0xb6, 0x04,
      ]
    )
  }
}
