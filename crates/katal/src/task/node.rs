use crate::task::{AsyncFn, TaskResult, TaskSuccess};
use async_trait::async_trait;
use asys::Host;
use katal_loader::tree::TarGz;
use semver::{Version, VersionReq};
use serde::de::Error;
use serde::{Deserialize, Deserializer, Serialize, Serializer};
use std::collections::BTreeMap;
use url::Url;

pub struct GetNode {
  release: NodeRelease,
}

impl GetNode {
  pub fn release(release: NodeRelease) -> Self {
    Self { release }
  }
}

#[async_trait]
impl<'h, H: Host> AsyncFn<&'h H> for GetNode {
  type Output = TaskResult<TarGz, anyhow::Error>;

  async fn run(&self, _host: &'h H) -> Self::Output {
    let client = reqwest::Client::builder()
      .user_agent("katal")
      .build()
      .expect("building the client always succeeds");
    let url = self.release.download_url(NodePlatform::Linux, NodeArch::X64);
    let res = client.get(url.clone()).send().await?;
    if res.status().is_success() {
      let archive = res.bytes().await?;
      let tree = TarGz::new(&archive)?;
      Ok(TaskSuccess {
        changed: false,
        output: tree,
      })
    } else {
      Err(anyhow::Error::msg(format!("download error: [{}] {url}", res.status())))
    }
  }
}

/// Node index
///
/// See:
/// - <https://github.com/nodejs/nodejs-dist-indexer>
/// - <https://nodejs.org/download/release/index.json>
#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct NodeIndex {
  by_version: BTreeMap<Version, NodeRelease>,
}

impl NodeIndex {
  pub fn embedded() -> Self {
    Self::from_raw(serde_json::from_str(include_str!("../../files/node/index.json")).expect("embedded index is valid"))
      .expect("embedded index is valid")
  }

  fn from_raw(raw: RawNodeIndex) -> Result<Self, anyhow::Error> {
    Ok(Self {
      by_version: raw
        .0
        .into_iter()
        .enumerate()
        .map(|(index, raw)| {
          NodeRelease::from_raw(raw)
            .map(|release| (release.version.clone(), release))
            .map_err(|e| e.context(format!("index: {index}")))
        })
        .collect::<Result<_, _>>()?,
    })
  }

  pub fn resolve(&self, req: &VersionReq) -> Option<&NodeRelease> {
    self.by_version.values().rev().find(|e| req.matches(&e.version))
  }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct NodeRelease {
  pub version: Version,
  date: String,
  files: Vec<String>,
  npm: Option<Version>,
  v8: String,
  uv: Option<String>,
  zlib: Option<String>,
  openssl: Option<String>,
  modules: Option<String>,
  lts: Option<LtsCodename>,
  security: bool,
}

impl NodeRelease {
  pub fn download_url(&self, platform: NodePlatform, arch: NodeArch) -> Url {
    let mut url = Url::parse("https://nodejs.org/download/release/").expect("valid URL");
    {
      let mut segments = url.path_segments_mut().unwrap();
      segments.push(format!("v{}", self.version).as_str());
      let platform = match platform {
        NodePlatform::Linux => "linux",
        NodePlatform::Darwin => "darwin",
        NodePlatform::Aix => "aix",
        NodePlatform::Win => "win",
      };
      let arch = match arch {
        NodeArch::Arm64 => "arm64",
        NodeArch::Armv7l => "arm7l",
        NodeArch::Ppc64 => "ppc64",
        NodeArch::X64 => "x64",
        NodeArch::X86 => "x86",
        NodeArch::Ppc64le => "ppc64le",
        NodeArch::S390x => "s390x",
      };
      // TODO: Use `NodeFormat`
      let ext = ".tar.gz";
      segments.push(format!("node-v{}-{platform}-{arch}{ext}", self.version).as_str());
    }
    url
  }

  pub fn from_raw(raw: RawNodeRelease) -> Result<Self, anyhow::Error> {
    let version = raw.version.strip_prefix('v').unwrap_or(raw.version.as_str());
    Ok(Self {
      version: Version::parse(version)?,
      date: raw.date,
      files: raw.files,
      npm: raw
        .npm
        .as_deref()
        .map(Version::parse)
        .transpose()
        .map_err(anyhow::Error::from)
        .map_err(|e| e.context("npm version"))?,
      v8: raw.v8,
      uv: raw.uv,
      zlib: raw.zlib,
      openssl: raw.openssl,
      modules: raw.modules,
      lts: match raw.lts {
        StringOrFalse::False => None,
        StringOrFalse::String(codename) => Some(LtsCodename(codename)),
      },
      security: raw.security,
    })
  }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct RawNodeIndex(Vec<RawNodeRelease>);

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct RawNodeRelease {
  version: String,
  date: String,
  files: Vec<String>,
  npm: Option<String>,
  v8: String,
  uv: Option<String>,
  zlib: Option<String>,
  openssl: Option<String>,
  modules: Option<String>,
  lts: StringOrFalse,
  security: bool,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
enum StringOrFalse {
  False,
  String(String),
}

impl Serialize for StringOrFalse {
  fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
  where
    S: Serializer,
  {
    match self {
      Self::False => serializer.serialize_bool(false),
      Self::String(s) => serializer.serialize_str(s),
    }
  }
}

impl<'de> Deserialize<'de> for StringOrFalse {
  fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
  where
    D: Deserializer<'de>,
  {
    #[derive(Debug, Deserialize)]
    #[serde(untagged)]
    enum RawStringOrFalse {
      Bool(bool),
      String(String),
    }
    let raw = RawStringOrFalse::deserialize(deserializer)?;
    match raw {
      RawStringOrFalse::Bool(false) => Ok(Self::False),
      RawStringOrFalse::String(s) => Ok(Self::String(s)),
      RawStringOrFalse::Bool(true) => Err(D::Error::custom("unexpected value `true`")),
    }
  }
}

/// See:
/// - <https://github.com/nodejs/Release/blob/main/CODENAMES.md>
/// - <https://raw.githubusercontent.com/nodejs/Release/main/schedule.json>
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
struct LtsCodename(String);

pub enum NodePlatform {
  Aix,
  Darwin,
  Linux,
  Win,
}

pub enum NodeArch {
  Ppc64,
  Ppc64le,
  Arm64,
  Armv7l,
  X64,
  X86,
  S390x,
}

pub enum NodeFormat {
  None,
  SevenZip,
  Exe,
  Zip,
  Msi,
  Pkg,
  Tar,
}

#[cfg(test)]
mod test {
  use crate::task::node::NodeIndex;
  use semver::VersionReq;

  #[test]
  fn get_latest() {
    let index = NodeIndex::embedded();
    let version = index
      .resolve(&VersionReq::parse("^18.0.0").unwrap())
      .map(|e| e.version.to_string());
    assert_eq!(version, Some("18.19.1".to_string()));
  }

  // Commented out to avoid depending on the internet
  // #[tokio::test]
  // async fn download_latest() {
  //   let index = NodeIndex::embedded();
  //   let latest = index.resolve(&VersionReq::parse("=18.10.0").unwrap()).unwrap();
  //   let node: TarGz = GetNode {
  //     release: latest.clone(),
  //   }
  //   .run(&LocalLinux)
  //   .await
  //   .unwrap()
  //   .output;
  //   assert_eq!(node.len(), 43_229_335);
  // }
}
