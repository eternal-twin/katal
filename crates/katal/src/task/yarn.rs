use crate::task::fs::{EnsureDir, EnsureDirError, EnsureFile, EnsureFileError};
use crate::task::witness::{FsWitness, FsWitnessError};
use crate::task::{AsyncFn, TaskResult, TaskSuccess};
use async_trait::async_trait;
use asys::common::fs::FsHost;
use asys::linux::fs::{FileMode, LinuxFsHost, LinuxMetadata};
use asys::linux::user::{LinuxUserHost, UserRef};
use asys::ExecHost;
use serde::{Deserialize, Serialize};
pub use sqlx::postgres::types::Oid;
use std::fmt::Debug;
use std::path::PathBuf;
use thiserror::Error;
use url::Url;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct YarnReady {
  pub version: String,
}

impl YarnReady {
  pub fn new(version: String) -> Self {
    Self { version }
  }
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, Error)]
pub enum YarnReadyError {
  #[error("failed to use witness file")]
  Witness(#[source] FsWitnessError),
  #[error("yarn version not found, use 1.22.22")]
  VersionNotFound,
  #[error("failed to ensure build dir exists")]
  EnsureBuildDir(#[source] EnsureDirError),
  #[error("failed to ensure yarn.js file")]
  EnsureYarnJs(#[source] EnsureFileError),
  #[error("failed to ensure yarn exe file")]
  EnsureYarnExe(#[source] EnsureFileError),
  #[error("request failed: {0}")]
  Request(String),
  #[error("response has error status")]
  ResponseStatus,
  #[error("response failed: {0}")]
  Response(String),
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct YarnEnv {
  pub bin_dir: PathBuf,
}

#[async_trait]
impl<'h, H: ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for YarnReady
where
  <H as FsHost>::Metadata: LinuxMetadata,
  H::Group: Send + Sync,
  H::User: Send + Sync + Debug,
{
  type Output = TaskResult<YarnEnv, YarnReadyError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let yarn_dir = PathBuf::from(format!("/katal/store/yarn-{}", self.version));
    let t = FsWitness::new(yarn_dir.join("ok"), InstallYarn(self));
    AsyncFn::<&'h H>::run(&t, host)
      .await
      .map_err(YarnReadyError::Witness)?
      .output
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize)]
struct InstallYarn<'a>(&'a YarnReady);

#[async_trait]
impl<'a, 'h, H: ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for InstallYarn<'a>
where
  <H as FsHost>::Metadata: LinuxMetadata,
  H::Group: Send + Sync,
  H::User: Send + Sync + Debug,
{
  type Output = TaskResult<YarnEnv, YarnReadyError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let this = &self.0;

    let mut changed = false;

    if this.version != "1.22.22" {
      return Err(YarnReadyError::VersionNotFound);
    }

    let yarn_dir = PathBuf::from(format!("/katal/store/yarn-{}", this.version));
    let bin_dir = yarn_dir.join("bin");
    let env = YarnEnv {
      bin_dir: bin_dir.clone(),
    };

    eprintln!("ensure yarn target dir exists");
    changed = EnsureDir::new(PathBuf::from("/katal"))
      .owner(UserRef::ROOT)
      .run(host)
      .await
      .map_err(YarnReadyError::EnsureBuildDir)?
      .changed
      || changed;

    changed = EnsureDir::new(PathBuf::from("/katal/store"))
      .owner(UserRef::ROOT)
      .run(host)
      .await
      .map_err(YarnReadyError::EnsureBuildDir)?
      .changed
      || changed;

    changed = EnsureDir::new(yarn_dir.clone())
      .owner(UserRef::ROOT)
      .run(host)
      .await
      .map_err(YarnReadyError::EnsureBuildDir)?
      .changed
      || changed;

    changed = EnsureDir::new(bin_dir.clone())
      .owner(UserRef::ROOT)
      .run(host)
      .await
      .map_err(YarnReadyError::EnsureBuildDir)?
      .changed
      || changed;

    eprintln!("target dir exists");

    let url = Url::parse(&format!(
      "https://github.com/yarnpkg/yarn/releases/download/v{version}/yarn-{version}.js",
      version = this.version
    ))
    .expect("url is well formed");
    eprintln!("download Yarn: {url}");

    let res = reqwest::Client::new()
      .get(url.as_str())
      .send()
      .await
      .map_err(|e| YarnReadyError::Request(e.to_string()))?;
    if !res.status().is_success() {
      return Err(YarnReadyError::ResponseStatus);
    }
    let yarn_js: Vec<u8> = res
      .bytes()
      .await
      .map_err(|e| YarnReadyError::Response(e.to_string()))?
      .to_vec();
    let yarn_js_path = bin_dir.join("yarn.js");
    eprintln!("write yarn.js");
    changed = EnsureFile::new(yarn_js_path)
      .content(yarn_js)
      .mode(FileMode::ALL_READ)
      .run(host)
      .await
      .map_err(YarnReadyError::EnsureYarnJs)?
      .changed
      || changed;

    // language=sh
    let yarn_exe: String = String::from(
      r#"#!/bin/sh
argv0=$(echo "$0" | sed -e 's,\\,/,g')
basedir=$(dirname "$(readlink "$0" || echo "$argv0")")

case "$(uname -s)" in
  Darwin) basedir="$( cd "$( dirname "$argv0" )" && pwd )";;
  Linux) basedir=$(dirname "$(readlink -f "$0" || echo "$argv0")");;
  *CYGWIN*) basedir=`cygpath -w "$basedir"`;;
  *MSYS*) basedir=`cygpath -w "$basedir"`;;
esac

command_exists() {
  command -v "$1" >/dev/null 2>&1;
}

if command_exists node; then
  if [ "$YARN_FORCE_WINPTY" = 1 ] || command_exists winpty && test -t 1; then
    winpty node "$basedir/yarn.js" "$@"
  else
    exec node "$basedir/yarn.js" "$@"
  fi
  ret=$?
else
  >&2 echo 'Yarn requires Node.js 4.0 or higher to be installed.'
  ret=1
fi

exit $ret
"#,
    );
    let yarn_exe_path = bin_dir.join("yarn");
    eprintln!("write yarn");
    changed = EnsureFile::new(yarn_exe_path)
      .content(yarn_exe)
      .mode(FileMode::ALL_READ | FileMode::ALL_EXEC)
      .run(host)
      .await
      .map_err(YarnReadyError::EnsureYarnExe)?
      .changed
      || changed;

    eprintln!("Yarn build complete");

    Ok(TaskSuccess { changed, output: env })
  }
}
