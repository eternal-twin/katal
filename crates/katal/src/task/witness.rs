use crate::task::fs::{EnsureFile, EnsureFileError};
use crate::task::{AsyncFn, TaskResult, TaskSuccess};
use crate::utils::digest::DigestSha2_256;
use crate::utils::display_error_chain::DisplayErrorChain;
use async_trait::async_trait;
use asys::common::fs::{FsHost, ReadFileError};
use asys::linux::fs::{LinuxFsHost, LinuxMetadata};
use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};
use std::fmt::Debug;
use std::path::PathBuf;
use thiserror::Error;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct FsWitness<TyTask> {
  pub path: PathBuf,
  pub task: TyTask,
}

impl<TyTask> FsWitness<TyTask> {
  pub fn new(path: PathBuf, task: TyTask) -> Self {
    Self { path, task }
  }
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, Error)]
pub enum FsWitnessError {
  #[error("failed to check witness file")]
  CheckWitness(#[source] ReadFileError),
  #[error("failed to ensure witness file")]
  EnsureWitness(#[source] EnsureFileError),
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
struct WitnessCacheKey<TyInput> {
  input: TyInput,
  katal_revision: String,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
struct WitnessData<TyOut> {
  input_digest: DigestSha2_256,
  output: TyOut,
}

#[async_trait]
impl<'h, H, TyTask> AsyncFn<&'h H> for FsWitness<TyTask>
where
  H: LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  TyTask: AsyncFn<&'h H> + Send + Sync + Serialize,
  for<'hh> <TyTask as AsyncFn<&'hh H>>::Output: DeserializeOwned + Serialize + Send,
{
  type Output = TaskResult<TyTask::Output, FsWitnessError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    eprintln!("check witness file: {}", self.path.display());
    let actual_data: Option<WitnessData<TyTask::Output>> = match host.read_file(self.path.as_path()) {
      Ok(data) => match serde_json::from_slice::<WitnessData<TyTask::Output>>(&data) {
        Ok(actual) => {
          eprintln!("actual digest: {}", actual.input_digest);
          Some(actual)
        }
        Err(e) => {
          eprintln!("malformed witness file: {}", DisplayErrorChain(&e));
          None
        }
      },
      Err(ReadFileError::NotFound) => None,
      Err(e) => {
        return Err(FsWitnessError::CheckWitness(e));
      }
    };
    let task_state = serde_json::to_vec(&WitnessCacheKey {
      input: &self.task,
      katal_revision: String::from(crate::meta::GIT_REVISION),
    })
    .expect("task serialization succeeds");
    let expected_digest = DigestSha2_256::digest(&task_state);
    eprintln!("expected digest: {expected_digest}");
    match actual_data {
      Some(actual_data) if actual_data.input_digest == expected_digest => {
        eprintln!("actual digest matches expected, skipping task and using cached result");
        Ok(TaskSuccess {
          changed: false,
          output: actual_data.output,
        })
      }
      _ => {
        eprintln!("actual digest does not match expected, running task");
        let output = self.task.run(host).await;
        let data = WitnessData {
          input_digest: expected_digest,
          output,
        };
        let witness_data = serde_json::to_vec(&data).expect("output serialization succeeds");
        EnsureFile::new(self.path.as_path())
          .content(witness_data)
          .run(host)
          .await
          .map_err(FsWitnessError::EnsureWitness)?;
        eprintln!("task output cached in witness file");
        Ok(TaskSuccess {
          changed: true,
          output: data.output,
        })
      }
    }
  }
}
