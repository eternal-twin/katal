use crate::task::fs::{
  EnsureDir, EnsureDirError, EnsureFile, EnsureFileError, EnsureFileSymlink, EnsureFileSymlinkError,
};
use crate::task::pacman::{EnsurePacmanPackages, EnsurePacmanPackagesError};
use crate::task::systemd::{
  get_status, ActiveState, SystemdGetStatusError, SystemdState, SystemdUnit, SystemdUnitError,
};
use crate::task::{AsyncFn, TaskResult, TaskSuccess};
use async_trait::async_trait;
use asys::common::fs::FsHost;
use asys::linux::fs::{FileMode, LinuxFsHost, LinuxMetadata};
use asys::linux::user::{LinuxUserHost, Uid, UserRef};
use asys::{Command, ExecError, ExecHost, TryExecError};
use serde::{Deserialize, Serialize};
use std::fmt::Debug;
use std::path::PathBuf;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[non_exhaustive]
pub struct NginxReady {}

impl NginxReady {
  #[allow(clippy::new_without_default)]
  pub fn new() -> Self {
    Self {}
  }
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, thiserror::Error)]
pub enum NginxReadyError {
  #[error("failed to ensure `nginx` package presence")]
  EnsurePackage(#[source] EnsurePacmanPackagesError),
  #[error("failed to ensure nginx base config")]
  BaseConfig(#[source] EnsureNginxBaseConfigError),
  #[error("failed to enable systemd unit")]
  SystemdUnit(#[source] SystemdUnitError),
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct NginxEnv {
  pub config_dir: PathBuf,
}

#[async_trait]
impl<'h, H: ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for NginxReady
where
  <H as FsHost>::Metadata: LinuxMetadata,
  H::Group: Send + Sync,
  H::User: Send + Sync + Debug,
{
  type Output = TaskResult<NginxEnv, NginxReadyError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let mut changed = false;
    changed = EnsurePacmanPackages::new()
      .present("nginx")
      .run(host)
      .await
      .map_err(NginxReadyError::EnsurePackage)?
      .changed
      || changed;
    changed = EnsureNginxBaseConfig
      .run(host)
      .await
      .map_err(NginxReadyError::BaseConfig)?
      .changed
      || changed;

    eprintln!("enabling nginx service");
    changed = SystemdUnit::new("nginx")
      .enabled(true)
      .state(SystemdState::Active)
      .run(host)
      .await
      .map_err(NginxReadyError::SystemdUnit)?
      .changed
      || changed;

    Ok(TaskSuccess {
      changed,
      output: NginxEnv {
        config_dir: PathBuf::from("/etc/nginx"),
      },
    })
  }
}

pub struct EnsureNginxBaseConfig;

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, thiserror::Error)]
pub enum EnsureNginxBaseConfigError {
  #[error("failed to ensure presence of nginx package")]
  EnsureNginxPresent(#[from] EnsurePacmanPackagesError),
  #[error("failed to ensure base config file")]
  EnsureConfig(#[from] EnsureFileError),
  #[error("failed to ensure `sites-available` directory")]
  SitesAvailable(#[source] EnsureDirError),
  #[error("failed to ensure `sites-enabled` directory")]
  SitesEnabled(#[source] EnsureDirError),
  #[error("failed to execute `nginx -t` (config test)")]
  ExecTest(#[source] TryExecError),
  #[error("detected invalid nginx config: {0}")]
  InvalidConfig(String),
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for EnsureNginxBaseConfig
where
  H: ExecHost + LinuxFsHost + LinuxUserHost,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<(), EnsureNginxBaseConfigError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let mut changed = EnsurePacmanPackages::new().present("nginx").run(host).await?.changed;
    const BASE_CONFIG: &str = include_str!("../../files/nginx/nginx.conf");

    changed = EnsureFile::new("/etc/nginx/nginx.conf")
      .content(BASE_CONFIG)
      .mode(FileMode::ALL_READ)
      .owner(Uid::ROOT)
      .run(host)
      .await?
      .changed
      || changed;

    changed = EnsureDir::new(PathBuf::from("/etc/nginx/sites-available"))
      .mode(FileMode::OWNER_WRITE | FileMode::ALL_READ | FileMode::ALL_EXEC)
      .owner(UserRef::Id(Uid::ROOT))
      .run(host)
      .await
      .map_err(EnsureNginxBaseConfigError::SitesAvailable)?
      .changed
      || changed;

    changed = EnsureDir::new(PathBuf::from("/etc/nginx/sites-enabled"))
      .mode(FileMode::OWNER_WRITE | FileMode::ALL_READ | FileMode::ALL_EXEC)
      .owner(UserRef::Id(Uid::ROOT))
      .run(host)
      .await
      .map_err(EnsureNginxBaseConfigError::SitesEnabled)?
      .changed
      || changed;

    let test = Command::new("nginx").arg("-t").hint_no_host_mutation();
    let out = host.try_exec(&test).map_err(EnsureNginxBaseConfigError::ExecTest)?;
    if !out.termination.success() {
      return Err(EnsureNginxBaseConfigError::InvalidConfig(
        String::from_utf8_lossy(&out.stderr).to_string(),
      ));
    }

    Ok(TaskSuccess { changed, output: () })
  }
}

/// Check that the config is valid.
/// If the config is valid, reload nginx.
pub struct TrySyncNginxConfig;

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, thiserror::Error)]
pub enum TrySyncNginxConfigError {
  #[error("failed to ensure base nginx config")]
  EnsureNginxBaseConfig(#[from] EnsureNginxBaseConfigError),
  #[error("failed to execute `nginx -t` (config test)")]
  ExecTest(#[source] TryExecError),
  #[error("detected invalid nginx config: {0}")]
  InvalidConfig(String),
  #[error("failed to retrieve nginx status")]
  NginxServiceStatus(#[from] SystemdGetStatusError),
  #[error("failed to execute `nginx -s reload` (config reload)")]
  ExecReload(#[source] ExecError),
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for TrySyncNginxConfig
where
  H: ExecHost + LinuxFsHost + LinuxUserHost,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<(), TrySyncNginxConfigError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let changed = EnsureNginxBaseConfig.run(host).await?.changed;
    let test = Command::new("nginx").arg("-t").hint_no_host_mutation();
    let out = host.try_exec(&test).map_err(TrySyncNginxConfigError::ExecTest)?;
    if !out.termination.success() {
      return Err(TrySyncNginxConfigError::InvalidConfig(
        String::from_utf8_lossy(&out.stderr).to_string(),
      ));
    }
    let status = get_status("nginx").await?;
    let changed = if status.active_state == ActiveState::Active {
      let reload = Command::new("nginx").arg("-s").arg("reload");
      host.exec(&reload).map_err(TrySyncNginxConfigError::ExecReload)?;
      true
    } else {
      false
    } || changed;
    Ok(TaskSuccess { changed, output: () })
  }
}

pub struct NginxAvailableSite {
  name: String,
  config: String,
}

impl NginxAvailableSite {
  pub fn new(name: impl AsRef<str>, config: impl AsRef<str>) -> Self {
    Self {
      name: name.as_ref().to_string(),
      config: config.as_ref().to_string(),
    }
  }
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, thiserror::Error)]
pub enum NginxAvailableSiteError {
  #[error("failed to ensure `sites-available` nginx directory presence")]
  EnsureSitesAvailableDir(#[from] EnsureDirError),
  #[error("failed to ensure nginx config file")]
  EnsureConfig(#[from] EnsureFileError),
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for NginxAvailableSite
where
  H: LinuxFsHost + LinuxUserHost,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<(), NginxAvailableSiteError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let sites_available_dir = PathBuf::from("/etc/nginx/sites-available");
    let config_path = sites_available_dir.join(format!("{}.nginx", &self.name));

    EnsureDir::new(sites_available_dir)
      .mode(FileMode::OWNER_ALL | FileMode::ALL_READ | FileMode::ALL_EXEC)
      .owner(UserRef::ROOT)
      .run(host)
      .await?;

    Ok(
      EnsureFile::new(config_path)
        .content(self.config.as_bytes())
        .mode(FileMode::ALL_READ)
        .owner(Uid::ROOT)
        .run(host)
        .await?,
    )
  }
}

pub struct NginxEnableSite {
  name: String,
}

impl NginxEnableSite {
  pub fn new(name: impl AsRef<str>) -> Self {
    Self {
      name: name.as_ref().to_string(),
    }
  }
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize, thiserror::Error)]
pub enum NginxEnableSiteError {
  #[error("failed to ensure `sites-enabled` nginx directory presence")]
  EnsureSitesEnabledDir(#[from] EnsureDirError),
  #[error("failed to ensure symlink in `sites-enabled` for {1:?}")]
  EnsureSiteSymlink(EnsureFileSymlinkError, String),
  #[error("failed to sync nginx config")]
  SyncConfig(#[from] TrySyncNginxConfigError),
}

#[async_trait]
impl<'h, H: ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for NginxEnableSite
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<(), NginxEnableSiteError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    eprintln!("Enabling Nginx site: {}", &self.name);
    let sites_enable_dir = PathBuf::from("/etc/nginx/sites-enabled");
    let path = sites_enable_dir.join(format!("{}.nginx", &self.name));
    let pointee = PathBuf::from("/etc/nginx/sites-available").join(format!("{}.nginx", &self.name));

    EnsureDir::new(sites_enable_dir)
      .mode(FileMode::OWNER_ALL | FileMode::ALL_READ | FileMode::ALL_EXEC)
      .owner(UserRef::ROOT)
      .run(host)
      .await?;

    EnsureFileSymlink::new(path)
      .points_to(pointee)
      .owner(Uid::ROOT)
      .run(host)
      .await
      .map_err(|e| NginxEnableSiteError::EnsureSiteSymlink(e, self.name.clone()))?;

    Ok(TrySyncNginxConfig.run(host).await?)
  }
}
