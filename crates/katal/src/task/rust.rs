use crate::task::{exec_as, AsyncFn, NamedTask, TaskName, TaskResult, TaskSuccess};
use async_trait::async_trait;
use asys::common::fs::FsHost;
use asys::linux::fs::{LinuxFsHost, LinuxMetadata};
use asys::linux::user::{LinuxUserHost, Uid, UserRef};
use asys::local_linux::LocalLinux;
use asys::{Command, ExecHost};
use reqwest::Client;
use serde::{Deserialize, Serialize};
use std::path::PathBuf;

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct RustEnv {
  /// Absolute path to the cargo home
  ///
  /// This is `~/.cargo` by default, with a `bin` subdirectory containing the binaries.
  pub home: PathBuf,
}

impl RustEnv {
  pub fn bin_dir(&self) -> PathBuf {
    self.home.join("bin")
  }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Rustup {
  owner: Uid,
}

impl Rustup {
  pub fn new(owner: Uid) -> Self {
    Self { owner }
  }
}

#[async_trait]
impl<'h, H: ExecHost> AsyncFn<&'h H> for Rustup {
  type Output = TaskResult<RustEnv, anyhow::Error>;

  async fn run(&self, _host: &'h H) -> Self::Output {
    let res = exec_as::<LocalLinux, RustupCommand>(UserRef::Id(self.owner), None, &RustupCommand(self.clone())).await?;
    match res {
      Ok(p) => Ok(TaskSuccess {
        changed: true,
        output: p,
      }),
      Err(()) => Err(anyhow::Error::msg("FailedRustupInitialization")),
    }
  }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct RustupCommand(Rustup);

impl<'h, H: ExecHost + LinuxUserHost + LinuxFsHost> NamedTask<&'h H> for RustupCommand
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  const NAME: TaskName = TaskName::new("RustupCommand");
}

#[async_trait]
impl<'h, H: ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for RustupCommand
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = Result<RustEnv, ()>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let client = Client::builder()
      .user_agent("katal_rustup")
      .build()
      .expect("building the RustupCommand client should always succeed");

    let script_url = "https://sh.rustup.rs";
    let res: String = client
      .get(script_url)
      .send()
      .await
      .expect("downloading the rustup script should succeed")
      .text()
      .await
      .expect("invalid rustup script body");

    // `-s` allows passing arguments to a script passed through stdin
    let cmd = Command::new("sh")
      .arg("-s")
      .arg("--")
      .stdin(res)
      .arg("-y")
      .arg("--quiet")
      .arg("--no-modify-path")
      .arg("--profile")
      .arg("minimal")
      .arg("--default-toolchain")
      .arg("stable");
    host.exec(&cmd).expect("failed to run rustup installation");

    let home = dirs::home_dir().expect("failed to resolve home").join(".cargo");
    Ok(RustEnv { home })
  }
}
