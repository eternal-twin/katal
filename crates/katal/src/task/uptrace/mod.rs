mod config;

use crate::config::{RawUptraceChannelProjectConfig, RawUptraceChannelUserConfig};
use crate::task::{exec_as, AsyncFn, ExecAsError, NamedTask, TaskName, TaskRef, TaskResult, TaskSuccess};
use async_trait::async_trait;
use asys::common::fs::{FsHost, ReadFileError};
use asys::linux::fs::{FileMode, LinuxFsHost, LinuxMetadata, ResolveError};
use asys::linux::user::{Gid, GroupRef, LinuxUser, LinuxUserHost, Uid, UserRef};
use asys::local_linux::LocalLinux;
use asys::ExecHost;
use core::fmt::Debug;
use hex::ToHex;
use reqwest::Client;
use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;
use std::path::PathBuf;
use url::Url;

use crate::etwin::channel::{
  ChannelBorgbase, ChannelClickhouse, ChannelDomain, ChannelFallbackServer, ChannelPaths, ChannelPostgres,
  ChannelTarget,
};
use crate::task::fs::{EnsureDir, EnsureDirError, EnsureDirSymlink, EnsureFile, EnsureFileError};
use crate::task::nginx::{NginxAvailableSite, NginxEnableSite};
use crate::task::systemd::config::{CommandLine, Exec, Service, ServiceType, Unit};
use crate::task::systemd::{
  EnsureSystemdServiceConfig, EnsureSystemdSliceConfig, SystemdSliceConfig, SystemdState, SystemdUnit,
};
use crate::task::uptrace::config::{
  UptraceConfig, UptraceConfigAddr, UptraceConfigAuth, UptraceConfigCh, UptraceConfigChSchema,
  UptraceConfigChSchemaMetrics, UptraceConfigChSchemaSpans, UptraceConfigClient, UptraceConfigListen,
  UptraceConfigLogging, UptraceConfigMetrics, UptraceConfigPg, UptraceConfigProject, UptraceConfigServiceGraph,
  UptraceConfigServiceGraphStore, UptraceConfigSmtpMailer, UptraceConfigSpans, UptraceConfigTelegram,
  UptraceConfigUser,
};
use crate::task::witness::{FsWitness, FsWitnessError};
use crate::utils::display_error_chain::DisplayErrorChainExt;

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct UptraceAppTarget<S: AsRef<str> = String> {
  pub name: S,
  pub user_name: S,
  pub group_name: S,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct UptraceApp<S: AsRef<str> = String> {
  name: S,
  user_name: S,
  group_name: S,
  uid: Uid,
  gid: Gid,
  home: PathBuf,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct UptraceChannelTarget<S: AsRef<str> = String> {
  pub inner: ChannelTarget<S>,
  pub grpc_port: u16,
  pub uptrace_projects: BTreeMap<String, RawUptraceChannelProjectConfig>,
  pub uptrace_users: BTreeMap<String, RawUptraceChannelUserConfig>,
  pub vault: UptraceVault,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct UptraceVault {
  pub pg_admin_password: String,
  pub pg_main_password: String,
  pub pg_read_password: String,
  pub ch_main_password: String,
  pub ch_read_password: String,
  pub secret: String,
  pub uptrace_system_project_token: String,
  pub uptrace_project_token: BTreeMap<String, String>,
  pub uptrace_user_password: BTreeMap<String, String>,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct UptraceChannel<User: LinuxUser, S: AsRef<str> = String> {
  full_name: S,
  short_name: S,
  user: User,
  domain: ChannelDomain<S>,
  paths: ChannelPaths,
  main_port: u16,
  grpc_port: u16,
  uptrace_projects: BTreeMap<String, RawUptraceChannelProjectConfig>,
  uptrace_users: BTreeMap<String, RawUptraceChannelUserConfig>,
  fallback_server: ChannelFallbackServer,
  postgres: ChannelPostgres,
  clickhouse: ChannelClickhouse,
  vault: UptraceVault,
  borg: Option<ChannelBorgbase>,
}

impl<User, S> UptraceChannel<User, S>
where
  User: LinuxUser,
  S: AsRef<str>,
{
  pub fn service_name(&self) -> String {
    self.full_name.as_ref().to_string()
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct UptraceReleaseTarget<S: AsRef<str> = String> {
  pub channel: UptraceChannelTarget,
  pub version: S,
  pub sha256_digest: String,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct UptraceRelease<User: LinuxUser, S: AsRef<str> = String> {
  release_dir: PathBuf,
  exe_path: PathBuf,
  config_path: PathBuf,
  channel: UptraceChannel<User, S>,
  version: S,
  sha256_digest: String,
}

#[derive(Serialize, Deserialize)]
pub struct DeployUptrace<S: AsRef<str> = String> {
  pub pass: S,
  pub target: UptraceReleaseTarget,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum DeployUptraceError {
  #[error("failed to exec uptrace deployment task")]
  Exec(#[source] ExecAsError),
  #[error("failed to deploy uptrace release")]
  Deploy(#[source] DeployUptraceAsRootError),
}

#[async_trait]
impl<'h, H, S> AsyncFn<&'h H> for DeployUptrace<S>
where
  H: ExecHost,
  S: AsRef<str> + Send + Sync,
{
  type Output = TaskResult<(), DeployUptraceError>;

  async fn run(&self, _host: &'h H) -> Self::Output {
    let input = DeployUptraceAsRoot {
      target: self.target.clone(),
    };
    exec_as::<LocalLinux, DeployUptraceAsRoot>(UserRef::ROOT, Some(self.pass.as_ref()), &input)
      .await
      .map_err(DeployUptraceError::Exec)?
      .map_err(DeployUptraceError::Deploy)
  }
}

impl<'h, H: 'h + ExecHost> NamedTask<&'h H> for DeployUptrace {
  const NAME: TaskName = TaskName::new("uptrace::DeployUptrace");
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DeployUptraceAsRoot {
  target: UptraceReleaseTarget,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum DeployUptraceAsRootError {
  #[error("failed to use witness file")]
  Witness(#[source] FsWitnessError),
  #[error("failed to run task `UptraceReleaseTarget`")]
  Task(#[source] UptraceReleaseTargetError),
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for DeployUptraceAsRoot
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  H::User: Debug + Clone + Send + Sync + Serialize + DeserializeOwned,
  H::Group: Debug + Send + Sync,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<(), DeployUptraceAsRootError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let paths: ChannelPaths = self.target.channel.inner.paths();
    let witness = paths.home.join("uptrace_release.witness");
    let mut changed = false;
    let res = FsWitness::new(witness, TaskRef(&self.target))
      .run(host)
      .await
      .map_err(DeployUptraceAsRootError::Witness)?;
    changed = res.changed || changed;
    let res = res.output.map_err(DeployUptraceAsRootError::Task)?;
    changed = res.changed || changed;
    Ok(TaskSuccess { changed, output: () })
  }
}

impl<'h, H> NamedTask<&'h H> for DeployUptraceAsRoot
where
  H: 'h + ExecHost + LinuxUserHost + LinuxFsHost,
  H::User: Debug + Clone + Send + Sync + Serialize + DeserializeOwned,
  H::Group: Debug + Send + Sync,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  const NAME: TaskName = TaskName::new("uptrace::DeployUptraceAsRoot");
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum UptraceReleaseTargetError {
  #[error("failed to ensure release directory")]
  ReleaseDir(#[source] EnsureDirError),
  #[error("failed to execute build task")]
  ExecBuild(#[source] ExecAsError),
  #[error("unexpected error: {0}")]
  Other(String),
}

impl From<anyhow::Error> for UptraceReleaseTargetError {
  fn from(value: anyhow::Error) -> Self {
    Self::Other(value.display_chain().to_string())
  }
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for UptraceReleaseTarget
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  H::User: Debug + Clone + Send + Sync,
  H::Group: Debug + Send + Sync,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<UptraceRelease<H::User>, UptraceReleaseTargetError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let res = self.channel.run(host).await?;
    let channel: UptraceChannel<H::User> = res.output;
    let mut changed = res.changed;

    let release_dir = channel.paths.releases.join(&self.version);

    eprintln!("Create directory: {}", release_dir.display());
    changed = EnsureDir::new(release_dir.clone())
      .owner(UserRef::Id(channel.user.uid()))
      .group(GroupRef::Id(channel.user.gid()))
      .mode(FileMode::OWNER_ALL | FileMode::ALL_READ | FileMode::ALL_EXEC)
      .run(host)
      .await
      .map_err(UptraceReleaseTargetError::ReleaseDir)?
      .changed
      || changed;

    let witness = release_dir.join("ok");

    let witness_exists = match host.read_file(witness.as_path()) {
      Ok(_) => true,
      Err(ReadFileError::NotFound) => false,
      Err(e) => {
        panic!("failed to check witness file: {e:?}")
      }
    };

    let exe_path = release_dir.join("uptrace");
    let config_path = release_dir.join("uptrace.yaml");

    let release = UptraceRelease {
      release_dir,
      exe_path,
      config_path,
      channel: channel.clone(),
      version: self.version.clone(),
      sha256_digest: self.sha256_digest.clone(),
    };

    if witness_exists {
      eprintln!(
        "bailing-out, version already downloaded (assuming the release already completed) (todo: better tracking)"
      );
      return Ok(TaskSuccess {
        changed,
        output: release,
      });
    }

    let br = BuildUptraceRelease {
      exe_path: release.exe_path.clone(),
      config_path: release.config_path.clone(),
      version: release.version.clone(),
      sha256_digest: release.sha256_digest.clone(),
      config: config_from_channel(&channel),
    };

    changed = exec_as::<LocalLinux, _>(UserRef::Id(channel.user.uid()), None, &br)
      .await
      .map_err(UptraceReleaseTargetError::ExecBuild)?
      .unwrap()
      .changed
      || changed;

    changed = (UpdateActiveRelease {
      release: release.clone(),
    })
    .run(host)
    .await?
    .changed
      || changed;

    changed = EnsureFile::new(witness).content([]).run(host).await.unwrap().changed || changed;

    Ok(TaskSuccess {
      changed,
      output: release,
    })
  }
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for UptraceChannelTarget
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  H::Group: Debug + Send + Sync,
  H::User: Debug + Clone + Send + Sync,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<UptraceChannel<H::User>, anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    eprintln!("Uptrace: Create channel");
    let res = self.inner.run(host).await?;

    eprintln!("Uptrace: Channel ready");
    let output = UptraceChannel {
      full_name: res.output.full_name,
      short_name: res.output.short_name,
      user: res.output.user,
      domain: res.output.domain.expect("Expected associated domain name"),
      paths: res.output.paths,
      main_port: res.output.main_port.expect("Expected main port"),
      grpc_port: self.grpc_port,
      uptrace_projects: self.uptrace_projects.clone(),
      uptrace_users: self.uptrace_users.clone(),
      fallback_server: res.output.fallback_server.expect("Expected fallback server"),
      postgres: res.output.postgres.expect("Expected Postgres database"),
      clickhouse: res.output.clickhouse.expect("Expected Clickhouse database"),
      vault: self.vault.clone(),
      borg: res.output.borg,
    };

    Ok(TaskSuccess {
      changed: res.changed,
      output,
    })
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct BuildUptraceRelease {
  exe_path: PathBuf,
  config_path: PathBuf,
  version: String,
  sha256_digest: String,
  config: UptraceConfig,
}

#[derive(Clone, Debug, Eq, PartialEq, serde::Serialize, serde::Deserialize, thiserror::Error)]
pub enum BuildUptraceReleaseError {
  #[error("failed to send uptrace binary request: {0}")]
  Request(String),
  #[error("failed to get uptrace binary response: {0}")]
  Response(String),
  #[error("failed to ensure uptrace binary")]
  EnsureBinary(EnsureFileError),
}

#[async_trait]
impl<'h, H: ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for BuildUptraceRelease
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = Result<TaskSuccess<()>, BuildUptraceReleaseError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let linux_release_url = Url::parse(&format!(
      "https://github.com/uptrace/uptrace/releases/download/v{}/uptrace_linux_amd64",
      self.version
    ))
    .expect("release URL is valid");

    eprintln!("Download {}", &linux_release_url);

    let http_client = Client::builder()
      .user_agent("katal_uptrace")
      .build()
      .expect("creating the reqwest client always succeeds");

    let response = http_client
      .get(linux_release_url)
      .send()
      .await
      .map_err(|e| BuildUptraceReleaseError::Request(e.to_string()))?;
    let binary = response
      .bytes()
      .await
      .map_err(|e| BuildUptraceReleaseError::Response(e.to_string()))?;

    {
      use sha2::Digest;
      let mut hasher = sha2::Sha256::new();
      hasher.update(binary.as_ref());
      let actual_digest: String = hasher.finalize().encode_hex();
      assert_eq!(actual_digest, self.sha256_digest);
    }

    let mut changed = EnsureFile::new(self.exe_path.clone())
      .content(binary.as_ref())
      // .owner(self.uid)
      // .group(self.gid)
      .mode(FileMode::OWNER_ALL)
      .run(host)
      .await
      .map_err(BuildUptraceReleaseError::EnsureBinary)?
      .changed;

    eprintln!("Write config");
    changed = EnsureFile::new(self.config_path.clone())
      .content(self.config.to_yaml())
      // .owner(app.uid)
      // .group(app.gid)
      .mode(FileMode::OWNER_READ | FileMode::OWNER_WRITE)
      .run(host)
      .await
      .unwrap()
      .changed
      || changed;

    Ok(TaskSuccess { changed, output: () })
  }
}

impl<'h, H: 'h + ExecHost + LinuxUserHost + LinuxFsHost> NamedTask<&'h H> for BuildUptraceRelease
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  const NAME: TaskName = TaskName::new("uptrace::BuildUptraceRelease");
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize)]
pub struct UpdateActiveRelease<User: LinuxUser, S: AsRef<str> = String> {
  release: UptraceRelease<User, S>,
}

#[async_trait]
impl<'h, H, S> AsyncFn<&'h H> for UpdateActiveRelease<H::User, S>
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Send + Sync,
  S: AsRef<str> + Send + Sync,
{
  type Output = TaskResult<(), anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let uid = self.release.channel.user.uid();
    let gid = self.release.channel.user.gid();

    let service_name = format!("{}.service", self.release.channel.service_name());

    let mut changed = false;
    let active_release_link = &self.release.channel.paths.active_release_link;
    let previous_release_link = &self.release.channel.paths.previous_release_link;
    let old_release_dir = {
      match host.resolve(active_release_link) {
        Ok(old) => Some(old),
        Err(ResolveError::NotFound) => None,
        Err(e) => return Err(e.into()),
      }
    };
    let new_release_dir = host.resolve(&self.release.release_dir)?;

    if let Some(old_release_dir) = old_release_dir {
      if old_release_dir != new_release_dir {
        eprintln!("Start to disable old release");
        // There is already an active release but it does not match the target
        // release: stop the old version before proceeding to free the channel
        // resources (database, port)
        EnsureDirSymlink::new(previous_release_link.clone())
          .points_to(old_release_dir.clone())
          .owner(uid)
          .group(gid)
          .mode(FileMode::ALL_READ | FileMode::ALL_EXEC)
          .run(host)
          .await?;

        SystemdUnit::new(&service_name)
          .state(SystemdState::Inactive)
          .run(host)
          .await?;
        eprintln!("Old release disabled");
        changed = true;
      }
    }

    eprintln!("Ensure Systemd slice (uptrace)");
    {
      use crate::task::systemd::config::{ResourceControl, Slice, Unit};
      let slice = EnsureSystemdSliceConfig {
        name: self.release.channel.service_name(),
        config: SystemdSliceConfig {
          unit: Some(Unit {
            description: Some(format!("Slice for {}", self.release.channel.service_name())),
            ..Unit::default()
          }),
          slice: Some(Slice {
            resource_control: Some(ResourceControl {
              memory_high: None,
              memory_max: Some(String::from("1G")),
              cpu_quota: Some(String::from("100%")),
              ..ResourceControl::default()
            }),
            ..Slice::default()
          }),
          ..SystemdSliceConfig::default()
        },
      };
      changed = slice.run(host).await?.changed || changed;
    };

    eprintln!("Ensure Systemd service (uptrace)");
    let service: EnsureSystemdServiceConfig<String> = EnsureSystemdServiceConfig {
      name: self.release.channel.service_name(),
      config: crate::task::systemd::config::Config {
        unit: Some(Unit {
          description: Some("Uptrace monitoring server".to_string()),
          after: vec!["network.target".to_string()],
          requires: vec![
            format!("{}.service", self.release.channel.postgres.cluster.service),
            "clickhouse-server.service".to_string(),
          ],
          ..Unit::default()
        }),
        service: Some(Service {
          r#type: Some(ServiceType::Exec),
          exec: Exec {
            user: Some(self.release.channel.user.name().to_string()),
            working_directory: Some(self.release.release_dir.display().to_string()),
            ..Exec::default()
          },
          restart: Some("on-failure".to_string()),
          // slice: Some("todo".to_string()),
          exec_start: Some(CommandLine {
            line: format!(
              "{} --config {} serve",
              self.release.exe_path.display(),
              self.release.config_path.display()
            ),
          }),
          ..Service::default()
        }),
        timer: None,
        install: None,
      },
    };
    changed = service.run(host).await?.changed || changed;

    eprintln!("Generating nginx config");
    let nginx_config = get_nginx_config(&self.release.channel);

    eprintln!("Marking release as active");
    EnsureDirSymlink::new(active_release_link.clone())
      .points_to(new_release_dir.clone())
      .owner(uid)
      .group(gid)
      .mode(FileMode::ALL_READ | FileMode::ALL_EXEC)
      .run(host)
      .await?;

    eprintln!("Starting service");
    changed = SystemdUnit::new(&service_name)
      .enabled(true)
      .state(SystemdState::Active)
      .run(host)
      .await?
      .changed
      || changed;

    eprintln!("Writing nginx config");
    NginxAvailableSite::new(&service_name, nginx_config).run(host).await?;

    eprintln!("Enabling nginx config");
    NginxEnableSite::new(&service_name).run(host).await?;

    Ok(TaskSuccess { changed, output: () })
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
struct UptraceService<S: AsRef<str> = String> {
  channel_name: S,
  user: S,
  slice: S,
  working_dir: PathBuf,
  uptrace_exe: PathBuf,
}

pub fn get_nginx_config<S: AsRef<str>>(channel: &UptraceChannel<impl LinuxUser, S>) -> String {
  let upstream_name = channel.full_name.as_ref().to_string().replace('.', "_");
  let access_log = channel.paths.logs.join("main.nginx.access.log");
  let error_log = channel.paths.logs.join("main.nginx.error.log");

  let fallback_server = format!(
    r#"# Fallback server
server {{
  # HTTP port
  listen {fallback_port};
  listen [::]:{fallback_port};

  # Hide nginx version in `Server` header.
  server_tokens off;
  add_header Referrer-Policy same-origin;
  add_header X-Content-Type-Options nosniff;
  add_header X-XSS-Protection "1; mode=block";

  index index.html;

  root {fallback_dir};
}}
"#,
    fallback_port = channel.fallback_server.port,
    fallback_dir = channel.fallback_server.dir.to_str().unwrap(),
  );

  let upstream_server = format!(
    r#"# Define target of the reverse-proxy
upstream {upstream_name} {{
  server [::1]:{port};
  server [::1]:{fallback_port} backup;
  keepalive 8;
}}
"#,
    upstream_name = upstream_name,
    port = channel.main_port,
    fallback_port = channel.fallback_server.port,
  );

  let main_directives = format!(
    r#"  # Space-separated hostnames (wildcard * is accepted)
  server_name {domain};

  access_log {access_log};
  error_log {error_log};
  client_max_body_size 100M;

  location / {{
    try_files $uri @uptraceproxy;
    gzip_static on;
  }}

  location @uptraceproxy {{
    # Upstream reference
    proxy_pass http://{upstream_name};

    # Configuration of the proxy
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection 'upgrade';
    proxy_set_header Host $host;
    proxy_set_header X-NginX-Proxy true;
    proxy_cache_bypass $http_upgrade;
    proxy_redirect off;
  }}
"#,
    domain = channel.domain.main.as_ref(),
    access_log = access_log.to_str().unwrap(),
    error_log = error_log.to_str().unwrap(),
    upstream_name = upstream_name,
  );

  let cert_files = match channel.domain.cert.as_ref() {
    Some(https) => format!(
      r#"  ssl_certificate {cert};
  ssl_certificate_key {cert_key};"#,
      cert = https.fullchain_cert_path.to_str().unwrap(),
      cert_key = https.cert_key_path.to_str().unwrap(),
    ),
    None => String::new(),
  };

  let legacy = if channel.domain.legacy.is_empty() {
    String::new()
  } else {
    let legacy: Vec<&str> = channel.domain.legacy.iter().map(|l| l.as_ref()).collect();
    let https_port = if channel.domain.cert.is_some() {
      "listen 443 ssl;\n  listen [::]:443 ssl;"
    } else {
      ""
    };
    format!(
      r#"# Legacy
server {{
  listen 80;
  listen [::]:80;
  {https_port}

  server_name {legacy};

  {cert_files}

  return 308 https://{domain}$request_uri;
}}
"#,
      legacy = legacy.join(" "),
      domain = channel.domain.main.as_ref(),
    )
  };

  match channel.domain.cert.as_ref() {
    Some(_) => format!(
      r#"# This configuration sets a reverse proxy for a specific domain

{fallback_server}

{upstream_server}

# HTTPS
server {{
  # HTTPS port
  listen 443 ssl;
  listen [::]:443 ssl;

  {cert_files}

  # # HSTS (ngx_http_headers_module is required) (63072000 seconds)
  # add_header Strict-Transport-Security "max-age=63072000" always;

  {main_directives}
}}

# HTTP
server {{
  # HTTP port
  listen 80;
  listen [::]:80;
  server_name {domain};
  return 301 https://$host$request_uri;
}}

# www.
server {{
  listen 80;
  listen [::]:80;
  listen 443 ssl;
  listen [::]:443 ssl;

  server_name www.{domain};

  {cert_files}

  return 307 https://{domain}$request_uri;
}}

{legacy}

"#,
      fallback_server = fallback_server,
      upstream_server = upstream_server,
      main_directives = main_directives,
      domain = channel.domain.main.as_ref(),
      cert_files = cert_files,
    ),
    None => format!(
      r#"# This configuration sets a reverse proxy for a specific domain

{fallback_server}

{upstream_server}

# HTTP
server {{
  # HTTP port
  listen 80;
  listen [::]:80;

  {main_directives}
}}

# www.
server {{
  listen 80;
  listen [::]:80;

  server_name www.{domain};

  return 307 http://{domain}$request_uri;
}}

{legacy}

"#,
      fallback_server = fallback_server,
      upstream_server = upstream_server,
      main_directives = main_directives,
      domain = channel.domain.main.as_ref(),
    ),
  }
}

fn config_from_channel<User, S>(channel: &UptraceChannel<User, S>) -> UptraceConfig
where
  User: LinuxUser,
  S: AsRef<str>,
{
  UptraceConfig {
    ch: UptraceConfigCh {
      addr: format!("[::1]:{}", channel.clickhouse.ch_tcp_port),
      user: channel.clickhouse.main.name.to_string(),
      password: channel.clickhouse.main.password.clone(),
      database: channel.clickhouse.name.to_string(),
      tls: None,
      max_execution_time: "30s".to_string(),
    },
    pg: UptraceConfigPg {
      addr: format!("[::1]:{}", channel.postgres.port),
      user: channel.postgres.admin.name.to_string(),
      password: channel.postgres.admin.password.clone(),
      database: channel.postgres.name.to_string(),
      tls: None,
    },
    projects: {
      let mut projects = vec![UptraceConfigProject {
        id: 1,
        name: "uptrace".to_string(),
        token: channel.vault.uptrace_system_project_token.clone(),
        pinned_attrs: vec![],
        group_by_env: true,
        group_funcs_by_service: true,
        prom_compat: false,
        force_span_name: vec![],
      }];
      let mut id = 1;
      for (key, project_config) in &channel.uptrace_projects {
        id += 1;
        let token = channel
          .vault
          .uptrace_project_token
          .get(key)
          .expect("project token must exist");
        projects.push(UptraceConfigProject {
          id,
          name: project_config.name.clone(),
          token: token.clone(),
          pinned_attrs: vec![],
          group_by_env: true,
          group_funcs_by_service: true,
          prom_compat: false,
          force_span_name: vec![],
        });
      }
      projects
    },
    metrics_from_spans: vec![],
    auth: UptraceConfigAuth {
      users: channel
        .uptrace_users
        .iter()
        .map(|(key, user_config)| UptraceConfigUser {
          name: user_config.name.clone(),
          email: user_config.email.clone(),
          password: channel
            .vault
            .uptrace_user_password
            .get(key)
            .expect("uptrace user password exists")
            .clone(),
          notify_by_email: false,
          auth_token: None,
        })
        .collect(),
    },
    ch_schema: UptraceConfigChSchema {
      compression: "ZSTD(3)".to_string(),
      cluster: None,
      replicated: Some(false),
      spans: UptraceConfigChSchemaSpans {
        ttl_delete: "7 DAY".to_string(),
        storage_policy: "default".to_string(),
      },
      metrics: UptraceConfigChSchemaMetrics {
        ttl_delete: "30 DAY".to_string(),
        storage_policy: "default".to_string(),
      },
    },
    listen: UptraceConfigListen {
      grpc: UptraceConfigAddr {
        addr: format!(":{}", channel.grpc_port),
      },
      http: UptraceConfigAddr {
        addr: format!(":{}", channel.main_port),
      },
      tls: None,
    },
    site: UptraceConfigAddr {
      addr: "http://uptrace.localhost/".to_string(),
    },
    spans: UptraceConfigSpans {
      buffer_size: Some(100000),
      batch_size: Some(10000),
    },
    metrics: UptraceConfigMetrics {
      drop_attrs: vec![
        "telemetry_sdk_language".to_string(),
        "telemetry_sdk_name".to_string(),
        "telemetry_sdk_version".to_string(),
      ],
      buffer_size: Some(100000),
      batch_size: Some(10000),
      cum_to_delta_size: Some(100000),
    },
    service_graph: UptraceConfigServiceGraph {
      disabled: Some(true),
      store: UptraceConfigServiceGraphStore {
        size: 1000000,
        ttl: "5s".to_string(),
      },
    },
    smtp_mailer: UptraceConfigSmtpMailer {
      enabled: false,
      host: "localhost".to_string(),
      port: 1025,
      username: "".to_string(),
      password: "".to_string(),
      tls: None,
      from: "uptrace@localhost".to_string(),
    },
    uptrace_go: UptraceConfigClient {
      disabled: Some(true),
      dsn: None,
      tls: None,
    },
    telegram: UptraceConfigTelegram {
      bot_token: "".to_string(),
    },
    logging: UptraceConfigLogging {
      level: "WARN".to_string(),
    },
    secret_key: channel.vault.secret.to_string(),
    debug: false,
  }
}
