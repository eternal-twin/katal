/// Uptrace config based on reference config file.
///
/// <https://raw.githubusercontent.com/uptrace/uptrace/master/config/uptrace.dist.yml>
#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct UptraceConfig {
  pub ch: UptraceConfigCh,
  pub pg: UptraceConfigPg,
  pub projects: Vec<UptraceConfigProject>,
  pub metrics_from_spans: Vec<UptraceConfigSpanMetric>,
  pub auth: UptraceConfigAuth,
  pub ch_schema: UptraceConfigChSchema,
  pub listen: UptraceConfigListen,
  pub site: UptraceConfigAddr,
  pub spans: UptraceConfigSpans,
  pub metrics: UptraceConfigMetrics,
  pub service_graph: UptraceConfigServiceGraph,
  pub smtp_mailer: UptraceConfigSmtpMailer,
  pub uptrace_go: UptraceConfigClient,
  pub telegram: UptraceConfigTelegram,
  pub logging: UptraceConfigLogging,
  pub secret_key: String,
  pub debug: bool,
}

impl UptraceConfig {
  pub fn to_yaml(&self) -> String {
    let mut config = serde_yaml::to_string(&self).expect("serializing always succeeds");
    config.push('\n');
    config
  }
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct UptraceConfigCh {
  pub addr: String,
  pub user: String,
  pub password: String,
  pub database: String,
  pub tls: Option<UptraceConfigTls>,
  pub max_execution_time: String,
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct UptraceConfigPg {
  pub addr: String,
  pub user: String,
  pub password: String,
  pub database: String,
  pub tls: Option<UptraceConfigTls>,
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct UptraceConfigTls {
  pub insecure_skip_verify: bool,
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct UptraceConfigProject {
  pub id: u32,
  pub name: String,
  pub token: String,
  pub pinned_attrs: Vec<String>,
  pub group_by_env: bool,
  pub group_funcs_by_service: bool,
  pub prom_compat: bool,
  pub force_span_name: Vec<String>,
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct UptraceConfigSpanMetric {
  pub name: String,
  pub description: String,
  pub instrument: String,
  pub unit: String,
  pub value: String,
  pub attrs: Vec<String>,
  pub annotations: Vec<String>,
  pub r#where: String,
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct UptraceConfigAuth {
  pub users: Vec<UptraceConfigUser>,
  // pub cloudflare: Vec<UptraceConfigCloudflare>,
  // pub oidc: Vec<UptraceConfigOidc>,
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct UptraceConfigUser {
  pub name: String,
  pub email: String,
  pub password: String,
  pub notify_by_email: bool,
  pub auth_token: Option<String>,
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct UptraceConfigChSchema {
  pub compression: String,
  pub cluster: Option<String>,
  pub replicated: Option<bool>,
  pub spans: UptraceConfigChSchemaSpans,
  pub metrics: UptraceConfigChSchemaMetrics,
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct UptraceConfigChSchemaSpans {
  pub ttl_delete: String,
  pub storage_policy: String,
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct UptraceConfigChSchemaMetrics {
  pub ttl_delete: String,
  pub storage_policy: String,
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct UptraceConfigListen {
  pub grpc: UptraceConfigAddr,
  pub http: UptraceConfigAddr,
  pub tls: Option<UptraceConfigListenTls>,
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct UptraceConfigListenTls {
  pub cert_file: String,
  pub key_file: String,
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct UptraceConfigAddr {
  pub addr: String,
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct UptraceConfigSpans {
  pub buffer_size: Option<u32>,
  pub batch_size: Option<u32>,
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct UptraceConfigMetrics {
  pub drop_attrs: Vec<String>,
  pub buffer_size: Option<u32>,
  pub batch_size: Option<u32>,
  pub cum_to_delta_size: Option<u32>,
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct UptraceConfigServiceGraph {
  pub disabled: Option<bool>,
  pub store: UptraceConfigServiceGraphStore,
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct UptraceConfigServiceGraphStore {
  pub size: u32,
  pub ttl: String,
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct UptraceConfigSmtpMailer {
  pub enabled: bool,
  pub host: String,
  pub port: u16,
  pub username: String,
  pub password: String,
  pub tls: Option<UptraceConfigSmtpMailerTls>,
  pub from: String,
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct UptraceConfigSmtpMailerTls {
  pub disabled: bool,
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct UptraceConfigClient {
  pub disabled: Option<bool>,
  pub dsn: Option<String>,
  pub tls: Option<UptraceConfigClientTls>,
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct UptraceConfigClientTls {
  pub cert_file: String,
  pub key_file: String,
  pub insecure_skip_verify: bool,
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct UptraceConfigTelegram {
  pub bot_token: String,
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct UptraceConfigLogging {
  pub level: String,
}
