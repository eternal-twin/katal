use crate::ipc_sudo::{IpcSudoChildHandler, SudoCommand};
use crate::job_runner::opaque_data::{BincodeBuf, ReadableOpaqueData, WritableOpaqueData};
use crate::job_runner::{
  ChildStdoutJobServerReader, JobClientMessage, JobComplete, JobServerMessage, JobServerReadError, JobServerReader,
  JobServerWriter, StartJob,
};
use async_trait::async_trait;
use asys::linux::user::UserRef;
use asys::Host;
pub use fn_trait::AsyncFn;
use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fmt::Debug;
use std::future::Future;
use std::io;
use std::marker::PhantomData;
use std::pin::Pin;
use std::process::Stdio;
use thiserror::Error;
use tokio::io::{AsyncBufReadExt, AsyncWriteExt, BufReader};
use tokio::process::{Child, ChildStdin};

pub mod bashrc;
pub mod borg;
pub mod borgbase;
pub mod borgmatic;
mod builder;
pub mod certbot;
pub mod clickhouse;
pub mod composer;
pub mod elasticsearch;
pub mod fs;
pub mod fused;
pub mod git;
pub mod java;
pub mod nginx;
pub mod node;
pub mod nvm;
pub mod pacman;
pub mod php;
pub mod postgres;
pub mod rust;
pub mod systemd;
pub mod uptrace;
pub mod user;
pub mod witness;
pub mod yarn;

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize)]
pub struct TaskRef<'t, TyTask>(pub &'t TyTask);

impl<'t, TyTask, Input> AsyncFn<Input> for TaskRef<'t, TyTask>
where
  TyTask: AsyncFn<Input>,
{
  type Output = TyTask::Output;

  fn run<'this, 'fut>(&'this self, input: Input) -> Pin<Box<dyn Future<Output = Self::Output> + Send + 'fut>>
  where
    'this: 'fut,
    Input: 'fut,
    Self: 'fut,
  {
    self.0.run(input)
  }
}

#[derive(Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
pub struct TaskName(&'static str);

impl TaskName {
  pub const fn new(name: &'static str) -> Self {
    Self(name)
  }

  pub const fn as_str(&self) -> &'static str {
    self.0
  }
}

impl From<&'static str> for TaskName {
  fn from(s: &'static str) -> Self {
    Self::new(s)
  }
}

impl From<TaskName> for &'static str {
  fn from(n: TaskName) -> Self {
    n.as_str()
  }
}

/// A task with a self-selected name
pub trait NamedTask<Input>: AsyncFn<Input> {
  const NAME: TaskName;
}

#[derive(Debug, Serialize, Deserialize)]
pub struct TaskSuccess<Out> {
  // If any change was applied to the host
  pub changed: bool,
  pub output: Out,
}

pub type TaskResult<Out, E> = Result<TaskSuccess<Out>, E>;

pub struct TaskContext<'a, H: Host + ?Sized> {
  scope: PhantomData<&'a mut ()>,
  host: &'a H,
}

impl<'a, H: Host + ?Sized> TaskContext<'a, H> {
  pub fn new(host: &'a H) -> Self {
    Self {
      scope: PhantomData,
      host,
    }
  }

  pub fn host(&self) -> &H {
    self.host
  }
}

pub trait SerializeDeserialize: Serialize + for<'de> Deserialize<'de> {}

impl<T: Serialize + for<'de> Deserialize<'de>> SerializeDeserialize for T {}

pub trait AsyncRunner<I> {
  type Fn: AsyncFn<I>;

  #[must_use]
  fn run_fn<'fut, 'f: 'fut, 's: 'fut>(
    &'s self,
    f: &'f Self::Fn,
    input: I,
  ) -> Pin<Box<dyn Future<Output = <Self::Fn as AsyncFn<I>>::Output> + Send + 'fut>>
  where
    I: 'fut;
}

pub struct PhantomAsyncRunner<F, I>
where
  F: AsyncFn<I>,
{
  phantom: PhantomData<fn(F, I) -> ()>,
}

impl<F, I> Default for PhantomAsyncRunner<F, I>
where
  F: AsyncFn<I>,
{
  fn default() -> Self {
    Self::new()
  }
}

impl<F, I> PhantomAsyncRunner<F, I>
where
  F: AsyncFn<I>,
{
  pub fn new() -> Self {
    Self { phantom: PhantomData }
  }
}

impl<F, I> AsyncRunner<I> for PhantomAsyncRunner<F, I>
where
  F: AsyncFn<I>,
{
  type Fn = F;

  fn run_fn<'fut, 'f: 'fut, 's: 'fut>(
    &'s self,
    f: &'f Self::Fn,
    input: I,
  ) -> Pin<Box<dyn Future<Output = <Self::Fn as AsyncFn<I>>::Output> + Send + 'fut>>
  where
    I: 'fut,
  {
    f.run(input)
  }
}

pub trait ErasedAsyncRunner<F: ReadableOpaqueData, I, O: WritableOpaqueData>: Send + Sync {
  #[must_use]
  fn erased_run<'fut, 'r: 'fut, 'f: 'fut>(&'r self, f: &'f F, i: I) -> Pin<Box<dyn Future<Output = O> + Send + 'fut>>
  where
    I: 'fut;
}

impl<F, I, O, R> ErasedAsyncRunner<F, I, O> for R
where
  F: ReadableOpaqueData + Send + Sync,
  I: Send + Sync,
  O: WritableOpaqueData,
  R: AsyncRunner<I> + Send + Sync,
  R::Fn: DeserializeOwned + Send + Sync,
  <R::Fn as AsyncFn<I>>::Output: Serialize + Send + Sync,
{
  fn erased_run<'fut, 'r: 'fut, 'f: 'fut>(&'r self, f: &'f F, i: I) -> Pin<Box<dyn Future<Output = O> + Send + 'fut>>
  where
    I: 'fut,
  {
    Box::pin(async move {
      let ff: R::Fn = f.try_read().unwrap();
      let o = ff.run(i).await;
      O::try_write(&o).unwrap()
    })
  }
}

/// Registry of runners accepting a an opaque function `F`, calling it with
/// `I` and returning the result back as an opaque `O`.
pub struct TaskRunnerRegistry<'r, F, I, O>
where
  F: ReadableOpaqueData,
  O: WritableOpaqueData,
{
  runners: HashMap<&'static str, Box<dyn 'r + ErasedAsyncRunner<F, I, O>>>,
}

impl<'r, F, I, O> Default for TaskRunnerRegistry<'r, F, I, O>
where
  F: ReadableOpaqueData,
  O: WritableOpaqueData,
{
  fn default() -> Self {
    Self::new()
  }
}

impl<'r, F, I, O> TaskRunnerRegistry<'r, F, I, O>
where
  F: ReadableOpaqueData,
  O: WritableOpaqueData,
{
  pub fn new() -> Self {
    Self {
      runners: HashMap::new(),
    }
  }

  pub fn register_runner_as<R>(&mut self, name: &'static str, runner: R)
  where
    R: 'r + ErasedAsyncRunner<F, I, O>,
  {
    self.runners.insert(name, Box::new(runner));
  }

  pub fn get(&self, task: &str) -> Option<&dyn ErasedAsyncRunner<F, I, O>> {
    self.runners.get(task).map(|r| &**r)
  }
}

impl<'r, F, I, O> TaskRunnerRegistry<'r, F, I, O>
where
  F: ReadableOpaqueData + Send + Sync,
  I: Copy + Send + Sync,
  O: WritableOpaqueData,
{
  pub fn bind(&self, input: I) -> ClosedTaskRunnerRegistry<'_, 'r, F, I, O> {
    ClosedTaskRunnerRegistry { registry: self, input }
  }
}

impl<'r, F, I, O> TaskRunnerRegistry<'r, F, I, O>
where
  F: ReadableOpaqueData + Send + Sync,
  I: Send + Sync,
  O: WritableOpaqueData,
{
  pub fn register_runner<R>(&mut self, runner: R)
  where
    R: 'r + AsyncRunner<I> + Send + Sync,
    R::Fn: DeserializeOwned + Send + Sync + NamedTask<I>,
    <R::Fn as AsyncFn<I>>::Output: Serialize + Send + Sync,
  {
    self.runners.insert(R::Fn::NAME.as_str(), Box::new(runner));
  }

  pub fn register<Fn>(&mut self)
  where
    Fn: 'r + DeserializeOwned + Send + Sync + NamedTask<I>,
    <Fn as AsyncFn<I>>::Output: Serialize + Send + Sync,
    I: 'r,
  {
    let runner: PhantomAsyncRunner<Fn, I> = PhantomAsyncRunner::new();
    self.register_runner(runner);
  }
}

/// Registry of runners accepting a an opaque function `F`, calling it with
/// a closed-over `I` and returning the result back as an opaque `O`.
pub struct ClosedTaskRunnerRegistry<'a, 'b, F, I, O>
where
  F: ReadableOpaqueData + Send + Sync,
  I: Copy + Send + Sync,
  O: WritableOpaqueData,
{
  pub registry: &'a TaskRunnerRegistry<'b, F, I, O>,
  pub input: I,
}

#[derive(Debug, Error)]
pub enum RunClosedTaskError<WriteError> {
  #[error("task not found: {0:?}")]
  NotFound(String),
  #[error("write error")]
  Write(#[source] WriteError),
}

impl<'a, 'b, Name: AsRef<str>, F, I, O> AsyncFn<(Name, F)> for ClosedTaskRunnerRegistry<'a, 'b, F, I, O>
where
  Name: Send,
  F: ReadableOpaqueData + Send + Sync,
  I: Copy + Send + Sync,
  O: WritableOpaqueData + Debug,
{
  type Output = Result<O, RunClosedTaskError<<O as WritableOpaqueData>::Error>>;

  #[must_use]
  fn run<'afn, 'fut>(&'afn self, request: (Name, F)) -> Pin<Box<dyn Future<Output = Self::Output> + Send + 'fut>>
  where
    'afn: 'fut,
    (Name, F): 'fut,
    Self: 'fut,
  {
    Box::pin(async move {
      let (name, func) = request;
      let runner = self.registry.get(name.as_ref()).unwrap();
      let out = runner.erased_run(&func, self.input).await;
      Ok(out)
    })
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize, Error)]
pub enum ExecAsError {
  #[error("failed to spawn agent child process")]
  SpawnAgent(String),
  #[error("spawned agent child process immediately stopped")]
  AgentStopped,
  #[error("failed to acquire writer for agent child process stdin")]
  GetAgentStdin,
  #[error("failed to acquire reader for agent child process stdout")]
  GetAgentStdout,
  #[error("failed to acquire reader for agent child process stderr")]
  GetAgentStderr,
  #[error("failed to read message from job server in agent child process")]
  ReadError(#[from] JobServerReadError),
  #[error("missing response message from job server in agent child process")]
  MissingResponse,
  #[error("received response with unexpected job id: actual = {actual}, expected = {expected}")]
  UnexpectResponseJobId { actual: u64, expected: u64 },
  #[error("failed to read job output: {0}")]
  JobOutput(String),
}

type RegistryResult = Result<BincodeBuf, RunClosedTaskError<bincode::Error>>;

/// Execute the task `T` with a process running as the provided user.
///
/// The inner process can only return a final result, if you need to support callbacks,
/// use `exec_as_rec`.
pub async fn exec_as<'a, RemoteHost: 'static + Host + ?Sized, T>(
  user: UserRef,
  pass: Option<&'a str>,
  input: &'a T,
) -> Result<T::Output, ExecAsError>
where
  T: NamedTask<&'static RemoteHost>,
  T: Serialize + Sync,
  T::Output: for<'de> Deserialize<'de> + Send,
{
  exec_as_inner::<_, _>(user, pass, input, None).await
}

/// Execute the task `T` with a process running as the provided user.
///
/// The inner process can communicate with the parent process (callbacks) through the supplied
/// registry.
pub async fn exec_as_rec<'a, RemoteHost: 'static + Host + ?Sized, T>(
  user: UserRef,
  pass: Option<&'a str>,
  input: &'a T,
  registry: Box<dyn 'a + Send + Sync + AsyncFn<(String, BincodeBuf), Output = RegistryResult>>,
) -> Result<T::Output, ExecAsError>
where
  T: NamedTask<&'static RemoteHost>,
  T: Serialize + Sync,
  T::Output: for<'de> Deserialize<'de> + Send,
{
  exec_as_inner(user, pass, input, Some(registry)).await
}

pub async fn exec_as_inner<'a, RemoteHost: 'static + Host + ?Sized, T>(
  user: UserRef,
  pass: Option<&'a str>,
  input: &'a T,
  registry: Option<Box<dyn 'a + Send + Sync + AsyncFn<(String, BincodeBuf), Output = RegistryResult>>>,
) -> Result<T::Output, ExecAsError>
where
  T: NamedTask<&'static RemoteHost>,
  T: Serialize + Sync,
  T::Output: for<'de> Deserialize<'de> + Send,
{
  let mut cmd = SudoCommand::new(std::env::current_exe().unwrap().to_str().unwrap());
  if let Some(pass) = pass {
    cmd.password(pass);
  }
  cmd.user(user);
  cmd.stdin(Stdio::piped());
  cmd.stdout(Stdio::piped());
  cmd.stderr(Stdio::piped());
  cmd.arg("stdio-agent");

  struct Handler<'input, H: 'static + Host + ?Sized, T>
  where
    T: NamedTask<&'static H>,
    T: Serialize + Sync,
    T::Output: for<'de> Deserialize<'de> + Send,
  {
    phantom: PhantomData<&'static H>,
    input: &'input T,
    registry: Option<Box<dyn 'input + Send + Sync + AsyncFn<(String, BincodeBuf), Output = RegistryResult>>>,
  }

  #[async_trait]
  impl<'input, H: Host + ?Sized, T> IpcSudoChildHandler for Handler<'input, H, T>
  where
    T: NamedTask<&'static H>,
    T: Serialize + Sync,
    T::Output: for<'de> Deserialize<'de> + Send,
  {
    type Output = Result<T::Output, ExecAsError>;

    async fn handle<'a>(self, child: io::Result<&'a mut Child>) -> Self::Output {
      let child = child.map_err(|e| ExecAsError::SpawnAgent(e.to_string()))?;
      let child_id = child.id().ok_or(ExecAsError::AgentStopped)?;
      let stdin = child.stdin.take().ok_or(ExecAsError::GetAgentStdin)?;
      let stdout = child.stdout.take().ok_or(ExecAsError::GetAgentStdout)?;
      let stderr = child.stderr.take().ok_or(ExecAsError::GetAgentStderr)?;
      tokio::spawn(async move {
        let stderr = BufReader::new(stderr);
        let mut lines = stderr.lines();
        loop {
          let line = lines.next_line().await.expect("FailedToReadLine");
          match line {
            Some(line) => eprintln!("[{}] {}", child_id, line),
            None => break,
          }
        }
      });
      let mut agent_writer = StdinRemoteAgent(stdin);
      let job_id: u64 = 0;
      agent_writer.start_job::<H, T>(job_id, self.input).await;
      let mut agent_reader = ChildStdoutJobServerReader::new(stdout);
      loop {
        let res = agent_reader
          .next_server_message()
          .await?
          .ok_or(ExecAsError::MissingResponse)?;
        match res {
          JobServerMessage::RequestStart(job) => {
            let registry = self.registry.as_ref().expect("missing local registry");
            let out = registry.run((job.task, job.input.clone())).await.expect("run error");
            // let out = runner.erased_run(&job.input, &LocalLinux).await;
            agent_writer.request_end(job.job_id, out).await;
          }
          JobServerMessage::NotifyEnd(res) => {
            if res.job_id != job_id {
              return Err(ExecAsError::UnexpectResponseJobId {
                actual: res.job_id,
                expected: job_id,
              });
            }
            let out: T::Output = res
              .output
              .try_read()
              .map_err(|e| ExecAsError::JobOutput(e.to_string()))?;
            return Ok(out);
          }
        }
      }
    }
  }

  let handler: Handler<RemoteHost, T> = Handler {
    phantom: PhantomData,
    input,
    registry,
  };
  let out = cmd.with_child(handler).await?;
  Ok(out)
}

struct StdinRemoteAgent(ChildStdin);

#[async_trait]
impl JobServerWriter for StdinRemoteAgent {
  async fn start_job<'h, H: 'h + Host + ?Sized, T>(&mut self, job_id: u64, input: &T)
  where
    T: NamedTask<&'h H>,
    T: Serialize + Sync,
  {
    let payload = JobClientMessage::RequestStart(StartJob {
      job_id,
      task: T::NAME.as_str().to_string(),
      input: BincodeBuf::try_write(&input).unwrap(),
    });
    let mut payload = serde_json::to_vec(&payload).unwrap();
    payload.push(b'\n');
    self.0.write_all(&payload).await.unwrap()
  }

  async fn request_end(&mut self, job_id: u64, output: BincodeBuf) {
    let payload = JobClientMessage::NotifyEnd(JobComplete { job_id, output });
    let mut payload = serde_json::to_vec(&payload).unwrap();
    payload.push(b'\n');
    self.0.write_all(&payload).await.unwrap()
  }
}
