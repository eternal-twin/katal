use crate::job_runner::opaque_data::{BincodeBuf, ReadableOpaqueData};
use crate::job_runner::{
  JobClientMessage, JobClientReader, JobClientWriter, StdinJobClientReader, StdoutJobClientWriter,
};
use crate::task::{AsyncFn, NamedTask, TaskName, TaskResult, TaskRunnerRegistry, TaskSuccess};
use async_trait::async_trait;
use asys::common::fs::{CreateDirError, CreateFileError, FsHost, MetadataError, ReadDirError, ReadFileError};
use asys::common::user::UserHost;
use asys::linux::fs::{
  CreateSymlinkError, FileMode, LinuxFsHost, LinuxMetadata, ResolveError, SetFileModeError, SetFileOwnerError,
};
use asys::linux::user::{
  CreateGroupError, GetUserGroupsError, Gid, LinuxUserHost, TryGroupFromIdError, TryGroupFromNameError,
  TryUserFromNameError, Uid, UpdateGroupError,
};
use asys::local_linux::LocalLinux;
use asys::{Command, CommandOutput, ExecHost, Host, NixHost, TryExecError};
use katal_state::sqlite::SqliteStore;
use katal_state::{AcquireIdempotencyTokenError, IdempotencyKey, IdempotencyToken, StateStore, try_idempotency_token_result, TryAcquireIdempotencyTokenError};
use serde::{Deserialize, Serialize};
use std::future::Future;
use std::path::{Path, PathBuf};
use std::pin::Pin;
use std::sync::atomic::{AtomicBool, Ordering};
use thiserror::Error;
use tokio::sync::Mutex;

/// A wrapper ensuring idempotency of a task using a fuse.
/// TODO: Implement fuse with an idempotency token (so it's cross-process)
pub struct FuseTask<Task> {
  inner: Task,
  fuse: AtomicBool,
}

impl<Input, Task> AsyncFn<Input> for FuseTask<Task>
where
  Task: AsyncFn<Input> + Send + Sync,
  Input: Send,
{
  type Output = Option<Task::Output>;

  #[must_use]
  fn run<'afn, 'fut>(&'afn self, input: Input) -> Pin<Box<dyn Future<Output = Self::Output> + Send + 'fut>>
  where
    'afn: 'fut,
    Input: 'fut,
    Self: 'fut,
  {
    Box::pin(async move {
      if self.fuse.load(Ordering::SeqCst) {
        return None;
      }
      let out = self.inner.run(input).await;
      self.fuse.store(true, Ordering::SeqCst);
      Some(out)
    })
  }
}

#[async_trait]
pub trait StatefulHost: Host {
  /// Acquire an idempotency token ("at most once" semantics).
  ///
  /// See
  async fn acquire_idempotency_token(&self, key: IdempotencyKey) -> Result<IdempotencyToken, AcquireIdempotencyTokenError>;

  async fn try_acquire_idempotency_token(&self, key: IdempotencyKey) -> Result<Option<IdempotencyToken>, TryAcquireIdempotencyTokenError> {
    try_idempotency_token_result(self.acquire_idempotency_token(key).await)
  }
}

// pub struct IpcStateStore {
//   writer: StdoutJobClientWriter,
// }
//
// #[async_trait]
// impl StateStore for IpcStateStore {
//   async fn acquire_idempotency_token(&self, key: &IdempotencyKey) -> IdempotencyToken {
//     self.writer.job_complete()
//   }
// }

pub struct StateAndHost<H, S> {
  pub host: H,
  pub state: S,
}

impl<H, S> StateAndHost<H, S> {
  pub fn new(host: H, state: S) -> Self {
    Self { host, state }
  }
}

impl<H, S> Host for StateAndHost<H, S>
where
  H: Host,
  S: Send + Sync,
{
}

impl<H, S> ExecHost for StateAndHost<H, S>
where
  H: ExecHost,
  S: Send + Sync,
{
  fn try_exec(&self, command: &Command) -> Result<CommandOutput, TryExecError> {
    self.host.try_exec(command)
  }
}

impl<H, S> UserHost for StateAndHost<H, S>
where
  H: UserHost,
  S: Send + Sync,
{
}

impl<H, S> LinuxUserHost for StateAndHost<H, S>
where
  H: LinuxUserHost,
  S: Send + Sync,
{
  type Group = H::Group;
  type User = H::User;

  fn create_group(&self, name: &str, gid: Option<Gid>) -> Result<(), CreateGroupError> {
    self.host.create_group(name, gid)
  }

  fn update_group(&self, name: &str, gid: Option<Gid>) -> Result<(), UpdateGroupError> {
    self.host.update_group(name, gid)
  }

  fn try_group_from_name(&self, name: &str) -> Result<Option<Self::Group>, TryGroupFromNameError> {
    self.host.try_group_from_name(name)
  }

  fn try_group_from_id(&self, gid: Gid) -> Result<Option<Self::Group>, TryGroupFromIdError> {
    self.host.try_group_from_id(gid)
  }

  fn try_user_from_name(&self, name: &str) -> Result<Option<Self::User>, TryUserFromNameError> {
    self.host.try_user_from_name(name)
  }

  fn get_user_groups(&self, user_name: &str, extra_group: Gid) -> Result<Vec<Gid>, GetUserGroupsError> {
    self.host.get_user_groups(user_name, extra_group)
  }
}

impl<H, S> FsHost for StateAndHost<H, S>
where
  H: FsHost,
  S: Send + Sync,
{
  type Metadata = H::Metadata;
  type DirEntry = H::DirEntry;

  fn create_dir<P: AsRef<Path>>(&self, path: P) -> Result<(), CreateDirError> {
    self.host.create_dir(path)
  }

  fn read_dir<P: AsRef<Path>>(&self, path: P) -> Result<Vec<Self::DirEntry>, ReadDirError> {
    self.host.read_dir(path)
  }

  fn read_file<P: AsRef<Path>>(&self, path: P) -> Result<Vec<u8>, ReadFileError> {
    self.host.read_file(path)
  }

  fn create_file<P: AsRef<Path>, C: AsRef<[u8]>>(&self, path: P, content: C) -> Result<(), CreateFileError> {
    self.host.create_file(path, content)
  }

  fn metadata<P: AsRef<Path>>(&self, path: P) -> Result<Self::Metadata, MetadataError> {
    self.host.metadata(path)
  }

  fn symlink_metadata<P: AsRef<Path>>(&self, path: P) -> Result<Self::Metadata, MetadataError> {
    self.host.symlink_metadata(path)
  }
}

impl<H, S> LinuxFsHost for StateAndHost<H, S>
where
  H: LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  S: Send + Sync,
{
  fn create_dir_with_mode<P: AsRef<Path>>(&self, path: P, mode: FileMode) -> Result<(), CreateDirError> {
    self.host.create_dir_with_mode(path, mode)
  }

  fn set_file_mode<P: AsRef<Path>>(&self, path: P, mode: FileMode) -> Result<(), SetFileModeError> {
    self.host.set_file_mode(path, mode)
  }

  fn set_file_owner<P: AsRef<Path>>(
    &self,
    path: P,
    uid: Option<Uid>,
    gid: Option<Gid>,
  ) -> Result<(), SetFileOwnerError> {
    self.host.set_file_owner(path, uid, gid)
  }

  fn resolve<P: AsRef<Path>>(&self, path: P) -> Result<PathBuf, ResolveError> {
    self.host.resolve(path)
  }

  fn create_symlink(&self, link_path: impl AsRef<Path>, pointee: impl AsRef<Path>) -> Result<(), CreateSymlinkError> {
    self.host.create_symlink(link_path, pointee)
  }
}

impl<H, S> NixHost for StateAndHost<H, S>
where
  H: NixHost,
  S: Send + Sync,
{
}

#[async_trait]
impl<H, S> StatefulHost for StateAndHost<H, S>
where
  H: Host,
  S: StateStore + Send + Sync,
{
  async fn acquire_idempotency_token(&self, key: IdempotencyKey) -> Result<IdempotencyToken, AcquireIdempotencyTokenError> {
    self.state.acquire_idempotency_token(&key).await
  }
}

pub struct RemoteStateStore<'r> {
  inner: Mutex<InnerRemoteStateStore<'r>>,
}

pub struct InnerRemoteStateStore<'r> {
  job_id: u64,
  rx: StdinJobClientReader,
  tx: StdoutJobClientWriter,
  registry: TaskRunnerRegistry<'r, BincodeBuf, &'static LocalLinux, BincodeBuf>,
}

impl<'r> RemoteStateStore<'r> {
  pub fn new(
    rx: StdinJobClientReader,
    tx: StdoutJobClientWriter,
    registry: TaskRunnerRegistry<'r, BincodeBuf, &'static LocalLinux, BincodeBuf>,
  ) -> Self {
    Self {
      inner: Mutex::new(InnerRemoteStateStore {
        job_id: 1,
        rx,
        tx,
        registry,
      }),
    }
  }

  pub fn into_parts(self) -> (StdinJobClientReader, StdoutJobClientWriter) {
    let inner = self.inner.into_inner();
    (inner.rx, inner.tx)
  }
}

#[derive(Debug, Error)]
pub enum ExecError {
  #[error("missing client response (unexpected end of stream)")]
  MissingResponse,
  #[error("client response for invalid job")]
  UnexpectResponseJobId { actual: u64, expected: u64 },
  #[error("client response is malformed: {0}")]
  JobOutput(String),
}

impl<'r> InnerRemoteStateStore<'r> {
  pub async fn exec<'a, RemoteHost: 'static + Host + ?Sized, T>(&mut self, input: &'a T) -> Result<T::Output, ExecError>
  where
    T: NamedTask<&'static RemoteHost>,
    T: Serialize + Sync,
    T::Output: for<'de> Deserialize<'de> + Send,
  {
    let job_id = self.job_id;
    self.job_id += 1;
    self.tx.start_job(job_id, input).await.unwrap();
    loop {
      let res = self
        .rx
        .next_client_message()
        .await
        .unwrap()
        .ok_or(ExecError::MissingResponse)?;
      match res {
        JobClientMessage::RequestStart(job) => {
          let runner = match self.registry.get(job.task.as_str()) {
            Some(r) => r,
            None => panic!("todo: return error / task not found: {:?}", job.task.as_str()),
          };
          let out = runner.erased_run(&job.input, &LocalLinux).await;
          self.tx.job_complete(job.job_id, out).await;
        }
        JobClientMessage::NotifyEnd(res) => {
          if res.job_id != job_id {
            return Err(ExecError::UnexpectResponseJobId {
              actual: res.job_id,
              expected: job_id,
            });
          }
          let out: T::Output = res.output.try_read().map_err(|e| ExecError::JobOutput(e.to_string()))?;
          return Ok(out);
        }
      }
    }
  }
}

#[async_trait]
impl<'r> StateStore for RemoteStateStore<'r> {
  async fn acquire_idempotency_token(&self, key: &IdempotencyKey) -> Result<IdempotencyToken, AcquireIdempotencyTokenError> {
    let mut inner = self.inner.lock().await;
    let result = inner
      .exec::<StateAndHost<LocalLinux, SqliteStore>, _>(&AcquireIdempotencyToken { key: key.clone() })
      .await;

    let result: TaskResult<IdempotencyToken, AcquireIdempotencyTokenError> = match result {
      Ok(t) => t,
      Err(e) => return Err(AcquireIdempotencyTokenError::Other(e.to_string())),
    };
    Ok(result?.output)
  }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct AcquireIdempotencyToken {
  key: IdempotencyKey,
}

#[async_trait]
impl<'h, H: StatefulHost> AsyncFn<&'h H> for AcquireIdempotencyToken {
  type Output = TaskResult<IdempotencyToken, AcquireIdempotencyTokenError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let token = host.acquire_idempotency_token(self.key.clone()).await?;
    Ok(TaskSuccess {
      changed: true,
      output: token,
    })
  }
}

impl<'h, H> NamedTask<&'h H> for AcquireIdempotencyToken
where
  H: 'h + StatefulHost,
{
  const NAME: TaskName = TaskName::new("state::AcquireIdempotencyToken");
}
