use core::fmt;
use std::str::FromStr;

mod config;
pub mod env;
pub mod index;
pub mod sql;

/// ClickHouse uses four components for its version string.
///
/// Their format is `major.minor.patch.tweak`.
///
/// It's formatted in <https://github.com/ClickHouse/ClickHouse/blob/11e4029c6b080e1ac0b6b47ec919e42e929c9b37/tests/ci/version_helper.py#L139>
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseVersion {
  pub major: u32,
  pub minor: u32,
  pub patch: u32,
  pub tweak: u32,
}

impl ClickhouseVersion {
  pub const fn new(major: u32, minor: u32, patch: u32, tweak: u32) -> Self {
    Self {
      major,
      minor,
      patch,
      tweak,
    }
  }
}

impl fmt::Display for ClickhouseVersion {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    write!(f, "{}.{}.{}.{}", self.major, self.minor, self.patch, self.tweak)
  }
}

#[derive(
  Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize, thiserror::Error,
)]
#[error("invalid `ClickhouseVersion` format for input string")]
pub struct InvalidClickhouseVersion;

impl FromStr for ClickhouseVersion {
  type Err = InvalidClickhouseVersion;

  fn from_str(s: &str) -> Result<Self, Self::Err> {
    let mut parts = s.split('.');
    let major: u32 = parts
      .next()
      .ok_or(InvalidClickhouseVersion)?
      .parse()
      .map_err(|_| InvalidClickhouseVersion)?;
    let minor: u32 = parts
      .next()
      .ok_or(InvalidClickhouseVersion)?
      .parse()
      .map_err(|_| InvalidClickhouseVersion)?;
    let patch: u32 = parts
      .next()
      .ok_or(InvalidClickhouseVersion)?
      .parse()
      .map_err(|_| InvalidClickhouseVersion)?;
    let tweak: u32 = parts
      .next()
      .ok_or(InvalidClickhouseVersion)?
      .parse()
      .map_err(|_| InvalidClickhouseVersion)?;
    if parts.next().is_some() {
      return Err(InvalidClickhouseVersion);
    }
    Ok(Self {
      major,
      minor,
      patch,
      tweak,
    })
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct ChUserName<S: AsRef<str> = String>(S);

impl<S: AsRef<str>> ChUserName<S> {
  pub fn new(value: S) -> Self {
    Self::try_new(value).expect("InvalidChUserNameValue")
  }

  pub fn try_new(value: S) -> Option<Self> {
    if value.as_ref().is_empty() {
      return None;
    }
    Some(Self(value))
  }

  pub fn as_str(&self) -> &str {
    self.0.as_ref()
  }

  pub fn as_ref(&self) -> ChUserName<&str> {
    ChUserName(self.as_str())
  }

  pub fn to_owned(&self) -> ChUserName<String> {
    ChUserName(self.as_str().to_owned())
  }

  pub fn inner(self) -> S {
    self.0
  }
}

impl<S: AsRef<str>> fmt::Display for ChUserName<S> {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    self.0.as_ref().fmt(f)
  }
}

impl<S: AsRef<str>> AsRef<str> for ChUserName<S> {
  fn as_ref(&self) -> &str {
    self.as_str()
  }
}
