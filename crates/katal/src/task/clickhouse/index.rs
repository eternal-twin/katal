use crate::task::clickhouse::ClickhouseVersion;
use crate::utils::regex::regex;
use crate::utils::scraper::{get_one_text, selector, GetOneTextError};
use ::scraper::Html;
use itertools::Itertools;
use regex::Regex;
use reqwest::Client;
use scraper::selector::CssLocalName;
use scraper::{CaseSensitivity, Element, ElementRef};
use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;
use std::str::FromStr;
use url::Url;

/// Clickhouse index of releases
///
/// See:
/// - <https://packages.clickhouse.com/tgz/>
#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct ClickhouseIndex {
  releases: BTreeMap<ClickhouseVersion, ClickhouseRelease>,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum ClickhouseIndexFromServerError {
  #[error("failed to fetch index")]
  Fetch(#[from] ClickhouseIndexFetchEntriesError),
  #[error("failed to load index")]
  Load(#[from] ClickhouseIndexFromEntriesError),
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum ClickhouseIndexFromEntriesError {
  #[error("unexpected index entry name {0:?}")]
  UnexpectedName(String),
  #[error("invalid version for index entry {0:?}")]
  InvalidVersion(String),
  #[error("missing package digest")]
  MissingDigest,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum ClickhouseIndexFetchEntriesError {
  #[error("failed to send index page request: {0}")]
  PageRequest(String),
  #[error("failed to read index page response: {0}")]
  PageResponse(String),
  #[error("failed to parse response page")]
  PageParse,
  #[error("failed to scrape response page")]
  PageScrape(#[from] ScrapeIndexPageError),
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum ScrapeIndexPageError {
  #[error("`#contents` must be present exactly once")]
  NonUniqueContents,
  #[error("`#contents` must start with breadcrumbs")]
  MissingBreadCrumbs,
  #[error("`#contents` must have a spacer as its second child")]
  MissingSpacer,
  #[error("`#contents` contains too many entries")]
  TooManyEntries,
  #[error("`#contents` entry {0} is missing `href`")]
  MissingEntryHref(u16),
  #[error("`#contents` entry {0} has invalid name")]
  InvalidEntryName(u16),
  #[error("`#contents` entry {0} is missing size")]
  MissingEntrySize(u16),
  #[error("`#contents` entry {0} is missing pretty size")]
  MissingEntrySizePretty(u16),
  #[error("`#contents` entry {0} is missing time")]
  MissingEntryTime(u16),
  #[error("`#contents` entry {1} has invalid `href`: {0}")]
  InvalidEntryHref(String, u16),
  #[error("`#contents` is missing next page cursor")]
  MissingCursor,
  #[error("`#contents` next page cursor is missing `href`")]
  MissingCursorHref,
  #[error("`#contents` next page cursor has invalid `href`: {0}")]
  InvalidCursorHref(String),
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
struct IndexPage {
  pub breadcrumbs: Vec<String>,
  pub entries: Vec<IndexEntry>,
  pub next: Option<String>,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct IndexEntry {
  pub name: String,
  pub target: Url,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub enum ClickhousePackageName {
  Client,
  CommonStatic,
  CommonStaticDbg,
  Keeper,
  KeeperDbg,
  LibraryBridge,
  OdbcBridge,
  Server,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub enum Arch {
  Amd64,
  Arm64,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct ClickhouseRelease {
  pub version: ClickhouseVersion,
  pub common_static: Option<ClickhousePackage>,
  pub common_static_dbg: Option<ClickhousePackage>,
  pub client: Option<ClickhousePackage>,
  pub keeper: Option<ClickhousePackage>,
  pub keeper_dbg: Option<ClickhousePackage>,
  pub library_bridge: Option<ClickhousePackage>,
  pub odbc_bridge: Option<ClickhousePackage>,
  pub server: Option<ClickhousePackage>,
}

impl ClickhouseRelease {
  fn extend(&mut self, other: Self) {
    assert_eq!(self.version, other.version);
    Self::extend_package(&mut self.common_static, other.common_static);
    Self::extend_package(&mut self.common_static_dbg, other.common_static_dbg);
    Self::extend_package(&mut self.client, other.client);
    Self::extend_package(&mut self.keeper, other.keeper);
    Self::extend_package(&mut self.keeper_dbg, other.keeper_dbg);
    Self::extend_package(&mut self.library_bridge, other.library_bridge);
    Self::extend_package(&mut self.odbc_bridge, other.odbc_bridge);
    Self::extend_package(&mut self.server, other.server);
  }

  fn extend_package(old: &mut Option<ClickhousePackage>, new: Option<ClickhousePackage>) {
    if let Some(new) = new {
      match old {
        None => *old = Some(new),
        Some(old) => old.extend(new),
      }
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct ClickhousePackage {
  pub package: ClickhousePackageName,
  pub version: ClickhouseVersion,
  pub dist: BTreeMap<Arch, ClickhouseDist>,
}

impl ClickhousePackage {
  pub fn download_url(&self, arch: Arch) -> Option<Url> {
    self.dist.get(&arch).map(|d| d.archive.clone())
  }

  pub fn extend(&mut self, other: Self) {
    assert_eq!(self.package, other.package);
    assert_eq!(self.version, other.version);
    let old_size = self.dist.len();
    let new_size = other.dist.len();
    self.dist.extend(other.dist.iter().map(|(k, v)| (*k, v.clone())));
    assert_eq!(
      self.dist.len(),
      old_size + new_size,
      "conflict, duplicated dist entries"
    )
  }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct ClickhouseDist {
  pub archive: Url,
  pub digests: Url,
}

impl ClickhouseIndex {
  pub fn embedded() -> Self {
    let entries: Vec<IndexEntry> =
      serde_json::from_str(include_str!("../../../files/clickhouse/index.json")).expect("embedded index is valid");
    Self::from_entries(&entries).expect("embedded index is valid")
  }

  /// Load index data from server (<https://packages.clickhouse.com/tgz/>)
  pub async fn from_server(user_agent: &str) -> Result<Self, ClickhouseIndexFromServerError> {
    let entries = Self::fetch_entries(user_agent).await?;
    let index = Self::from_entries(&entries)?;
    Ok(index)
  }

  /// Load index from entries
  ///
  /// You can fetch the entries with `fetch_index`.
  pub fn from_entries(entries: &[IndexEntry]) -> Result<Self, ClickhouseIndexFromEntriesError> {
    // language=regexp
    let entry_re: &Regex = regex!(
      r#"^(clickhouse-(?:client|common-static|common-static-dbg|keeper|keeper-dbg|library-bridge|odbc-bridge|server))-(\d+(?:\.\d+){3})(?:-(amd64|arm64))?\.tgz$"#
    );

    let by_name: BTreeMap<String, &IndexEntry> = BTreeMap::from_iter(entries.iter().map(|e| (e.name.clone(), e)));

    let mut releases: BTreeMap<ClickhouseVersion, ClickhouseRelease> = BTreeMap::new();

    for entry in entries {
      if entry.name.is_empty() || entry.name.ends_with(".sha512") {
        // skip self index and digest files
        continue;
      }
      match entry_re.captures(entry.name.as_str()) {
        Some(captures) => {
          let pkg_name = match captures.get(1).expect("capture 1 exists on match").as_str() {
            "clickhouse-client" => ClickhousePackageName::Client,
            "clickhouse-common-static" => ClickhousePackageName::CommonStatic,
            "clickhouse-common-static-dbg" => ClickhousePackageName::CommonStaticDbg,
            "clickhouse-keeper" => ClickhousePackageName::Keeper,
            "clickhouse-keeper-dbg" => ClickhousePackageName::KeeperDbg,
            "clickhouse-library-bridge" => ClickhousePackageName::LibraryBridge,
            "clickhouse-odbc-bridge" => ClickhousePackageName::OdbcBridge,
            "clickhouse-server" => ClickhousePackageName::Server,
            _ => unreachable!(),
          };
          let version = captures.get(2).expect("capture 2 exists on match").as_str().to_string();
          let version = ClickhouseVersion::from_str(&version)
            .map_err(|_| ClickhouseIndexFromEntriesError::InvalidVersion(entry.name.clone()))?;
          let arch = match captures.get(3).map(|m| m.as_str()) {
            None | Some("amd64") => Arch::Amd64,
            Some("arm64") => Arch::Arm64,
            a => unreachable!("unexpected arch value {a:?}"),
          };

          let package = ClickhousePackage {
            version,
            package: pkg_name,
            dist: BTreeMap::from_iter(
              [(
                arch,
                ClickhouseDist {
                  archive: entry.target.clone(),
                  digests: {
                    let digest_name = format!("{}.sha512", entry.name);
                    let digest_entry = by_name
                      .get(&digest_name)
                      .ok_or(crate::task::clickhouse::index::ClickhouseIndexFromEntriesError::MissingDigest)?;
                    digest_entry.target.clone()
                  },
                },
              )]
              .into_iter(),
            ),
          };

          let mut release = ClickhouseRelease {
            version,
            client: None,
            common_static: None,
            common_static_dbg: None,
            keeper: None,
            keeper_dbg: None,
            library_bridge: None,
            odbc_bridge: None,
            server: None,
          };
          match package.package {
            ClickhousePackageName::Client => release.client = Some(package),
            ClickhousePackageName::CommonStatic => release.common_static = Some(package),
            ClickhousePackageName::CommonStaticDbg => release.common_static_dbg = Some(package),
            ClickhousePackageName::Keeper => release.keeper = Some(package),
            ClickhousePackageName::KeeperDbg => release.keeper_dbg = Some(package),
            ClickhousePackageName::LibraryBridge => release.library_bridge = Some(package),
            ClickhousePackageName::OdbcBridge => release.odbc_bridge = Some(package),
            ClickhousePackageName::Server => release.server = Some(package),
          }

          releases
            .entry(version)
            .and_modify(|old| old.extend(release.clone()))
            .or_insert(release);
        }
        None => return Err(ClickhouseIndexFromEntriesError::UnexpectedName(entry.name.clone())),
      }
    }

    Ok(Self { releases })
  }

  /// Download index entries from <https://packages.clickhouse.com/tgz/>
  pub async fn fetch_entries(user_agent: &str) -> Result<Vec<IndexEntry>, ClickhouseIndexFetchEntriesError> {
    let client = Client::builder()
      .user_agent(user_agent)
      .build()
      .expect("building the client succeeds");
    let mut result: Vec<IndexEntry> = Vec::new();
    let mut cursor: Option<String> = None;
    loop {
      let (url, page) = Self::fetch_tgz_stable(&client, cursor.as_deref()).await?;
      let page = Self::scrape_tgz_stable(&url, &page)?;
      result.extend_from_slice(&page.entries);
      match page.next {
        Some(c) => cursor = Some(c),
        None => return Ok(result),
      }
    }
  }

  async fn fetch_tgz_stable(
    client: &Client,
    cursor: Option<&str>,
  ) -> Result<(Url, String), ClickhouseIndexFetchEntriesError> {
    let url = {
      let mut url = Url::parse("https://packages.clickhouse.com/tgz/stable/?directoryListingLimit=1000")
        .expect("the base URL is valid");
      if let Some(cursor) = cursor {
        url.query_pairs_mut().append_pair("cursor", cursor);
      }
      url
    };
    let req = client.get(url.clone()).build().expect("request is valid");
    let res = client
      .execute(req)
      .await
      .map_err(|e| ClickhouseIndexFetchEntriesError::PageRequest(e.to_string()))?;
    let body = res
      .text()
      .await
      .map_err(|e| ClickhouseIndexFetchEntriesError::PageResponse(e.to_string()))?;
    Ok((url, body))
  }

  fn scrape_tgz_stable(url: &Url, page: &str) -> Result<IndexPage, ScrapeIndexPageError> {
    let html: Html = Html::parse_document(page);
    let root: ElementRef = html.root_element();

    let contents = root
      .select(selector!("#contents"))
      .exactly_one()
      .map_err(|_| ScrapeIndexPageError::NonUniqueContents)?;
    let mut contents = contents.children().filter_map(ElementRef::wrap);
    let breadcrumbs: ElementRef = contents.next().ok_or(ScrapeIndexPageError::MissingBreadCrumbs)?;
    let mut breadcrumbs: Vec<String> = breadcrumbs
      .text()
      .filter_map(|t| {
        let t = t.trim();
        if matches!(t, "" | "⟩") {
          None
        } else {
          Some(t.to_string())
        }
      })
      .collect();
    if let Some(t) = breadcrumbs.last_mut() {
      if let Some(stripped) = t.strip_prefix("⟩ ") {
        *t = stripped.to_string();
      }
    }

    let spacer = contents.next().ok_or(ScrapeIndexPageError::MissingSpacer)?;
    if !spacer.has_class(&CssLocalName::from("full"), CaseSensitivity::CaseSensitive) {
      return Err(ScrapeIndexPageError::MissingSpacer);
    }

    let mut entries: Vec<IndexEntry> = Vec::new();
    const MAX_LEN: usize = 2000; // safety value to avoid overlong lists
    let mut i: u16 = 0;
    let has_end_spacer: bool;
    loop {
      if entries.len() >= MAX_LEN {
        return Err(ScrapeIndexPageError::TooManyEntries);
      }
      let name: ElementRef = match contents.next() {
        Some(name) => name,
        None => {
          has_end_spacer = false;
          break;
        }
      };
      if name.value().name() == "div" && name.has_class(&CssLocalName::from("full"), CaseSensitivity::CaseSensitive) {
        has_end_spacer = true;
        break;
      }
      if name.value().name() != "a" {
        return Err(ScrapeIndexPageError::InvalidEntryName(i));
      }
      let target = name.attr("href").ok_or(ScrapeIndexPageError::MissingEntryHref(i))?;
      let name = match get_one_text(name) {
        Ok(name) => name,
        Err(GetOneTextError::Empty) if i == 0 => "", // self entry
        Err(_) => return Err(ScrapeIndexPageError::InvalidEntryName(i)),
      };
      let _byte_size = contents.next().ok_or(ScrapeIndexPageError::MissingEntrySize(i))?;
      let _byte_size_pretty = contents.next().ok_or(ScrapeIndexPageError::MissingEntrySizePretty(i))?;
      let _time = contents.next().ok_or(ScrapeIndexPageError::MissingEntryTime(i))?;
      entries.push(IndexEntry {
        name: name.to_string(),
        target: Url::options()
          .base_url(Some(url))
          .parse(target)
          .map_err(|e| ScrapeIndexPageError::InvalidEntryHref(e.to_string(), i))?,
      });
      i += 1;
    }

    let next = if has_end_spacer {
      match contents.next() {
        Some(next) => {
          let next: ElementRef = next
            .select(selector!(":scope > a"))
            .next()
            .ok_or(ScrapeIndexPageError::MissingCursor)?;
          let next: &str = next.attr("href").ok_or(ScrapeIndexPageError::MissingCursorHref)?;
          let next: Url = Url::options()
            .base_url(Some(url))
            .parse(next)
            .map_err(|e| ScrapeIndexPageError::InvalidCursorHref(e.to_string()))?;
          next
            .query_pairs()
            .find(|(k, _v)| k == "cursor")
            .map(|(_k, v)| v.to_string())
        }
        None => None,
      }
    } else {
      None
    };

    Ok(IndexPage {
      breadcrumbs,
      entries,
      next,
    })
  }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize, thiserror::Error)]
#[error("package not found")]
pub struct PackageNotFound;

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize, thiserror::Error)]
#[error("release not found")]
pub struct ReleaseNotFound;

impl ClickhouseIndex {
  pub fn get_release(&self, version: ClickhouseVersion) -> Result<&ClickhouseRelease, ReleaseNotFound> {
    self.releases.get(&version).ok_or(ReleaseNotFound)
  }
  pub fn get_server(&self, version: ClickhouseVersion) -> Result<&ClickhousePackage, PackageNotFound> {
    match self.releases.get(&version).and_then(|r| r.server.as_ref()) {
      Some(pkg) => Ok(pkg),
      None => Err(PackageNotFound),
    }
  }
}

#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn get_latest() {
    let index = ClickhouseIndex::embedded();
    let actual = index.get_server(ClickhouseVersion::new(24, 11, 1, 2557));
    let expected = ClickhousePackage {
      package: ClickhousePackageName::Server,
      version: ClickhouseVersion::new(24, 11, 1, 2557),
      dist: BTreeMap::from_iter([
        (
          Arch::Amd64,
          ClickhouseDist {
            archive: Url::parse("https://packages.clickhouse.com/tgz/stable/clickhouse-server-24.11.1.2557-amd64.tgz")
              .unwrap(),
            digests: Url::parse(
              "https://packages.clickhouse.com/tgz/stable/clickhouse-server-24.11.1.2557-amd64.tgz.sha512",
            )
            .unwrap(),
          },
        ),
        (
          Arch::Arm64,
          ClickhouseDist {
            archive: Url::parse("https://packages.clickhouse.com/tgz/stable/clickhouse-server-24.11.1.2557-arm64.tgz")
              .unwrap(),
            digests: Url::parse(
              "https://packages.clickhouse.com/tgz/stable/clickhouse-server-24.11.1.2557-arm64.tgz.sha512",
            )
            .unwrap(),
          },
        ),
      ]),
    };
    let expected = Ok(&expected);
    assert_eq!(actual, expected);
  }
}
