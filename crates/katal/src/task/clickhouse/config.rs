use core::fmt;
use indexmap::IndexMap;
use std::borrow::Cow;
use std::path::{Path, PathBuf};
use url::Url;
use xmltree::{Element, EmitterConfig, XMLNode};

#[derive(Debug, Default, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseServerConfig {
  pub logger: Option<ClickhouseServerLogger>,
  pub url_scheme_mappers: IndexMap<String, String>,
  pub http_options_response: IndexMap<String, String>,
  pub display_name: Option<String>,
  pub http_port: Option<u16>,
  pub tcp_port: Option<u16>,
  pub mysql_port: Option<u16>,
  pub postgresql_port: Option<u16>,
  pub https_port: Option<u16>,
  pub tcp_port_secure: Option<u16>,
  pub tcp_with_proxy_port: Option<u16>,
  pub interserver_http_port: Option<u16>,
  pub interserver_https_port: Option<u16>,
  pub interserver_http_host: Option<String>,
  pub interserver_http_credentials: Option<(String, String)>,
  pub listen_host: Option<String>,
  pub interserver_listen_host: Option<String>,
  pub listen_try: Option<bool>,
  pub listen_reuse_port: Option<bool>,
  pub listen_backlog: Option<u64>,
  pub max_connections: Option<u64>,
  pub keep_alive_timeout: Option<u64>,
  pub grpc: Option<ClickhouseServerGrpc>,
  pub open_ssl: Option<ClickhouseServerOpenSsl>,
  pub http_server_default_response: Option<String>,
  pub concurrent_threads_soft_limit_num: Option<String>,
  pub concurrent_threads_soft_limit_ratio_to_cores: Option<String>,
  pub max_concurrent_queries: Option<u64>,
  pub max_server_memory_usage: Option<u64>,
  pub max_thread_pool_size: Option<u64>,
  pub background_buffer_flush_schedule_pool_size: Option<u64>,
  pub background_pool_size: Option<u64>,
  pub background_merges_mutations_concurrency_ratio: Option<u64>,
  pub background_merges_mutations_scheduling_policy: Option<u64>,
  pub background_move_pool_size: Option<u64>,
  pub background_fetches_pool_size: Option<u64>,
  pub background_common_pool_size: Option<u64>,
  pub background_schedule_pool_size: Option<u64>,
  pub background_message_broker_schedule_pool_size: Option<u64>,
  pub background_distributed_schedule_pool_size: Option<u64>,
  pub max_server_memory_usage_to_ram_ratio: Option<u64>,
  pub total_memory_profiler_step: Option<u64>,
  pub total_memory_tracker_sample_probability: Option<u64>,
  pub max_open_files: Option<u64>,
  pub uncompressed_cache_size: Option<u64>,
  pub mark_cache_size: Option<u64>,
  pub mmap_cache_size: Option<u64>,
  pub compiled_expression_cache_size: Option<u64>,
  pub compiled_expression_cache_elements_size: Option<u64>,
  pub validate_tcp_client_information: Option<bool>,
  pub path: Option<PathBuf>,
  pub storage_configuration: Option<String>,
  pub tmp_path: Option<PathBuf>,
  pub allow_plaintext_password: Option<bool>,
  pub allow_no_password: Option<bool>,
  pub allow_implicit_no_password: Option<bool>,
  pub default_password_type: Option<ClickhouseServerPasswordType>,
  pub bcrypt_workfactor: Option<u16>,
  pub password_complexity: Vec<ClickhouseServerPasswordComplexityRule>,
  pub tmp_policy: Option<String>,
  pub user_files_path: Option<PathBuf>,
  pub ldap_servers: IndexMap<String, ClickhouseServerLdap>,
  pub kerberos: Option<ClickhouseServerKerberos>,
  pub user_directories: Option<ClickhouseServerUserDirectories>,
  pub access_control_improvements: Option<ClickhouseServerAccessControlImprovements>,
  pub default_profile: Option<String>,
  pub custom_settings_prefixes: Option<String>,
  pub default_database: Option<String>,
  pub timezone: Option<String>,
  pub umask: Option<String>,
  pub mlock_executable: Option<bool>,
  pub remap_executable: Option<bool>,
  pub jdbc_bridge: Option<ClickhouseServerJdbcBridge>,
  pub remote_servers: IndexMap<String, ClickhouseServerRemoteServer>,
  pub remote_url_allow_hosts: Option<Vec<String>>,
  pub http_forbid_headers: Vec<ClickhouseServerHttpForbidHeader>,
  pub zookeeper: Vec<ClickhouseServerZookeeperNode>,
  pub macros: Option<ClickhouseServerMacros>,
  pub builtin_dictionaries_reload_interval: Option<u64>,
  pub max_session_timeout: Option<u64>,
  pub default_session_timeout: Option<u64>,
  pub graphite: Vec<ClickhouseServerGraphite>,
  pub prometheus: Option<ClickhouseServerPrometheus>,
  pub query_log: Option<ClickhouseServerLog>,
  pub trace_log: Option<ClickhouseServerLog>,
  pub query_thread_log: Option<ClickhouseServerLog>,
  pub query_views_log: Option<ClickhouseServerLog>,
  pub part_log: Option<ClickhouseServerLog>,
  pub text_log: Option<ClickhouseServerLog>,
  pub metric_log: Option<ClickhouseServerMetricLog>,
  pub asynchronous_metric_log: Option<ClickhouseServerLog>,
  pub opentelemetry_span_log: Option<ClickhouseServerLog>,
  pub crash_log: Option<ClickhouseServerLog>,
  pub session_log: Option<ClickhouseServerLog>,
  pub processors_profile_log: Option<ClickhouseServerLog>,
  pub asynchronous_insert_log: Option<ClickhouseServerLog>,
  pub backup_log: Option<ClickhouseServerLog>,
  pub top_level_domains_path: Option<PathBuf>,
  pub top_level_domains_lists: Vec<PathBuf>,
  pub dictionaries_config: Option<String>,
  pub user_defined_executable_functions_config: Option<String>,
  pub user_defined_zookeeper_path: Option<String>,
  pub compression: Vec<ClickhouseServerCompression>,
  pub encryption_codecs: Vec<()>,
  pub distributed_ddl: Option<ClickhouseServerDistributedDdl>,
  pub merge_tree: Option<ClickhouseServerMergeTree>,
  pub max_table_size_to_drop: Option<u64>,
  pub max_partition_size_to_drop: Option<u64>,
  pub graphite_rollup_example: Option<()>,
  pub format_schema_path: Option<PathBuf>,
  pub query_masking_rules: Vec<ClickhouseServerQueryMaskingRule>,
  pub http_handlers: Vec<ClickhouseServerHttpHandlerRule>,
  pub send_crash_reports: Option<ClickhouseServerSendCrashReports>,
  pub disable_internal_dns_cache: Option<bool>,
  pub rocksdb: Option<ClickhouseServerRocksdb>,
  pub query_cache: Option<ClickhouseServerQueryCache>,
  pub show_addresses_in_stack_traces: Option<bool>,
  pub oom_score: Option<u64>,
  pub shutdown_wait_unfinished: Option<u64>,
  pub shutdown_wait_unfinished_queries: Option<bool>,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseServerLogger {
  pub level: LoggerLevel,
  pub log: Option<PathBuf>,
  pub error_log: Option<PathBuf>,
  pub size: u64,
  pub count: u64,
  pub console: Option<bool>,
}

impl ClickhouseServerLogger {
  pub fn to_xml(&self) -> Element {
    let mut logger = Element::new("logger");
    push_text_elem(&mut logger, "level", self.level.as_str());
    if let Some(v) = self.log.as_ref() {
      push_text_elem(&mut logger, "log", v.display().to_string());
    }
    if let Some(v) = self.error_log.as_ref() {
      push_text_elem(&mut logger, "errorlog", v.display().to_string());
    }
    push_text_elem(&mut logger, "size", self.size.to_string());
    push_text_elem(&mut logger, "count", self.count.to_string());
    if let Some(v) = self.console {
      push_text_elem(&mut logger, "console", if v { "1" } else { "0" });
    }
    logger
  }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub enum LoggerLevel {
  None,
  Fatal,
  Critical,
  Error,
  Warning,
  Notice,
  Information,
  Debug,
  Trace,
  Test,
}

impl LoggerLevel {
  pub const fn as_str(&self) -> &'static str {
    match self {
      Self::None => "none",
      Self::Fatal => "fatal",
      Self::Critical => "critical",
      Self::Error => "error",
      Self::Warning => "warning",
      Self::Notice => "notice",
      Self::Information => "information",
      Self::Debug => "debug",
      Self::Trace => "trace",
      Self::Test => "test",
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub enum LoggerFormatting {
  Json { names: IndexMap<String, String> },
}

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseServerGrpc {
  enable_ssl: bool,
  ssl_cert_file: PathBuf,
  ssl_key_file: PathBuf,
  ssl_require_client_auth: bool,
  ssl_ca_cert_file: PathBuf,
  transport_compression_type: String,
  transport_compression_level: String,
  max_send_message_size: String,
  max_receive_message_size: String,
  verbose_logs: bool,
}

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseServerOpenSsl {
  server: ClickhouseServerOpenSslServer,
  client: ClickhouseServerOpenSslClient,
}

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseServerOpenSslServer {
  verification_mode: String,
  load_default_ca_file: bool,
  cache_sessions: bool,
  disable_protocols: String,
  prefer_server_ciphers: bool,
  invalid_certificate_handler: String,
}

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseServerOpenSslClient {
  load_default_ca_file: bool,
  cache_sessions: bool,
  disable_protocols: String,
  prefer_server_ciphers: bool,
  invalid_certificate_handler: String,
}

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseServerStorageConfig {
  disks: Vec<ClickhouseServerStorageDisk>,
  policies: Vec<()>,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub enum ClickhouseServerStorageDisk {
  Default {
    keep_free_space_bytes: u64,
  },
  Data {
    path: PathBuf,
    keep_free_space_bytes: u64,
  },
  S3 {
    r#type: String,
    endpoint: Url,
    access_key_id: String,
    secret_access_key: String,
  },
  BlobStorageDisk {
    r#type: String,
    storage_account_url: Url,
    container_name: String,
    account_name: String,
    account_key: String,
    metadata_path: PathBuf,
    skip_access_check: bool,
  },
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub enum ClickhouseServerPasswordType {
  Plaintext,
  Sha256,
  DoubleSha1,
  Bcrypt,
}

impl ClickhouseServerPasswordType {
  pub const fn as_str(&self) -> &'static str {
    match self {
      Self::Plaintext => "plaintext_password",
      Self::Sha256 => "sha256_password",
      Self::DoubleSha1 => "double_sha1_password",
      Self::Bcrypt => "bcrypt_password",
    }
  }
}

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseServerPasswordComplexityRule {
  pattern: String,
  message: String,
}

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseServerLdap {
  host: Option<String>,
  port: Option<u16>,
  bind_dn: Option<String>,
  user_dn_detection: Option<ClickhouseServerLdapUserDnDetection>,
  verification_cooldown: Option<String>,
  enable_tls: Option<String>,
  tls_minimum_protocol_version: Option<String>,
  tls_require_cert: Option<String>,
  tls_cert_file: Option<PathBuf>,
  tls_key_file: Option<PathBuf>,
  tls_ca_cert_file: Option<PathBuf>,
  tls_ca_cert_dir: Option<PathBuf>,
  tls_cipher_suite: Option<String>,
}

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseServerLdapUserDnDetection {
  base_dn: Option<String>,
  scope: Option<String>,
  search_filter: Option<String>,
}

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseServerKerberos {
  principal: Option<String>,
  realm: Option<String>,
}

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseServerUserDirectories {
  pub users_xml: Option<PathBuf>,
  pub local_directory: Option<PathBuf>,
  pub ldap: Option<ClickhouseServerUserDirectoryLdap>,
}

impl ClickhouseServerUserDirectories {
  pub fn to_xml(&self) -> Element {
    let mut user_directories = Element::new("user_directories");
    if let Some(v) = self.users_xml.as_ref() {
      user_directories.children.push(XMLNode::Element({
        let mut e = Element::new("users_xml");
        push_text_elem(&mut e, "path", v.display().to_string());
        e
      }));
    }
    if let Some(v) = self.local_directory.as_ref() {
      user_directories.children.push(XMLNode::Element({
        let mut e = Element::new("local_directory");
        push_text_elem(&mut e, "path", to_dir_path(v).display().to_string());
        e
      }));
    }
    if let Some(_v) = self.ldap.as_ref() {
      todo!()
    }
    user_directories
  }
}

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseServerUserDirectoryLdap {
  server: Option<String>,
  roles: Vec<String>,
  role_mapping: Vec<ClickhouseServerUserDirectoryLdapRoleMapping>,
}

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseServerUserDirectoryLdapRoleMapping {
  base_dn: Option<String>,
  scope: Option<String>,
  search_filter: Option<String>,
  attribute: Option<String>,
  prefix: Option<String>,
}

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseServerAccessControlImprovements {
  pub users_without_row_policies_can_read_rows: Option<bool>,
  pub on_cluster_queries_require_cluster_grant: Option<bool>,
  pub select_from_system_db_requires_grant: Option<bool>,
  pub select_from_information_schema_requires_grant: Option<bool>,
  pub settings_constraints_replace_previous: Option<bool>,
  pub role_cache_expiration_time_seconds: Option<u16>,
}

impl ClickhouseServerAccessControlImprovements {
  pub fn to_xml(&self) -> Element {
    let mut access_control_improvements = Element::new("access_control_improvements");
    if let Some(v) = self.users_without_row_policies_can_read_rows {
      push_text_elem(
        &mut access_control_improvements,
        "users_without_row_policies_can_read_rows",
        v.to_string(),
      );
    }
    if let Some(v) = self.on_cluster_queries_require_cluster_grant {
      push_text_elem(
        &mut access_control_improvements,
        "on_cluster_queries_require_cluster_grant",
        v.to_string(),
      );
    }
    if let Some(v) = self.select_from_system_db_requires_grant {
      push_text_elem(
        &mut access_control_improvements,
        "select_from_system_db_requires_grant",
        v.to_string(),
      );
    }
    if let Some(v) = self.select_from_information_schema_requires_grant {
      push_text_elem(
        &mut access_control_improvements,
        "select_from_information_schema_requires_grant",
        v.to_string(),
      );
    }
    if let Some(v) = self.settings_constraints_replace_previous {
      push_text_elem(
        &mut access_control_improvements,
        "settings_constraints_replace_previous",
        v.to_string(),
      );
    }
    if let Some(v) = self.role_cache_expiration_time_seconds {
      push_text_elem(
        &mut access_control_improvements,
        "role_cache_expiration_time_seconds",
        v.to_string(),
      );
    }
    access_control_improvements
  }
}

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseServerJdbcBridge {
  host: Option<String>,
  port: Option<u16>,
}

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseServerRemoteServer {
  secret: Option<String>,
  shard: Option<ClickhouseServerRemoteServerShard>,
}

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseServerRemoteServerShard {
  weight: Option<u16>,
  replica: Option<ClickhouseServerRemoteServerShardReplica>,
}

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseServerRemoteServerShardReplica {
  host: Option<String>,
  port: Option<u16>,
  priority: Option<u16>,
  secure: Option<bool>,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub enum ClickhouseServerHttpForbidHeader {
  Simple(String),
  Regexp(String),
}

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseServerZookeeperNode {
  host: Option<String>,
  port: Option<u16>,
}

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseServerMacros {
  shard: Option<String>,
  replica: Option<String>,
}

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseServerGraphite {
  host: Option<String>,
  port: Option<u16>,
  timeout: Option<String>,
  interval: Option<String>,
  root_path: Option<String>,
  hostname_in_path: Option<bool>,
  metrics: Option<bool>,
  events: Option<bool>,
  events_cumulative: Option<bool>,
  asynchronous_metrics: Option<bool>,
}

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseServerPrometheus {
  endpoint: Option<String>,
  port: Option<u16>,
  metrics: Option<bool>,
  events: Option<bool>,
  asynchronous_metrics: Option<bool>,
}

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseServerLog {
  pub database: Option<String>,
  pub table: Option<String>,
  pub partition_by: Option<String>,
  pub ttl: Option<String>,
  pub order_by: Option<String>,
  pub flush_interval_milliseconds: Option<u64>,
  pub max_size_rows: Option<u64>,
  pub reserved_size_rows: Option<u64>,
  pub buffer_size_rows_flush_threshold: Option<u64>,
  pub flush_on_crash: Option<bool>,
}

impl ClickhouseServerLog {
  pub fn extend_xml_children(&self, container: &mut Element) {
    if let Some(v) = self.database.as_ref() {
      push_text_elem(container, "database", v);
    }
    if let Some(v) = self.table.as_ref() {
      push_text_elem(container, "table", v);
    }
    if let Some(v) = self.partition_by.as_ref() {
      push_text_elem(container, "partition_by", v);
    }
    if let Some(v) = self.ttl.as_ref() {
      push_text_elem(container, "ttl", v);
    }
    if let Some(v) = self.order_by.as_ref() {
      push_text_elem(container, "order_by", v);
    }
    if let Some(v) = self.flush_interval_milliseconds {
      push_text_elem(container, "flush_interval_milliseconds", v.to_string());
    }
    if let Some(v) = self.max_size_rows {
      push_text_elem(container, "max_size_rows", v.to_string());
    }
    if let Some(v) = self.reserved_size_rows {
      push_text_elem(container, "reserved_size_rows", v.to_string());
    }
    if let Some(v) = self.buffer_size_rows_flush_threshold {
      push_text_elem(container, "buffer_size_rows_flush_threshold", v.to_string());
    }
    if let Some(v) = self.flush_on_crash {
      push_text_elem(container, "flush_on_crash", v.to_string());
    }
  }
}

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseServerMetricLog {
  pub base: ClickhouseServerLog,
  pub collect_interval_milliseconds: Option<u64>,
}

impl ClickhouseServerMetricLog {
  pub fn extend_xml_children(&self, container: &mut Element) {
    self.base.extend_xml_children(container);
    if let Some(v) = self.collect_interval_milliseconds {
      push_text_elem(container, "collect_interval_milliseconds", v.to_string());
    }
  }
}

#[derive(Debug, Default, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseServerCompression {
  min_part_size: Option<u64>,
  min_part_size_ratio: Option<String>,
  method: Option<String>,
}

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseServerDistributedDdl {
  path: Option<String>,
  profile: Option<String>,
  pool_size: Option<u64>,
  task_max_lifetime: Option<u64>,
  cleanup_delay_period: Option<u64>,
  max_tasks_in_queue: Option<u64>,
}

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseServerMergeTree {
  max_suspicious_broken_parts: Option<u64>,
}

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseServerQueryMaskingRule {
  name: Option<String>,
  regexp: Option<String>,
  replace: Option<String>,
}

#[derive(Debug, Default, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseServerHttpHandlerRule {
  url: Option<String>,
  methods: Option<String>,
  headers: IndexMap<String, String>,
  handler: Option<ClickhouseServerHttpHandlerRuleHandler>,
}

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseServerHttpHandlerRuleHandler {
  r#type: Option<String>,
  query_param_name: Option<String>,
  query: Option<String>,
  status: Option<String>,
  content_type: Option<String>,
  response_content: Option<String>,
}

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseServerSendCrashReports {
  enabled: Option<bool>,
  anonymize: Option<bool>,
  endpoint: Option<Url>,
}

#[derive(Debug, Default, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseServerRocksdb {
  options: IndexMap<String, String>,
  column_family_options: IndexMap<String, String>,
  tables: Vec<ClickhouseServerRocksdbTable>,
}

#[derive(Debug, Default, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseServerRocksdbTable {
  name: Option<String>,
  options: IndexMap<String, String>,
  column_family_options: IndexMap<String, String>,
}

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseServerQueryCache {
  pub max_size_in_bytes: Option<u64>,
  pub max_entries: Option<u64>,
  pub max_entry_size_in_bytes: Option<u64>,
  pub max_entry_size_in_rows: Option<u64>,
}

impl ClickhouseServerQueryCache {
  pub fn to_xml(&self) -> Element {
    let mut query_cache = Element::new("query_cache");
    if let Some(v) = self.max_size_in_bytes {
      push_text_elem(&mut query_cache, "max_size_in_bytes", v.to_string());
    }
    if let Some(v) = self.max_entries {
      push_text_elem(&mut query_cache, "max_entries", v.to_string());
    }
    if let Some(v) = self.max_entry_size_in_bytes {
      push_text_elem(&mut query_cache, "max_entry_size_in_bytes", v.to_string());
    }
    if let Some(v) = self.max_entry_size_in_rows {
      push_text_elem(&mut query_cache, "max_entry_size_in_rows", v.to_string());
    }
    query_cache
  }
}

impl ClickhouseServerConfig {
  pub fn to_xml(&self) -> Element {
    let mut clickhouse = Element::new("clickhouse");
    if let Some(v) = self.logger.as_ref() {
      clickhouse.children.push(XMLNode::Element(v.to_xml()));
    }
    if !self.url_scheme_mappers.is_empty() {
      todo!()
    }
    if !self.http_options_response.is_empty() {
      todo!()
    }
    if let Some(v) = self.display_name.as_ref() {
      push_text_elem(&mut clickhouse, "display_name", v);
    }
    if let Some(v) = self.http_port.as_ref() {
      push_text_elem(&mut clickhouse, "http_port", v.to_string());
    }
    if let Some(v) = self.tcp_port.as_ref() {
      push_text_elem(&mut clickhouse, "tcp_port", v.to_string());
    }
    if let Some(_v) = self.mysql_port.as_ref() {
      todo!()
    }
    if let Some(_v) = self.postgresql_port.as_ref() {
      todo!()
    }
    if let Some(_v) = self.https_port.as_ref() {
      todo!()
    }
    if let Some(_v) = self.tcp_port_secure.as_ref() {
      todo!()
    }
    if let Some(_v) = self.tcp_with_proxy_port.as_ref() {
      todo!()
    }
    if let Some(_v) = self.interserver_http_port.as_ref() {
      todo!()
    }
    if let Some(_v) = self.interserver_https_port.as_ref() {
      todo!()
    }
    if let Some(_v) = self.interserver_http_host.as_ref() {
      todo!()
    }
    if let Some(_v) = self.interserver_http_credentials.as_ref() {
      todo!()
    }
    if let Some(v) = self.listen_host.as_ref() {
      push_text_elem(&mut clickhouse, "listen_host", v);
    }
    if let Some(_v) = self.interserver_listen_host.as_ref() {
      todo!()
    }
    if let Some(_v) = self.listen_try.as_ref() {
      todo!()
    }
    if let Some(_v) = self.listen_reuse_port.as_ref() {
      todo!()
    }
    if let Some(_v) = self.listen_backlog.as_ref() {
      todo!()
    }
    if let Some(v) = self.max_connections.as_ref() {
      push_text_elem(&mut clickhouse, "max_connections", v.to_string());
    }
    if let Some(v) = self.keep_alive_timeout.as_ref() {
      push_text_elem(&mut clickhouse, "keep_alive_timeout", v.to_string());
    }
    if let Some(_v) = self.grpc.as_ref() {
      todo!()
    }
    if let Some(_v) = self.open_ssl.as_ref() {
      todo!()
    }
    if let Some(_v) = self.http_server_default_response.as_ref() {
      todo!()
    }
    if let Some(v) = self.concurrent_threads_soft_limit_num.as_ref() {
      push_text_elem(&mut clickhouse, "concurrent_threads_soft_limit_num", v);
    }
    if let Some(v) = self.concurrent_threads_soft_limit_ratio_to_cores.as_ref() {
      push_text_elem(&mut clickhouse, "concurrent_threads_soft_limit_ratio_to_cores", v);
    }
    if let Some(v) = self.max_concurrent_queries.as_ref() {
      push_text_elem(&mut clickhouse, "max_concurrent_queries", v.to_string());
    }
    if let Some(v) = self.max_server_memory_usage.as_ref() {
      push_text_elem(&mut clickhouse, "max_server_memory_usage", v.to_string());
    }
    if let Some(v) = self.max_thread_pool_size.as_ref() {
      push_text_elem(&mut clickhouse, "max_thread_pool_size", v.to_string());
    }
    if let Some(_v) = self.background_buffer_flush_schedule_pool_size.as_ref() {
      todo!()
    }
    if let Some(_v) = self.background_pool_size.as_ref() {
      todo!()
    }
    if let Some(_v) = self.background_merges_mutations_concurrency_ratio.as_ref() {
      todo!()
    }
    if let Some(_v) = self.background_merges_mutations_scheduling_policy.as_ref() {
      todo!()
    }
    if let Some(_v) = self.background_move_pool_size.as_ref() {
      todo!()
    }
    if let Some(_v) = self.background_fetches_pool_size.as_ref() {
      todo!()
    }
    if let Some(_v) = self.background_common_pool_size.as_ref() {
      todo!()
    }
    if let Some(_v) = self.background_schedule_pool_size.as_ref() {
      todo!()
    }
    if let Some(_v) = self.background_message_broker_schedule_pool_size.as_ref() {
      todo!()
    }
    if let Some(_v) = self.background_distributed_schedule_pool_size.as_ref() {
      todo!()
    }
    if let Some(_v) = self.max_server_memory_usage_to_ram_ratio.as_ref() {
      todo!()
    }
    if let Some(_v) = self.total_memory_profiler_step.as_ref() {
      todo!()
    }
    if let Some(_v) = self.total_memory_tracker_sample_probability.as_ref() {
      todo!()
    }
    if let Some(_v) = self.max_open_files.as_ref() {
      todo!()
    }
    if let Some(_v) = self.uncompressed_cache_size.as_ref() {
      todo!()
    }
    if let Some(_v) = self.mark_cache_size.as_ref() {
      todo!()
    }
    if let Some(_v) = self.mmap_cache_size.as_ref() {
      todo!()
    }
    if let Some(_v) = self.compiled_expression_cache_size.as_ref() {
      todo!()
    }
    if let Some(_v) = self.compiled_expression_cache_elements_size.as_ref() {
      todo!()
    }
    if let Some(_v) = self.validate_tcp_client_information.as_ref() {
      todo!()
    }
    if let Some(v) = self.path.as_ref() {
      push_text_elem(&mut clickhouse, "path", to_dir_path(v).display().to_string());
    }
    if let Some(_v) = self.storage_configuration.as_ref() {
      todo!()
    }
    if let Some(v) = self.tmp_path.as_ref() {
      push_text_elem(&mut clickhouse, "tmp_path", to_dir_path(v).display().to_string());
    }
    if let Some(v) = self.allow_plaintext_password.as_ref() {
      push_text_elem(&mut clickhouse, "allow_plaintext_password", v.to_string());
    }
    if let Some(v) = self.allow_no_password.as_ref() {
      push_text_elem(&mut clickhouse, "allow_no_password", v.to_string());
    }
    if let Some(v) = self.allow_implicit_no_password.as_ref() {
      push_text_elem(&mut clickhouse, "allow_implicit_no_password", v.to_string());
    }
    if let Some(v) = self.default_password_type.as_ref() {
      push_text_elem(&mut clickhouse, "default_password_type", v.as_str());
    }
    if let Some(v) = self.bcrypt_workfactor.as_ref() {
      push_text_elem(&mut clickhouse, "bcrypt_workfactor", v.to_string());
    }
    if !self.password_complexity.is_empty() {
      todo!()
    }
    if let Some(_v) = self.tmp_policy.as_ref() {
      todo!()
    }
    if let Some(v) = self.user_files_path.as_ref() {
      push_text_elem(&mut clickhouse, "user_files_path", to_dir_path(v).display().to_string());
    }
    if !self.ldap_servers.is_empty() {
      todo!()
    }
    if let Some(_v) = self.kerberos.as_ref() {
      todo!()
    }
    if let Some(v) = self.user_directories.as_ref() {
      clickhouse.children.push(XMLNode::Element(v.to_xml()));
    }
    if let Some(v) = self.access_control_improvements.as_ref() {
      clickhouse.children.push(XMLNode::Element(v.to_xml()));
    }
    if let Some(v) = self.default_profile.as_ref() {
      push_text_elem(&mut clickhouse, "default_profile", v);
    }
    if let Some(v) = self.custom_settings_prefixes.as_ref() {
      push_text_elem(&mut clickhouse, "custom_settings_prefixes", v);
    }
    if let Some(v) = self.default_database.as_ref() {
      push_text_elem(&mut clickhouse, "default_database", v);
    }
    if let Some(v) = self.timezone.as_ref() {
      push_text_elem(&mut clickhouse, "timezone", v);
    }
    if let Some(v) = self.umask.as_ref() {
      push_text_elem(&mut clickhouse, "umask", v);
    }
    if let Some(v) = self.mlock_executable.as_ref() {
      push_text_elem(&mut clickhouse, "mlock_executable", v.to_string());
    }
    if let Some(v) = self.remap_executable.as_ref() {
      push_text_elem(&mut clickhouse, "remap_executable", v.to_string());
    }
    if let Some(_v) = self.jdbc_bridge.as_ref() {
      todo!()
    }
    if !self.remote_servers.is_empty() {
      todo!()
    }
    if let Some(_v) = self.remote_url_allow_hosts.as_ref() {
      todo!()
    }
    if !self.http_forbid_headers.is_empty() {
      todo!()
    }
    if !self.zookeeper.is_empty() {
      todo!()
    }
    if let Some(_v) = self.macros.as_ref() {
      todo!()
    }
    if let Some(v) = self.builtin_dictionaries_reload_interval.as_ref() {
      push_text_elem(&mut clickhouse, "builtin_dictionaries_reload_interval", v.to_string());
    }
    if let Some(v) = self.max_session_timeout.as_ref() {
      push_text_elem(&mut clickhouse, "max_session_timeout", v.to_string());
    }
    if let Some(v) = self.default_session_timeout.as_ref() {
      push_text_elem(&mut clickhouse, "default_session_timeout", v.to_string());
    }
    if !self.graphite.is_empty() {
      todo!()
    }
    if let Some(_v) = self.prometheus.as_ref() {
      todo!()
    }
    if let Some(v) = self.query_log.as_ref() {
      clickhouse.children.push(XMLNode::Element({
        let mut e = Element::new("query_log");
        v.extend_xml_children(&mut e);
        e
      }));
    }
    if let Some(v) = self.trace_log.as_ref() {
      clickhouse.children.push(XMLNode::Element({
        let mut e = Element::new("trace_log");
        v.extend_xml_children(&mut e);
        e
      }));
    }
    if let Some(v) = self.query_thread_log.as_ref() {
      clickhouse.children.push(XMLNode::Element({
        let mut e = Element::new("query_thread_log");
        v.extend_xml_children(&mut e);
        e
      }));
    }
    if let Some(v) = self.query_views_log.as_ref() {
      clickhouse.children.push(XMLNode::Element({
        let mut e = Element::new("query_views_log");
        v.extend_xml_children(&mut e);
        e
      }));
    }
    if let Some(v) = self.part_log.as_ref() {
      clickhouse.children.push(XMLNode::Element({
        let mut e = Element::new("part_log");
        v.extend_xml_children(&mut e);
        e
      }));
    }
    if let Some(_v) = self.text_log.as_ref() {
      todo!()
    }
    if let Some(v) = self.metric_log.as_ref() {
      clickhouse.children.push(XMLNode::Element({
        let mut e = Element::new("metric_log");
        v.extend_xml_children(&mut e);
        e
      }));
    }
    if let Some(v) = self.asynchronous_metric_log.as_ref() {
      clickhouse.children.push(XMLNode::Element({
        let mut e = Element::new("asynchronous_metric_log");
        v.extend_xml_children(&mut e);
        e
      }));
    }
    if let Some(_v) = self.opentelemetry_span_log.as_ref() {
      todo!()
    }
    if let Some(v) = self.crash_log.as_ref() {
      clickhouse.children.push(XMLNode::Element({
        let mut e = Element::new("crash_log");
        v.extend_xml_children(&mut e);
        e
      }));
    }
    if let Some(v) = self.session_log.as_ref() {
      clickhouse.children.push(XMLNode::Element({
        let mut e = Element::new("session_log");
        v.extend_xml_children(&mut e);
        e
      }));
    }
    if let Some(v) = self.processors_profile_log.as_ref() {
      clickhouse.children.push(XMLNode::Element({
        let mut e = Element::new("processors_profile_log");
        v.extend_xml_children(&mut e);
        e
      }));
    }
    if let Some(v) = self.asynchronous_insert_log.as_ref() {
      clickhouse.children.push(XMLNode::Element({
        let mut e = Element::new("asynchronous_insert_log");
        v.extend_xml_children(&mut e);
        e
      }));
    }
    if let Some(v) = self.backup_log.as_ref() {
      clickhouse.children.push(XMLNode::Element({
        let mut e = Element::new("backup_log");
        v.extend_xml_children(&mut e);
        e
      }));
    }
    if let Some(_v) = self.top_level_domains_path.as_ref() {
      todo!()
    }
    if !self.top_level_domains_lists.is_empty() {
      todo!()
    }
    if let Some(_v) = self.dictionaries_config.as_ref() {
      todo!()
    }
    if let Some(_v) = self.user_defined_executable_functions_config.as_ref() {
      todo!()
    }
    if let Some(_v) = self.user_defined_zookeeper_path.as_ref() {
      todo!()
    }
    if !self.compression.is_empty() {
      todo!()
    }
    if !self.encryption_codecs.is_empty() {
      todo!()
    }
    if let Some(_v) = self.distributed_ddl.as_ref() {
      todo!()
    }
    if let Some(_v) = self.merge_tree.as_ref() {
      todo!()
    }
    if let Some(_v) = self.max_table_size_to_drop.as_ref() {
      todo!()
    }
    if let Some(_v) = self.max_partition_size_to_drop.as_ref() {
      todo!()
    }
    if let Some(_v) = self.graphite_rollup_example.as_ref() {
      todo!()
    }
    if let Some(v) = self.format_schema_path.as_ref() {
      push_text_elem(
        &mut clickhouse,
        "format_schema_path",
        to_dir_path(v).display().to_string(),
      );
    }
    if !self.query_masking_rules.is_empty() {
      todo!()
    }
    if !self.http_handlers.is_empty() {
      todo!()
    }
    if let Some(_v) = self.send_crash_reports.as_ref() {
      todo!()
    }
    if let Some(_v) = self.disable_internal_dns_cache.as_ref() {
      todo!()
    }
    if let Some(_v) = self.rocksdb.as_ref() {
      todo!()
    }
    if let Some(v) = self.query_cache.as_ref() {
      clickhouse.children.push(XMLNode::Element(v.to_xml()));
    }
    if let Some(_v) = self.show_addresses_in_stack_traces.as_ref() {
      todo!()
    }
    if let Some(_v) = self.oom_score.as_ref() {
      todo!()
    }
    if let Some(_v) = self.shutdown_wait_unfinished.as_ref() {
      todo!()
    }
    if let Some(_v) = self.shutdown_wait_unfinished_queries.as_ref() {
      todo!()
    }
    clickhouse
  }
}

impl fmt::Display for ClickhouseServerConfig {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    let xml = self.to_xml();
    let mut writer: Vec<u8> = Vec::new();
    let mut config = EmitterConfig::new();
    config.perform_indent = true;
    xml
      .write_with_config(&mut writer, config)
      .expect("writing to `Vec<u8>` always succeeds");
    let writer = String::from_utf8(writer).expect("output is valid utf-8");
    f.write_str(writer.as_str())
  }
}

#[derive(Debug, Default, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseUsers {
  pub profiles: IndexMap<String, ClickhouseUserProfile>,
  pub users: IndexMap<String, ClickhouseUserConfig>,
  pub quotas: IndexMap<String, ClickhouseUserQuota>,
}

impl ClickhouseUsers {
  pub fn to_xml(&self) -> Element {
    let mut clickhouse = Element::new("clickhouse");
    if !self.profiles.is_empty() {
      let mut profiles = Element::new("profiles");
      for (profile_name, profile_config) in &self.profiles {
        let mut profile = Element::new(profile_name);
        profile_config.extend_xml_children(&mut profile);
        profiles.children.push(XMLNode::Element(profile));
      }
      clickhouse.children.push(XMLNode::Element(profiles));
    }
    if !self.users.is_empty() {
      let mut users = Element::new("users");
      for (user_name, user_config) in &self.users {
        let mut user = Element::new(user_name);
        user_config.extend_xml_children(&mut user);
        users.children.push(XMLNode::Element(user));
      }
      clickhouse.children.push(XMLNode::Element(users));
    }
    if !self.quotas.is_empty() {
      let mut quotas = Element::new("quotas");
      for (quota_name, quota_config) in &self.quotas {
        let mut quota = Element::new(quota_name);
        quota_config.extend_xml_children(&mut quota);
        quotas.children.push(XMLNode::Element(quota));
      }
      clickhouse.children.push(XMLNode::Element(quotas));
    }
    clickhouse
  }
}

impl fmt::Display for ClickhouseUsers {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    let xml = self.to_xml();
    let mut writer: Vec<u8> = Vec::new();
    let mut config = EmitterConfig::new();
    config.perform_indent = true;
    xml
      .write_with_config(&mut writer, config)
      .expect("writing to `Vec<u8>` always succeeds");
    let writer = String::from_utf8(writer).expect("output is valid utf-8");
    f.write_str(writer.as_str())
  }
}

#[derive(Debug, Default, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseUserProfile {
  pub readonly: Option<bool>,
}

impl ClickhouseUserProfile {
  pub fn extend_xml_children(&self, container: &mut Element) {
    if let Some(v) = self.readonly {
      push_text_elem(container, "readonly", if v { "1" } else { "0" });
    }
  }
}

#[derive(Debug, Default, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseUserConfig {
  pub password: Option<ClickhouseUserPassword>,
  pub networks: Vec<ClickhouseUserNetwork>,
  pub profile: Option<String>,
  pub quota: Option<String>,
  pub access_management: Option<bool>,
}

impl ClickhouseUserConfig {
  pub fn extend_xml_children(&self, container: &mut Element) {
    if let Some(v) = self.password.as_ref() {
      match v {
        ClickhouseUserPassword::PlainText(v) => push_text_elem(container, "password", v),
        ClickhouseUserPassword::Sha256(v) => push_text_elem(container, "password_sha256_hex", v),
        ClickhouseUserPassword::DoubleSha1(v) => push_text_elem(container, "password_double_sha1_hex", v),
      };
    }
    if !self.networks.is_empty() {
      let mut networks = Element::new("networks");
      for network in &self.networks {
        match network {
          ClickhouseUserNetwork::Ip(v) => push_text_elem(&mut networks, "ip", v),
          ClickhouseUserNetwork::Host(v) => push_text_elem(&mut networks, "host", v),
          ClickhouseUserNetwork::HostRegexp(v) => push_text_elem(&mut networks, "host_regexp", v),
        }
      }
      container.children.push(XMLNode::Element(networks));
    }
    if let Some(v) = self.profile.as_ref() {
      push_text_elem(container, "profile", v);
    }
    if let Some(v) = self.quota.as_ref() {
      push_text_elem(container, "quota", v);
    }
    if let Some(v) = self.access_management {
      push_text_elem(container, "access_management", if v { "1" } else { "0" });
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub enum ClickhouseUserPassword {
  PlainText(String),
  Sha256(String),
  DoubleSha1(String),
}
#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub enum ClickhouseUserNetwork {
  Ip(String),
  Host(String),
  HostRegexp(String),
}

#[derive(Debug, Default, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseUserQuota {
  pub intervals: Option<ClickhouseUserQuotaInterval>,
}

impl ClickhouseUserQuota {
  pub fn extend_xml_children(&self, container: &mut Element) {
    if let Some(interval) = &self.intervals {
      container.children.push(XMLNode::Element(interval.to_xml()))
    }
  }
}

#[derive(Debug, Default, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseUserQuotaInterval {
  pub duration: Option<u64>,
  pub queries: Option<u64>,
  pub errors: Option<u64>,
  pub result_rows: Option<u64>,
  pub read_rows: Option<u64>,
  pub execution_time: Option<u64>,
}

impl ClickhouseUserQuotaInterval {
  pub fn to_xml(&self) -> Element {
    let mut interval = Element::new("interval");
    self.extend_xml_children(&mut interval);
    interval
  }

  fn extend_xml_children(&self, container: &mut Element) {
    if let Some(v) = self.duration {
      push_text_elem(container, "duration", v.to_string());
    }
    if let Some(v) = self.queries {
      push_text_elem(container, "queries", v.to_string());
    }
    if let Some(v) = self.errors {
      push_text_elem(container, "errors", v.to_string());
    }
    if let Some(v) = self.result_rows {
      push_text_elem(container, "result_rows", v.to_string());
    }
    if let Some(v) = self.read_rows {
      push_text_elem(container, "read_rows", v.to_string());
    }
    if let Some(v) = self.execution_time {
      push_text_elem(container, "execution_time", v.to_string());
    }
  }
}

fn push_text_elem<Val: AsRef<str>>(parent: &mut Element, child: &str, value: Val) {
  parent.children.push(XMLNode::Element({
    let mut e = Element::new(child);
    e.children.push(XMLNode::Text(value.as_ref().to_string()));
    e
  }));
}

fn to_dir_path(path: &Path) -> Cow<'_, Path> {
  if path.as_os_str().to_string_lossy().ends_with('/') {
    Cow::Borrowed(path)
  } else {
    Cow::Owned({
      let mut p = path.to_path_buf();
      p.push("");
      p
    })
  }
}

#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn test_to_dir_path() {
    assert_eq!(
      to_dir_path(&PathBuf::from("/")).display().to_string(),
      String::from("/")
    );
    assert_eq!(
      to_dir_path(&PathBuf::from("/var/lib/clickhouse")).display().to_string(),
      String::from("/var/lib/clickhouse/")
    );
    assert_eq!(
      to_dir_path(&PathBuf::from("/var/lib/clickhouse/"))
        .display()
        .to_string(),
      String::from("/var/lib/clickhouse/")
    );
  }
}
