use chrono::{DateTime, Utc};
use clickhouse::Client;
use std::fmt;
use std::fmt::Write;
use thiserror::Error;
use vec1::Vec1;

#[derive(Debug, Clone, PartialEq, Eq, Error)]
pub enum CreateUserError {
  #[error("unexpected error: {0}")]
  Other(String),
}

/// <https://clickhouse.com/docs/en/sql-reference/statements/create/user>
#[derive(Debug, Copy, Clone)]
pub struct CreateUserQuery<'q> {
  on_conflict: Option<OnUserConflict>,
  // todo: support multiple (name, cluster) pairs
  name: &'q str,
  cluster: Option<&'q str>,
  identify_by: Option<UserIdentification<'q>>,
  // todo: support multiple hosts
  hosts: Option<UserHost<'q>>,
  valid_until: Option<DateTime<Utc>>,
  storage_type: Option<&'q str>,
  // todo: support muoltiple default roles
  default_role: Option<&'q str>,
  default_database: Option<Option<&'q str>>,
  // todo: grantees support
  grantees: Option<()>,
  // todo: settings support
  settings: Option<()>,
}

#[derive(Debug, Copy, Clone)]
pub enum OnUserConflict {
  /// `CREATE USER IF NOT EXISTS`
  Ignore,
  /// `CREATE USER OR REPLACE`
  Replace,
}

#[derive(Debug, Copy, Clone)]
pub enum UserIdentification<'q> {
  /// No password
  NoPassword,
  PlaintextPassword(&'q str),
  Sha256Password(&'q str),
  Sha256Hash(&'q str, Option<&'q str>),
  DoubleSha1Password(&'q str),
  DoubleSha1Hash(&'q str),
  BcryptPassword(&'q str),
  BcryptHash(&'q str),
  Ldap(&'q str),
  Kerberos(Option<&'q str>),
  SslCertificate(&'q str),
  SshKey(&'q str, &'q str), // todo: support multiple keys
  Http(&'q str),
}

#[derive(Debug, Copy, Clone)]
pub enum UserHost<'q> {
  Local,
  Name(&'q str),
  Regexp(&'q str),
  Ip(&'q str),
  Like(&'q str),
  Any,
  None,
}

impl<'q> CreateUserQuery<'q> {
  pub fn new(name: &'q str) -> Self {
    Self {
      on_conflict: None,
      name,
      cluster: None,
      identify_by: None,
      hosts: None,
      valid_until: None,
      storage_type: None,
      default_role: None,
      default_database: None,
      grantees: None,
      settings: None,
    }
  }

  pub fn on_conflict(mut self, on_conflict: OnUserConflict) -> Self {
    self.on_conflict = Some(on_conflict);
    self
  }

  pub fn cluster(mut self, cluster: &'q str) -> Self {
    self.cluster = Some(cluster);
    self
  }

  pub fn identify_by(mut self, identify_by: UserIdentification<'q>) -> Self {
    self.identify_by = Some(identify_by);
    self
  }

  pub fn host(mut self, host: UserHost<'q>) -> Self {
    self.hosts = Some(host);
    self
  }

  pub fn valid_until(mut self, valid_until: DateTime<Utc>) -> Self {
    self.valid_until = Some(valid_until);
    self
  }

  pub fn storage_type(mut self, storage_type: &'q str) -> Self {
    self.storage_type = Some(storage_type);
    self
  }

  pub fn default_role(mut self, default_role: &'q str) -> Self {
    self.default_role = Some(default_role);
    self
  }

  pub fn default_database(mut self, default_database: Option<&'q str>) -> Self {
    self.default_database = Some(default_database);
    self
  }

  pub fn to_sql(&self) -> String {
    let mut query: String = "CREATE USER ".to_string();
    if let Some(on_conflict) = self.on_conflict {
      match on_conflict {
        OnUserConflict::Ignore => query.push_str("IF NOT EXISTS "),
        OnUserConflict::Replace => query.push_str("OR REPLACE "),
      };
    }
    query.push_str(ChSqlQuotedId(self.name).to_string().as_str());
    if let Some(user_identification) = self.identify_by {
      query.push_str(" IDENTIFIED WITH ");
      match user_identification {
        UserIdentification::NoPassword => query.push_str("no_password"),
        UserIdentification::PlaintextPassword(pwd) => {
          query.push_str(&format!("plaintext_password BY {}", ChSqlStrLit(pwd)))
        }
        UserIdentification::Sha256Password(pwd) => query.push_str(&format!("sha256_password BY {}", ChSqlStrLit(pwd))),
        UserIdentification::Sha256Hash(hash, salt) => {
          query.push_str(&format!("sha256_hash BY {}", ChSqlStrLit(hash)));
          if let Some(salt) = salt {
            query.push_str(&format!(" SALT {}", ChSqlStrLit(salt)));
          }
        }
        UserIdentification::DoubleSha1Password(pwd) => {
          query.push_str(&format!("double_sha1_password BY {}", ChSqlStrLit(pwd)))
        }
        UserIdentification::DoubleSha1Hash(hash) => {
          query.push_str(&format!("double_sha1_hash BY {}", ChSqlStrLit(hash)))
        }
        UserIdentification::BcryptPassword(pwd) => query.push_str(&format!("bcrypt_password BY {}", ChSqlStrLit(pwd))),
        UserIdentification::BcryptHash(hash) => query.push_str(&format!("bcrypt_hash BY {}", ChSqlStrLit(hash))),
        UserIdentification::Ldap(server) => query.push_str(&format!("ldap SERVER {}", ChSqlStrLit(server))),
        UserIdentification::Kerberos(realm) => {
          query.push_str("kerberos");
          if let Some(realm) = realm {
            query.push_str(&format!(" REALM {}", ChSqlStrLit(realm)))
          }
        }
        UserIdentification::SslCertificate(canonical_name) => {
          query.push_str(&format!("ssl_certificate CN {}", ChSqlStrLit(canonical_name)))
        }
        UserIdentification::SshKey(_, _) => todo!(),
        UserIdentification::Http(server) => query.push_str(&format!("http SERVER {}", ChSqlStrLit(server))),
      }
    }
    if let Some(host) = self.hosts {
      query.push_str(" HOST ");
      match host {
        UserHost::Local => query.push_str("LOCAL"),
        UserHost::Name(host) => query.push_str(&format!("NAME {}", ChSqlStrLit(host))),
        UserHost::Regexp(host_re) => query.push_str(&format!("REGEXP {}", ChSqlStrLit(host_re))),
        UserHost::Ip(ip) => query.push_str(&format!("IP {}", ChSqlStrLit(ip))),
        UserHost::Like(pattern) => query.push_str(&format!("LIKE {}", ChSqlStrLit(pattern))),
        UserHost::Any => query.push_str("ANY"),
        UserHost::None => query.push_str("NONE"),
      }
    }
    if let Some(_valid_until) = self.valid_until {
      todo!()
    }
    if let Some(storage_type) = self.storage_type {
      query.push_str(&format!(" IN {}", ChSqlQuotedId(storage_type)))
    }
    if let Some(default_role) = self.default_role {
      query.push_str(&format!(" DEFAULT ROLE {}", ChSqlQuotedId(default_role)))
    }
    if let Some(default_database) = self.default_database {
      query.push_str(" DEFAULT DATBASE ");
      match default_database {
        None => query.push_str("NONE"),
        Some(default_database) => query.push_str(ChSqlQuotedId(default_database).to_string().as_str()),
      }
    }
    if let Some(_grantees) = self.grantees {
      todo!()
    }
    if let Some(_settings) = self.settings {
      todo!()
    }
    query.push(';');
    query
  }

  pub async fn execute(self, client: &Client) -> Result<(), CreateUserError> {
    let query: String = self.to_sql();

    client
      .query(&query)
      .execute()
      .await
      .map_err(|e| CreateUserError::Other(e.to_string()))?;

    Ok(())
  }
}

#[derive(Debug, Clone, PartialEq, Eq, Error)]
pub enum CreateDatabaseError {
  #[error("unexpected error: {0}")]
  Other(String),
}

/// <https://clickhouse.com/docs/en/sql-reference/statements/create/database>
#[derive(Debug, Copy, Clone)]
pub struct CreateDatabaseQuery<'q> {
  name: &'q str,
  if_not_exists: bool,
  cluster: Option<&'q str>,
  engine: Option<DatabaseEngine>,
  comment: Option<&'q str>,
}

#[derive(Debug, Copy, Clone)]
pub enum DatabaseEngine {
  Atomic,
  Lazy,
  Mysql,
  Memory,
  Postgresql,
  MaterializedMysql,
  MaterializedPostgresql,
  Replicated,
  Sqlite,
}

impl DatabaseEngine {
  pub const fn as_str(&self) -> &'static str {
    match self {
      Self::Atomic => "Atomic",
      _ => todo!(),
    }
  }
}

impl<'q> CreateDatabaseQuery<'q> {
  pub fn new(name: &'q str) -> Self {
    Self {
      name,
      if_not_exists: false,
      cluster: None,
      engine: None,
      comment: None,
    }
  }

  pub fn if_not_exists(mut self) -> Self {
    self.if_not_exists = true;
    self
  }

  pub fn cluster(mut self, cluster: &'q str) -> Self {
    self.cluster = Some(cluster);
    self
  }

  pub fn engine(mut self, engine: DatabaseEngine) -> Self {
    self.engine = Some(engine);
    self
  }

  pub fn comment(mut self, comment: &'q str) -> Self {
    self.comment = Some(comment);
    self
  }

  pub fn to_sql(&self) -> String {
    let mut query: String = "CREATE DATABASE ".to_string();
    if self.if_not_exists {
      query.push_str("IF NOT EXISTS ");
    }
    query.push_str(ChSqlQuotedId(self.name).to_string().as_str());
    if let Some(engine) = self.engine {
      query.push_str(" ENGINE = ");
      query.push_str(engine.as_str());
    }
    if let Some(comment) = self.comment {
      query.push_str(" COMMENT ");
      query.push_str(&ChSqlStrLit(comment).to_string());
    }
    query.push(';');
    query
  }

  pub async fn execute(self, client: &Client) -> Result<(), CreateDatabaseError> {
    let query: String = self.to_sql();

    client
      .query(&query)
      .execute()
      .await
      .map_err(|e| CreateDatabaseError::Other(e.to_string()))?;

    Ok(())
  }
}

#[derive(Debug, Clone, PartialEq, Eq, Error)]
pub enum GrantError {
  #[error("unexpected error: {0}")]
  Other(String),
}

/// <https://clickhouse.com/docs/en/sql-reference/statements/create/database>
#[derive(Debug, Clone)]
pub struct GrantQuery<'q> {
  grantee: &'q str,
  cluster: Option<&'q str>,
  privilege: Vec1<Privilege>,
  scope: GrantScope<'q>,
  with_grant_option: Option<bool>,
  with_replace_option: Option<bool>,
}

#[derive(Debug, Copy, Clone)]
pub enum GrantScope<'q> {
  DatabaseTable(&'q str, &'q str),
  Database(&'q str),
  AllDatabase,
  Table(&'q str),
  AllTable,
}

// todo: support privilege hierarchy and options
#[derive(Debug, Copy, Clone)]
pub enum Privilege {
  All,
  Select,
  Insert,
  Alter,
  Create,
  Drop,
  Truncate,
  Optimize,
  Show,
  KillQuery,
  AccessManagement,
  System,
  Introspection,
  Sources,
  DictGet,
  DisplaySecretsInShowAndSelect,
  NamedCollectionAdmin,
}

impl Privilege {
  pub fn as_str(self) -> &'static str {
    match self {
      Self::All => "ALL",
      Self::Select => "SELECT",
      Self::Insert => "INSERT",
      Self::Alter => "ALTER",
      Self::Create => "CREATE",
      Self::Drop => "DROP",
      Self::Truncate => "TRUNCATE",
      Self::Optimize => "OPTIMIZE",
      Self::Show => "SHOW",
      Self::KillQuery => "KILL QUERY",
      Self::AccessManagement => "ACCESS MANAGEMENT",
      Self::System => "SYSTEM",
      Self::Introspection => "INTROSPECTION",
      Self::Sources => "SOURCES",
      Self::DictGet => "dictGet",
      Self::DisplaySecretsInShowAndSelect => "displaySecretsInShowAndSelect",
      Self::NamedCollectionAdmin => "NAMED COLLECTION ADMIN",
    }
  }
}

impl<'q> GrantQuery<'q> {
  pub fn new(grantee: &'q str, privilege: Privilege, scope: GrantScope<'q>) -> Self {
    Self {
      grantee,
      cluster: None,
      privilege: Vec1::new(privilege),
      scope,
      with_grant_option: None,
      with_replace_option: None,
    }
  }

  pub fn cluster(mut self, cluster: &'q str) -> Self {
    self.cluster = Some(cluster);
    self
  }

  pub fn privilege(mut self, privilege: Privilege) -> Self {
    self.privilege.push(privilege);
    self
  }

  pub fn to_sql(&self) -> String {
    let mut query: String = "GRANT ".to_string();
    if let Some(cluster) = self.cluster {
      query.push_str("ON CLUSTER ");
      query.push_str(&ChSqlQuotedId(cluster).to_string());
    }
    query.push_str(self.privilege.first().as_str());
    for p in self.privilege.iter().skip(1) {
      query.push_str(", ");
      query.push_str(p.as_str());
    }
    query.push_str(" ON ");
    match self.scope {
      GrantScope::DatabaseTable(db, t) => {
        query.push_str(ChSqlQuotedId(db).to_string().as_str());
        query.push('.');
        query.push_str(ChSqlQuotedId(t).to_string().as_str());
      }
      GrantScope::Database(db) => {
        query.push_str(ChSqlQuotedId(db).to_string().as_str());
        query.push_str(".*");
      }
      GrantScope::AllDatabase => {
        query.push_str("*.*");
      }
      GrantScope::Table(t) => {
        query.push_str(ChSqlQuotedId(t).to_string().as_str());
      }
      GrantScope::AllTable => {
        query.push('*');
      }
    }
    query.push_str(" TO ");
    query.push_str(ChSqlQuotedId(self.grantee).to_string().as_str());
    if self.with_grant_option.unwrap_or(false) {
      query.push_str(" WITH GRANT OPTION");
    }
    if self.with_replace_option.unwrap_or(false) {
      query.push_str(" WITH REPLACE OPTION");
    }
    query.push(';');
    query
  }

  pub async fn execute(self, client: &Client) -> Result<(), CreateDatabaseError> {
    let query: String = self.to_sql();

    client
      .query(&query)
      .execute()
      .await
      .map_err(|e| CreateDatabaseError::Other(e.to_string()))?;

    Ok(())
  }
}

/// A wrapper to quote identifiers
#[derive(Copy, Clone, Debug)]
pub struct ChSqlQuotedId<'a>(pub &'a str);

impl<'a> fmt::Display for ChSqlQuotedId<'a> {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    f.write_char('"')?;
    for c in self.0.chars() {
      if c == '"' {
        f.write_str("\"\"")?;
      } else {
        f.write_char(c)?;
      }
    }
    f.write_char('"')?;
    Ok(())
  }
}

/// A wrapper to write string literals
#[derive(Copy, Clone, Debug)]
pub struct ChSqlStrLit<'a>(pub &'a str);

impl<'a> fmt::Display for ChSqlStrLit<'a> {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    f.write_char('\'')?;
    for c in self.0.chars() {
      if c == '\'' {
        f.write_str("\'\'")?;
      } else {
        f.write_char(c)?;
      }
    }
    f.write_char('\'')?;
    Ok(())
  }
}

#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn test_create_database() {
    let actual = CreateDatabaseQuery::new("katal.test")
      .if_not_exists()
      .comment("just a test")
      .to_sql();
    let expected = r#"CREATE DATABASE IF NOT EXISTS "katal.test" COMMENT 'just a test';"#;
    assert_eq!(actual, expected);
  }

  #[test]
  fn test_grant() {
    let actual = GrantQuery::new("katal_user", Privilege::Select, GrantScope::Database("katal_db")).to_sql();
    let expected = r#"GRANT SELECT ON "katal_db".* TO "katal_user";"#;
    assert_eq!(actual, expected);
  }
}
