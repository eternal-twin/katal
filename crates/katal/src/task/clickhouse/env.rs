use crate::task::clickhouse::config::{
  ClickhouseServerAccessControlImprovements, ClickhouseServerConfig, ClickhouseServerLog, ClickhouseServerLogger,
  ClickhouseServerMetricLog, ClickhouseServerPasswordType, ClickhouseServerQueryCache, ClickhouseServerUserDirectories,
  ClickhouseUserConfig, ClickhouseUserNetwork, ClickhouseUserPassword, ClickhouseUserProfile, ClickhouseUserQuota,
  ClickhouseUsers, LoggerLevel,
};
use crate::task::clickhouse::index::{Arch, ClickhouseIndex, ClickhouseRelease, ReleaseNotFound};
use crate::task::clickhouse::sql::{
  CreateDatabaseQuery, CreateUserQuery, GrantQuery, GrantScope, OnUserConflict, Privilege, UserIdentification,
};
use crate::task::clickhouse::{ChUserName, ClickhouseVersion};
use crate::task::fs::{EnsureDir, EnsureDirError, EnsureFile, EnsureFileError, EnsureTree, EnsureTreeError};
use crate::task::systemd::config::{CommandLine, Config, Exec, Service, ServiceType, Unit};
use crate::task::systemd::{
  EnsureSystemdServiceConfig, EnsureSystemdServiceConfigError, SystemdState, SystemdUnit, SystemdUnitError,
};
use crate::task::user::{Group, UpsertGroupByNameError, UpsertUserByNameError, User};
use crate::task::{AsyncFn, TaskResult, TaskSuccess};
use async_trait::async_trait;
use asys::common::fs::{FsHost, ReadFileError};
use asys::linux::fs::{FileMode, LinuxFsHost, LinuxMetadata};
use asys::linux::user::{GroupRef, LinuxGroup, LinuxUser, LinuxUserHost, UserRef};
use asys::ExecHost;
use hex::ToHex;
use katal_loader::tree::{ReadDirError, ReadError, SubTree};
use katal_loader::tree::{TarGz, TarGzError};
use serde::{Deserialize, Serialize};
use std::fmt::Debug;
use std::path::PathBuf;

pub const SYS_GROUP: &str = "clickhouse";
pub const SYS_USER: &str = "clickhouse";

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct TargetClickhouseEnv {
  /// target version to resolve
  pub version: ClickhouseVersion,
  pub user_group: TargetClickhouseUserGroup,
  pub database_group: TargetClickhouseDatabaseGroup,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseEnv<User, Group> {
  pub sys_user: User,
  pub sys_group: Group,
  pub service: String,
  pub http_port: u16,
  pub ch_tcp_port: u16,
  pub home_dir: PathBuf,
  pub bin_dir: PathBuf,
  pub log_dir: PathBuf,
  pub state_dir: PathBuf,
  pub server_config_dir: PathBuf,
  pub server_config_path: PathBuf,
  pub server_users_config_path: PathBuf,
  pub runtime_dir: PathBuf,
  pub user_group: ClickhouseUserGroup,
  pub database_group: ClickhouseDatabaseGroup,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct TargetClickhouseUserGroup {
  pub sys_password: String,
  pub users: Vec<TargetClickhouseUser>,
  pub unmatched: ClickhouseUserStatus,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseUserGroup {
  pub databases: Vec<ClickhouseDatabase>,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct TargetClickhouseUser {
  pub name: ChUserName,
  pub status: ClickhouseUserStatus,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseUser {
  pub name: ChUserName,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub enum ClickhouseUserStatus {
  /// Ensure that the corresponding user exists
  ///
  /// If target state is provided, check that it matches
  Present(Option<TargetClickhouseUserState>),
  /// DANGEROUS! Ensure that the corresponding user does not exist, delete it if needed
  Missing,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct TargetClickhouseUserState {
  pub password: Option<String>,
  pub permissions: Option<Vec<TargetClickhousePermission>>,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct TargetClickhousePermission {
  pub database: String,
  pub write: bool,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct TargetClickhouseDatabaseGroup {
  pub databases: Vec<TargetClickhouseDatabase>,
  pub unmatched: ClickhouseDatabaseStatus,
}

impl Default for TargetClickhouseDatabaseGroup {
  fn default() -> Self {
    Self {
      databases: vec![],
      unmatched: ClickhouseDatabaseStatus::Present,
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseDatabaseGroup {
  pub databases: Vec<ClickhouseDatabase>,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct TargetClickhouseDatabase {
  pub name: String,
  pub status: ClickhouseDatabaseStatus,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct ClickhouseDatabase {
  pub name: String,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub enum ClickhouseDatabaseStatus {
  /// Ensure that the corresponding database exists
  Present,
  /// DANGEROUS! Ensure that the corresponding database does not exist, delete it if needed
  Missing,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum TargetClickhouseEnvError {
  #[error("failed to resolve release for version {0:?}")]
  ResolveRelease(#[source] ReleaseNotFound, ClickhouseVersion),
  #[error("failed to create directory")]
  EnsureDir(#[from] EnsureDirError),
  #[error("failed to fetch clickhouse-common-static package")]
  GetClickhouseCommonError(#[from] GetClickhouseCommonError),
  #[error("failed to fetch clickhouse-server package")]
  GetClickhouseServer(#[from] GetClickhouseServerError),
  #[error("failed to read package content")]
  Read(#[from] ReadError),
  #[error("failed to read package directory")]
  ReadDir(#[from] ReadDirError),
  #[error("failed to upsert system group")]
  UpsertGroupByName(#[from] UpsertGroupByNameError),
  #[error("failed to upsert system user")]
  UpsertUserByName(#[from] UpsertUserByNameError),
  #[error("failed to ensure subtree for common package")]
  EnsureCommonTree(#[source] EnsureTreeError),
  #[error("failed to ensure subtree for server package")]
  EnsureServerTree(#[source] EnsureTreeError),
  #[error("failed to ensure server config dir")]
  EnsureServerConfigDir(#[source] EnsureDirError),
  #[error("failed to ensure server runtime dir")]
  EnsureServerRuntimeDir(#[source] EnsureDirError),
  #[error("failed to ensure server state dir")]
  EnsureServerStateDir(#[source] EnsureDirError),
  #[error("failed to ensure server users config file")]
  EnsureServerUsersConfigFile(#[source] EnsureFileError),
  #[error("failed to ensure server config file")]
  EnsureServerConfigFile(#[source] EnsureFileError),
  #[error("failed to ensure server systemd service file")]
  EnsureSystemdService(#[from] EnsureSystemdServiceConfigError),
  #[error("failed to configure SystemD unit")]
  SystemdUnit(#[from] SystemdUnitError),
}

#[expect(
  clippy::field_reassign_with_default,
  reason = "it's clearer to reassign outside of the struc when there are so many fields"
)]
#[async_trait]
impl<'h, H> AsyncFn<&'h H> for TargetClickhouseEnv
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  H::Group: Debug + Send + Sync,
  H::User: Debug + Clone + Send + Sync,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<ClickhouseEnv<H::User, H::Group>, TargetClickhouseEnvError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let index = ClickhouseIndex::embedded();
    let release: &ClickhouseRelease = index
      .get_release(self.version)
      .map_err(|e| TargetClickhouseEnvError::ResolveRelease(e, self.version))?;
    let mut changed: bool = false;

    eprintln!("resolved Clickhouse version: {}", &release.version);
    let target_dir: PathBuf = PathBuf::from("/opt/clickhouse");
    let target_dir = target_dir.join(release.version.to_string());
    let ancestors = target_dir.ancestors().collect::<Vec<_>>();
    for dir in ancestors.into_iter().rev() {
      eprintln!("Create directory: {}", dir.display());
      changed = EnsureDir::new(dir.to_path_buf()).run(host).await?.changed || changed;
    }

    let witness = target_dir.join("ok");

    let witness_exists = match host.read_file(witness.as_path()) {
      Ok(_) => true,
      Err(ReadFileError::NotFound) => false,
      Err(e) => {
        panic!("failed to check witness file: {e:?}")
      }
    };

    eprintln!("Create group: {}", SYS_GROUP);
    let res = Group::upsert_by_name(SYS_GROUP).run(host).await?;
    changed = res.changed || changed;
    let sys_group: H::Group = res.output;
    eprintln!("Create user: {}", SYS_USER);
    let res = User::upsert_by_name(SYS_USER)
      .comment("clickhouse")
      .group(GroupRef::Id(sys_group.gid()))
      .clear_secondary_groups()
      .shell("/bin/bash")
      .system(true)
      .home("/opt/clickhouse")
      .run(host)
      .await?;
    let sys_user: H::User = res.output;
    changed = res.changed || changed;

    let server_config_dir = PathBuf::from("/etc/clickhouse-server");
    let server_config_path = server_config_dir.join("config.xml");
    let server_users_config_path = server_config_dir.join("users.xml");

    let env = ClickhouseEnv {
      sys_user,
      sys_group,
      service: "clickhouse-server".to_string(),
      http_port: 8123,
      ch_tcp_port: 9000,
      home_dir: target_dir.clone(),
      bin_dir: target_dir.join("bin"),
      log_dir: PathBuf::from("/var/log/clickhouse-server"),
      state_dir: PathBuf::from("/var/lib/clickhouse"),
      runtime_dir: PathBuf::from("/run/clickhouse-server"),
      server_config_dir,
      server_config_path,
      server_users_config_path,
      user_group: ClickhouseUserGroup { databases: vec![] },
      database_group: ClickhouseDatabaseGroup { databases: vec![] },
    };

    if witness_exists {
      eprintln!(
        "bailing-out, commit already downloaded (assuming the clickhouse version is already configured) (todo: better tracking)"
      );
      return Ok(TaskSuccess { changed, output: env });
    }

    eprintln!("Clickhouse will be installed");

    eprintln!("ensure previous version is stopped");

    SystemdUnit::new(&env.service)
      .state(SystemdState::Inactive)
      .run(host)
      .await?;

    eprintln!("create dir {}", env.bin_dir.display());
    changed = EnsureDir::new(env.bin_dir.clone()).run(host).await?.changed || changed;
    {
      eprintln!("Download clickhouse common");
      let pkg: TarGz = GetClickhouseCommon::release(release.clone()).run(host).await?.output;
      eprintln!("Downloaded clickhouse common, extracting");
      changed = EnsureTree::new(
        env.bin_dir.clone(),
        SubTree {
          tree: pkg,
          prefix: vec!["usr".to_string(), "bin".to_string()],
        },
      )
      .file_mode(FileMode::ALL_EXEC | FileMode::ALL_READ)
      .run(host)
      .await
      .map_err(TargetClickhouseEnvError::EnsureCommonTree)?
      .changed
        || changed;
    }
    {
      eprintln!("Download clickhouse server");
      let pkg: TarGz = GetClickhouseServer::release(release.clone()).run(host).await?.output;
      eprintln!("Downloaded clickhouse server, extracting");
      changed = EnsureTree::new(
        env.bin_dir.clone(),
        SubTree {
          tree: pkg,
          prefix: vec!["usr".to_string(), "bin".to_string()],
        },
      )
      .run(host)
      .await
      .map_err(TargetClickhouseEnvError::EnsureServerTree)?
      .changed
        || changed;
    }

    eprintln!("ensure clickhouse server config dir exists");
    changed = EnsureDir::new(env.server_config_dir.clone())
      .run(host)
      .await
      .map_err(TargetClickhouseEnvError::EnsureServerConfigDir)?
      .changed
      || changed;

    eprintln!(
      "ensure clickhouse server runtime dir exists: {}",
      env.runtime_dir.display()
    );
    changed = EnsureDir::new(env.runtime_dir.clone())
      .owner(UserRef::Id(env.sys_user.uid()))
      .group(GroupRef::Id(env.sys_group.gid()))
      .mode(FileMode::OWNER_ALL | FileMode::GROUP_READ | FileMode::GROUP_EXEC)
      .run(host)
      .await
      .map_err(TargetClickhouseEnvError::EnsureServerRuntimeDir)?
      .changed
      || changed;

    eprintln!("ensure clickhouse server state dir exists: {}", env.state_dir.display());
    changed = EnsureDir::new(env.state_dir.clone())
      .owner(UserRef::Id(env.sys_user.uid()))
      .group(GroupRef::Id(env.sys_group.gid()))
      .mode(FileMode::OWNER_ALL | FileMode::GROUP_READ | FileMode::GROUP_EXEC)
      .run(host)
      .await
      .map_err(TargetClickhouseEnvError::EnsureServerStateDir)?
      .changed
      || changed;

    eprintln!("ensure clickhouse server config file is up-to-date");
    let mut server_users = ClickhouseUsers::default();
    server_users
      .profiles
      .insert("default".to_string(), ClickhouseUserProfile::default());
    server_users.profiles.insert("readonly".to_string(), {
      let mut profile = ClickhouseUserProfile::default();
      profile.readonly = Some(true);
      profile
    });
    server_users
      .quotas
      .insert("default".to_string(), ClickhouseUserQuota::default());
    server_users.users.insert("clickhouse".to_string(), {
      let mut user = ClickhouseUserConfig::default();
      user.password = Some(ClickhouseUserPassword::Sha256({
        use sha2::Digest;
        let mut hasher = sha2::Sha256::new();
        hasher.update(self.user_group.sys_password.as_bytes());
        hasher.finalize().encode_hex()
      }));
      user.networks.push(ClickhouseUserNetwork::Ip("::1".to_string()));
      user.profile = Some("default".to_string());
      user.quota = Some("default".to_string());
      user.access_management = Some(true);
      user
    });

    changed = EnsureFile::new(env.server_users_config_path.clone())
      .content(server_users.to_string())
      .run(host)
      .await
      .map_err(TargetClickhouseEnvError::EnsureServerUsersConfigFile)?
      .changed
      || changed;

    let mut server_config = ClickhouseServerConfig::default();
    server_config.logger = Some(ClickhouseServerLogger {
      level: LoggerLevel::Information,
      log: None,
      error_log: None,
      size: 1024 * 1024 * 1024, // 1GiB
      count: 10,
      console: Some(true),
    });
    server_config.display_name = Some("production".to_string());
    server_config.http_port = Some(env.http_port);
    server_config.tcp_port = Some(env.ch_tcp_port);
    server_config.listen_host = Some("::".to_string());
    server_config.max_connections = Some(64);
    server_config.keep_alive_timeout = Some(5);
    server_config.path = Some(env.state_dir.clone());
    server_config.tmp_path = Some(env.state_dir.join("tmp"));
    server_config.user_files_path = Some(env.state_dir.join("user_files"));
    server_config.allow_plaintext_password = Some(false);
    server_config.allow_no_password = Some(false);
    server_config.allow_implicit_no_password = Some(false);
    server_config.default_password_type = Some(ClickhouseServerPasswordType::Bcrypt);
    server_config.bcrypt_workfactor = Some(12);
    server_config.user_directories = Some({
      let mut user_directories = ClickhouseServerUserDirectories::default();
      user_directories.users_xml = Some(env.server_users_config_path.clone());
      user_directories.local_directory = Some(env.state_dir.join("access"));
      user_directories
    });
    server_config.access_control_improvements = Some({
      let mut access_control_improvements = ClickhouseServerAccessControlImprovements::default();
      access_control_improvements.users_without_row_policies_can_read_rows = Some(false);
      access_control_improvements.on_cluster_queries_require_cluster_grant = Some(true);
      access_control_improvements.select_from_system_db_requires_grant = Some(true);
      access_control_improvements.select_from_information_schema_requires_grant = Some(true);
      access_control_improvements.settings_constraints_replace_previous = Some(true);
      access_control_improvements.role_cache_expiration_time_seconds = Some(600);
      access_control_improvements
    });
    server_config.default_profile = Some("default".to_string());
    server_config.default_database = Some("default".to_string());
    server_config.timezone = Some("UTC".to_string());
    server_config.umask = Some("027".to_string());
    server_config.mlock_executable = Some(true);
    server_config.remap_executable = Some(false);
    fn server_log_config(table: &str) -> ClickhouseServerLog {
      let mut log = ClickhouseServerLog::default();
      log.database = Some("system".to_string());
      log.table = Some(table.to_string());
      log.partition_by = Some("toYYYYMM(event_date)".to_string());
      log.ttl = Some("event_date + INTERVAL 30 DAY DELETE".to_string());
      log.flush_interval_milliseconds = Some(7500);
      log.max_size_rows = Some(1024 * 1024);
      log.reserved_size_rows = Some(8 * 1024);
      log.buffer_size_rows_flush_threshold = Some(512 * 1024);
      log.flush_on_crash = Some(false);
      log
    }
    server_config.query_log = Some(server_log_config("query_log"));
    server_config.trace_log = Some(server_log_config("trace_log"));
    server_config.query_thread_log = Some(server_log_config("query_thread_log"));
    server_config.query_views_log = Some({
      let mut log = ClickhouseServerLog::default();
      log.database = Some("system".to_string());
      log.table = Some("query_views_log".to_string());
      log.partition_by = Some("toYYYYMM(event_date)".to_string());
      log.ttl = Some("event_date + INTERVAL 30 DAY DELETE".to_string());
      log.flush_interval_milliseconds = Some(7500);
      log
    });
    server_config.part_log = Some(server_log_config("part_log"));
    server_config.metric_log = Some(ClickhouseServerMetricLog {
      base: server_log_config("metric_log"),
      collect_interval_milliseconds: Some(1000),
    });
    server_config.asynchronous_metric_log = Some(server_log_config("asynchronous_metric_log"));
    // server_config.opentelemetry_span_log = Some();
    server_config.crash_log = Some({
      let mut log = ClickhouseServerLog::default();
      log.database = Some("system".to_string());
      log.table = Some("crash_log".to_string());
      log.partition_by = Some("".to_string());
      log.ttl = Some("event_date + INTERVAL 30 DAY DELETE".to_string());
      log.flush_interval_milliseconds = Some(1000);
      log.max_size_rows = Some(1024);
      log.reserved_size_rows = Some(1024);
      log.buffer_size_rows_flush_threshold = Some(512);
      log.flush_on_crash = Some(true);
      log
    });
    server_config.processors_profile_log = Some(server_log_config("processors_profile_log"));
    server_config.asynchronous_insert_log = Some({
      let mut log = server_log_config("asynchronous_insert_log");
      log.database = Some("system".to_string());
      log.partition_by = Some("event_date".to_string());
      log.ttl = Some("event_date + INTERVAL 3 DAY".to_string());
      log
    });
    server_config.backup_log = Some({
      let mut log = ClickhouseServerLog::default();
      log.database = Some("system".to_string());
      log.table = Some("backup_log".to_string());
      log.partition_by = Some("toYYYYMM(event_date)".to_string());
      log.flush_interval_milliseconds = Some(7500);
      log
    });
    server_config.format_schema_path = Some(env.state_dir.join("format_schemas"));
    server_config.query_cache = Some(ClickhouseServerQueryCache {
      max_size_in_bytes: Some(1024 * 1024 * 1024),
      max_entries: Some(1024),
      max_entry_size_in_bytes: Some(1024 * 1024),
      max_entry_size_in_rows: Some(30000000),
    });

    changed = EnsureFile::new(env.server_config_path.clone())
      .content(server_config.to_string())
      .run(host)
      .await
      .map_err(TargetClickhouseEnvError::EnsureServerConfigFile)?
      .changed
      || changed;

    let server_exe = env.bin_dir.join("clickhouse-server");
    // todo: ensure executable flag is set

    let home_dir = env.home_dir.display().to_string();
    let exec_start = format!(
      "{server_exe} --config {server_config_path} --pid-file=%t/clickhouse-server/clickhouse-server.pid",
      server_exe = server_exe.display(),
      server_config_path = env.server_config_path.display(),
    );
    let mut service = Config::default();
    service.unit = Some({
      let mut unit = Unit::default();
      unit.description = Some("Clickhouse database server");
      unit.after = vec!["time-sync.target", "network-online.target"];
      unit.requires = vec!["network-online.target"];
      unit.wants = vec!["time-sync.target"];
      unit
    });
    service.service = Some({
      let mut service = Service::default();
      service.r#type = Some(ServiceType::Notify);
      service.exec = {
        let mut exec = Exec::default();
        exec.user = Some(env.sys_user.name());
        exec.group = Some(env.sys_group.name());
        exec.working_directory = Some(&home_dir);
        exec.runtime_directory = Some("clickhouse-server");
        exec.environment = vec!["CLICKHOUSE_WATCHDOG_NO_FORWARD=1"];
        exec
      };
      service.exec_start = Some(CommandLine { line: &exec_start });
      service.restart = Some("always");
      service.restart_sec = Some("30");
      service.timeout_start_sec = Some("0");
      service.timeout_stop_sec = Some("infinity");
      service
    });

    let service = EnsureSystemdServiceConfig {
      name: "clickhouse-server".to_string(),
      config: service,
    };
    changed = service.run(host).await?.changed || changed;

    eprintln!("enabling clickhouse service: {}", env.service.as_str());
    changed = SystemdUnit::new(&env.service)
      .enabled(true)
      .state(SystemdState::Active)
      .run(host)
      .await?
      .changed
      || changed;

    eprintln!("create clickhouse system client");
    let system_client = clickhouse::Client::default()
      .with_url(format!("http://localhost:{}", env.http_port))
      .with_user("clickhouse")
      .with_password(self.user_group.sys_password.as_str())
      .with_database("system");

    for db in &self.database_group.databases {
      eprintln!("create clickhouse database {}", db.name);
      CreateDatabaseQuery::new(&db.name)
        .if_not_exists()
        .execute(&system_client)
        .await
        .expect("creating ch db succeeds");
    }

    for user in &self.user_group.users {
      if let ClickhouseUserStatus::Present(Some(state)) = &user.status {
        eprintln!("create clickhouse user {}", user.name.as_str());
        CreateUserQuery::new(user.name.as_str())
          .on_conflict(OnUserConflict::Replace)
          .identify_by(UserIdentification::Sha256Password(
            state.password.as_deref().unwrap_or(""),
          ))
          .execute(&system_client)
          .await
          .expect("creating ch user succeeds");
        if let Some(permissions) = state.permissions.as_deref() {
          for perm in permissions {
            eprintln!("apply permission {perm:?}");
            let mut query = GrantQuery::new(
              user.name.as_str(),
              Privilege::Select,
              GrantScope::Database(perm.database.as_str()),
            );
            if perm.write {
              query = query
                .privilege(Privilege::Insert)
                .privilege(Privilege::Alter)
                .privilege(Privilege::Create)
                .privilege(Privilege::Drop)
                .privilege(Privilege::Truncate)
                .privilege(Privilege::Optimize)
                .privilege(Privilege::Show);
            }
            query.execute(&system_client).await.expect("creating ch user succeeds");
          }
        }
      }
    }

    changed = EnsureFile::new(witness).content([]).run(host).await.unwrap().changed || changed;

    Ok(TaskSuccess { changed, output: env })
  }
}

struct GetClickhouseCommon {
  pub release: ClickhouseRelease,
}

impl GetClickhouseCommon {
  pub const fn release(release: ClickhouseRelease) -> Self {
    Self { release }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum GetClickhouseCommonError {
  #[error("no package `clickhouse-common-static` found")]
  PackageNotFound,
  #[error("no dist found for `amd64` arch")]
  DistNotFound,
  #[error("failed to submit download request: {0:?}")]
  Request(String),
  #[error("failed to get download response: {0:?}")]
  Response(String),
  #[error("failed to read .tgz content")]
  TarGz(#[from] TarGzError),
  #[error("failed to download archive: status_code={0:?}")]
  Download(u16),
}

#[async_trait]
impl<'h, H: ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for GetClickhouseCommon
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<TarGz, GetClickhouseCommonError>;

  async fn run(&self, _host: &'h H) -> Self::Output {
    let client = reqwest::Client::builder()
      .user_agent("katal")
      .build()
      .expect("building the client always succeeds");
    let url = self
      .release
      .common_static
      .as_ref()
      .ok_or(GetClickhouseCommonError::PackageNotFound)?
      .download_url(Arch::Amd64)
      .ok_or(GetClickhouseCommonError::DistNotFound)?;
    let res = client
      .get(url.clone())
      .send()
      .await
      .map_err(|e| GetClickhouseCommonError::Request(e.to_string()))?;
    if res.status().is_success() {
      let archive = res
        .bytes()
        .await
        .map_err(|e| GetClickhouseCommonError::Response(e.to_string()))?;
      let tree = TarGz::new(&archive)?;
      Ok(TaskSuccess {
        changed: false,
        output: tree,
      })
    } else {
      Err(GetClickhouseCommonError::Download(res.status().as_u16()))
    }
  }
}

pub struct GetClickhouseServer {
  pub release: ClickhouseRelease,
}

impl GetClickhouseServer {
  pub const fn release(release: ClickhouseRelease) -> Self {
    Self { release }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum GetClickhouseServerError {
  #[error("no package `clickhouse-server` found")]
  PackageNotFound,
  #[error("no dist found for `amd64` arch")]
  DistNotFound,
  #[error("failed to submit download request: {0:?}")]
  Request(String),
  #[error("failed to get download response: {0:?}")]
  Response(String),
  #[error("failed to read .tgz content")]
  TarGz(#[from] TarGzError),
  #[error("failed to download archive: status_code={0:?}")]
  Download(u16),
}

#[async_trait]
impl<'h, H: ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for GetClickhouseServer
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<TarGz, GetClickhouseServerError>;

  async fn run(&self, _host: &'h H) -> Self::Output {
    let client = reqwest::Client::builder()
      .user_agent("katal")
      .build()
      .expect("building the client always succeeds");
    let url = self
      .release
      .server
      .as_ref()
      .ok_or(GetClickhouseServerError::PackageNotFound)?
      .download_url(Arch::Amd64)
      .ok_or(GetClickhouseServerError::DistNotFound)?;
    let res = client
      .get(url.clone())
      .send()
      .await
      .map_err(|e| GetClickhouseServerError::Request(e.to_string()))?;
    if res.status().is_success() {
      let archive = res
        .bytes()
        .await
        .map_err(|e| GetClickhouseServerError::Response(e.to_string()))?;
      let tree = TarGz::new(&archive)?;
      Ok(TaskSuccess {
        changed: false,
        output: tree,
      })
    } else {
      Err(GetClickhouseServerError::Download(res.status().as_u16()))
    }
  }
}
