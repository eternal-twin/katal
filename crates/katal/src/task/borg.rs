use crate::task::borgmatic::BorgRepository;
use crate::task::{AsyncFn, NamedTask, TaskName, TaskSuccess};
use async_trait::async_trait;
use asys::{Command, ExecHost};
use chrono::Utc;
use serde::{Deserialize, Serialize};
use std::path::PathBuf;

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct InitBorgRepo {
  pub password: String,
  pub priv_key: PathBuf,
  pub repo_path: BorgRepository,
}

#[async_trait]
impl<'h, H: ExecHost> AsyncFn<&'h H> for InitBorgRepo {
  type Output = Result<TaskSuccess<()>, ()>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let mut cmd = Command::new("borg").arg("init");
    cmd = cmd.arg("--encryption").arg("repokey-blake2");
    cmd = cmd
      .arg("--rsh")
      .arg(format!("ssh -i {}", self.priv_key.to_str().unwrap()));
    cmd = cmd.arg(&self.repo_path.0);
    cmd = cmd.stdin(format!("{pass}\n{pass}\nN\n", pass = &self.password));
    host.exec(&cmd).unwrap();

    Ok(TaskSuccess {
      changed: true,
      output: (),
    })
  }
}

impl<'h, H> NamedTask<&'h H> for InitBorgRepo
where
  H: 'h + ExecHost,
{
  const NAME: TaskName = TaskName::new("InitBorgRepo");
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct CreateBorgArchive {
  pub password: String,
  pub priv_key: PathBuf,
  pub repo_path: BorgRepository,
  pub archive_name: String,
  pub dir: PathBuf,
}

impl CreateBorgArchive {
  pub fn new(
    password: String,
    priv_key: PathBuf,
    repo_path: BorgRepository,
    dir: PathBuf,
    archive_name: String,
  ) -> Self {
    Self {
      password,
      priv_key,
      repo_path,
      archive_name,
      dir,
    }
  }

  pub fn new_from_time(password: String, priv_key: PathBuf, repo_path: BorgRepository, dir: PathBuf) -> Self {
    const ARCHIVE_FORMAT: &str = "%Y-%m-%d-%H-%M-%S";
    let archive_name = Utc::now().format(ARCHIVE_FORMAT).to_string();
    Self {
      password,
      priv_key,
      repo_path,
      dir,
      archive_name,
    }
  }
}

#[async_trait]
impl<'h, H: ExecHost> AsyncFn<&'h H> for CreateBorgArchive {
  type Output = Result<TaskSuccess<()>, ()>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let mut cmd = Command::new("borg").arg("create");
    cmd = cmd.current_dir(self.dir.clone().to_str().unwrap());
    cmd = cmd.env("BORG_PASSPHRASE", self.password.clone());
    cmd = cmd.env("SSH_ASKPASS_REQUIRE", "never");
    cmd = cmd
      .arg("--rsh")
      .arg(format!("ssh -i {}", self.priv_key.to_str().unwrap()));
    cmd = cmd.arg(format!("{}::{}", &self.repo_path.0, &self.archive_name));
    cmd = cmd.arg(".");
    // cmd = cmd.stdin(format!("{pass}\n{pass}\nN\n", pass = &self.password));
    host.exec(&cmd).unwrap();

    Ok(TaskSuccess {
      changed: true,
      output: (),
    })
  }
}

impl<'h, H> NamedTask<&'h H> for CreateBorgArchive
where
  H: 'h + ExecHost,
{
  const NAME: TaskName = TaskName::new("CreateBorgArchive");
}
