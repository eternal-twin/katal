use crate::ipc_sudo::BoundSudoCommand;
use crate::net_ipc::NetIpcServer;
use crate::temp_exe::TempExe;
use async_trait::async_trait;
use auto_impl::auto_impl;
use std::ffi::OsStr;
use std::path::PathBuf;

#[async_trait]
#[auto_impl(&mut)]
pub trait IpcAskpassOnPrompt {
  async fn on_prompt(&mut self, prompt: Option<String>) -> Result<String, ()>;
}

/// Dummy `IpcAskpass` implementation that fails if a password is prompted.
/// Use this when you must ensure a passwordless behavior.
pub struct IpcAskpassOnPromptNone;

#[async_trait]
impl IpcAskpassOnPrompt for IpcAskpassOnPromptNone {
  async fn on_prompt(&mut self, _prompt: Option<String>) -> Result<String, ()> {
    Err(())
  }
}

/// Dummy `IpcAskpass` implementation returning a constant string password.
pub struct IpcAskpassOnPromptString {
  password: String,
}

impl IpcAskpassOnPromptString {
  pub fn new(password: impl AsRef<str>) -> Self {
    Self {
      password: password.as_ref().to_string(),
    }
  }
}

#[async_trait]
impl IpcAskpassOnPrompt for IpcAskpassOnPromptString {
  async fn on_prompt(&mut self, _prompt: Option<String>) -> Result<String, ()> {
    Ok(self.password.clone())
  }
}

#[async_trait]
pub trait IpcAskpassHandler {
  type Output;

  async fn handle<'env>(self, env: AskpassEnv<'env>) -> Self::Output;
}

pub async fn with_ipc_askpass<OnPrompt, Handler>(mut on_prompt: OnPrompt, handler: Handler) -> Handler::Output
where
  OnPrompt: IpcAskpassOnPrompt,
  Handler: IpcAskpassHandler,
{
  let (done_tx, done_rx) = tokio::sync::oneshot::channel::<()>();
  let (ipc_server, ipc_server_name) = NetIpcServer::<Option<String>>::new().await.unwrap();

  let server_loop = async move {
    loop {
      let (mut rx, mut tx) = ipc_server.accept::<Option<String>>().await.unwrap();
      let prompt = rx.recv().await.unwrap().unwrap();
      let pass = on_prompt.on_prompt(prompt).await;
      tx.send(&Some(pass.unwrap())).await.unwrap();
    }
  };

  let server = async move {
    tokio::select! {
      _ = done_rx => (),
      _ = server_loop => (),
    }
  };

  let exe = TempExe::builder(std::env::current_exe().unwrap().to_str().unwrap().to_string())
    .arg(String::from("askpass"))
    .arg(ipc_server_name)
    .file_name(String::from("ipc_askpass"))
    .build()
    .unwrap();

  let main = async move {
    let askpass_path = exe.path();
    let env = AskpassEnv { askpass_path };
    let res = handler.handle(env).await;
    let send_shutdown_res = done_tx.send(());
    debug_assert!(send_shutdown_res.is_ok());
    res
  };

  let out = tokio::join!(main, server);

  out.0
}

#[derive(Debug, Copy, Clone)]
pub struct AskpassEnv<'a> {
  askpass_path: &'a PathBuf,
}

impl<'a> AskpassEnv<'a> {
  pub fn path(&self) -> &'a PathBuf {
    self.askpass_path
  }

  pub fn sudo(&self, program: impl AsRef<OsStr>) -> BoundSudoCommand<'a> {
    BoundSudoCommand::new(*self, program)
  }
}
