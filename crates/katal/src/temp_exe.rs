use std::fs;
use std::path::PathBuf;
use tempfile::TempDir;

pub struct TempExe {
  // This field is never used but must be kept here to ensure that the directory
  // is not removed until `TempExe` is dropped.
  #[allow(dead_code)]
  dir: TempDir,
  exe_path: PathBuf,
}

impl TempExe {
  pub fn builder(program: String) -> TempExeBuilder {
    TempExeBuilder {
      file_name: String::from("temp_exe"),
      program,
      args: Vec::new(),
      locked_args: false,
      env_kind: TempExeEnv::Inherit,
    }
  }

  pub fn path(&self) -> &PathBuf {
    &self.exe_path
  }
}

pub enum TempExeEnv {
  /// Inherit environment from the parent process who will spawn the executable.
  /// (Default behavior when spawning a process.)
  /// Then apply custom changes.
  Inherit,
  /// Only environment variables passed to the `TempExeBuilder` will be available.
  Clear,
}

pub struct TempExeBuilder {
  file_name: String,
  program: String,
  args: Vec<String>,
  /// Prevent the caller from passing extra arguments following the ones
  /// defined in the builder.
  locked_args: bool,
  // env: HashMap<String, String>,
  env_kind: TempExeEnv,
}

impl TempExeBuilder {
  pub fn get_shell_script(&self) -> String {
    // We assume that `/bin/sh` contains a POSIX shell, even if it is not spec
    // compliant:
    // > Applications should note that the standard PATH to the shell cannot be assumed to be either
    // > **/bin/sh** or **/usr/bin/sh**, and should be determined by interrogation of the PATH
    // > returned by getconf PATH, ensuring that the returned pathname is an absolute pathname and
    // > not a shell built-in.
    // https://pubs.opengroup.org/onlinepubs/9699919799/utilities/sh.html#tag_20_117_16
    // The right way is to check the `getconf PATH` locations. The regular `PATH` environment
    // variable may be modified by the user, hence the requirement to go through `getconf` to
    // get the actual system value. Dealing with this properly is not the priority right now
    // but a fix is welcome. The main difficulty is that it makes this function system-dependent.
    // https://github.com/harryfei/which-rs/issues/38
    self.inner_get_shell_script("/bin/sh", "/usr/bin/env")
  }

  fn inner_get_shell_script(&self, sh_abs_path: &str, env_abs_path: &str) -> String {
    let mut script = String::from("#!");
    script.push_str(&shlex::try_quote(sh_abs_path).expect("invalid sh_abs_path"));
    script.push('\n');

    // Use either `env -i` or `exec`
    // We would prefer to use `exec -c` instead of `env -i` since `exec` is a builtin, but
    // the `-c` flag is a bash extension.
    // https://pubs.opengroup.org/onlinepubs/9699919799/utilities/env.html#tag_20_39
    // https://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html#tag_18_20
    match self.env_kind {
      TempExeEnv::Clear => {
        script.push_str(&shlex::try_quote(env_abs_path).expect("invalid env_abs_path"));
        script.push_str(" -i ");
      }
      TempExeEnv::Inherit => script.push_str("exec "),
    }
    script.push_str(&shlex::try_quote(&self.program).expect("invalid self.program"));
    for arg in self.args.iter() {
      script.push(' ');
      script.push_str(&shlex::try_quote(arg).expect("invalid arg"));
    }
    if !self.locked_args {
      // https://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html#tag_18_05_02
      script.push_str(" \"$@\"");
    }
    script.push('\n');
    script
  }

  pub fn build(self) -> Result<TempExe, anyhow::Error> {
    use ::std::os::unix::fs::PermissionsExt;

    let dir = tempfile::tempdir()?;
    let exe_path = dir.path().join(&self.file_name);
    let script = self.get_shell_script();
    fs::write(&exe_path, script)?;
    let exe_meta = exe_path.metadata()?;
    let mut permissions = exe_meta.permissions();
    permissions.set_mode(0o700);
    fs::set_permissions(&exe_path, permissions)?;
    Ok(TempExe { dir, exe_path })
  }

  pub fn arg(mut self, arg: String) -> Self {
    self.args.push(arg);
    self
  }

  pub fn file_name(mut self, file_name: String) -> Self {
    self.file_name = file_name;
    self
  }
}
