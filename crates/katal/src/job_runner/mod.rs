pub mod opaque_data;

use crate::job_runner::opaque_data::{BincodeBuf, WritableOpaqueData};
use crate::task::NamedTask;
use async_trait::async_trait;
use asys::Host;
use serde::{Deserialize, Serialize};
use thiserror::Error;
use tokio::io::{AsyncBufReadExt, AsyncWriteExt, BufReader, Lines, Stdin, Stdout};
use tokio::process::ChildStdout;

/// Trait to send messages from the client (local) to the server (remote).
#[async_trait]
pub trait JobServerWriter {
  async fn start_job<'h, H: 'h + Host + ?Sized, T>(&mut self, job_id: u64, input: &T)
  where
    T: NamedTask<&'h H>,
    T: Serialize + Sync;

  async fn request_end(&mut self, job_id: u64, output: BincodeBuf);
}

#[async_trait]
pub trait JobClientReader {
  /// Get the next message sent by the client
  async fn next_client_message(&mut self) -> Result<Option<JobClientMessage<BincodeBuf>>, JobClientReadError>;
}

#[async_trait]
pub trait JobClientWriter {
  async fn job_complete(&mut self, job_id: u64, output: BincodeBuf);

  async fn start_job<'h, H: 'h + Host + ?Sized, T>(&mut self, job_id: u64, input: &T) -> Result<(), String>
  where
    T: NamedTask<&'h H>,
    T: Serialize + Sync;
}

/// Trait to receive messages sent from the server (remote) to the client (local).
#[async_trait]
pub trait JobServerReader {
  /// Get the next message sent by the server
  async fn next_server_message(&mut self) -> Result<Option<JobServerMessage<BincodeBuf>>, JobServerReadError>;
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct StartJob<Opaque> {
  pub job_id: u64,
  pub task: String,
  pub input: Opaque,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum JobServerMessage<Opaque> {
  RequestStart(StartJob<Opaque>),
  NotifyEnd(JobComplete<Opaque>),
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct JobComplete<Opaque> {
  pub job_id: u64,
  pub output: Opaque,
}

pub struct StdinJobClientReader {
  lines: Lines<BufReader<Stdin>>,
}

impl Default for StdinJobClientReader {
  fn default() -> Self {
    Self::new()
  }
}

impl StdinJobClientReader {
  pub fn new() -> Self {
    let stdin = tokio::io::stdin();
    let stdin = BufReader::new(stdin);
    let lines = stdin.lines();
    Self { lines }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize, Error)]
pub enum JobClientReadError {
  #[error("input/output error while reading job client message: {0}")]
  Io(String),
  #[error("received malformed data while reading job client message: {0}")]
  Format(String),
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum JobClientMessage<Opaque> {
  RequestStart(StartJob<Opaque>),
  NotifyEnd(JobComplete<Opaque>),
}

#[async_trait]
impl JobClientReader for StdinJobClientReader {
  async fn next_client_message(&mut self) -> Result<Option<JobClientMessage<BincodeBuf>>, JobClientReadError> {
    let line = self.lines.next_line().await;
    let line = line.map_err(|e| JobClientReadError::Io(e.to_string()))?;
    match line {
      Some(line) => {
        let job: JobClientMessage<BincodeBuf> =
          serde_json::from_str(&line).map_err(|e| JobClientReadError::Format(e.to_string()))?;
        Ok(Some(job))
      }
      None => Ok(None),
    }
  }
}

pub struct StdoutJobClientWriter {
  stdout: Stdout,
}

impl Default for StdoutJobClientWriter {
  fn default() -> Self {
    Self::new()
  }
}

impl StdoutJobClientWriter {
  pub fn new() -> Self {
    let stdout = tokio::io::stdout();
    Self { stdout }
  }
}

#[async_trait]
impl JobClientWriter for StdoutJobClientWriter {
  async fn job_complete(&mut self, job_id: u64, output: BincodeBuf) {
    let payload = JobServerMessage::NotifyEnd(JobComplete { job_id, output });
    let mut payload = serde_json::to_vec(&payload).unwrap();
    payload.push(b'\n');
    self.stdout.write_all(&payload).await.unwrap()
  }

  async fn start_job<'h, H: 'h + Host + ?Sized, T>(&mut self, job_id: u64, input: &T) -> Result<(), String>
  where
    T: NamedTask<&'h H>,
    T: Serialize + Sync,
  {
    let payload = JobServerMessage::RequestStart(StartJob {
      job_id,
      task: T::NAME.as_str().to_string(),
      input: BincodeBuf::try_write(&input).map_err(|e| format!("{e:?}"))?,
    });
    let mut payload = serde_json::to_vec(&payload).unwrap();
    payload.push(b'\n');
    self.stdout.write_all(&payload).await.map_err(|e| e.to_string())
  }
}

pub struct ChildStdoutJobServerReader {
  lines: Lines<BufReader<ChildStdout>>,
}

impl ChildStdoutJobServerReader {
  pub fn new(stdout: ChildStdout) -> Self {
    let stdout = BufReader::new(stdout);
    let lines = stdout.lines();
    Self { lines }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize, Error)]
pub enum JobServerReadError {
  #[error("input/output error while reading job server message: {0}")]
  Io(String),
  #[error("received malformed data while reading job server message: {0}")]
  Format(String),
}

#[async_trait]
impl JobServerReader for ChildStdoutJobServerReader {
  async fn next_server_message(&mut self) -> Result<Option<JobServerMessage<BincodeBuf>>, JobServerReadError> {
    let line = self.lines.next_line().await;
    let line = line.map_err(|e| JobServerReadError::Io(e.to_string()))?;
    match line {
      Some(line) => {
        let job: JobServerMessage<BincodeBuf> =
          serde_json::from_str(&line).map_err(|e| JobServerReadError::Format(e.to_string()))?;
        Ok(Some(job))
      }
      None => Ok(None),
    }
  }
}
