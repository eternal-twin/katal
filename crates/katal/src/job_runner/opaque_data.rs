use serde::{Deserialize, Serialize};
use std::fmt::Debug;

pub trait WritableOpaqueData: Serialize {
  type Error: Debug;

  fn try_write<T: Serialize>(value: &T) -> Result<Self, Self::Error>
  where
    Self: Sized;
}

pub trait ReadableOpaqueData: for<'de> Deserialize<'de> {
  type Error: Debug;

  fn try_read<'de, T: Deserialize<'de>>(&'de self) -> Result<T, Self::Error>;
}

/// Represents opaque serialized bincode data.
///
/// This is used as a workaround to missing `Box<dyn Deserialize>` by serde, we
/// instead use a concrete representation for opaque data inside another
/// serialized datastructure.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct BincodeBuf(Vec<u8>);

impl BincodeBuf {
  pub fn as_slice(&self) -> &[u8] {
    self.0.as_slice()
  }
}

impl WritableOpaqueData for BincodeBuf {
  type Error = bincode::Error;

  fn try_write<T: Serialize>(value: &T) -> Result<Self, bincode::Error> {
    Ok(Self(bincode::serialize(value)?))
  }
}

impl ReadableOpaqueData for BincodeBuf {
  type Error = bincode::Error;

  fn try_read<'de, T: Deserialize<'de>>(&'de self) -> Result<T, bincode::Error> {
    bincode::deserialize(&self.0)
  }
}
