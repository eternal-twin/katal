use crate::ipc_askpass::{
  with_ipc_askpass, AskpassEnv, IpcAskpassHandler, IpcAskpassOnPrompt, IpcAskpassOnPromptNone, IpcAskpassOnPromptString,
};
use async_trait::async_trait;
use asys::linux::user::UserRef;
use std::ffi::{OsStr, OsString};
use std::future::Future;
use std::io;
use std::process::Stdio;
use tokio::process::Command;

pub struct SudoCommand {
  /// Path to the `sudo` program
  ///
  /// If `sudo_path` is not an absolute path, the `PATH` will be searched.
  /// If `sudo_path` is `None`, it defaults to searching `sudo` in the `PATH`.
  sudo_program: OsString,
  /// Handler for password prompts
  sudo_password: Box<dyn IpcAskpassOnPrompt + Send>,
  /// User to use to run the inner command, defaults to `UserRef::root()`.
  user: UserRef,
  /// Path to the program to run through `sudo`
  program: OsString,
  /// Arguments to pass to inner program.
  args: Vec<OsString>,
  stdin: Option<Stdio>,
  stdout: Option<Stdio>,
  stderr: Option<Stdio>,
}

#[async_trait]
pub trait IpcSudoChildHandler {
  type Output: Send;

  async fn handle<'a>(self, child: io::Result<&'a mut tokio::process::Child>) -> Self::Output;
}

impl SudoCommand {
  pub fn new(program: impl AsRef<OsStr>) -> Self {
    Self {
      sudo_program: OsString::from("sudo"),
      sudo_password: Box::new(IpcAskpassOnPromptNone),
      user: UserRef::ROOT,
      program: program.as_ref().to_os_string(),
      args: Vec::new(),
      stdin: None,
      stdout: None,
      stderr: None,
    }
  }

  pub fn user(&mut self, user: UserRef) -> &mut Self {
    self.user = user;
    self
  }

  pub fn password(&mut self, password: impl AsRef<str>) -> &mut Self {
    self.sudo_password = Box::new(IpcAskpassOnPromptString::new(password));
    self
  }

  pub fn arg(&mut self, a: impl AsRef<OsStr>) -> &mut Self {
    self.args.push(a.as_ref().to_os_string());
    self
  }

  pub fn stdin(&mut self, cfg: impl Into<Stdio>) -> &mut Self {
    self.stdin = Some(cfg.into());
    self
  }

  pub fn stdout(&mut self, cfg: impl Into<Stdio>) -> &mut Self {
    self.stdout = Some(cfg.into());
    self
  }

  pub fn stderr(&mut self, cfg: impl Into<Stdio>) -> &mut Self {
    self.stderr = Some(cfg.into());
    self
  }

  pub fn output(&mut self) -> impl Future<Output = io::Result<std::process::Output>> + '_ {
    struct AskpassHandler<'cmd> {
      sudo_program: &'cmd OsString,
      user: &'cmd UserRef,
      program: &'cmd OsString,
      args: &'cmd Vec<OsString>,
    }

    #[async_trait]
    impl<'cmd> IpcAskpassHandler for AskpassHandler<'cmd> {
      type Output = io::Result<std::process::Output>;

      async fn handle<'a>(self, askpass: AskpassEnv<'a>) -> Self::Output {
        let mut cmd = Command::new(self.sudo_program);
        cmd
          .kill_on_drop(true)
          .env("SUDO_ASKPASS", askpass.path())
          .arg("-A")
          .arg("--user")
          .arg(match self.user {
            UserRef::Id(uid) => format!("#{}", uid),
            UserRef::Name(name) => name.to_string(),
          })
          .arg("--")
          .arg(self.program);

        for arg in self.args.iter() {
          cmd.arg(arg);
        }

        cmd.output().await
      }
    }

    let handler = AskpassHandler {
      sudo_program: &self.sudo_program,
      user: &self.user,
      program: &self.program,
      args: &self.args,
    };

    with_ipc_askpass(self.sudo_password.as_mut(), handler)
  }

  // TODO: Future-aware pipes

  pub async fn with_child<Handler: IpcSudoChildHandler + Send>(&mut self, handler: Handler) -> Handler::Output {
    struct AskpassHandler<'cmd, Inner: IpcSudoChildHandler + Send> {
      inner: Inner,
      sudo_program: &'cmd OsString,
      user: &'cmd UserRef,
      program: &'cmd OsString,
      args: &'cmd Vec<OsString>,
      stdin: &'cmd mut Option<Stdio>,
      stdout: &'cmd mut Option<Stdio>,
      stderr: &'cmd mut Option<Stdio>,
    }

    #[async_trait]
    impl<'cmd, Inner: IpcSudoChildHandler + Send> IpcAskpassHandler for AskpassHandler<'cmd, Inner> {
      type Output = Inner::Output;

      async fn handle<'a>(self, askpass: AskpassEnv<'a>) -> Self::Output {
        let mut cmd = Command::new(self.sudo_program);
        cmd
          .kill_on_drop(true)
          .env("SUDO_ASKPASS", askpass.path())
          .arg("-A")
          .arg("--user")
          .arg(match self.user {
            UserRef::Id(uid) => format!("#{}", uid),
            UserRef::Name(name) => name.to_string(),
          })
          .arg("--")
          .arg(self.program);

        for arg in self.args.iter() {
          cmd.arg(arg);
        }

        // TODO: Move stdio back into the command after the execution is complete (or reuse std command)
        if let Some(cfg) = self.stdin.take() {
          cmd.stdin(cfg);
        }
        if let Some(cfg) = self.stdout.take() {
          cmd.stdout(cfg);
        }
        if let Some(cfg) = self.stderr.take() {
          cmd.stderr(cfg);
        }

        match cmd.spawn() {
          Ok(mut c) => {
            let r = self.inner.handle(Ok(&mut c)).await;
            c.wait().await.unwrap();
            r
          }
          Err(e) => self.inner.handle(Err(e)).await,
        }
      }
    }

    let handler = AskpassHandler {
      inner: handler,
      sudo_program: &self.sudo_program,
      user: &self.user,
      program: &self.program,
      args: &self.args,
      stdin: &mut self.stdin,
      stdout: &mut self.stdout,
      stderr: &mut self.stderr,
    };

    with_ipc_askpass(self.sudo_password.as_mut(), handler).await
  }
}

#[async_trait]
pub trait IpcSudoHandler {
  type Output;

  async fn handle<'a>(self, sudo_env: SudoEnv<'a>) -> Self::Output;
}

pub async fn with_ipc_sudo<OnPrompt, Handler>(on_prompt: OnPrompt, handler: Handler) -> Handler::Output
where
  OnPrompt: IpcAskpassOnPrompt,
  Handler: IpcSudoHandler + Send,
{
  struct AskpassHandler<Handler: IpcSudoHandler + Send> {
    inner: Handler,
  }

  #[async_trait]
  impl<Handler: IpcSudoHandler + Send> IpcAskpassHandler for AskpassHandler<Handler> {
    type Output = Handler::Output;

    async fn handle<'a>(self, askpass: AskpassEnv<'a>) -> Self::Output {
      let sudo_env = SudoEnv { askpass };
      self.inner.handle(sudo_env).await
    }
  }

  with_ipc_askpass(on_prompt, AskpassHandler { inner: handler }).await
}

#[derive(Debug, Copy, Clone)]
pub struct SudoEnv<'askpass> {
  askpass: AskpassEnv<'askpass>,
}

impl<'askpass> SudoEnv<'askpass> {
  // pub(crate) fn new(askpass: AskpassEnv<'askpass>) -> Self {
  //   Self { askpass }
  // }

  pub fn program(&self, program: impl AsRef<OsStr>) -> BoundSudoCommand<'askpass> {
    BoundSudoCommand::new(self.askpass, program)
  }
}

pub struct BoundSudoCommand<'askpass> {
  askpass: AskpassEnv<'askpass>,
  user: UserRef,
  program: OsString,
  args: Vec<OsString>,
}

impl<'askpass> BoundSudoCommand<'askpass> {
  pub(crate) fn new(askpass: AskpassEnv<'askpass>, program: impl AsRef<OsStr>) -> Self {
    Self {
      askpass,
      user: UserRef::ROOT,
      program: OsString::from(program.as_ref()),
      args: Vec::new(),
    }
  }

  pub fn arg(&mut self, a: impl AsRef<OsStr>) -> &mut Self {
    self.args.push(a.as_ref().to_os_string());
    self
  }

  pub fn output(&mut self) -> impl Future<Output = io::Result<std::process::Output>> {
    let mut cmd = Command::new("sudo");
    cmd
      .kill_on_drop(true)
      .env("SUDO_ASKPASS", self.askpass.path())
      .arg("-A")
      .arg("--user")
      .arg(match &self.user {
        UserRef::Id(uid) => format!("#{}", uid),
        UserRef::Name(name) => name.to_string(),
      })
      .arg("--")
      .arg(&self.program);

    for arg in self.args.iter() {
      cmd.arg(arg);
    }

    cmd.output()
  }

  //   let cmd = cmd
  //     .env_clear()
  //     .env("SSH_ASKPASS", bound_askpass.path())
  //     .env("SSH_ASKPASS_REQUIRE", "force")
  //     // .arg("-A")
  //     // .arg("--user")
  //     // .arg(match user { UserRef::Id(uid) => format!("#{}", uid), UserRef::Name(name) => name })
  //     // .arg("--")
  //     // .arg(std::env::current_exe()?)
  //     // .arg("ipc-agent")
  //     // .arg("touch")
  //     .arg("demurgos@demurgos.net")
  //     .kill_on_drop(false);
}
