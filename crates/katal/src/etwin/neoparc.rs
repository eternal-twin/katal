use crate::etwin::channel::{
  ChannelDomain, ChannelFallbackServer, ChannelPaths, ChannelPostgres, ChannelTarget, ChannelTargetError, NodeEnv,
  PostgresCredentials,
};
use crate::task::fs::{
  EnsureDir, EnsureDirError, EnsureDirSymlink, EnsureDirSymlinkError, EnsureFile, EnsureFileError,
};
use crate::task::git::{GitCheckout, GitCheckoutError};
use crate::task::java::JavaEnv;
use crate::task::nginx::{NginxAvailableSite, NginxAvailableSiteError, NginxEnableSite, NginxEnableSiteError};
use crate::task::systemd::{
  EnsureSystemdServiceConfig, EnsureSystemdServiceConfigError, SystemdState, SystemdUnit, SystemdUnitError,
};
use crate::task::witness::{FsWitness, FsWitnessError};
use crate::task::yarn::YarnEnv;
use crate::task::{exec_as, AsyncFn, ExecAsError, NamedTask, TaskName, TaskRef, TaskResult, TaskSuccess};
use async_trait::async_trait;
use asys::common::fs::FsHost;
use asys::linux::fs::{FileMode, LinuxFsHost, LinuxMetadata, ResolveError};
use asys::linux::user::{GroupRef, LinuxUser, LinuxUserHost, UserRef};
use asys::local_linux::LocalLinux;
use asys::{Command, ExecError, ExecHost};
use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};
use std::fmt;
use std::fmt::Debug;
use std::path::PathBuf;
use url::Url;

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct NeoparcReleaseTarget<S: AsRef<str> = String> {
  pub channel: NeoparcChannelTarget,
  pub repository: S,
  pub git_ref: String,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct NeoparcChannelTarget<S: AsRef<str> = String> {
  pub inner: ChannelTarget<S>,
  pub vault: NeoparcVault,
  pub etwin_uri: String,
  pub etwin_client_id: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct NeoparcVault {
  pub secret: Option<String>,
  pub discord_token: Option<String>,
  pub salt: String,
  pub seed: String,
  pub test: String,
  pub etwin_client_secret: String,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct NeoparcChannel<User: LinuxUser, S: AsRef<str> = String> {
  full_name: S,
  short_name: S,
  user: User,
  domain: ChannelDomain<S>,
  paths: ChannelPaths,
  fallback_server: ChannelFallbackServer,
  postgres: ChannelPostgres,
  main_port: u16,
  java: JavaEnv,
  node: NodeEnv,
  yarn: YarnEnv,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct NeoparcRelease<User: LinuxUser, S: AsRef<str> = String> {
  release_dir: PathBuf,
  repository: S,
  repo_dir: PathBuf,
  channel: NeoparcChannel<User, S>,
  git_ref: String,
}

#[derive(Serialize, Deserialize)]
pub struct DeployNeoparc<S: AsRef<str> = String> {
  pub pass: S,
  pub target: NeoparcReleaseTarget,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum DeployNeoparcError {
  #[error("failed to exec neoparc deployment task")]
  Exec(#[source] ExecAsError),
  #[error("failed to deploy neoparc release")]
  Deploy(#[source] DeployNeoparcAsRootError),
}

#[async_trait]
impl<'h, H: ExecHost, S: AsRef<str> + Send + Sync> AsyncFn<&'h H> for DeployNeoparc<S> {
  type Output = TaskResult<(), DeployNeoparcError>;

  async fn run(&self, _host: &'h H) -> Self::Output {
    let input = DeployNeoparcAsRoot {
      target: self.target.clone(),
    };
    exec_as::<LocalLinux, DeployNeoparcAsRoot>(UserRef::ROOT, Some(self.pass.as_ref()), &input)
      .await
      .map_err(DeployNeoparcError::Exec)?
      .map_err(DeployNeoparcError::Deploy)
  }
}

impl<'h, H: 'h + ExecHost> NamedTask<&'h H> for DeployNeoparc {
  const NAME: TaskName = TaskName::new("neoparc::DeployNeoparc");
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DeployNeoparcAsRoot {
  target: NeoparcReleaseTarget,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum DeployNeoparcAsRootError {
  #[error("failed to use witness file")]
  Witness(#[source] FsWitnessError),
  #[error("failed to run task `NeoparcReleaseTarget`")]
  Task(#[source] NeoparcReleaseTargetError),
}

#[async_trait]
impl<'h, H: ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for DeployNeoparcAsRoot
where
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Debug + Clone + Send + Sync + Serialize + DeserializeOwned,
  H::Group: Debug + Send + Sync,
{
  type Output = TaskResult<(), DeployNeoparcAsRootError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let paths: ChannelPaths = self.target.channel.inner.paths();
    let witness = paths.home.join("neoparc_release.witness");
    let mut changed = false;
    let res = FsWitness::new(witness, TaskRef(&self.target))
      .run(host)
      .await
      .map_err(DeployNeoparcAsRootError::Witness)?;
    changed = res.changed || changed;
    let res = res.output.map_err(DeployNeoparcAsRootError::Task)?;
    changed = res.changed || changed;
    Ok(TaskSuccess { changed, output: () })
  }
}

impl<'h, H> NamedTask<&'h H> for DeployNeoparcAsRoot
where
  H: 'h + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Debug + Clone + Send + Sync + Serialize + DeserializeOwned,
  H::Group: Debug + Send + Sync,
{
  const NAME: TaskName = TaskName::new("neoparc::DeployNeoparcAsRoot");
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum NeoparcReleaseTargetError {
  #[error("failed to initialize neoparc channel")]
  Channel(#[source] NeoparcChannelTargetError),
  #[error("failed to ensure release directory")]
  ReleaseDir(#[source] EnsureDirError),
  #[error("failed to execute build task")]
  ExecBuild(#[source] ExecAsError),
  #[error("failed to build neoparc release")]
  Build(#[source] BuildNeoparcReleaseError),
  #[error("failed to update active neoparc release")]
  UpdateActive(#[source] UpdateActiveNeoparcReleaseError),
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for NeoparcReleaseTarget
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Debug + Clone + Send + Sync,
  H::Group: Debug + Send + Sync,
{
  type Output = TaskResult<NeoparcRelease<H::User>, NeoparcReleaseTargetError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let res = self
      .channel
      .run(host)
      .await
      .map_err(NeoparcReleaseTargetError::Channel)?;
    let channel = res.output;
    let mut changed = res.changed;

    let release_dir = channel.paths.releases.join(&self.git_ref);

    eprintln!("Create directory: {}", release_dir.display());
    changed = EnsureDir::new(release_dir.clone())
      .owner(UserRef::Id(channel.user.uid()))
      .group(GroupRef::Id(channel.user.gid()))
      .mode(FileMode::OWNER_ALL | FileMode::ALL_READ | FileMode::ALL_EXEC)
      .run(host)
      .await
      .map_err(NeoparcReleaseTargetError::ReleaseDir)?
      .changed
      || changed;

    let repo_dir = release_dir.join("repo");

    let protocol = if channel.domain.cert.is_some() { "https" } else { "http" };
    let uri = format!("{}://{}", protocol, channel.domain.main.as_str());

    let br = BuildNeoparcRelease {
      repo_dir: repo_dir.clone(),
      remote: self.repository.clone(),
      git_ref: self.git_ref.clone(),
      frontend_config: FrontendConfig {
        backend_root: Url::parse(&uri).expect("uri is well formed"),
      },
      backend_config_admin: BackendConfig {
        db_url: format!(
          "jdbc:postgresql://{}:{}/{}",
          channel.postgres.host, channel.postgres.port, channel.postgres.name
        ),
        db_credentials: channel.postgres.admin.clone(),
        main_port: channel.main_port,
        backend_root: Url::parse(&uri).expect("uri is well formed"),
        frontend_root: Url::parse(&uri).expect("uri is well formed"),
        secret: self.channel.vault.secret.clone(),
        discord_token: self.channel.vault.discord_token.clone(),
        salt: self.channel.vault.salt.clone(),
        seed: self.channel.vault.seed.clone(),
        test: self.channel.vault.test.clone(),
        etwin_uri: Url::parse(self.channel.etwin_uri.as_str()).expect("uri is well formed"),
        etwin_client_id: self.channel.etwin_client_id.clone(),
        etwin_client_secret: self.channel.vault.etwin_client_secret.clone(),
      },
      backend_config_main: BackendConfig {
        db_url: format!(
          "jdbc:postgresql://{}:{}/{}",
          channel.postgres.host, channel.postgres.port, channel.postgres.name
        ),
        db_credentials: channel.postgres.main.clone(),
        main_port: channel.main_port,
        backend_root: Url::parse(&uri).expect("uri is well formed"),
        frontend_root: Url::parse(&uri).expect("uri is well formed"),
        secret: self.channel.vault.secret.clone(),
        discord_token: self.channel.vault.discord_token.clone(),
        salt: self.channel.vault.salt.clone(),
        seed: self.channel.vault.seed.clone(),
        test: self.channel.vault.test.clone(),
        etwin_uri: Url::parse(self.channel.etwin_uri.as_str()).expect("uri is well formed"),
        etwin_client_id: self.channel.etwin_client_id.clone(),
        etwin_client_secret: self.channel.vault.etwin_client_secret.clone(),
      },
      java: channel.java.clone(),
      node: channel.node.clone(),
      yarn: channel.yarn.clone(),
    };

    changed = exec_as::<LocalLinux, _>(UserRef::Id(channel.user.uid()), None, &br)
      .await
      .map_err(NeoparcReleaseTargetError::ExecBuild)?
      .map_err(NeoparcReleaseTargetError::Build)?
      .changed
      || changed;

    let release = NeoparcRelease {
      release_dir,
      repository: self.repository.clone(),
      repo_dir,
      channel,
      git_ref: self.git_ref.clone(),
    };

    changed = (UpdateActiveNeoparcRelease {
      release: release.clone(),
    })
    .run(host)
    .await
    .map_err(NeoparcReleaseTargetError::UpdateActive)?
    .changed
      || changed;

    Ok(TaskSuccess {
      changed,
      output: release,
    })
  }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum NeoparcChannelTargetError {
  #[error("base channel creation failed")]
  Base(#[source] ChannelTargetError),
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for NeoparcChannelTarget
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  H::Group: Debug + Send + Sync,
  H::User: Debug + Clone + Send + Sync,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<NeoparcChannel<H::User>, NeoparcChannelTargetError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    eprintln!("Neoparc: Create channel");
    let res = self.inner.run(host).await.map_err(NeoparcChannelTargetError::Base)?;

    eprintln!("Channel ready");
    let output = NeoparcChannel {
      full_name: res.output.full_name,
      short_name: res.output.short_name,
      user: res.output.user,
      domain: res.output.domain.expect("Expected associated domain name"),
      paths: res.output.paths,
      fallback_server: res.output.fallback_server.expect("Expected fallback server"),
      postgres: res.output.postgres.expect("Expected Postgres database"),
      main_port: res.output.main_port.expect("Expected main port"),
      java: res.output.java.expect("Expected Java environment"),
      node: res.output.node.expect("Expected Node environment"),
      yarn: res.output.yarn.expect("Expected Yarn environment"),
    };

    Ok(TaskSuccess {
      changed: res.changed,
      output,
    })
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct BuildNeoparcRelease {
  repo_dir: PathBuf,
  remote: String,
  git_ref: String,
  frontend_config: FrontendConfig,
  backend_config_admin: BackendConfig,
  backend_config_main: BackendConfig,
  java: JavaEnv,
  node: NodeEnv,
  yarn: YarnEnv,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum BuildNeoparcReleaseError {
  #[error("failed Git checkout")]
  GitCheckout(#[source] GitCheckoutError),
  #[error("failed to ensure backend config file (admin)")]
  EnsureBackendConfigAdmin(#[source] EnsureFileError),
  #[error("failed database synchronization")]
  DbSync(#[source] ExecError),
  #[error("failed to ensure backend config file (main)")]
  EnsureBackendConfigMain(#[source] EnsureFileError),
  #[error("failed to ensure backend config file (main)")]
  EnsureFrontendConfig(#[source] EnsureFileError),
  #[error("failed to build the backend")]
  BuildBackend(#[source] ExecError),
  #[error("failed to install node dependencies")]
  NodeDependencies(#[source] ExecError),
  #[error("failed to build the frontend")]
  BuildFrontend(#[source] ExecError),
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for BuildNeoparcRelease
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Send + Sync,
{
  type Output = Result<TaskSuccess<()>, BuildNeoparcReleaseError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let gc = GitCheckout {
      path: self.repo_dir.clone(),
      remote: self.remote.clone(),
      r#ref: self.git_ref.clone(),
    };

    eprintln!("Checkout repository: {:?} {:?}", self.remote, self.git_ref);
    let res = gc.run(host).await.map_err(BuildNeoparcReleaseError::GitCheckout)?;
    let changed = res.changed;

    let backend_dir = self.repo_dir.join("backend");
    let frontend_dir = self.repo_dir.join("frontend");

    eprintln!("Configure the backend build (admin)");
    EnsureFile::new(backend_dir.join("src/main/resources/application.yml"))
      .content(self.backend_config_admin.to_string())
      .mode(FileMode::OWNER_READ | FileMode::OWNER_WRITE)
      .run(host)
      .await
      .map_err(BuildNeoparcReleaseError::EnsureBackendConfigAdmin)?;

    eprintln!("Sync the DB");
    let cmd = Command::new(backend_dir.join("gradlew").to_str().unwrap())
      .arg("--no-daemon")
      .arg(":flywayMigrate")
      .env("JAVA_HOME", self.java.java_home.display().to_string())
      .current_dir(backend_dir.to_str().unwrap());
    host.exec(&cmd).map_err(BuildNeoparcReleaseError::DbSync)?;

    eprintln!("Configure the backend build (main)");
    EnsureFile::new(backend_dir.join("src/main/resources/application.yml"))
      .content(self.backend_config_main.to_string())
      .mode(FileMode::OWNER_READ | FileMode::OWNER_WRITE)
      .run(host)
      .await
      .map_err(BuildNeoparcReleaseError::EnsureBackendConfigMain)?;

    eprintln!("Build the backend");
    let cmd = Command::new(backend_dir.join("gradlew").to_str().unwrap())
      .arg("--no-daemon")
      .arg(":jar")
      .env("JAVA_HOME", self.java.java_home.display().to_string())
      .current_dir(backend_dir.to_str().unwrap());
    host.exec(&cmd).map_err(BuildNeoparcReleaseError::BuildBackend)?;

    eprintln!("Install Node dependencies");
    let cmd = Command::new("yarn")
      .arg("workspaces")
      .arg("focus")
      .prepend_path(self.node.bin_dir.display().to_string())
      .prepend_path(self.yarn.bin_dir.display().to_string())
      .current_dir(frontend_dir.to_str().unwrap());
    host.exec(&cmd).map_err(BuildNeoparcReleaseError::NodeDependencies)?;

    eprintln!("Configure the frontend build");
    EnsureFile::new(frontend_dir.join(".env"))
      .content(self.frontend_config.to_string())
      .mode(FileMode::OWNER_READ | FileMode::OWNER_WRITE)
      .run(host)
      .await
      .map_err(BuildNeoparcReleaseError::EnsureFrontendConfig)?;

    eprintln!("Build the frontend");
    let cmd = Command::new("yarn")
      .arg("run")
      .arg("build")
      .prepend_path(self.node.bin_dir.display().to_string())
      .prepend_path(self.yarn.bin_dir.display().to_string())
      .current_dir(frontend_dir.to_str().unwrap());
    host.exec(&cmd).map_err(BuildNeoparcReleaseError::BuildFrontend)?;

    Ok(TaskSuccess { changed, output: () })
  }
}

impl<'h, H> NamedTask<&'h H> for BuildNeoparcRelease
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Send + Sync,
{
  const NAME: TaskName = TaskName::new("neoparc::BuildNeoparcRelease");
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize)]
pub struct UpdateActiveNeoparcRelease<User: LinuxUser, S: AsRef<str> = String> {
  release: NeoparcRelease<User, S>,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum UpdateActiveNeoparcReleaseError {
  #[error("failed to resolve link to the active release")]
  ResolveActive(#[source] ResolveError),
  #[error("failed to resolve new release")]
  ResolveNew(#[source] ResolveError),
  #[error("failed to symlink previous release")]
  LinkPrevious(#[source] EnsureDirSymlinkError),
  #[error("failed to disable previous release")]
  SystemdDisablePrevious(#[source] SystemdUnitError),
  #[error("failed to ensure systemd service config")]
  ServiceConfig(#[source] EnsureSystemdServiceConfigError),
  #[error("failed to configure nginx (available)")]
  NginxAvailable(#[source] NginxAvailableSiteError),
  #[error("failed to configure nginx (enable)")]
  NginxEnable(#[source] NginxEnableSiteError),
  #[error("failed to execute DB sync task")]
  ExecDbSync(#[source] ExecAsError),
  #[error("failed to DB sync")]
  DbSync(#[source] UpgradeNeoparcDatabaseError),
  #[error("failed to symlink active release")]
  LinkActive(#[source] EnsureDirSymlinkError),
  #[error("failed to enable current release")]
  SystemdEnable(#[source] SystemdUnitError),
}

#[async_trait]
impl<'h, H, S> AsyncFn<&'h H> for UpdateActiveNeoparcRelease<H::User, S>
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Send + Sync,
  S: AsRef<str> + Send + Sync,
{
  type Output = TaskResult<(), UpdateActiveNeoparcReleaseError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let service_name = format!("{}.service", self.release.channel.full_name.as_ref());

    let mut changed = false;
    let active_release_link = self.release.channel.paths.home.join("active");
    let previous_release_link = self.release.channel.paths.home.join("previous");
    let old_release_dir = {
      match host.resolve(&active_release_link) {
        Ok(old) => Some(old),
        Err(ResolveError::NotFound) => None,
        Err(e) => return Err(UpdateActiveNeoparcReleaseError::ResolveActive(e)),
      }
    };
    let new_release_dir = host
      .resolve(&self.release.release_dir)
      .map_err(UpdateActiveNeoparcReleaseError::ResolveNew)?;

    if let Some(old_release_dir) = old_release_dir {
      if old_release_dir != new_release_dir {
        eprintln!("Start to disable old release");
        // There is already an active release but it does not match the target
        // release: stop the old version before proceeding to free the channel
        // resources (database, port)
        EnsureDirSymlink::new(previous_release_link.clone())
          .points_to(old_release_dir.clone())
          .owner(self.release.channel.user.uid())
          .group(self.release.channel.user.gid())
          .mode(FileMode::ALL_READ | FileMode::ALL_EXEC)
          .run(host)
          .await
          .map_err(UpdateActiveNeoparcReleaseError::LinkPrevious)?;

        SystemdUnit::new(&service_name)
          .state(SystemdState::Inactive)
          .run(host)
          .await
          .map_err(UpdateActiveNeoparcReleaseError::SystemdDisablePrevious)?;

        eprintln!("Old release disabled");
        changed = true;
      }
    }

    eprintln!("UpgradingDatabase");
    changed = exec_as::<LocalLinux, _>(
      UserRef::Id(self.release.channel.user.uid()),
      None,
      &UpgradeNeoparcDatabase {
        repo_dir: self.release.repo_dir.clone(),
      },
    )
    .await
    .map_err(UpdateActiveNeoparcReleaseError::ExecDbSync)?
    .map_err(UpdateActiveNeoparcReleaseError::DbSync)?
    .changed
      || changed;

    eprintln!("Generating Systemd unit");
    let backend_dir = self.release.channel.paths.home.join("active/repo/backend");
    {
      use crate::task::systemd::config::{CommandLine, Config, Exec, Install, Service, ServiceType, Unit};
      let description = format!(
        "Neoparc ({channel_name})",
        channel_name = self.release.channel.full_name.as_ref()
      );
      let backend_dir_str = backend_dir.display().to_string();
      let gradlew_str = backend_dir.join("gradlew").display().to_string();
      let exec_start = format!("{gradlew_str} :run");
      let java_home_env = format!("JAVA_HOME={}", self.release.channel.java.java_home.display());
      let service_config = Config {
        unit: Some(Unit {
          description: Some(description.as_str()),
          after: vec!["network.target"],
          ..Unit::default()
        }),
        service: Some(Service {
          r#type: Some(ServiceType::Simple),
          restart: Some("on-failure"),
          exec_start: Some(CommandLine { line: exec_start.as_str() }),
          exec: Exec {
            user: Some(self.release.channel.user.name()),
            working_directory: Some(backend_dir_str.as_str()),
            limit_no_file: Some("infinity"),
            limit_nproc: Some("infinity"),
            limit_core: Some("infinity"),
            environment: vec![
              "PATH=/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl:/usr/bin:/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin",
              java_home_env.as_str(),
            ],
            ..Exec::default()
          },
          ..Service::default()
        }),
        install: Some(Install {
          wanted_by: vec!["multi-user.target"],
          ..Install::default()
        }),
        ..Config::default()
      };
      eprintln!("Writing Systemd unit");
      changed = EnsureSystemdServiceConfig {
        name: self.release.channel.full_name.as_ref().to_string(),
        config: service_config,
      }
      .run(host)
      .await
      .map_err(UpdateActiveNeoparcReleaseError::ServiceConfig)?
      .changed
        || changed;
    };

    eprintln!("Generating nginx config");
    let nginx_config = get_nginx_config(&self.release.channel, self.release.git_ref.as_str());
    eprintln!("Writing nginx config");
    NginxAvailableSite::new(self.release.channel.full_name.as_ref(), nginx_config)
      .run(host)
      .await
      .map_err(UpdateActiveNeoparcReleaseError::NginxAvailable)?;
    eprintln!("Enabling nginx config");
    NginxEnableSite::new(self.release.channel.full_name.as_ref())
      .run(host)
      .await
      .map_err(UpdateActiveNeoparcReleaseError::NginxEnable)?;

    eprintln!("Marking release as active");
    EnsureDirSymlink::new(active_release_link.clone())
      .points_to(new_release_dir.clone())
      .owner(self.release.channel.user.uid())
      .group(self.release.channel.user.gid())
      .mode(FileMode::ALL_READ | FileMode::ALL_EXEC)
      .run(host)
      .await
      .map_err(UpdateActiveNeoparcReleaseError::LinkActive)?;

    eprintln!("Starting service");
    changed = SystemdUnit::new(&service_name)
      .enabled(true)
      .state(SystemdState::Active)
      .run(host)
      .await
      .map_err(UpdateActiveNeoparcReleaseError::SystemdEnable)?
      .changed
      || changed;

    Ok(TaskSuccess { changed, output: () })
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct UpgradeNeoparcDatabase {
  repo_dir: PathBuf,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum UpgradeNeoparcDatabaseError {}

#[async_trait]
impl<'h, H: ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for UpgradeNeoparcDatabase
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = Result<TaskSuccess<()>, UpgradeNeoparcDatabaseError>;

  async fn run(&self, _host: &'h H) -> Self::Output {
    Ok(TaskSuccess {
      changed: true,
      output: (),
    })
  }
}

impl<'h, H: 'h + ExecHost + LinuxUserHost + LinuxFsHost> NamedTask<&'h H> for UpgradeNeoparcDatabase
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  const NAME: TaskName = TaskName::new("neoparc::UpgradeNeoparcDatabase");
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
struct FrontendConfig {
  backend_root: Url,
}

impl fmt::Display for FrontendConfig {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    writeln!(f, "REACT_APP_HOST=0.0.0.0")?;
    writeln!(f, "HOST=0.0.0.0")?;
    writeln!(f, "REACT_APP_API_SERVER={}", &self.backend_root)?;
    Ok(())
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
struct BackendConfig<S: AsRef<str> = String> {
  db_url: String,
  db_credentials: PostgresCredentials,
  main_port: u16,
  backend_root: Url,
  frontend_root: Url,
  secret: Option<S>,
  discord_token: Option<S>,
  salt: S,
  seed: S,
  test: S,
  etwin_uri: Url,
  etwin_client_id: S,
  etwin_client_secret: S,
}

impl<S: AsRef<str>> fmt::Display for BackendConfig<S> {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    let db_url = serde_json::to_string(self.db_url.as_str()).unwrap();
    let db_user = serde_json::to_string(self.db_credentials.name.as_str()).unwrap();
    let db_password = serde_json::to_string(self.db_credentials.password.as_str()).unwrap();
    writeln!(f, "spring:")?;
    writeln!(f, "  datasource:")?;
    writeln!(f, "    url: {}", db_url)?;
    writeln!(f, "    driver-class-name: org.postgresql.Driver")?;
    writeln!(f, "    username: {}", db_user)?;
    writeln!(f, "    password: {}", db_password)?;
    writeln!(f, "  jooq:")?;
    writeln!(f, "    sql-dialect: postgres")?;
    writeln!(f, "  elasticsearch:")?;
    writeln!(f, "    url: http://[::1]:9200")?;
    writeln!(f, "  main:")?;
    writeln!(f, "    allow-bean-definition-overriding: true")?;
    writeln!(f, "server:")?;
    writeln!(f, "  port: {}", self.main_port)?;
    writeln!(f, "  backend: {}", &self.backend_root)?;
    writeln!(f, "  frontend: {}", &self.frontend_root)?;
    if let Some(secret) = self.secret.as_ref().map(|s| s.as_ref()) {
      writeln!(f, "  secret: {}", secret)?;
    }
    if let Some(discord_token) = self.discord_token.as_ref().map(|s| s.as_ref()) {
      writeln!(f, "  discord-token: {}", discord_token)?;
    }
    writeln!(f, "  salt: {}", &self.salt.as_ref())?;
    writeln!(f, "  seed: {}", &self.seed.as_ref())?;
    writeln!(f, "  test: {}", &self.test.as_ref())?;
    writeln!(f, "etwin:")?;
    writeln!(f, "  uri: {}", &self.etwin_uri)?;
    writeln!(f, "  client-id: {}", self.etwin_client_id.as_ref())?;
    writeln!(f, "  client-secret: {}", self.etwin_client_secret.as_ref())?;
    Ok(())
  }
}

pub fn get_nginx_config<U: LinuxUser, S: AsRef<str>>(channel: &NeoparcChannel<U, S>, commit_hash: &str) -> String {
  assert_eq!(commit_hash.len(), 40);
  let upstream_name = channel.full_name.as_ref().to_string().replace('.', "_");
  let access_log = channel.paths.logs.join("main.nginx.access.log");
  let error_log = channel.paths.logs.join("main.nginx.error.log");
  let static_dir = channel.paths.home.join("active/repo/frontend/build");

  let fallback_server = format!(
    r#"# Fallback server
server {{
  # HTTP port
  listen {fallback_port};
  listen [::]:{fallback_port};

  # Hide nginx version in `Server` header.
  server_tokens off;
  add_header Referrer-Policy same-origin;
  add_header X-Content-Type-Options nosniff;
  add_header X-XSS-Protection "1; mode=block";

  index index.html;

  root {fallback_dir};
}}
"#,
    fallback_port = channel.fallback_server.port,
    fallback_dir = channel.fallback_server.dir.to_str().unwrap(),
  );

  let upstream_server = format!(
    r#"# Define target of the reverse-proxy
upstream {upstream_name} {{
  server [::1]:{port};
  server [::1]:{fallback_port} backup;
  keepalive 8;
}}
"#,
    upstream_name = upstream_name,
    port = channel.main_port,
    fallback_port = channel.fallback_server.port,
  );

  let main_directives = format!(
    r#"  # Space-separated hostnames (wildcard * is accepted)
  server_name {domain};

  access_log {access_log};
  error_log {error_log};
  client_max_body_size 100M;

  root {static_dir};

  location ~* ^/(?:api)(?:/|$) {{
    # Upstream reference
    proxy_pass http://{upstream_name};

    # Configuration of the proxy
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection 'upgrade';
    proxy_set_header Host $host;
    proxy_set_header X-NginX-Proxy true;
    proxy_cache_bypass $http_upgrade;
    proxy_redirect off;
  }}

  location /static {{
    gzip_static on;
    add_header Cache-Control "public,max-age=31536000,immutable";
    etag off;
  }}

  location / {{
    try_files $uri /index.html;
    add_header Cache-Control "no-cache";
    add_header ETag "{git_commit}";
  }}
"#,
    domain = channel.domain.main.as_ref(),
    access_log = access_log.to_str().unwrap(),
    error_log = error_log.to_str().unwrap(),
    static_dir = static_dir.to_str().unwrap(),
    upstream_name = upstream_name,
    git_commit = commit_hash,
  );

  let cert_files = match channel.domain.cert.as_ref() {
    Some(https) => format!(
      r#"  ssl_certificate {cert};
  ssl_certificate_key {cert_key};"#,
      cert = https.fullchain_cert_path.to_str().unwrap(),
      cert_key = https.cert_key_path.to_str().unwrap(),
    ),
    None => String::new(),
  };

  let legacy = if channel.domain.legacy.is_empty() {
    String::new()
  } else {
    let legacy: Vec<&str> = channel.domain.legacy.iter().map(|l| l.as_ref()).collect();
    let https_port = if channel.domain.cert.is_some() {
      "listen 443 ssl;\n  listen [::]:443 ssl;"
    } else {
      ""
    };
    format!(
      r#"# Legacy
server {{
  listen 80;
  listen [::]:80;
  {https_port}

  server_name {legacy};

  {cert_files}

  return 308 https://{domain}$request_uri;
}}
"#,
      legacy = legacy.join(" "),
      domain = channel.domain.main.as_ref(),
    )
  };

  match channel.domain.cert.as_ref() {
    Some(_) => format!(
      r#"# This configuration sets a reverse proxy for a specific domain

{fallback_server}

{upstream_server}

# HTTPS
server {{
  # HTTPS port
  listen 443 ssl;
  listen [::]:443 ssl;

  {cert_files}

  # # HSTS (ngx_http_headers_module is required) (63072000 seconds)
  # add_header Strict-Transport-Security "max-age=63072000" always;

  {main_directives}
}}

# HTTP
server {{
  # HTTP port
  listen 80;
  listen [::]:80;
  server_name {domain};
  return 301 https://$host$request_uri;
}}

# www.
server {{
  listen 80;
  listen 443 ssl;
  listen [::]:80;
  listen [::]:443 ssl;

  server_name www.{domain};

  {cert_files}

  return 307 https://{domain}$request_uri;
}}

{legacy}
"#,
      fallback_server = fallback_server,
      upstream_server = upstream_server,
      main_directives = main_directives,
      domain = channel.domain.main.as_ref(),
    ),
    None => format!(
      r#"# This configuration sets a reverse proxy for a specific domain

{fallback_server}

{upstream_server}

# HTTP
server {{
  # HTTP port
  listen 80;
  listen [::]:80;

  {main_directives}
}}

# www.
server {{
  listen 80;
  listen [::]:80;

  server_name www.{domain};

  return 307 http://{domain}$request_uri;
}}

{legacy}
"#,
      fallback_server = fallback_server,
      upstream_server = upstream_server,
      main_directives = main_directives,
      domain = channel.domain.main.as_ref(),
    ),
  }
}
