use crate::etwin::channel::{
  ChannelBorgbase, ChannelDomain, ChannelFallbackServer, ChannelPaths, ChannelPostgres, ChannelTarget, NodeEnv,
};
use crate::etwin::etwin::config::SsrConfig;
use crate::task::fs::{
  EnsureDir, EnsureDirError, EnsureDirSymlink, EnsureDirSymlinkError, EnsureFile, EnsureFileError,
};
use crate::task::git::GitCheckout;
use crate::task::nginx::{NginxAvailableSite, NginxAvailableSiteError, NginxEnableSite, NginxEnableSiteError};
use crate::task::rust::RustEnv;
use crate::task::systemd::{
  EnsureSystemdServiceConfig, EnsureSystemdServiceConfigError, EnsureSystemdSliceConfig, EnsureSystemdSliceConfigError,
  SystemdSliceConfig, SystemdState, SystemdUnit, SystemdUnitError,
};
use crate::task::witness::{FsWitness, FsWitnessError};
use crate::task::yarn::YarnEnv;
use crate::task::{exec_as, AsyncFn, ExecAsError, NamedTask, TaskName, TaskRef, TaskResult, TaskSuccess};
use crate::utils::display_error_chain::DisplayErrorChainExt;
use async_trait::async_trait;
use asys::common::fs::{FsHost, ReadFileError};
use asys::linux::fs::{FileMode, LinuxFsHost, LinuxMetadata, ResolveError};
use asys::linux::user::{Gid, GroupRef, LinuxUser, LinuxUserHost, Uid, UserRef};
use asys::local_linux::LocalLinux;
use asys::{Command, ExecError, ExecHost};
use chrono::Duration;
use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};
use std::fmt::Debug;
use std::path::PathBuf;

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct EtwinAppTarget<S: AsRef<str> = String> {
  pub name: S,
  pub user_name: S,
  pub group_name: S,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct EtwinApp<S: AsRef<str> = String> {
  name: S,
  user_name: S,
  group_name: S,
  uid: Uid,
  gid: Gid,
  home: PathBuf,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct EternaltwinChannelTarget<S: AsRef<str> = String> {
  pub inner: ChannelTarget<S>,
  pub backend_port: u16,
  pub opentelemetry: Option<EternaltwinOpentelemetry>,
  pub vault: EternaltwinVault,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct EternaltwinVault {
  pub secret_key: String,
  pub mailer: String,
  pub brute_production_secret: String,
  pub brute_staging_secret: String,
  pub dinocard_production_secret: String,
  pub dinocard_staging_secret: String,
  pub dinorpg_production_secret: String,
  pub dinorpg_staging_secret: String,
  pub directquiz_secret: String,
  pub emush_production_secret: String,
  pub emush_staging_secret: String,
  pub epopotamo_production_secret: String,
  pub epopotamo_staging_secret: String,
  pub eternalfest_production_secret: String,
  pub eternalfest_staging_secret: String,
  pub kadokadeo_production_secret: String,
  pub kadokadeo_staging_secret: String,
  pub kingdom_production_secret: String,
  pub kingdom_staging_secret: String,
  pub myhordes_secret: String,
  pub neoparc_production_secret: String,
  pub neoparc_staging_secret: String,
  pub mjrt_secret: String,
  pub wiki_secret: String,
  pub twinoid_client_id: String,
  pub twinoid_client_key: String,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct EternaltwinChannel<User: LinuxUser, S: AsRef<str> = String> {
  full_name: S,
  short_name: S,
  user: User,
  domain: ChannelDomain<S>,
  paths: ChannelPaths,
  main_port: u16,
  backend_port: u16,
  opentelemetry: Option<EternaltwinOpentelemetry>,
  fallback_server: ChannelFallbackServer,
  postgres: ChannelPostgres,
  node: NodeEnv,
  yarn: YarnEnv,
  rust: RustEnv,
  vault: EternaltwinVault,
  borg: Option<ChannelBorgbase>,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct EternaltwinOpentelemetry {
  pub endpoint: String,
  pub uptrace_dsn: String,
}

impl<User, S> EternaltwinChannel<User, S>
where
  User: LinuxUser,
  S: AsRef<str>,
{
  pub fn backend_service_name(&self) -> String {
    format!("{}.backend", self.full_name.as_ref())
  }

  pub fn frontend_service_name(&self) -> String {
    format!("{}.frontend", self.full_name.as_ref())
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct EternaltwinReleaseTarget<S: AsRef<str> = String> {
  pub channel: EternaltwinChannelTarget,
  pub repository: S,
  pub git_ref: String,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct EternaltwinRelease<User: LinuxUser, S: AsRef<str> = String> {
  release_dir: PathBuf,
  repository: S,
  repo_dir: PathBuf,
  channel: EternaltwinChannel<User, S>,
  git_ref: String,
}

#[derive(Serialize, Deserialize)]
pub struct DeployEternaltwin<S: AsRef<str> = String> {
  pub pass: S,
  pub target: EternaltwinReleaseTarget,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum DeployEternaltwinError {
  #[error("failed to exec eternaltwin deployment task")]
  Exec(#[source] ExecAsError),
  #[error("failed to deploy eternaltwin release")]
  Deploy(#[source] DeployEternaltwinAsRootError),
}

#[async_trait]
impl<'h, H, S> AsyncFn<&'h H> for DeployEternaltwin<S>
where
  H: ExecHost,
  S: AsRef<str> + Send + Sync,
{
  type Output = TaskResult<(), DeployEternaltwinError>;

  async fn run(&self, _host: &'h H) -> Self::Output {
    let input = DeployEternaltwinAsRoot {
      target: self.target.clone(),
    };
    exec_as::<LocalLinux, DeployEternaltwinAsRoot>(UserRef::ROOT, Some(self.pass.as_ref()), &input)
      .await
      .map_err(DeployEternaltwinError::Exec)?
      .map_err(DeployEternaltwinError::Deploy)
  }
}

impl<'h, H: 'h + ExecHost> NamedTask<&'h H> for DeployEternaltwin {
  const NAME: TaskName = TaskName::new("etwin::DeployEternaltwin");
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DeployEternaltwinAsRoot {
  target: EternaltwinReleaseTarget,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum DeployEternaltwinAsRootError {
  #[error("failed to use witness file")]
  Witness(#[source] FsWitnessError),
  #[error("failed to run task `EternaltwinReleaseTarget`")]
  Task(#[source] EternaltwinReleaseTargetError),
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for DeployEternaltwinAsRoot
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  H::User: Debug + Clone + Send + Sync + Serialize + DeserializeOwned,
  H::Group: Debug + Send + Sync,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<(), DeployEternaltwinAsRootError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let paths: ChannelPaths = self.target.channel.inner.paths();
    let witness = paths.home.join("eternaltwin_release.witness");
    let mut changed = false;
    let res = FsWitness::new(witness, TaskRef(&self.target))
      .run(host)
      .await
      .map_err(DeployEternaltwinAsRootError::Witness)?;
    changed = res.changed || changed;
    let res = res.output.map_err(DeployEternaltwinAsRootError::Task)?;
    changed = res.changed || changed;
    Ok(TaskSuccess { changed, output: () })
  }
}

impl<'h, H> NamedTask<&'h H> for DeployEternaltwinAsRoot
where
  H: 'h + ExecHost + LinuxUserHost + LinuxFsHost,
  H::User: Debug + Clone + Send + Sync + Serialize + DeserializeOwned,
  H::Group: Debug + Send + Sync,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  const NAME: TaskName = TaskName::new("etwin::DeployEtwinAsRoot");
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum EternaltwinReleaseTargetError {
  #[error("failed to ensure release directory")]
  ReleaseDir(#[source] EnsureDirError),
  #[error("failed to execute build task")]
  ExecBuild(#[source] ExecAsError),
  #[error("failed to update active eternaltwin release")]
  UpdateActive(#[source] UpdateActiveEternaltwinReleaseError),
  #[error("unexpected error: {0}")]
  Other(String),
}

impl From<anyhow::Error> for EternaltwinReleaseTargetError {
  fn from(value: anyhow::Error) -> Self {
    Self::Other(value.display_chain().to_string())
  }
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for EternaltwinReleaseTarget
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  H::User: Debug + Clone + Send + Sync,
  H::Group: Debug + Send + Sync,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<EternaltwinRelease<H::User>, EternaltwinReleaseTargetError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let res = self.channel.run(host).await?;
    let channel: EternaltwinChannel<H::User> = res.output;
    let mut changed = res.changed;

    let release_dir = channel.paths.releases.join(&self.git_ref);

    eprintln!("creating repo dir");

    eprintln!("Create directory: {}", release_dir.display());
    changed = EnsureDir::new(release_dir.clone())
      .owner(UserRef::Id(channel.user.uid()))
      .group(GroupRef::Id(channel.user.gid()))
      .mode(FileMode::OWNER_ALL | FileMode::ALL_READ | FileMode::ALL_EXEC)
      .run(host)
      .await
      .map_err(EternaltwinReleaseTargetError::ReleaseDir)?
      .changed
      || changed;

    eprintln!("witness check");

    let witness = release_dir.join("ok");

    let witness_exists = match host.read_file(witness.as_path()) {
      Ok(_) => true,
      Err(ReadFileError::NotFound) => false,
      Err(e) => {
        panic!("failed to check witness file: {e:?}")
      }
    };
    eprintln!("witness check done");

    // let uptrace = UptraceServerTarget {};
    // let uptrace = uptrace.run(host).await?;
    // changed = uptrace.changed || changed;
    // let uptrace = uptrace.output;

    let repo_dir = release_dir.join("repo");

    let release = EternaltwinRelease {
      release_dir,
      repository: self.repository.clone(),
      repo_dir: repo_dir.clone(),
      channel: channel.clone(),
      git_ref: self.git_ref.clone(),
    };

    if witness_exists {
      eprintln!(
        "bailing-out, commit already downloaded (assuming the release already completed) (todo: better tracking)"
      );
      return Ok(TaskSuccess {
        changed,
        output: release,
      });
    }

    let br = BuildEtwinRelease {
      repo_dir: repo_dir.clone(),
      remote: self.repository.clone(),
      git_ref: self.git_ref.clone(),
      node: channel.node.clone(),
      yarn: channel.yarn.clone(),
      rust: channel.rust.clone(),
      config: config::Config::from_channel(&channel, self.git_ref.as_str()),
    };
    eprintln!("release ready to run");

    changed = exec_as::<LocalLinux, _>(UserRef::Id(channel.user.uid()), None, &br)
      .await
      .map_err(EternaltwinReleaseTargetError::ExecBuild)?
      .unwrap()
      .changed
      || changed;

    eprintln!("release done");

    changed = (UpdateActiveRelease {
      release: release.clone(),
    })
    .run(host)
    .await
    .map_err(EternaltwinReleaseTargetError::UpdateActive)?
    .changed
      || changed;

    changed = EnsureFile::new(witness).content([]).run(host).await.unwrap().changed || changed;

    Ok(TaskSuccess {
      changed,
      output: release,
    })
  }
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for EternaltwinChannelTarget
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  H::Group: Debug + Send + Sync,
  H::User: Debug + Clone + Send + Sync,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<EternaltwinChannel<H::User>, anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    eprintln!("Eternaltwin: Create channel");
    let res = self.inner.run(host).await?;

    eprintln!("Eternaltwin: Channel ready");
    let output = EternaltwinChannel {
      full_name: res.output.full_name,
      short_name: res.output.short_name,
      user: res.output.user,
      domain: res.output.domain.expect("Expected associated domain name"),
      paths: res.output.paths,
      main_port: res.output.main_port.expect("Expected main port"),
      backend_port: self.backend_port,
      opentelemetry: self.opentelemetry.clone(),
      fallback_server: res.output.fallback_server.expect("Expected fallback server"),
      postgres: res.output.postgres.expect("Expected Postgres database"),
      node: res.output.node.expect("Expected NVM environment"),
      yarn: res.output.yarn.expect("Expected Yarn environment"),
      rust: res.output.rust.expect("Expected Rust environment"),
      vault: self.vault.clone(),
      borg: res.output.borg,
    };

    Ok(TaskSuccess {
      changed: res.changed,
      output,
    })
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct BuildEtwinRelease {
  repo_dir: PathBuf,
  remote: String,
  git_ref: String,
  node: NodeEnv,
  yarn: YarnEnv,
  rust: RustEnv,
  config: config::Config,
}

#[async_trait]
impl<'h, H: ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for BuildEtwinRelease
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = Result<TaskSuccess<()>, ()>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let mut changed: bool;
    let gc = GitCheckout {
      path: self.repo_dir.clone(),
      remote: self.remote.clone(),
      r#ref: self.git_ref.clone(),
    };

    eprintln!("Checkout repository: {:?} {:?}", self.remote, self.git_ref);
    let res = gc.run(host).await.expect("failed to checkout repository");
    changed = res.changed;

    {
      let cmd = Command::new("cargo")
        .prepend_path(self.rust.bin_dir().display().to_string())
        .arg("--version");
      let has_cargo = host
        .try_exec(&cmd)
        .map(|out| out.termination.success())
        .unwrap_or(false);
      if !has_cargo {
        eprintln!("cargo not found: installing");
        let cmd = Command::new("rustup")
          .arg("toolchain")
          .arg("install")
          .arg("stable")
          .arg("--profile")
          .arg("minimal");
        host.exec(&cmd).expect("failed to install minimal rust toolchain");
        let cmd = Command::new("rustup").arg("default").arg("stable");
        host.exec(&cmd).expect("failed to enable stable rust toolchain");
      }
    }

    eprintln!("Install Node dependencies");
    let cmd = Command::new("yarn")
      .prepend_path(self.node.bin_dir.display().to_string())
      .prepend_path(self.yarn.bin_dir.display().to_string())
      .arg("install")
      .arg("--immutable")
      .current_dir(self.repo_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    eprintln!("Build Node project");
    let cmd = Command::new("yarn")
      .prepend_path(self.node.bin_dir.display().to_string())
      .prepend_path(self.rust.bin_dir().display().to_string())
      .prepend_path(self.yarn.bin_dir.display().to_string())
      .arg("run")
      .arg("build:production")
      .current_dir(self.repo_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    eprintln!("Build CLI");
    let cmd = Command::new("cargo")
      .prepend_path(self.node.bin_dir.display().to_string())
      .prepend_path(self.rust.bin_dir().display().to_string())
      .prepend_path(self.yarn.bin_dir.display().to_string())
      .arg("build")
      .arg("--package")
      .arg("eternaltwin")
      .arg("--bin")
      .arg("eternaltwin")
      .arg("--release")
      .current_dir(self.repo_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    eprintln!("Write backend config");
    changed = EnsureFile::new(self.repo_dir.join("eternaltwin.toml"))
      .content(self.config.to_string())
      // .owner(app.uid)
      // .group(app.gid)
      .mode(FileMode::OWNER_READ | FileMode::OWNER_WRITE)
      .run(host)
      .await
      .unwrap()
      .changed
      || changed;

    eprintln!("Write backend-frontend secret");
    changed = EnsureFile::new(self.repo_dir.join("backend.secret.txt"))
      .content(self.config.backend.secret.clone())
      // .owner(app.uid)
      // .group(app.gid)
      .mode(FileMode::OWNER_READ | FileMode::OWNER_WRITE)
      .run(host)
      .await
      .unwrap()
      .changed
      || changed;

    Ok(TaskSuccess { changed, output: () })
  }
}

impl<'h, H: 'h + ExecHost + LinuxUserHost + LinuxFsHost> NamedTask<&'h H> for BuildEtwinRelease
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  const NAME: TaskName = TaskName::new("etwin::BuildEtwinRelease");
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize)]
pub struct UpdateActiveRelease<User: LinuxUser, S: AsRef<str> = String> {
  release: EternaltwinRelease<User, S>,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum UpdateActiveEternaltwinReleaseError {
  #[error("failed to resolve link to the active release")]
  ResolveActive(#[source] ResolveError),
  #[error("failed to resolve new release")]
  ResolveNew(#[source] ResolveError),
  #[error("failed to symlink previous release")]
  LinkPrevious(#[source] EnsureDirSymlinkError),
  #[error("failed to disable previous release")]
  SystemdDisablePrevious(#[source] SystemdUnitError),
  #[error("failed to ensure systemd slice config (backend)")]
  SliceConfigBackend(#[source] EnsureSystemdSliceConfigError),
  #[error("failed to ensure systemd service config (backend)")]
  ServiceConfigBackend(#[source] EnsureSystemdServiceConfigError),
  #[error("failed to ensure systemd slice config (frontend)")]
  SliceConfigFrontend(#[source] EnsureSystemdSliceConfigError),
  #[error("failed to ensure systemd service config (frontend)")]
  ServiceConfigFrontend(#[source] EnsureSystemdServiceConfigError),
  #[error("failed to configure nginx (available)")]
  NginxAvailable(#[source] NginxAvailableSiteError),
  #[error("failed to configure nginx (enable)")]
  NginxEnable(#[source] NginxEnableSiteError),
  #[error("failed to execute DB sync task")]
  ExecDbSync(#[source] ExecAsError),
  #[error("failed to DB sync")]
  DbSync(#[source] UpgradeEternaltwinDatabaseError),
  #[error("failed to ensure archive script")]
  EnsureArchiveScript(#[source] EnsureFileError),
  #[error("failed to ensure archive systemd service")]
  ArchiveService(#[source] EnsureFileError),
  #[error("failed to ensure archive systemd timer")]
  ArchiveTimer(#[source] EnsureFileError),
  #[error("failed to enable archive timer")]
  ArchiveEnable(#[source] SystemdUnitError),
  #[error("failed to symlink active release")]
  LinkActive(#[source] EnsureDirSymlinkError),
  #[error("failed to enable current release")]
  SystemdEnable(#[source] SystemdUnitError),
}

#[async_trait]
impl<'h, H, S> AsyncFn<&'h H> for UpdateActiveRelease<H::User, S>
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Send + Sync,
  S: AsRef<str> + Send + Sync,
{
  type Output = TaskResult<(), UpdateActiveEternaltwinReleaseError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let uid = self.release.channel.user.uid();
    let gid = self.release.channel.user.gid();

    let backend_service_name = format!("{}.service", self.release.channel.backend_service_name());
    let frontend_service_name = format!("{}.service", self.release.channel.frontend_service_name());

    let mut changed = false;
    let active_release_link = &self.release.channel.paths.active_release_link;
    let previous_release_link = &self.release.channel.paths.previous_release_link;
    let old_release_dir = {
      match host.resolve(active_release_link) {
        Ok(old) => Some(old),
        Err(ResolveError::NotFound) => None,
        Err(e) => return Err(UpdateActiveEternaltwinReleaseError::ResolveActive(e)),
      }
    };
    let new_release_dir = host
      .resolve(&self.release.release_dir)
      .map_err(UpdateActiveEternaltwinReleaseError::ResolveNew)?;

    if let Some(old_release_dir) = old_release_dir {
      if old_release_dir != new_release_dir {
        eprintln!("Start to disable old release");
        // There is already an active release but it does not match the target
        // release: stop the old version before proceeding to free the channel
        // resources (database, port)
        EnsureDirSymlink::new(previous_release_link.clone())
          .points_to(old_release_dir.clone())
          .owner(uid)
          .group(gid)
          .mode(FileMode::ALL_READ | FileMode::ALL_EXEC)
          .run(host)
          .await
          .map_err(UpdateActiveEternaltwinReleaseError::LinkPrevious)?;

        SystemdUnit::new(&backend_service_name)
          .state(SystemdState::Inactive)
          .run(host)
          .await
          .map_err(UpdateActiveEternaltwinReleaseError::SystemdDisablePrevious)?;
        eprintln!("Old release disabled");

        changed = true;
      }
    }

    // TODO: Trigger backup

    eprintln!("UpgradingDatabase");
    changed = exec_as::<LocalLinux, _>(
      UserRef::Id(uid),
      None,
      &UpgradeEtwinDatabase {
        repo_dir: self.release.repo_dir.clone(),
        node: self.release.channel.node.clone(),
        yarn: self.release.channel.yarn.clone(),
        rust: self.release.channel.rust.clone(),
      },
    )
    .await
    .map_err(UpdateActiveEternaltwinReleaseError::ExecDbSync)?
    .map_err(UpdateActiveEternaltwinReleaseError::DbSync)?
    .changed
      || changed;

    if let Some(borg) = self.release.channel.borg.as_ref() {
      eprintln!("Generating Borg archive script");
      let create_borg_archive = format!(
        r#"#!/usr/bin/env bash
set -eu
BACKUP_UUID=$(head -c 256 /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
UTC_TIME=$(date --utc --iso-8601=seconds | sed 's/[^0-9]/-/g' | sed 's/-00-00$//')
ARCHIVE_DIR="{channel_home}/tmp/${{UTC_TIME}}.${{BACKUP_UUID}}"
cd "{repo_dir}"
mkdir -p "${{ARCHIVE_DIR}}"
{etwin_dump} "${{ARCHIVE_DIR}}"
cd "${{ARCHIVE_DIR}}"
SSH_ASKPASS_REQUIRE=never BORG_PASSPHRASE={borg_pass} borg create --compression auto,zstd --rsh "ssh -i {priv_key}" "{borg_repo}::${{UTC_TIME}}" "."
cd {channel_home}
rm -rf "${{ARCHIVE_DIR}}"
"#,
        channel_home = self.release.channel.paths.home.display(),
        repo_dir = self.release.repo_dir.display(),
        etwin_dump = self.release.repo_dir.join("target/release/dump").display(),
        borg_pass = borg.password.clone(),
        priv_key = borg.ssh_priv_key.to_str().unwrap(),
        borg_repo = &borg.repo.0,
      );

      eprintln!("Writing Borg archive script");
      let archive_script = new_release_dir.join("archive.sh");
      changed = EnsureFile::new(archive_script.clone())
        .content(&create_borg_archive)
        .owner(uid)
        .group(gid)
        .mode(FileMode::OWNER_READ | FileMode::OWNER_EXEC)
        .run(host)
        .await
        .map_err(UpdateActiveEternaltwinReleaseError::EnsureArchiveScript)?
        .changed
        || changed;

      eprintln!("Enable borgmatic timer");
      {
        let user = self.release.channel.user.name();
        let name = self.release.channel.full_name.as_ref();
        let service_name = format!("{}.archive.service", name);
        let timer_name = format!("{}.archive.timer", name);

        let systemd_dir = PathBuf::from("/etc/systemd/system");
        let service = format!(
          r#"[Unit]
Description=Backup to Borg ({name})

[Service]
Type=oneshot
User={user}
WorkingDirectory={cwd}

ExecStart="{archive_script}"
"#,
          name = name,
          user = user,
          cwd = self.release.channel.paths.home.display(),
          archive_script = archive_script.display(),
        );
        let timer = format!(
          r#"[Unit]
Description=Timer for Borg ({name})

[Timer]
# Try to renew once a day, between 01:00 and 08:00
OnCalendar=*-*-* 01:00:00
RandomizedDelaySec={interval}
# Run immediately if the previous start time was missed (e.g. due to the server being powered off)
Persistent=true

[Install]
WantedBy=timers.target
"#,
          interval = Duration::hours(7).num_seconds(),
          name = name,
        );
        changed = EnsureFile::new(systemd_dir.join(&service_name))
          .content(service)
          .owner(Uid::ROOT)
          .mode(FileMode::ALL_READ)
          .run(host)
          .await
          .map_err(UpdateActiveEternaltwinReleaseError::ArchiveService)?
          .changed
          || changed;
        changed = EnsureFile::new(systemd_dir.join(&timer_name))
          .content(timer)
          .owner(Uid::ROOT)
          .mode(FileMode::ALL_READ)
          .run(host)
          .await
          .map_err(UpdateActiveEternaltwinReleaseError::ArchiveTimer)?
          .changed
          || changed;
        changed = SystemdUnit::new(&timer_name)
          .enabled(true)
          .state(SystemdState::Active)
          .run(host)
          .await
          .map_err(UpdateActiveEternaltwinReleaseError::ArchiveEnable)?
          .changed
          || changed;
      }
    }

    eprintln!("Generating Systemd slice (backend)");
    {
      use crate::task::systemd::config::{ResourceControl, Slice, Unit};
      let slice = EnsureSystemdSliceConfig {
        name: self.release.channel.backend_service_name(),
        config: SystemdSliceConfig {
          unit: Some(Unit {
            description: Some(format!(
              "Slice for Eternaltwin backend {}",
              self.release.channel.full_name.as_ref()
            )),
            ..Unit::default()
          }),
          slice: Some(Slice {
            resource_control: Some(ResourceControl {
              memory_high: None,
              memory_max: Some(String::from("4G")),
              cpu_quota: Some(String::from("400%")),
              ..ResourceControl::default()
            }),
            ..Slice::default()
          }),
          ..SystemdSliceConfig::default()
        },
      };
      changed = slice
        .run(host)
        .await
        .map_err(UpdateActiveEternaltwinReleaseError::SliceConfigBackend)?
        .changed
        || changed;
    };
    eprintln!("Generating Systemd service (backend)");
    {
      use crate::task::systemd::config::{
        CommandLine, Config, Exec, Install, ResourceControl, Service, ServiceType, Unit,
      };
      let description = format!(
        "Eternaltwin Backend ({channel_name})",
        channel_name = self.release.channel.full_name.as_ref()
      );
      let postgres = format!("{}.service", self.release.channel.postgres.cluster.service);
      let working_dir_str = self
        .release
        .channel
        .paths
        .home
        .join("active/repo")
        .display()
        .to_string();
      let etwin_exe_str = self
        .release
        .channel
        .paths
        .home
        .join("active/repo/target/release/eternaltwin")
        .display()
        .to_string();
      let exec_start = format!("{etwin_exe_str} backend --profile production");
      let pid_file_str = self
        .release
        .channel
        .paths
        .home
        .join(".local/var/run/eternaltwin_backend.pid")
        .display()
        .to_string();
      let slice_str = format!("{}.slice", self.release.channel.full_name.as_ref());
      let service_config = Config {
        unit: Some(Unit {
          description: Some(description.as_str()),
          after: vec!["network.target"],
          requires: vec![&postgres],
          ..Unit::default()
        }),
        service: Some(Service {
          r#type: Some(ServiceType::Simple),
          restart: Some("on-failure"),
          exec_start: Some(CommandLine {
            line: exec_start.as_str(),
          }),
          pid_file: Some(&pid_file_str),
          exec: Exec {
            user: Some(self.release.channel.user.name()),
            working_directory: Some(working_dir_str.as_str()),
            limit_no_file: Some("infinity"),
            limit_nproc: Some("infinity"),
            limit_core: Some("infinity"),
            environment: vec!["PATH=/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl:/usr/bin:/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin"],
            ..Exec::default()
          },
          resource_control: ResourceControl {
            slice: Some(&slice_str),
            ..ResourceControl::default()
          },
          ..Service::default()
        }),
        install: Some(Install {
          wanted_by: vec!["multi-user.target"],
          ..Install::default()
        }),
        ..Config::default()
      };
      eprintln!("Writing Systemd unit (backend)");
      changed = EnsureSystemdServiceConfig {
        name: self.release.channel.backend_service_name(),
        config: service_config,
      }
      .run(host)
      .await
      .map_err(UpdateActiveEternaltwinReleaseError::ServiceConfigBackend)?
      .changed
        || changed;
    };

    eprintln!("Generating Systemd slice (frontend)");
    {
      use crate::task::systemd::config::{ResourceControl, Slice, Unit};
      let slice = EnsureSystemdSliceConfig {
        name: self.release.channel.frontend_service_name(),
        config: SystemdSliceConfig {
          unit: Some(Unit {
            description: Some(format!(
              "Slice for Eternaltwin Frontend {}",
              self.release.channel.full_name.as_ref()
            )),
            ..Unit::default()
          }),
          slice: Some(Slice {
            resource_control: Some(ResourceControl {
              memory_high: None,
              memory_max: Some(String::from("2G")),
              cpu_quota: Some(String::from("100%")),
              ..ResourceControl::default()
            }),
            ..Slice::default()
          }),
          ..SystemdSliceConfig::default()
        },
      };
      changed = slice
        .run(host)
        .await
        .map_err(UpdateActiveEternaltwinReleaseError::SliceConfigFrontend)?
        .changed
        || changed;
    };
    eprintln!("Generating Systemd service (backend)");
    {
      use crate::task::systemd::config::{
        CommandLine, Config, Exec, Install, ResourceControl, Service, ServiceType, Unit,
      };
      let description = format!(
        "Eternaltwin Frontend ({channel_name})",
        channel_name = self.release.channel.full_name.as_ref()
      );
      let backend_service = format!("{}.service", self.release.channel.backend_service_name());
      let mut ssr_config = SsrConfig::from_channel(&self.release.channel, &self.release.git_ref);
      ssr_config.backend_secret = String::new();
      let ssr_config = serde_json::to_string(&ssr_config).expect("ssr config can be serialized");
      let ssr_config = serde_json::to_string(&ssr_config).expect("ssr config can be escaped");
      let working_dir_str = self
        .release
        .channel
        .paths
        .home
        .join("active/repo/packages/website")
        .display()
        .to_string();
      let node_exe_str = self.release.channel.node.exe.display().to_string();
      let script_str = self
        .release
        .channel
        .paths
        .home
        .join("active/repo/packages/website/dist/eternaltwin/main/main.mjs")
        .display()
        .to_string();
      let exec_start =
        format!("{node_exe_str} {script_str} --config {ssr_config} --backend-secret-url ../../backend.secret.txt");
      let path_env = format!("PATH={}:/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl:/usr/bin:/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin", self.release.channel.node.bin_dir.display());
      let pid_file_str = self
        .release
        .channel
        .paths
        .home
        .join(".local/var/run/eternaltwin_frontend.pid")
        .display()
        .to_string();
      let slice_str = format!("{}.slice", &self.release.channel.frontend_service_name());
      let service_config = Config {
        unit: Some(Unit {
          description: Some(description.as_str()),
          after: vec!["network.target"],
          requires: vec![&backend_service],
          ..Unit::default()
        }),
        service: Some(Service {
          r#type: Some(ServiceType::Exec),
          restart: Some("on-failure"),
          exec_start: Some(CommandLine {
            line: exec_start.as_str(),
          }),
          pid_file: Some(&pid_file_str),
          exec: Exec {
            user: Some(self.release.channel.user.name()),
            working_directory: Some(working_dir_str.as_str()),
            limit_no_file: Some("infinity"),
            limit_nproc: Some("infinity"),
            limit_core: Some("infinity"),
            environment: vec![&path_env, "NODE_ENV=production"],
            ..Exec::default()
          },
          resource_control: ResourceControl {
            slice: Some(&slice_str),
            ..ResourceControl::default()
          },
          ..Service::default()
        }),
        install: Some(Install {
          wanted_by: vec!["multi-user.target"],
          ..Install::default()
        }),
        ..Config::default()
      };
      eprintln!("Writing Systemd unit (backend)");
      changed = EnsureSystemdServiceConfig {
        name: self.release.channel.frontend_service_name(),
        config: service_config,
      }
      .run(host)
      .await
      .map_err(UpdateActiveEternaltwinReleaseError::ServiceConfigFrontend)?
      .changed
        || changed;
    };

    eprintln!("Generating nginx config");
    let nginx_config = get_nginx_config(&self.release.channel);

    eprintln!("Marking release as active");
    EnsureDirSymlink::new(active_release_link.clone())
      .points_to(new_release_dir.clone())
      .owner(uid)
      .group(gid)
      .mode(FileMode::ALL_READ | FileMode::ALL_EXEC)
      .run(host)
      .await
      .map_err(UpdateActiveEternaltwinReleaseError::LinkActive)?;

    eprintln!("Starting service");
    changed = SystemdUnit::new(&frontend_service_name)
      .enabled(true)
      .state(SystemdState::Active)
      .run(host)
      .await
      .map_err(UpdateActiveEternaltwinReleaseError::SystemdEnable)?
      .changed
      || changed;

    eprintln!("Writing nginx config");
    NginxAvailableSite::new(self.release.channel.full_name.as_ref(), nginx_config)
      .run(host)
      .await
      .map_err(UpdateActiveEternaltwinReleaseError::NginxAvailable)?;

    eprintln!("Enabling nginx config");
    NginxEnableSite::new(self.release.channel.full_name.as_ref())
      .run(host)
      .await
      .map_err(UpdateActiveEternaltwinReleaseError::NginxEnable)?;

    Ok(TaskSuccess { changed, output: () })
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct UpgradeEtwinDatabase {
  repo_dir: PathBuf,
  node: NodeEnv,
  yarn: YarnEnv,
  rust: RustEnv,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum UpgradeEternaltwinDatabaseError {
  #[error("failed database synchronization")]
  DbSync(#[source] ExecError),
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for UpgradeEtwinDatabase
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = Result<TaskSuccess<()>, UpgradeEternaltwinDatabaseError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let eternaltwin_bin = self.repo_dir.join("target/release/eternaltwin");
    let eternaltwin_bin = eternaltwin_bin
      .to_str()
      .expect("eternaltwin bin path is a valid string");

    let cmd = Command::new(eternaltwin_bin)
      .prepend_path(self.node.bin_dir.display().to_string())
      .prepend_path(self.rust.bin_dir().display().to_string())
      .arg("db")
      .arg("sync")
      .current_dir(self.repo_dir.display().to_string());
    host.exec(&cmd).map_err(UpgradeEternaltwinDatabaseError::DbSync)?;
    Ok(TaskSuccess {
      changed: true,
      output: (),
    })
  }
}

impl<'h, H> NamedTask<&'h H> for UpgradeEtwinDatabase
where
  H: 'h + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  const NAME: TaskName = TaskName::new("etwin::UpgradeEtwinDatabase");
}

mod config {
  use crate::etwin::etwin::EternaltwinChannel;
  use asys::linux::user::LinuxUser;
  use serde::{Deserialize, Serialize};
  use std::collections::BTreeMap;
  use std::fmt;
  use url::Url;

  #[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
  pub struct SsrConfig {
    pub backend_secret: String,
    pub backend_port: u16,
    pub port: u16,
    pub app_dir: Url,
    pub url: Url,
    pub forum: SsrForumConfig,
    pub is_production: bool,
  }

  impl SsrConfig {
    pub fn from_channel<User, S>(channel: &EternaltwinChannel<User, S>, version: &str) -> Self
    where
      User: LinuxUser,
      S: AsRef<str>,
    {
      let config = Config::from_channel(channel, version);
      let app_dir = channel
        .paths
        .home
        .join("active/repo/packages/website/dist/eternaltwin/app");
      let app_dir = Url::from_directory_path(app_dir).expect("app dir is valid");

      Self {
        backend_secret: config.backend.secret.clone(),
        backend_port: config.backend.port,
        port: config.frontend.port,
        app_dir,
        url: config.frontend.uri.clone(),
        forum: SsrForumConfig {
          threads_per_page: config.frontend.forum_threads_per_page,
          posts_per_page: config.frontend.forum_posts_per_page,
        },
        is_production: true,
      }
    }
  }

  #[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
  pub struct SsrForumConfig {
    pub threads_per_page: u32,
    pub posts_per_page: u32,
  }

  #[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
  pub struct Config {
    pub backend: BackendConfig,
    pub frontend: FrontendConfig,
    pub postgres: PostgresConfig,
    pub mailer: MailerConfig,
    pub scrypt: ScryptConfig,
    pub opentelemetry: OpentelemetryConfig,
    pub seed: SeedConfig,
  }

  impl Config {
    pub fn from_channel<User, S>(channel: &EternaltwinChannel<User, S>, version: &str) -> Self
    where
      User: LinuxUser,
      S: AsRef<str>,
    {
      let protocol = if channel.domain.cert.is_some() { "https" } else { "http" };
      let uri = format!("{}://{}", protocol, channel.domain.main.as_ref());
      Self {
        backend: BackendConfig {
          port: channel.backend_port,
          secret: channel.vault.secret_key.to_string(),
          clock: "System".to_string(),
          mailer: "Network".to_string(),
          store: "Postgres".to_string(),
          oauth_client: "Network".to_string(),
        },
        frontend: FrontendConfig {
          port: channel.main_port,
          uri: Url::parse(&uri).unwrap(),
          forum_posts_per_page: 10,
          forum_threads_per_page: 20,
        },
        postgres: PostgresConfig {
          host: channel.postgres.host.clone(),
          port: channel.postgres.port,
          name: channel.postgres.name.clone(),
          admin_user: channel.postgres.admin.name.to_string(),
          admin_password: channel.postgres.admin.password.clone(),
          user: channel.postgres.main.name.to_string(),
          password: channel.postgres.main.password.clone(),
        },
        mailer: MailerConfig {
          host: "smtp.migadu.com".to_string(),
          username: "contact@eternaltwin.org".to_string(),
          password: channel.vault.mailer.to_string(),
          sender: "contact@eternaltwin.org".to_string(),
        },
        scrypt: ScryptConfig {
          max_time: None,
          max_mem_frac: None,
        },
        opentelemetry: OpentelemetryConfig {
          enabled: true,
          attributes: [
            ("service.name".to_string(), "eternaltwin".to_string()),
            ("service.version".to_string(), version.to_string()),
            (
              "deployment.environment".to_string(),
              channel.short_name.as_ref().to_string(),
            ),
          ]
          .into_iter()
          .collect(),
          exporter: match channel.opentelemetry.clone() {
            Some(otel) => [(
              "uptrace".to_string(),
              OpentelemetryExporterConfig {
                r#type: "Grpc".to_string(),
                color: "Auto".to_string(),
                target: "eternaltwin://stdout".to_string(),
                endpoint: otel.endpoint,
                timeout: "5s".to_string(),
                metadata: [("uptrace-dsn".to_string(), otel.uptrace_dsn)].into_iter().collect(),
              },
            )]
            .into_iter()
            .collect(),
            None => [(
              "stdout".to_string(),
              OpentelemetryExporterConfig {
                r#type: "Human".to_string(),
                color: "Auto".to_string(),
                target: "eternaltwin://stdout".to_string(),
                endpoint: "http://uptrace.localhost/".to_string(),
                timeout: "5s".to_string(),
                metadata: BTreeMap::new(),
              },
            )]
            .into_iter()
            .collect(),
          },
        },
        seed: SeedConfig {
          app: {
            let mut clients = BTreeMap::new();
            clients.insert(
              "brute_production".to_string(),
              SeedAppConfig {
                display_name: "LaBrute".to_string(),
                uri: "https://brute.eternaltwin.org".to_string(),
                oauth_callback: "https://brute.eternaltwin.org/oauth/callback".to_string(),
                secret: channel.vault.brute_production_secret.to_string(),
              },
            );
            clients.insert(
              "brute_staging".to_string(),
              SeedAppConfig {
                display_name: "LaBruteStaging".to_string(),
                uri: "https://staging.brute.eternaltwin.org".to_string(),
                oauth_callback: "https://staging.brute.eternaltwin.org/oauth/callback".to_string(),
                secret: channel.vault.brute_staging_secret.to_string(),
              },
            );
            clients.insert(
              "dinocard_production".to_string(),
              SeedAppConfig {
                display_name: "DinoCard".to_string(),
                uri: "https://dinocard.eternaltwin.org".to_string(),
                oauth_callback: "https://dinocard.eternaltwin.org/oauth/callback".to_string(),
                secret: channel.vault.dinocard_production_secret.to_string(),
              },
            );
            clients.insert(
              "dinocard_staging".to_string(),
              SeedAppConfig {
                display_name: "DinoCardStaging".to_string(),
                uri: "https://staging.dinocard.eternaltwin.org".to_string(),
                oauth_callback: "https://staging.dinocard.eternaltwin.org/oauth/callback".to_string(),
                secret: channel.vault.dinocard_staging_secret.to_string(),
              },
            );
            clients.insert(
              "dinorpg_production".to_string(),
              SeedAppConfig {
                display_name: "DinoRpg".to_string(),
                uri: "https://dinorpg.eternaltwin.org".to_string(),
                oauth_callback: "https://dinorpg.eternaltwin.org/authentication".to_string(),
                secret: channel.vault.dinorpg_production_secret.to_string(),
              },
            );
            clients.insert(
              "dinorpg_staging".to_string(),
              SeedAppConfig {
                display_name: "DinoRpgStaging".to_string(),
                uri: "https://staging.dinorpg.eternaltwin.org".to_string(),
                oauth_callback: "https://staging.dinorpg.eternaltwin.org/authentication".to_string(),
                secret: channel.vault.dinorpg_staging_secret.to_string(),
              },
            );
            clients.insert(
              "directquiz".to_string(),
              SeedAppConfig {
                display_name: "DirectQuiz".to_string(),
                uri: "https://www.directquiz.org/".to_string(),
                oauth_callback: "https://www.directquiz.org/callback.php".to_string(),
                secret: channel.vault.directquiz_secret.to_string(),
              },
            );
            clients.insert(
              "emush_production".to_string(),
              SeedAppConfig {
                display_name: "eMush".to_string(),
                uri: "https://emush.eternaltwin.org".to_string(),
                oauth_callback: "https://emush.eternaltwin.org/oauth/callback".to_string(),
                secret: channel.vault.emush_production_secret.to_string(),
              },
            );
            clients.insert(
              "emush_staging".to_string(),
              SeedAppConfig {
                display_name: "eMushStaging".to_string(),
                uri: "https://staging.emush.eternaltwin.org".to_string(),
                oauth_callback: "https://staging.emush.eternaltwin.org/oauth/callback".to_string(),
                secret: channel.vault.emush_staging_secret.to_string(),
              },
            );
            clients.insert(
              "epopotamo_production".to_string(),
              SeedAppConfig {
                display_name: "ePopotamo".to_string(),
                uri: "https://epopotamo.eternaltwin.org".to_string(),
                oauth_callback: "https://epopotamo.eternaltwin.org/oauth/callback".to_string(),
                secret: channel.vault.epopotamo_production_secret.to_string(),
              },
            );
            clients.insert(
              "epopotamo_staging".to_string(),
              SeedAppConfig {
                display_name: "ePopotamoStaging".to_string(),
                uri: "https://staging.epopotamo.eternaltwin.org".to_string(),
                oauth_callback: "https://staging.epopotamo.eternaltwin.org/oauth/callback".to_string(),
                secret: channel.vault.epopotamo_staging_secret.to_string(),
              },
            );
            clients.insert(
              "eternalfest".to_string(),
              SeedAppConfig {
                display_name: "Eternalfest".to_string(),
                uri: "https://eternalfest.net".to_string(),
                oauth_callback: "https://eternalfest.net/oauth/callback".to_string(),
                secret: channel.vault.eternalfest_production_secret.to_string(),
              },
            );
            clients.insert(
              "eternalfest_staging".to_string(),
              SeedAppConfig {
                display_name: "EternalfestStaging".to_string(),
                uri: "https://staging.eternalfest.net".to_string(),
                oauth_callback: "https://staging.eternalfest.net/oauth/callback".to_string(),
                secret: channel.vault.eternalfest_staging_secret.to_string(),
              },
            );
            clients.insert(
              "kadokadeo_production".to_string(),
              SeedAppConfig {
                display_name: "Kadokadeo".to_string(),
                uri: "https://kadokadeo.eternaltwin.org".to_string(),
                oauth_callback: "https://kadokadeo.eternaltwin.org/oauth/callback".to_string(),
                secret: channel.vault.kadokadeo_production_secret.to_string(),
              },
            );
            clients.insert(
              "kadokadeo_staging".to_string(),
              SeedAppConfig {
                display_name: "KadokadeoStaging".to_string(),
                uri: "https://staging.kadokadeo.eternaltwin.org".to_string(),
                oauth_callback: "https://staging.kadokadeo.eternaltwin.org/oauth/callback".to_string(),
                secret: channel.vault.kadokadeo_staging_secret.to_string(),
              },
            );
            clients.insert(
              "kingdom_production".to_string(),
              SeedAppConfig {
                display_name: "Kingdom".to_string(),
                uri: "https://kingdom.eternaltwin.org".to_string(),
                oauth_callback: "https://kingdom.eternaltwin.org/oauth/callback".to_string(),
                secret: channel.vault.kingdom_production_secret.to_string(),
              },
            );
            clients.insert(
              "kingdom_staging".to_string(),
              SeedAppConfig {
                display_name: "KingdomStaging".to_string(),
                uri: "https://staging.kingdom.eternaltwin.org".to_string(),
                oauth_callback: "https://staging.kingdom.eternaltwin.org/oauth/callback".to_string(),
                secret: channel.vault.kingdom_staging_secret.to_string(),
              },
            );
            clients.insert(
              "mjrt".to_string(),
              SeedAppConfig {
                display_name: "Mjrt".to_string(),
                uri: "https://mjrt.eternal-twin.net".to_string(),
                oauth_callback: "https://mjrt.eternal-twin.net/oauth/callback".to_string(),
                secret: channel.vault.mjrt_secret.to_string(),
              },
            );
            clients.insert(
              "myhordes".to_string(),
              SeedAppConfig {
                display_name: "MyHordes".to_string(),
                uri: "https://myhordes.eu/".to_string(),
                oauth_callback: "https://myhordes.eu/twinoid".to_string(),
                secret: channel.vault.myhordes_secret.to_string(),
              },
            );
            clients.insert(
              "neoparc_production".to_string(),
              SeedAppConfig {
                display_name: "NeoParc".to_string(),
                uri: "https://neoparc.eternaltwin.org".to_string(),
                oauth_callback: "https://neoparc.eternaltwin.org/api/account/callback".to_string(),
                secret: channel.vault.neoparc_production_secret.to_string(),
              },
            );
            clients.insert(
              "neoparc_staging".to_string(),
              SeedAppConfig {
                display_name: "NeoParcStaging".to_string(),
                uri: "https://staging.neoparc.eternaltwin.org".to_string(),
                oauth_callback: "https://staging.neoparc.eternaltwin.org/api/account/callback".to_string(),
                secret: channel.vault.neoparc_staging_secret.to_string(),
              },
            );
            clients.insert(
              "wiki".to_string(),
              SeedAppConfig {
                display_name: "Eternal Twinpedia".to_string(),
                uri: "https://wiki.eternal-twin.net".to_string(),
                oauth_callback: "https://wiki.eternal-twin.net/oauth/callback".to_string(),
                secret: channel.vault.wiki_secret.to_string(),
              },
            );
            clients
          },
          forum_section: {
            let mut sections = BTreeMap::new();
            sections.insert(
              "en_main".to_string(),
              SeedForumSectionConfig {
                display_name: "Main Forum (en-US)".to_string(),
                locale: "en-US".to_string(),
              },
            );
            sections.insert(
              "fr_main".to_string(),
              SeedForumSectionConfig {
                display_name: "Forum Général (fr-FR)".to_string(),
                locale: "fr-FR".to_string(),
              },
            );
            sections.insert(
              "es_main".to_string(),
              SeedForumSectionConfig {
                display_name: "Foro principal (es-SP)".to_string(),
                locale: "es-SP".to_string(),
              },
            );
            sections.insert(
              "de_main".to_string(),
              SeedForumSectionConfig {
                display_name: "Hauptforum (de-DE)".to_string(),
                locale: "de-DE".to_string(),
              },
            );
            sections.insert(
              "eo_main".to_string(),
              SeedForumSectionConfig {
                display_name: "Ĉefa forumo (eo)".to_string(),
                locale: "eo".to_string(),
              },
            );
            sections.insert(
              "eternalfest_main".to_string(),
              SeedForumSectionConfig {
                display_name: "[Eternalfest] Le Panthéon".to_string(),
                locale: "fr-FR".to_string(),
              },
            );
            sections.insert(
              "emush_main".to_string(),
              SeedForumSectionConfig {
                display_name: "[Emush] Neron is watching you".to_string(),
                locale: "fr-FR".to_string(),
              },
            );
            sections.insert(
              "drpg_main".to_string(),
              SeedForumSectionConfig {
                display_name: "[DinoRPG] Jurassic Park".to_string(),
                locale: "fr-FR".to_string(),
              },
            );
            sections.insert(
              "myhordes_main".to_string(),
              SeedForumSectionConfig {
                display_name: "[Myhordes] Le Saloon".to_string(),
                locale: "fr-FR".to_string(),
              },
            );
            sections.insert(
              "kadokadeo_main".to_string(),
              SeedForumSectionConfig {
                display_name: "[Kadokadeo] Café des palabres".to_string(),
                locale: "fr-FR".to_string(),
              },
            );
            sections.insert(
              "kingdom_main".to_string(),
              SeedForumSectionConfig {
                display_name: "[Kingdom] La foire du trône".to_string(),
                locale: "fr-FR".to_string(),
              },
            );
            sections.insert(
              "mjrt_main".to_string(),
              SeedForumSectionConfig {
                display_name: "[Mjrt] La bergerie".to_string(),
                locale: "fr-FR".to_string(),
              },
            );
            sections.insert(
              "na_main".to_string(),
              SeedForumSectionConfig {
                display_name: "[Naturalchimie] Le laboratoire".to_string(),
                locale: "fr-FR".to_string(),
              },
            );
            sections.insert(
              "sq_main".to_string(),
              SeedForumSectionConfig {
                display_name: "[Studioquiz] Le bar à questions".to_string(),
                locale: "fr-FR".to_string(),
              },
            );
            sections.insert(
              "ts_main".to_string(),
              SeedForumSectionConfig {
                display_name: "[Teacher Story] La salle des profs".to_string(),
                locale: "fr-FR".to_string(),
              },
            );
            sections.insert(
              "ts_popotamo".to_string(),
              SeedForumSectionConfig {
                display_name: "[Popotamo] Le mot le plus long".to_string(),
                locale: "fr-FR".to_string(),
              },
            );
            sections
          },
        },
      }
    }
  }

  impl fmt::Display for Config {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
      writeln!(f, "{}", toml::to_string_pretty(&self).unwrap())
    }
  }

  #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
  pub struct BackendConfig {
    pub port: u16,
    pub secret: String,
    pub clock: String,
    pub mailer: String,
    pub store: String,
    pub oauth_client: String,
  }

  #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
  pub struct FrontendConfig {
    pub port: u16,
    pub uri: Url,
    pub forum_posts_per_page: u32,
    pub forum_threads_per_page: u32,
  }

  #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
  pub struct PostgresConfig {
    pub host: String,
    pub port: u16,
    pub name: String,
    pub admin_user: String,
    pub admin_password: String,
    pub user: String,
    pub password: String,
  }

  #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
  pub struct MailerConfig {
    pub host: String,
    pub username: String,
    pub password: String,
    pub sender: String,
  }

  #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
  pub struct ScryptConfig {
    pub max_time: Option<String>,
    pub max_mem_frac: Option<String>,
  }

  #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
  pub struct OpentelemetryConfig {
    pub enabled: bool,
    pub attributes: BTreeMap<String, String>,
    pub exporter: BTreeMap<String, OpentelemetryExporterConfig>,
  }

  #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
  // todo: this should be a struct ideally; but it's also passed through processes the serialization
  //       there does not support tagged enums
  pub struct OpentelemetryExporterConfig {
    pub r#type: String,
    pub color: String,
    pub target: String,
    pub endpoint: String,
    pub timeout: String,
    pub metadata: BTreeMap<String, String>,
  }

  #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
  pub struct SeedConfig {
    pub app: BTreeMap<String, SeedAppConfig>,
    pub forum_section: BTreeMap<String, SeedForumSectionConfig>,
  }

  #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
  pub struct SeedAppConfig {
    pub display_name: String,
    pub uri: String,
    pub oauth_callback: String,
    pub secret: String,
  }

  #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
  pub struct SeedForumSectionConfig {
    pub display_name: String,
    pub locale: String,
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
struct EternaltwinFrontendService<S: AsRef<str> = String> {
  channel_name: S,
  backend_service: S,
  user: S,
  pid_file: PathBuf,
  slice: S,
  working_dir: PathBuf,
  config: config::SsrConfig,
  // PnP was disabled as it's not well support by Angular
  // See <https://github.com/angular/angular-cli/issues/16980>
  // pnp_cjs: PathBuf,
  // pnp_mjs: PathBuf,
  // ExecStart={node_exe} --require {pnp_cjs} --experimental-loader {pnp_mjs} {script}
  script: PathBuf,
  node_env: NodeEnv,
  yarn_env: YarnEnv,
}

pub fn get_nginx_config<S: AsRef<str>>(channel: &EternaltwinChannel<impl LinuxUser, S>) -> String {
  let upstream_name = channel.full_name.as_ref().to_string().replace('.', "_");
  let backend_upstream_name = format!("{upstream_name}_backend");
  let access_log = channel.paths.logs.join("main.nginx.access.log");
  let error_log = channel.paths.logs.join("main.nginx.error.log");
  let static_dir = channel.paths.home.join("active/repo/packages/website/app/browser");

  let fallback_server = format!(
    r#"# Fallback server
server {{
  # HTTP port
  listen {fallback_port};
  listen [::]:{fallback_port};

  # Hide nginx version in `Server` header.
  server_tokens off;
  add_header Referrer-Policy same-origin;
  add_header X-Content-Type-Options nosniff;
  add_header X-XSS-Protection "1; mode=block";

  index index.html;

  root {fallback_dir};
}}
"#,
    fallback_port = channel.fallback_server.port,
    fallback_dir = channel.fallback_server.dir.to_str().unwrap(),
  );

  let backend_upstream_server = format!(
    r#"# Define target of the reverse-proxy (backend)
upstream {backend_upstream_name} {{
  server [::1]:{port};
  server [::1]:{fallback_port} backup;
  keepalive 8;
}}
"#,
    backend_upstream_name = backend_upstream_name,
    port = channel.backend_port,
    fallback_port = channel.fallback_server.port,
  );

  let upstream_server = format!(
    r#"# Define target of the reverse-proxy
upstream {upstream_name} {{
  server [::1]:{port};
  server [::1]:{fallback_port} backup;
  keepalive 8;
}}
"#,
    upstream_name = upstream_name,
    port = channel.main_port,
    fallback_port = channel.fallback_server.port,
  );

  let main_directives = format!(
    r#"  # Space-separated hostnames (wildcard * is accepted)
  server_name {domain};

  access_log {access_log};
  error_log {error_log};
  client_max_body_size 100M;

  root {static_dir};

  location /assets/app {{
    alias /var/www/etwin_app/;
  }}

  location ~* ^/(?:api|oauth|actions|v1)(?:/|$) {{
    # Upstream reference
    proxy_pass http://{backend_upstream_name};

    # Configuration of the proxy
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection 'upgrade';
    proxy_set_header Host $host;
    proxy_set_header X-NginX-Proxy true;
    proxy_cache_bypass $http_upgrade;
    proxy_redirect off;
  }}

  location / {{
    try_files $uri @etwinproxy;
    gzip_static on;
  }}

  location @etwinproxy {{
    # Upstream reference
    proxy_pass http://{upstream_name};

    # Configuration of the proxy
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection 'upgrade';
    proxy_set_header Host $host;
    proxy_set_header X-NginX-Proxy true;
    proxy_cache_bypass $http_upgrade;
    proxy_redirect off;
  }}
"#,
    domain = channel.domain.main.as_ref(),
    access_log = access_log.to_str().unwrap(),
    error_log = error_log.to_str().unwrap(),
    static_dir = static_dir.to_str().unwrap(),
    backend_upstream_name = backend_upstream_name,
    upstream_name = upstream_name,
  );

  let cert_files = match channel.domain.cert.as_ref() {
    Some(https) => format!(
      r#"  ssl_certificate {cert};
  ssl_certificate_key {cert_key};"#,
      cert = https.fullchain_cert_path.to_str().unwrap(),
      cert_key = https.cert_key_path.to_str().unwrap(),
    ),
    None => String::new(),
  };

  let legacy = if channel.domain.legacy.is_empty() {
    String::new()
  } else {
    let legacy: Vec<&str> = channel.domain.legacy.iter().map(|l| l.as_ref()).collect();
    let https_port = if channel.domain.cert.is_some() {
      "listen 443 ssl;\n  listen [::]:443 ssl;"
    } else {
      ""
    };
    format!(
      r#"# Legacy
server {{
  listen 80;
  listen [::]:80;
  {https_port}

  server_name {legacy};

  {cert_files}

  return 308 https://{domain}$request_uri;
}}
"#,
      legacy = legacy.join(" "),
      domain = channel.domain.main.as_ref(),
    )
  };

  match channel.domain.cert.as_ref() {
    Some(_) => format!(
      r#"# This configuration sets a reverse proxy for a specific domain

{fallback_server}

{backend_upstream_server}

{upstream_server}

# HTTPS
server {{
  # HTTPS port
  listen 443 ssl;
  listen [::]:443 ssl;

  {cert_files}

  # # HSTS (ngx_http_headers_module is required) (63072000 seconds)
  # add_header Strict-Transport-Security "max-age=63072000" always;

  {main_directives}
}}

# HTTP
server {{
  # HTTP port
  listen 80;
  listen [::]:80;
  server_name {domain};
  return 301 https://$host$request_uri;
}}

# www.
server {{
  listen 80;
  listen [::]:80;
  listen 443 ssl;
  listen [::]:443 ssl;

  server_name www.{domain};

  {cert_files}

  return 307 https://{domain}$request_uri;
}}

{legacy}

"#,
      fallback_server = fallback_server,
      backend_upstream_server = backend_upstream_server,
      upstream_server = upstream_server,
      main_directives = main_directives,
      domain = channel.domain.main.as_ref(),
      cert_files = cert_files,
    ),
    None => format!(
      r#"# This configuration sets a reverse proxy for a specific domain

{fallback_server}

{backend_upstream_server}

{upstream_server}

# HTTP
server {{
  # HTTP port
  listen 80;
  listen [::]:80;

  {main_directives}
}}

# www.
server {{
  listen 80;
  listen [::]:80;

  server_name www.{domain};

  return 307 http://{domain}$request_uri;
}}

{legacy}

"#,
      fallback_server = fallback_server,
      backend_upstream_server = backend_upstream_server,
      upstream_server = upstream_server,
      main_directives = main_directives,
      domain = channel.domain.main.as_ref(),
    ),
  }
}
