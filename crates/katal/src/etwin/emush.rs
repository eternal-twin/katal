use crate::etwin::channel::{
  ChannelDomain, ChannelFallbackServer, ChannelPaths, ChannelPhp, ChannelPostgres, ChannelTarget, NodeEnv,
  PostgresCredentials,
};
use crate::task::fs::{EnsureDir, EnsureDirError, EnsureDirSymlink, EnsureFile};
use crate::task::git::GitCheckout;
use crate::task::nginx::{NginxAvailableSite, NginxEnableSite};
use crate::task::postgres::PostgresUrl;
use crate::task::witness::{FsWitness, FsWitnessError};
use crate::task::yarn::YarnEnv;
use crate::task::{exec_as, AsyncFn, ExecAsError, NamedTask, TaskName, TaskRef, TaskResult, TaskSuccess};
use crate::utils::display_error_chain::DisplayErrorChainExt;
use async_trait::async_trait;
use asys::common::fs::{FsHost, ReadFileError};
use asys::linux::fs::{FileMode, LinuxFsHost, LinuxMetadata, ResolveError};
use asys::linux::user::{GroupRef, LinuxUser, LinuxUserHost, UserRef};
use asys::local_linux::LocalLinux;
use asys::{Command, ExecHost};
use katal_nginx::{LocationDirective, LocationPattern, RawFragment};
use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};
use std::fmt;
use std::fmt::Debug;
use std::path::PathBuf;
use url::Url;

const SKIP_BUILD: bool = false;
const PG_VERSION: u8 = 14;

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct EmushChannelTarget<S: AsRef<str> = String> {
  pub inner: ChannelTarget<S>,
  pub vault: EmushVault,
  pub etwin_uri: Url,
  pub etwin_client_id: String,
  pub admin_user_id: Option<String>,
  pub app_name: String,
  pub otel_endpoint: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct EmushVault {
  pub etwin_client_secret: String,
  pub app_secret: String,
  pub access_passphrase: String,
  pub discord_webhook: Option<String>,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct EmushChannel<User: LinuxUser, S: AsRef<str> = String> {
  full_name: S,
  short_name: S,
  user: User,
  domain: ChannelDomain<S>,
  paths: ChannelPaths,
  fallback_server: ChannelFallbackServer,
  postgres: ChannelPostgres,
  node: NodeEnv,
  yarn: YarnEnv,
  php: ChannelPhp,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct EmushReleaseTarget<S: AsRef<str> = String> {
  pub channel: EmushChannelTarget,
  pub repository: S,
  pub git_ref: String,
  /// Flag indicating that the DB was reset recently and needs to be initialized
  pub is_after_db_reset: bool,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct EmushRelease<User: LinuxUser, S: AsRef<str> = String> {
  release_dir: PathBuf,
  repository: S,
  repo_dir: PathBuf,
  channel: EmushChannel<User, S>,
  git_ref: String,
  /// Flag indicating that the DB was reset recently and needs to be initialized
  is_after_db_reset: bool,
}

#[derive(Serialize, Deserialize)]
pub struct DeployEmush<S: AsRef<str> = String> {
  pub pass: S,
  pub target: EmushReleaseTarget,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum DeployEmushError {
  #[error("failed to exec emush deployment task")]
  Exec(#[source] ExecAsError),
  #[error("failed to deploy emush release")]
  Deploy(#[source] DeployEmushAsRootError),
}

#[async_trait]
impl<'h, H: ExecHost, S: AsRef<str> + Send + Sync> AsyncFn<&'h H> for DeployEmush<S> {
  type Output = TaskResult<(), DeployEmushError>;

  async fn run(&self, _host: &'h H) -> Self::Output {
    let input = DeployEmushAsRoot {
      target: self.target.clone(),
    };
    exec_as::<LocalLinux, DeployEmushAsRoot>(UserRef::ROOT, Some(self.pass.as_ref()), &input)
      .await
      .map_err(DeployEmushError::Exec)?
      .map_err(DeployEmushError::Deploy)
  }
}

impl<'h, H: 'h + ExecHost> NamedTask<&'h H> for DeployEmush {
  const NAME: TaskName = TaskName::new("emush::DeployEmush");
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DeployEmushAsRoot {
  target: EmushReleaseTarget,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum DeployEmushAsRootError {
  #[error("failed to use witness file")]
  Witness(#[source] FsWitnessError),
  #[error("failed to run task `EmushReleaseTarget`")]
  Task(#[source] EmushReleaseTargetError),
}

#[async_trait]
impl<'h, H: ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for DeployEmushAsRoot
where
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Debug + Clone + Send + Sync + Serialize + DeserializeOwned,
  H::Group: Debug + Send + Sync,
{
  type Output = TaskResult<(), DeployEmushAsRootError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let paths: ChannelPaths = self.target.channel.inner.paths();
    let witness = paths.home.join("emush_release.witness");
    let mut changed = false;
    let res = FsWitness::new(witness, TaskRef(&self.target))
      .run(host)
      .await
      .map_err(DeployEmushAsRootError::Witness)?;
    changed = res.changed || changed;
    let res = res.output.map_err(DeployEmushAsRootError::Task)?;
    changed = res.changed || changed;
    Ok(TaskSuccess { changed, output: () })
  }
}

impl<'h, H> NamedTask<&'h H> for DeployEmushAsRoot
where
  H: 'h + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Debug + Clone + Send + Sync + Serialize + DeserializeOwned,
  H::Group: Debug + Send + Sync,
{
  const NAME: TaskName = TaskName::new("emush::DeployEmushAsRoot");
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum EmushReleaseTargetError {
  #[error("failed to ensure release directory")]
  ReleaseDir(#[source] EnsureDirError),
  #[error("failed to execute build task")]
  ExecBuild(#[source] ExecAsError),
  #[error("unexpected error: {0}")]
  Other(String),
}

impl From<anyhow::Error> for EmushReleaseTargetError {
  fn from(value: anyhow::Error) -> Self {
    Self::Other(value.display_chain().to_string())
  }
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for EmushReleaseTarget
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Debug + Clone + Send + Sync,
  H::Group: Debug + Send + Sync,
{
  type Output = TaskResult<EmushRelease<H::User>, EmushReleaseTargetError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let res = self.channel.run(host).await?;
    let channel = res.output;
    let mut changed = res.changed;

    let release_dir = channel.paths.releases.join(&self.git_ref);

    eprintln!("Create directory: {}", release_dir.display());
    changed = EnsureDir::new(release_dir.clone())
      .owner(UserRef::Id(channel.user.uid()))
      .group(GroupRef::Id(channel.user.gid()))
      .mode(FileMode::OWNER_ALL | FileMode::ALL_READ | FileMode::ALL_EXEC)
      .run(host)
      .await
      .map_err(EmushReleaseTargetError::ReleaseDir)?
      .changed
      || changed;

    let witness = release_dir.join("ok");

    let witness_exists = match host.read_file(witness.as_path()) {
      Ok(_) => true,
      Err(ReadFileError::NotFound) => false,
      Err(e) => {
        panic!("failed to check witness file: {e:?}")
      }
    };

    let repo_dir = release_dir.join("repo");

    let protocol = if channel.domain.cert.is_some() { "https" } else { "http" };
    let uri = format!("{}://{}", protocol, channel.domain.main.as_str());

    let release = EmushRelease {
      release_dir,
      repository: self.repository.clone(),
      repo_dir: repo_dir.clone(),
      channel: channel.clone(),
      git_ref: self.git_ref.clone(),
      is_after_db_reset: self.is_after_db_reset,
    };

    if witness_exists {
      eprintln!(
        "bailing-out, commit already downloaded (assuming the release already completed) (todo: better tracking)"
      );
      return Ok(TaskSuccess {
        changed,
        output: release,
      });
    }

    let br = BuildEmushRelease {
      node: channel.node.clone(),
      yarn: channel.yarn.clone(),
      repo_dir: repo_dir.clone(),
      remote: self.repository.clone(),
      git_ref: self.git_ref.clone(),
      backend_config: BackendConfig {
        app_secret: self.channel.vault.app_secret.to_string(),
        db_credentials: channel.postgres.main.clone(),
        db_name: channel.postgres.name.clone(),
        db_version: PG_VERSION,
        access_passphrase: self.channel.vault.access_passphrase.clone(),
        identity_server: self.channel.etwin_uri.clone(),
        oauth_callback: Url::parse(format!("{}/oauth/callback", uri.as_str()).as_str()).expect("uri is well formed"),
        oauth_client_id: self.channel.etwin_client_id.clone(),
        oauth_client_secret: self.channel.vault.etwin_client_secret.clone(),
        admin_user_id: self.channel.admin_user_id.clone(),
        discord_webhook: self.channel.vault.discord_webhook.clone(),
        channel_name: self.channel.inner.full_name.clone(),
        app_name: self.channel.app_name.clone(),
        otel_endpoint: self.channel.otel_endpoint.clone(),
      },
      frontend_config: FrontendConfig {
        vite_app_i18n_locale: "fr".to_string(),
        vite_app_i18n_fallback_locale: "en".to_string(),
        vite_app_uri: Url::parse(uri.as_str()).expect("uri is well formed"),
        vite_app_api_uri: Url::parse(format!("{}/api/v1/", uri.as_str()).as_str()).expect("uri is well formed"),
        vite_app_oauth_uri: Url::parse(format!("{}/oauth", uri.as_str()).as_str()).expect("uri is well formed"),
        vite_app_api_release_commit: self.git_ref.clone().to_string(),
        vite_app_api_release_channel: channel.full_name.clone(),
      },
    };

    if !SKIP_BUILD {
      changed = exec_as::<LocalLinux, _>(UserRef::Id(channel.user.uid()), None, &br)
        .await
        .map_err(EmushReleaseTargetError::ExecBuild)?
        .unwrap()
        .changed
        || changed;
    }

    changed = (UpdateActiveEmushRelease {
      release: release.clone(),
    })
    .run(host)
    .await?
    .changed
      || changed;

    // eprintln!("Reapply Postgres permissions");
    // changed = PostgresDbPermissions::new(
    //   &release.channel.postgres.name,
    //   release.channel.postgres.admin.name.clone(),
    //   release.channel.postgres.main.name.clone(),
    //   release.channel.postgres.read.name.clone(),
    // )
    // .run(host)
    // .await?
    // .changed
    //   || changed;

    changed = EnsureFile::new(witness).content([]).run(host).await.unwrap().changed || changed;

    Ok(TaskSuccess {
      changed,
      output: release,
    })
  }
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for EmushChannelTarget
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  H::Group: Debug + Send + Sync,
  H::User: Debug + Clone + Send + Sync,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<EmushChannel<H::User>, anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    eprintln!("eMush: Create channel");
    let res = self.inner.run(host).await?;

    eprintln!("Channel ready");
    let output = EmushChannel {
      full_name: res.output.full_name,
      short_name: res.output.short_name,
      user: res.output.user,
      domain: res.output.domain.expect("Expected associated domain name"),
      paths: res.output.paths,
      fallback_server: res.output.fallback_server.expect("Expected fallback server"),
      postgres: res.output.postgres.expect("Expected Postgres database"),
      node: res.output.node.expect("Expected Node environment"),
      yarn: res.output.yarn.expect("Expected Yarn environment"),
      php: res.output.php.expect("Expected PHP environment"),
    };

    Ok(TaskSuccess {
      changed: res.changed,
      output,
    })
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct BuildEmushRelease {
  node: NodeEnv,
  yarn: YarnEnv,
  repo_dir: PathBuf,
  remote: String,
  git_ref: String,
  backend_config: BackendConfig,
  frontend_config: FrontendConfig,
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for BuildEmushRelease
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Send + Sync,
{
  type Output = Result<TaskSuccess<()>, ()>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let gc = GitCheckout {
      path: self.repo_dir.clone(),
      remote: self.remote.clone(),
      r#ref: self.git_ref.clone(),
    };

    eprintln!("Checkout repository: {:?} {:?}", self.remote, self.git_ref);
    let res = gc.run(host).await.unwrap();
    let mut changed = res.changed;

    let api_dir = self.repo_dir.join("Api");
    let app_dir = self.repo_dir.join("App");
    let jwt_dir = self.repo_dir.join("Api/config/jwt");

    eprintln!("Install PHP dependencies");
    let cmd = Command::new("composer")
      .arg("install")
      .arg("--no-interaction")
      .arg("--no-ansi")
      .arg("--classmap-authoritative")
      .current_dir(api_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    eprintln!("Install Node dependencies");
    let cmd = Command::new("yarn")
      .arg("install")
      .prepend_path(self.node.bin_dir.display().to_string())
      .prepend_path(self.yarn.bin_dir.display().to_string())
      .current_dir(app_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    eprintln!("Create directory: {}", jwt_dir.display());
    changed = EnsureDir::new(jwt_dir.clone()).run(host).await.map_err(drop)?.changed || changed;

    eprintln!("Refresh JWT private key");
    let cmd = Command::new("openssl")
      .arg("genrsa")
      .arg("-out")
      .arg("private.pem")
      .current_dir(jwt_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    eprintln!("Refresh JWT public key");
    let cmd = Command::new("openssl")
      .arg("rsa")
      .arg("-in")
      .arg("private.pem")
      .arg("-out")
      .arg("public.pem")
      .arg("-pubout")
      .current_dir(jwt_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    eprintln!("Apply backend config");
    EnsureFile::new(api_dir.join(".env.local"))
      .content(get_backend_config(&self.backend_config))
      .mode(FileMode::OWNER_READ | FileMode::OWNER_WRITE)
      .run(host)
      .await
      .unwrap();

    eprintln!("Configure the frontend build");
    EnsureFile::new(app_dir.join(".env"))
      .content(self.frontend_config.to_string())
      .mode(FileMode::OWNER_READ | FileMode::OWNER_WRITE)
      .run(host)
      .await
      .unwrap();

    eprintln!("Build the frontend");
    let cmd = Command::new("yarn")
      .arg("run")
      .arg("build")
      .prepend_path(self.node.bin_dir.display().to_string())
      .prepend_path(self.yarn.bin_dir.display().to_string())
      .env("NODE_ENV", "production")
      .current_dir(app_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    Ok(TaskSuccess { changed, output: () })
  }
}

impl<'h, H> NamedTask<&'h H> for BuildEmushRelease
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Send + Sync,
{
  const NAME: TaskName = TaskName::new("emush::BuildEmushRelease");
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize)]
pub struct UpdateActiveEmushRelease<User: LinuxUser, S: AsRef<str> = String> {
  release: EmushRelease<User, S>,
}

#[async_trait]
impl<'h, H, S> AsyncFn<&'h H> for UpdateActiveEmushRelease<H::User, S>
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Send + Sync,
  S: AsRef<str> + Send + Sync,
{
  type Output = TaskResult<(), anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let mut changed = false;
    let active_release_link = self.release.channel.paths.home.join("active");
    let previous_release_link = self.release.channel.paths.home.join("previous");
    let old_release_dir = {
      match host.resolve(&active_release_link) {
        Ok(old) => Some(old),
        Err(ResolveError::NotFound) => None,
        Err(e) => return Err(e.into()),
      }
    };
    let new_release_dir = host.resolve(&self.release.release_dir)?;

    if let Some(old_release_dir) = old_release_dir {
      if old_release_dir != new_release_dir {
        eprintln!("Start to disable old release");
        // There is already an active release but it does not match the target
        // release: stop the old version before proceeding to free the channel
        // resources (database, port)
        EnsureDirSymlink::new(previous_release_link.clone())
          .points_to(old_release_dir.clone())
          .owner(self.release.channel.user.uid())
          .group(self.release.channel.user.gid())
          .mode(FileMode::ALL_READ | FileMode::ALL_EXEC)
          .run(host)
          .await?;

        eprintln!("Old release disabled");
        changed = true;
      }
    }

    eprintln!("UpgradingDatabase");
    changed = exec_as::<LocalLinux, _>(
      UserRef::Id(self.release.channel.user.uid()),
      None,
      &UpgradeEmushDatabase {
        repo_dir: self.release.repo_dir.clone(),
        db_name: self.release.channel.postgres.name.clone(),
        db_admin: self.release.channel.postgres.admin.clone(),
        is_after_db_reset: self.release.is_after_db_reset,
      },
    )
    .await?
    .unwrap()
    .changed
      || changed;

    eprintln!("Generating nginx config");
    let nginx_config = get_nginx_config(&self.release.channel);
    eprintln!("Writing nginx config");
    NginxAvailableSite::new(self.release.channel.full_name.as_ref(), nginx_config)
      .run(host)
      .await?;
    eprintln!("Enabling nginx config");
    NginxEnableSite::new(self.release.channel.full_name.as_ref())
      .run(host)
      .await?;

    eprintln!("Marking release as active");
    EnsureDirSymlink::new(active_release_link.clone())
      .points_to(new_release_dir.clone())
      .owner(self.release.channel.user.uid())
      .group(self.release.channel.user.gid())
      .mode(FileMode::ALL_READ | FileMode::ALL_EXEC)
      .run(host)
      .await?;

    Ok(TaskSuccess { changed, output: () })
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct UpgradeEmushDatabase {
  repo_dir: PathBuf,
  db_name: String,
  db_admin: PostgresCredentials,
  is_after_db_reset: bool,
}

#[async_trait]
impl<'h, H: ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for UpgradeEmushDatabase
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = Result<TaskSuccess<()>, ()>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let api_dir = self.repo_dir.join("Api");
    let console_bin = api_dir.join("bin/console");

    let admin_db_url = PostgresUrl::new(
      "localhost",
      5432,
      self.db_name.as_str(),
      self.db_admin.name.as_str(),
      self.db_admin.password.as_str(),
      Some(PG_VERSION),
      None,
    )
    .unwrap()
    .to_string();

    let cmd = Command::new(console_bin.to_str().unwrap())
      .arg("mush:migrate")
      .env("DATABASE_URL", admin_db_url)
      .current_dir(api_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    Ok(TaskSuccess {
      changed: true,
      output: (),
    })
  }
}

impl<'h, H: 'h + ExecHost + LinuxUserHost + LinuxFsHost> NamedTask<&'h H> for UpgradeEmushDatabase
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  const NAME: TaskName = TaskName::new("emush::UpgradeEmushDatabase");
}

pub fn get_nginx_config<U: LinuxUser, S: AsRef<str>>(channel: &EmushChannel<U, S>) -> String {
  let access_log = channel.paths.logs.join("main.nginx.access.log");
  let error_log = channel.paths.logs.join("main.nginx.error.log");
  let web_root = channel.paths.home.join("active/repo/Api/public");
  let static_root = channel.paths.home.join("active/repo/App/dist");

  let location_api = LocationDirective {
    pattern: LocationPattern::Regex {
      text: r#"^/(?:api/v1|oauth)(?:/|$)"#,
      case_sensitive: true,
    },
    body: RawFragment {
      text: r#"
    # try to serve file directly, fallback to index.php
    try_files $uri /index.php$is_args$args;
    "#,
    },
    indent: None,
  };

  let location_root_php = LocationDirective {
    pattern: LocationPattern::Regex {
      text: r#"^/index\.php(/|$)"#,
      case_sensitive: true,
    },
    body: RawFragment {
      text: &format!(
        r#"
    fastcgi_pass unix:{php_fpm_socket};
    fastcgi_split_path_info ^(.+\.php)(/.*)$;
    include fastcgi_params;

    # optionally set the value of the environment variables used in the application
    # fastcgi_param APP_ENV prod;
    # fastcgi_param APP_SECRET <app-secret-id>;
    # fastcgi_param DATABASE_URL "mysql://db_user:db_pass@host:3306/db_name";

    # When you are using symlinks to link the document root to the
    # current version of your application, you should pass the real
    # application path instead of the path to the symlink to PHP
    # FPM.
    # Otherwise, PHP's OPcache may not properly detect changes to
    # your PHP files (see https://github.com/zendtech/ZendOptimizerPlus/issues/126
    # for more information).
    fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
    fastcgi_param DOCUMENT_ROOT $realpath_root;
    # Prevents URIs that include the front controller. This will 404:
    # http://domain.tld/index.php/some-path
    # Remove the internal directive to allow URIs like this
    internal;
    "#,
        php_fpm_socket = channel.php.socket.to_str().unwrap(),
      ),
    },
    indent: None,
  };

  let location_php = LocationDirective {
    pattern: LocationPattern::Regex {
      text: r#"\.php$"#,
      case_sensitive: true,
    },
    body: RawFragment {
      text: r#"
    return 404;
    "#,
    },
    indent: None,
  };

  let location_default = LocationDirective {
    pattern: LocationPattern::WeakPrefix("/"),
    body: RawFragment {
      text: &format!(
        r#"
    root {static_root};
    index index.html;
    try_files $uri /index.html;
    "#,
        static_root = static_root.to_str().unwrap(),
      ),
    },
    indent: None,
  };

  let main_directives = format!(
    r#"  # Space-separated hostnames (wildcard * is accepted)
  server_name {domain};

  access_log {access_log};
  error_log {error_log};
  client_max_body_size 5M;

  root {web_root};

  {location_api}

  {location_root_php}

  # return 404 for all other php files not matching the front controller
  # this prevents access to other php files you don't want to be accessible.
  {location_php}

  {location_default}
"#,
    domain = channel.domain.main.as_ref(),
    access_log = access_log.to_str().unwrap(),
    error_log = error_log.to_str().unwrap(),
    web_root = web_root.to_str().unwrap(),
  );

  match channel.domain.cert.as_ref() {
    Some(https) => format!(
      r#"# HTTPS
server {{
  # HTTPS port
  listen 443 ssl;
  listen [::]:443 ssl;

  ssl_certificate {cert};
  ssl_certificate_key {cert_key};

  # # HSTS (ngx_http_headers_module is required) (63072000 seconds)
  # add_header Strict-Transport-Security "max-age=63072000" always;

  {main_directives}
}}

# HTTP
server {{
  # HTTP port
  listen 80;
  listen [::]:80;
  server_name {domain};
  return 301 https://$host$request_uri;
}}

# www.
server {{
  listen 80;
  listen 443 ssl;
  listen [::]:80;
  listen [::]:443 ssl;

  server_name www.{domain};

  ssl_certificate {cert};
  ssl_certificate_key {cert_key};

  return 307 https://{domain}$request_uri;
}}

"#,
      main_directives = main_directives,
      domain = channel.domain.main.as_ref(),
      cert = https.fullchain_cert_path.to_str().unwrap(),
      cert_key = https.cert_key_path.to_str().unwrap(),
    ),
    None => format!(
      r#"# HTTP
server {{
  # HTTP port
  listen 80;
  listen [::]:80;

  {main_directives}
}}

# www.
server {{
  listen 80;
  listen [::]:80;

  server_name www.{domain};

  return 307 http://{domain}$request_uri;
}}

"#,
      main_directives = main_directives,
      domain = channel.domain.main.as_ref(),
    ),
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
struct BackendConfig<S: AsRef<str> = String> {
  app_secret: S,
  db_credentials: PostgresCredentials,
  db_name: S,
  db_version: u8,
  access_passphrase: S,
  identity_server: Url,
  oauth_callback: Url,
  oauth_client_id: S,
  oauth_client_secret: S,
  admin_user_id: Option<S>,
  discord_webhook: Option<S>,
  channel_name: S,
  app_name: S,
  otel_endpoint: S,
}

fn get_backend_config(config: &BackendConfig) -> String {
  format!(
    r##"# In all environments, the following files are loaded if they exist,
# the latter taking precedence over the former:
#
#  * .env                contains default values for the environment variables needed by the app
#  * .env.local          uncommitted file with local overrides
#  * .env.$APP_ENV       committed environment-specific defaults
#  * .env.$APP_ENV.local uncommitted environment-specific overrides
#
# Real environment variables win over .env files.
#
# DO NOT DEFINE PRODUCTION SECRETS IN THIS FILE NOR IN ANY OTHER COMMITTED FILES.
#
# Run "composer dump-env prod" to compile .env files for production use (requires symfony/flex >=1.2).
# https://symfony.com/doc/current/best_practices.html#use-environment-variables-for-infrastructure-configuration

###> symfony/framework-bundle ###
APP_ENV=prod
APP_SECRET={app_secret}
#TRUSTED_PROXIES=127.0.0.0/8,10.0.0.0/8,172.16.0.0/12,192.168.0.0/16
#TRUSTED_HOSTS='^(localhost|example\.com)$'
###< symfony/framework-bundle ###

###> doctrine/doctrine-bundle ###
# Format described at https://www.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/configuration.html#connecting-using-a-url
# For an SQLite database, use: "sqlite:///%kernel.project_dir%/var/data.db"
# For a PostgreSQL database, use: "postgresql://db_user:db_password@127.0.0.1:5432/db_name?serverVersion=11&charset=utf8"
# IMPORTANT: You MUST configure your server version, either here or in config/packages/doctrine.yaml
DATABASE_URL={db_url}
###< doctrine/doctrine-bundle ###

###> lexik/jwt-authentication-bundle ###
JWT_SECRET_KEY=%kernel.project_dir%/config/jwt/private.pem
JWT_PUBLIC_KEY=%kernel.project_dir%/config/jwt/public.pem
JWT_PASSPHRASE=
###< lexik/jwt-authentication-bundle ###

###> nelmio/cors-bundle ###
CORS_ALLOW_ORIGIN=^https?://([a-z]\.)*localhost(:[0-9]+)?$
###< nelmio/cors-bundle ###

APP_NAME="{app_name}"
IDENTITY_SERVER_URI="{identity_server}"
OAUTH_CALLBACK="{oauth_callback}"
OAUTH_AUTHORIZATION_URI="{oauth_authorization_uri}"
OAUTH_TOKEN_URI="{oauth_token_uri}"
OAUTH_CLIENT_ID="{oauth_client_id}"
OAUTH_SECRET_ID="{oauth_client_secret}"
OTEL_EXPORTER_OTLP_ENDPOINT="{otel_endpoint}"

ALPHA_PASSPHRASE="{access_passphrase}"
ADMIN="{admin_user_id}"

LOG_DISCORD_WEBHOOK_URL={discord_webhook}
LOG_DISCORD_LOG_LEVEL=400
LOG_DISCORD_ENVIRONMENT_NAME={channel_name}
"##,
    app_secret = config.app_secret,
    db_url = PostgresUrl::new(
      "localhost",
      5432,
      config.db_name.as_str(),
      config.db_credentials.name.as_str(),
      config.db_credentials.password.as_str(),
      Some(config.db_version),
      None,
    )
    .unwrap(),
    access_passphrase = config.access_passphrase,
    identity_server = config.identity_server,
    oauth_callback = config.oauth_callback,
    oauth_authorization_uri = {
      let mut u = config.identity_server.clone();
      {
        let mut s = u.path_segments_mut().unwrap();
        s.push("oauth");
        s.push("authorize");
      }
      u
    },
    oauth_token_uri = {
      let mut u = config.identity_server.clone();
      {
        let mut s = u.path_segments_mut().unwrap();
        s.push("oauth");
        s.push("token");
      }
      u
    },
    oauth_client_id = config.oauth_client_id,
    oauth_client_secret = config.oauth_client_secret,
    admin_user_id = config.admin_user_id.as_deref().unwrap_or(""),
    discord_webhook = config.discord_webhook.as_deref().unwrap_or(""),
    channel_name = config.channel_name,
    app_name = config.app_name,
    otel_endpoint = config.otel_endpoint
  )
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
struct FrontendConfig<S: AsRef<str> = String> {
  vite_app_i18n_locale: S,
  vite_app_i18n_fallback_locale: S,
  vite_app_uri: Url,
  vite_app_api_uri: Url,
  vite_app_oauth_uri: Url,
  vite_app_api_release_commit: S,
  vite_app_api_release_channel: S,
}

impl<S: AsRef<str>> fmt::Display for FrontendConfig<S> {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    write!(
      f,
      r#"VITE_APP_I18N_LOCALE={i18n_locale}
VITE_APP_I18N_FALLBACK_LOCALE={i18n_fallback_locale}
VITE_APP_URL={url}
VITE_APP_API_URL={api_url}
VITE_APP_OAUTH_URL={oauth_url}
VITE_APP_API_RELEASE_COMMIT={vite_app_api_release_commit}
VITE_APP_API_RELEASE_CHANNEL={vite_app_api_release_channel}
"#,
      i18n_locale = self.vite_app_i18n_locale.as_ref(),
      i18n_fallback_locale = self.vite_app_i18n_fallback_locale.as_ref(),
      url = self.vite_app_uri,
      api_url = self.vite_app_api_uri,
      oauth_url = self.vite_app_oauth_uri,
      vite_app_api_release_commit = self.vite_app_api_release_commit.as_ref(),
      vite_app_api_release_channel = self.vite_app_api_release_channel.as_ref(),
    )?;
    Ok(())
  }
}
