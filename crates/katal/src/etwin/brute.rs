use crate::discord_webhook::DiscordWebhook;
use crate::etwin::channel::{
  ChannelBorgbase, ChannelDomain, ChannelFallbackServer, ChannelPaths, ChannelPostgres, ChannelTarget,
  ChannelTargetError, NodeEnv,
};
use crate::task::fs::{
  EnsureDir, EnsureDirError, EnsureDirSymlink, EnsureDirSymlinkError, EnsureFile, EnsureFileError,
};
use crate::task::git::{GitCheckout, GitCheckoutError};
use crate::task::nginx::{NginxAvailableSite, NginxAvailableSiteError, NginxEnableSite, NginxEnableSiteError};
use crate::task::postgres::PostgresUrl;
use crate::task::systemd::config::ResourceControl;
use crate::task::systemd::{
  EnsureSystemdServiceConfig, EnsureSystemdServiceConfigError, EnsureSystemdSliceConfig, EnsureSystemdSliceConfigError,
  SystemdSliceConfig, SystemdState, SystemdUnit, SystemdUnitError,
};
use crate::task::witness::{FsWitness, FsWitnessError};
use crate::task::yarn::YarnEnv;
use crate::task::{exec_as, AsyncFn, ExecAsError, NamedTask, TaskName, TaskRef, TaskResult, TaskSuccess};
use crate::utils::display_error_chain::DisplayErrorChainExt;
use async_trait::async_trait;
use asys::common::fs::{FsHost, ReadFileError};
use asys::linux::fs::{FileMode, LinuxFsHost, LinuxMetadata, ResolveError};
use asys::linux::user::{Gid, GroupRef, LinuxUser, LinuxUserHost, Uid, UserRef};
use asys::local_linux::LocalLinux;
use asys::{Command, ExecError, ExecHost};
use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};
use std::fmt::Debug;
use std::path::PathBuf;
use url::Url;

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct BruteAppTarget<S: AsRef<str> = String> {
  pub name: S,
  pub user_name: S,
  pub group_name: S,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct BruteApp<S: AsRef<str> = String> {
  name: S,
  user_name: S,
  group_name: S,
  uid: Uid,
  gid: Gid,
  home: PathBuf,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct BruteChannelTarget<S: AsRef<str> = String> {
  pub inner: ChannelTarget<S>,
  pub vault: BruteVault,
  pub etwin_uri: Url,
  pub etwin_client_id: String,
  pub cors_regex: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct BruteVault {
  pub etwin_client_secret: String,
  pub discord_webhook: Option<DiscordWebhook>,
  pub logs_webhook: Option<DiscordWebhook>,
  pub rankup_webhook: Option<DiscordWebhook>,
  pub release_webhook: Option<DiscordWebhook>,
  pub known_issues_webhook: Option<DiscordWebhook>,
  pub cookie_secret: String,
  pub csrf_secret: String,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct BruteChannel<User: LinuxUser, S: AsRef<str> = String> {
  full_name: S,
  short_name: S,
  user: User,
  domain: ChannelDomain<S>,
  paths: ChannelPaths,
  main_port: u16,
  fallback_server: ChannelFallbackServer,
  postgres: ChannelPostgres,
  vault: BruteVault,
  borg: Option<ChannelBorgbase>,
  etwin_uri: Url,
  etwin_client_id: String,
  node: NodeEnv,
  yarn: YarnEnv,
  cors_regex: String,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct BruteReleaseTarget<S: AsRef<str> = String> {
  pub channel: BruteChannelTarget,
  pub repository: S,
  pub git_ref: String,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct BruteRelease<User: LinuxUser, S: AsRef<str> = String> {
  release_dir: PathBuf,
  repository: S,
  repo_dir: PathBuf,
  channel: BruteChannel<User, S>,
  git_ref: String,
}

#[derive(Serialize, Deserialize)]
pub struct DeployBrute<S: AsRef<str> = String> {
  pub pass: S,
  pub target: BruteReleaseTarget,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum DeployBruteError {
  #[error("failed to exec brute deployment task")]
  Exec(#[source] ExecAsError),
  #[error("failed to deploy brute release")]
  Deploy(#[source] DeployBruteAsRootError),
}

#[async_trait]
impl<'h, H, S> AsyncFn<&'h H> for DeployBrute<S>
where
  H: ExecHost,
  S: AsRef<str> + Send + Sync,
{
  type Output = TaskResult<(), DeployBruteError>;

  async fn run(&self, _host: &'h H) -> Self::Output {
    let input = DeployBruteAsRoot {
      target: self.target.clone(),
    };
    exec_as::<LocalLinux, DeployBruteAsRoot>(UserRef::ROOT, Some(self.pass.as_ref()), &input)
      .await
      .map_err(DeployBruteError::Exec)?
      .map_err(DeployBruteError::Deploy)
  }
}

impl<'h, H: 'h + ExecHost> NamedTask<&'h H> for DeployBrute {
  const NAME: TaskName = TaskName::new("brute::DeployBrute");
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum DeployBruteAsRootError {
  #[error("failed to use witness file")]
  Witness(#[source] FsWitnessError),
  #[error("failed to run task `BruteReleaseTarget`")]
  Task(#[source] BruteReleaseTargetError),
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DeployBruteAsRoot {
  target: BruteReleaseTarget,
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for DeployBruteAsRoot
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  H::User: Debug + Clone + Send + Sync + Serialize + DeserializeOwned,
  H::Group: Debug + Send + Sync,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<(), DeployBruteAsRootError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let paths: ChannelPaths = self.target.channel.inner.paths();
    let witness = paths.home.join("brute_release.witness");
    let mut changed = false;
    let res = FsWitness::new(witness, TaskRef(&self.target))
      .run(host)
      .await
      .map_err(DeployBruteAsRootError::Witness)?;
    changed = res.changed || changed;
    let res = res.output.map_err(DeployBruteAsRootError::Task)?;
    changed = res.changed || changed;
    Ok(TaskSuccess { changed, output: () })
  }
}

impl<'h, H> NamedTask<&'h H> for DeployBruteAsRoot
where
  H: 'h + ExecHost + LinuxUserHost + LinuxFsHost,
  H::User: Debug + Clone + Send + Sync + Serialize + DeserializeOwned,
  H::Group: Debug + Send + Sync,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  const NAME: TaskName = TaskName::new("brute::DeployBruteAsRoot");
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum BruteReleaseTargetError {
  #[error("failed to initialize brute channel")]
  Channel(#[source] BruteChannelTargetError),
  #[error("failed to ensure release directory")]
  ReleaseDir(#[source] EnsureDirError),
  #[error("failed to execute build task")]
  ExecBuild(#[source] ExecAsError),
  #[error("failed to build brute release")]
  Build(#[source] BuildBruteReleaseError),
  #[error("failed to update active brute release")]
  UpdateActive(#[source] UpdateActiveBruteReleaseError),
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for BruteReleaseTarget
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  H::User: Debug + Clone + Send + Sync,
  H::Group: Debug + Send + Sync,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<BruteRelease<H::User>, BruteReleaseTargetError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let res = self.channel.run(host).await.map_err(BruteReleaseTargetError::Channel)?;
    let channel: BruteChannel<H::User> = res.output;
    let mut changed = res.changed;

    let release_dir = channel.paths.releases.join(&self.git_ref);

    eprintln!("Create directory: {}", release_dir.display());
    changed = EnsureDir::new(release_dir.clone())
      .owner(UserRef::Id(channel.user.uid()))
      .group(GroupRef::Id(channel.user.gid()))
      .mode(FileMode::OWNER_ALL | FileMode::ALL_READ | FileMode::ALL_EXEC)
      .run(host)
      .await
      .map_err(BruteReleaseTargetError::ReleaseDir)?
      .changed
      || changed;

    let witness = release_dir.join("ok");

    let witness_exists = match host.read_file(witness.as_path()) {
      Ok(_) => true,
      Err(ReadFileError::NotFound) => false,
      Err(e) => {
        panic!("failed to check witness file: {e:?}")
      }
    };

    let repo_dir = release_dir.join("repo");

    let release = BruteRelease {
      release_dir,
      repository: self.repository.clone(),
      repo_dir: repo_dir.clone(),
      channel: channel.clone(),
      git_ref: self.git_ref.clone(),
    };

    if witness_exists {
      eprintln!("bailing-out, already deployed");
      return Ok(TaskSuccess {
        changed,
        output: release,
      });
    }

    let br = BuildBruteRelease {
      node: channel.node.clone(),
      repo_dir: repo_dir.clone(),
      remote: self.repository.clone(),
      git_ref: self.git_ref.clone(),
      backend_config: config::BackendConfig::from_channel(&channel, self.git_ref.clone()),
      yarn: channel.yarn.clone(),
    };

    changed = exec_as::<LocalLinux, _>(UserRef::Id(channel.user.uid()), None, &br)
      .await
      .map_err(BruteReleaseTargetError::ExecBuild)?
      .map_err(BruteReleaseTargetError::Build)?
      .changed
      || changed;

    changed = (UpdateActiveBruteRelease {
      release: release.clone(),
    })
    .run(host)
    .await
    .map_err(BruteReleaseTargetError::UpdateActive)?
    .changed
      || changed;

    changed = EnsureFile::new(witness).content([]).run(host).await.unwrap().changed || changed;

    Ok(TaskSuccess {
      changed,
      output: release,
    })
  }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum BruteChannelTargetError {
  #[error("base channel creation failed")]
  Base(#[source] ChannelTargetError),
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for BruteChannelTarget
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  H::Group: Debug + Send + Sync,
  H::User: Debug + Clone + Send + Sync,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<BruteChannel<H::User>, BruteChannelTargetError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    eprintln!("Brute: Create channel");
    let res = self.inner.run(host).await.map_err(BruteChannelTargetError::Base)?;

    eprintln!("Channel ready");
    let output = BruteChannel {
      full_name: res.output.full_name,
      short_name: res.output.short_name,
      user: res.output.user,
      domain: res.output.domain.expect("Expected associated domain name"),
      paths: res.output.paths,
      main_port: res.output.main_port.expect("Expected main port"),
      fallback_server: res.output.fallback_server.expect("Expected fallback server"),
      postgres: res.output.postgres.expect("Expected Postgres database"),
      vault: self.vault.clone(),
      borg: res.output.borg,
      etwin_uri: self.etwin_uri.clone(),
      etwin_client_id: self.etwin_client_id.clone(),
      node: res.output.node.expect("Expected Node environment"),
      yarn: res.output.yarn.expect("Expected Yarn environment"),
      cors_regex: self.cors_regex.clone(),
    };

    Ok(TaskSuccess {
      changed: res.changed,
      output,
    })
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct BuildBruteRelease {
  repo_dir: PathBuf,
  remote: String,
  git_ref: String,
  backend_config: config::BackendConfig,
  node: NodeEnv,
  yarn: YarnEnv,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum BuildBruteReleaseError {
  #[error("failed Git checkout")]
  GitCheckout(#[source] GitCheckoutError),
  #[error("failed to install node dependencies")]
  NodeDependencies(#[source] ExecError),
  #[error("failed to ensure backend config file (root)")]
  EnsureBackendConfigRoot(#[source] EnsureFileError),
  #[error("failed to ensure backend config file (server)")]
  EnsureBackendConfigServer(#[source] EnsureFileError),
  #[error("failed to build the backend")]
  BuildBackend(#[source] ExecError),
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for BuildBruteRelease
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  H::User: Send + Sync,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = Result<TaskSuccess<()>, BuildBruteReleaseError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let mut changed: bool;
    let gc = GitCheckout {
      path: self.repo_dir.clone(),
      remote: self.remote.clone(),
      r#ref: self.git_ref.clone(),
    };

    eprintln!("Checkout repository: {:?} {:?}", self.remote, self.git_ref);
    let res = gc.run(host).await.map_err(BuildBruteReleaseError::GitCheckout)?;
    changed = res.changed;

    let backend_dir = self.repo_dir.join("server");

    eprintln!("Install Node dependencies");
    let cmd = Command::new("yarn")
      .arg("install")
      .arg("--immutable")
      .prepend_path(self.node.bin_dir.display().to_string())
      .prepend_path(self.yarn.bin_dir.display().to_string())
      .env("NODE_ENV", "production")
      .current_dir(self.repo_dir.to_str().unwrap());
    host.exec(&cmd).map_err(BuildBruteReleaseError::NodeDependencies)?;

    eprintln!("Write backend config (root)");
    changed = EnsureFile::new(self.repo_dir.join(".env"))
      .content(self.backend_config.to_string())
      // .owner(app.uid)
      // .group(app.gid)
      .mode(FileMode::OWNER_READ | FileMode::OWNER_WRITE)
      .run(host)
      .await
      .map_err(BuildBruteReleaseError::EnsureBackendConfigRoot)?
      .changed
      || changed;

    eprintln!("Write backend config (server)");
    changed = EnsureFile::new(backend_dir.join(".env"))
      .content(self.backend_config.to_string())
      // .owner(app.uid)
      // .group(app.gid)
      .mode(FileMode::OWNER_READ | FileMode::OWNER_WRITE)
      .run(host)
      .await
      .map_err(BuildBruteReleaseError::EnsureBackendConfigServer)?
      .changed
      || changed;

    eprintln!("Build the project");
    let cmd = Command::new("yarn")
      .arg("run")
      .arg("build")
      .prepend_path(self.node.bin_dir.display().to_string())
      .prepend_path(self.yarn.bin_dir.display().to_string())
      .env("NODE_ENV", "production")
      .current_dir(self.repo_dir.to_str().unwrap());
    host.exec(&cmd).map_err(BuildBruteReleaseError::BuildBackend)?;

    Ok(TaskSuccess { changed, output: () })
  }
}

impl<'h, H> NamedTask<&'h H> for BuildBruteRelease
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  H::User: Send + Sync,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  const NAME: TaskName = TaskName::new("brute::BuildBruteRelease");
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize)]
pub struct UpdateActiveBruteRelease<User: LinuxUser, S: AsRef<str> = String> {
  release: BruteRelease<User, S>,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum UpdateActiveBruteReleaseError {
  #[error("failed to resolve link to the active release")]
  ResolveActive(#[source] ResolveError),
  #[error("failed to resolve new release")]
  ResolveNew(#[source] ResolveError),
  #[error("failed to symlink previous release")]
  LinkPrevious(#[source] EnsureDirSymlinkError),
  #[error("failed to disable previous release")]
  SystemdDisablePrevious(#[source] SystemdUnitError),
  #[error("failed to execute DB sync task")]
  ExecDbSync(#[source] ExecAsError),
  #[error("failed to DB sync")]
  DbSync(#[source] UpgradeBruteDatabaseError),
  #[error("failed to ensure systemd service config")]
  ServiceConfig(#[source] EnsureSystemdServiceConfigError),
  #[error("failed to ensure systemd slice config")]
  SliceConfig(#[source] EnsureSystemdSliceConfigError),
  #[error("failed to configure nginx (available)")]
  NginxAvailable(#[source] NginxAvailableSiteError),
  #[error("failed to configure nginx (enable)")]
  NginxEnable(#[source] NginxEnableSiteError),
  #[error("failed to symlink active release")]
  LinkActive(#[source] EnsureDirSymlinkError),
  #[error("failed to enable current release")]
  SystemdEnable(#[source] SystemdUnitError),
}

#[async_trait]
impl<'h, H, S> AsyncFn<&'h H> for UpdateActiveBruteRelease<H::User, S>
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Send + Sync,
  S: AsRef<str> + Send + Sync,
{
  type Output = TaskResult<(), UpdateActiveBruteReleaseError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let uid = self.release.channel.user.uid();
    let gid = self.release.channel.user.gid();

    let service_name = format!("{}.service", self.release.channel.full_name.as_ref());

    let mut changed = false;
    let active_release_link = &self.release.channel.paths.active_release_link;
    let previous_release_link = &self.release.channel.paths.previous_release_link;
    let old_release_dir = {
      match host.resolve(active_release_link) {
        Ok(old) => Some(old),
        Err(ResolveError::NotFound) => None,
        Err(e) => return Err(UpdateActiveBruteReleaseError::ResolveActive(e)),
      }
    };
    let new_release_dir = host
      .resolve(&self.release.release_dir)
      .map_err(UpdateActiveBruteReleaseError::ResolveNew)?;

    if let Some(old_release_dir) = old_release_dir {
      if old_release_dir != new_release_dir {
        eprintln!("Start to disable old release");
        // There is already an active release but it does not match the target
        // release: stop the old version before proceeding to free the channel
        // resources (database, port)
        EnsureDirSymlink::new(previous_release_link.clone())
          .points_to(old_release_dir.clone())
          .owner(uid)
          .group(gid)
          .mode(FileMode::ALL_READ | FileMode::ALL_EXEC)
          .run(host)
          .await
          .map_err(UpdateActiveBruteReleaseError::LinkPrevious)?;

        SystemdUnit::new(&service_name)
          .state(SystemdState::Inactive)
          .run(host)
          .await
          .map_err(UpdateActiveBruteReleaseError::SystemdDisablePrevious)?;
        eprintln!("Old release disabled");

        changed = true;
      }
    }

    // TODO: Trigger backup

    eprintln!("UpgradingDatabase");
    changed = exec_as::<LocalLinux, _>(
      UserRef::Id(uid),
      None,
      &UpgradeBruteDatabase {
        node: self.release.channel.node.clone(),
        yarn: self.release.channel.yarn.clone(),
        repo_dir: self.release.repo_dir.clone(),
        postgres: self.release.channel.postgres.clone(),
      },
    )
    .await
    .map_err(UpdateActiveBruteReleaseError::ExecDbSync)?
    .map_err(UpdateActiveBruteReleaseError::DbSync)?
    .changed
      || changed;

    if let Some(_borg) = self.release.channel.borg.as_ref() {
      eprintln!("WARNING: backups are not supported for Brute");
    }

    eprintln!("Generating Systemd slice");
    {
      use crate::task::systemd::config::{ResourceControl, Slice, Unit};
      let slice = EnsureSystemdSliceConfig {
        name: self.release.channel.full_name.as_ref().to_string(),
        config: SystemdSliceConfig {
          unit: Some(Unit {
            description: Some(format!("Slice for {}", self.release.channel.full_name.as_ref())),
            ..Unit::default()
          }),
          slice: Some(Slice {
            resource_control: Some(ResourceControl {
              memory_high: None,
              memory_max: Some(String::from("16G")),
              cpu_quota: Some(String::from("400%")),
              ..ResourceControl::default()
            }),
            ..Slice::default()
          }),
          ..SystemdSliceConfig::default()
        },
      };
      changed = slice
        .run(host)
        .await
        .map_err(UpdateActiveBruteReleaseError::SliceConfig)?
        .changed
        || changed;
    };

    eprintln!("Generating Systemd unit");
    {
      use crate::task::systemd::config::{CommandLine, Config, Exec, Install, Service, ServiceType, Unit};
      let description = format!(
        "Brute ({channel_name})",
        channel_name = self.release.channel.full_name.as_ref()
      );
      let postgres = format!("{}.service", self.release.channel.postgres.cluster.service);
      let node_exe_str = self.release.channel.node.exe.display().to_string();
      let working_dir_str = self
        .release
        .channel
        .paths
        .home
        .join("active/repo/server")
        .display()
        .to_string();
      let script_str = self
        .release
        .channel
        .paths
        .home
        .join("active/repo/server/lib/main.js")
        .display()
        .to_string();
      let exec_start = format!("{node_exe_str} {script_str}");
      let path_env = format!("PATH={}:/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl:/usr/bin:/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin", self.release.channel.node.bin_dir.display());
      let pid_file_str = self
        .release
        .channel
        .paths
        .home
        .join(".local/var/run/brute.pid")
        .display()
        .to_string();
      let slice_str = format!("{}.slice", self.release.channel.full_name.as_ref());
      let service_config = Config {
        unit: Some(Unit {
          description: Some(description.as_str()),
          after: vec!["network.target"],
          requires: vec![&postgres],
          ..Unit::default()
        }),
        service: Some(Service {
          r#type: Some(ServiceType::Exec),
          restart: Some("on-failure"),
          exec_start: Some(CommandLine {
            line: exec_start.as_str(),
          }),
          pid_file: Some(&pid_file_str),
          exec: Exec {
            user: Some(self.release.channel.user.name()),
            working_directory: Some(working_dir_str.as_str()),
            limit_no_file: Some("infinity"),
            limit_nproc: Some("infinity"),
            limit_core: Some("infinity"),
            environment: vec![&path_env, "NODE_ENV=production"],
            ..Exec::default()
          },
          resource_control: ResourceControl {
            slice: Some(&slice_str),
            ..ResourceControl::default()
          },
          ..Service::default()
        }),
        install: Some(Install {
          wanted_by: vec!["multi-user.target"],
          ..Install::default()
        }),
        ..Config::default()
      };
      eprintln!("Writing Systemd unit");
      changed = EnsureSystemdServiceConfig {
        name: self.release.channel.full_name.as_ref().to_string(),
        config: service_config,
      }
      .run(host)
      .await
      .map_err(UpdateActiveBruteReleaseError::ServiceConfig)?
      .changed
        || changed;
    };

    eprintln!("Generating nginx config");
    let nginx_config = get_nginx_config(&self.release.channel);

    eprintln!("Marking release as active");
    EnsureDirSymlink::new(active_release_link.clone())
      .points_to(new_release_dir.clone())
      .owner(uid)
      .group(gid)
      .mode(FileMode::ALL_READ | FileMode::ALL_EXEC)
      .run(host)
      .await
      .map_err(UpdateActiveBruteReleaseError::LinkActive)?;

    eprintln!("Writing nginx config");
    NginxAvailableSite::new(self.release.channel.full_name.as_ref(), nginx_config)
      .run(host)
      .await
      .map_err(UpdateActiveBruteReleaseError::NginxAvailable)?;

    eprintln!("Enabling nginx config");
    NginxEnableSite::new(self.release.channel.full_name.as_ref())
      .run(host)
      .await
      .map_err(UpdateActiveBruteReleaseError::NginxEnable)?;

    eprintln!("Starting service");
    changed = SystemdUnit::new(&service_name)
      .enabled(true)
      .state(SystemdState::Active)
      .run(host)
      .await
      .map_err(UpdateActiveBruteReleaseError::SystemdEnable)?
      .changed
      || changed;

    Ok(TaskSuccess { changed, output: () })
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct UpgradeBruteDatabase {
  node: NodeEnv,
  yarn: YarnEnv,
  repo_dir: PathBuf,
  postgres: ChannelPostgres,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum UpgradeBruteDatabaseError {
  #[error("failed to build Postgres URL: {0}")]
  PostgresUrl(String),
  #[error("failed database synchronization")]
  DbSync(#[source] ExecError),
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for UpgradeBruteDatabase
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = Result<TaskSuccess<()>, UpgradeBruteDatabaseError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let changed = false;

    let database_url = PostgresUrl::new(
      &self.postgres.host,
      self.postgres.port,
      &self.postgres.name,
      &self.postgres.admin.name,
      &self.postgres.admin.password,
      None,
      Some(10),
    )
    .map_err(|e| UpgradeBruteDatabaseError::PostgresUrl(e.display_chain().to_string()))?;

    let cmd = Command::new("yarn")
      .arg("run")
      .arg("db:sync:prod")
      .prepend_path(self.node.bin_dir.display().to_string())
      .prepend_path(self.yarn.bin_dir.display().to_string())
      .env("NODE_ENV", "production")
      .env("DATABASE_URL", database_url.to_string())
      .current_dir(self.repo_dir.display().to_string());
    host.exec(&cmd).map_err(UpgradeBruteDatabaseError::DbSync)?;

    Ok(TaskSuccess { changed, output: () })
  }
}

impl<'h, H> NamedTask<&'h H> for UpgradeBruteDatabase
where
  H: 'h + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  const NAME: TaskName = TaskName::new("brute::UpgradeBruteDatabase");
}

mod config {
  use crate::discord_webhook::DiscordWebhook;
  use crate::etwin::brute::BruteChannel;
  use crate::task::postgres::PostgresUrl;
  use asys::linux::user::LinuxUser;
  use serde::{Deserialize, Serialize};
  use std::fmt;
  use url::Url;

  #[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
  pub struct BackendConfig<S: AsRef<str> = String> {
    // Database
    pub database_url: PostgresUrl,
    // Server
    pub self_url: S,
    pub port: u16,
    // Etwin
    pub etwin_url: Url,
    pub etwin_client_id: S,
    pub etwin_client_secret: S,
    pub release_commit: S,
    pub release_channel_short: S,
    pub release_channel: S,
    pub discord_webhook: Option<DiscordWebhook<S>>,
    pub logs_webhook: Option<DiscordWebhook<S>>,
    pub rankup_webhook: Option<DiscordWebhook<S>>,
    pub release_webhook: Option<DiscordWebhook<S>>,
    pub known_issues_webhook: Option<DiscordWebhook<S>>,
    pub cors_regex: S,
    pub cookie_secret: S,
    pub csrf_secret: S,
  }

  impl BackendConfig {
    pub fn from_channel(channel: &BruteChannel<impl LinuxUser>, git_ref: String) -> Self {
      let protocol = if channel.domain.cert.is_some() { "https" } else { "http" };
      let self_url = format!("{}://{}", protocol, channel.domain.main.as_str());
      Self {
        database_url: PostgresUrl::new(
          channel.postgres.host.as_str(),
          channel.postgres.port,
          channel.postgres.name.as_str(),
          channel.postgres.main.name.as_str(),
          channel.postgres.main.password.as_str(),
          None,
          Some(20),
        )
        .expect("failed to build postgres URL"),
        self_url,
        port: channel.main_port,
        etwin_url: channel.etwin_uri.clone(),
        etwin_client_id: channel.etwin_client_id.clone(),
        etwin_client_secret: channel.vault.etwin_client_secret.clone(),
        release_commit: git_ref,
        release_channel_short: channel.short_name.clone(),
        release_channel: channel.full_name.clone(),
        discord_webhook: channel.vault.discord_webhook.clone(),
        logs_webhook: channel.vault.logs_webhook.clone(),
        rankup_webhook: channel.vault.rankup_webhook.clone(),
        release_webhook: channel.vault.release_webhook.clone(),
        known_issues_webhook: channel.vault.known_issues_webhook.clone(),
        cors_regex: channel.cors_regex.clone(),
        csrf_secret: channel.vault.csrf_secret.clone(),
        cookie_secret: channel.vault.cookie_secret.clone(),
      }
    }
  }

  impl<S: AsRef<str>> fmt::Display for BackendConfig<S> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
      writeln!(f, "NODE_ENV=production")?;
      writeln!(f, "PORT={}", self.port)?;
      writeln!(f, "SELF_URL={}", self.self_url.as_ref())?;
      writeln!(f, "ETERNALTWIN_URL={}", self.etwin_url.as_str())?;
      writeln!(f, "ETWIN_URL={}", self.etwin_url.as_str())?;
      writeln!(f, "ETERNALTWIN_CLIENT_REF={}", self.etwin_client_id.as_ref())?;
      writeln!(f, "ETWIN_CLIENT_ID={}", self.etwin_client_id.as_ref())?;
      writeln!(f, "ETERNALTWIN_SECRET={}", self.etwin_client_secret.as_ref())?;
      writeln!(f, "ETWIN_CLIENT_SECRET={}", self.etwin_client_secret.as_ref())?;
      writeln!(f, "ETERNALTWIN_APP=brute")?;
      writeln!(f, "ETERNALTWIN_CHANNEL={}", self.release_channel_short.as_ref())?;
      writeln!(f, "DATABASE_URL={}", &self.database_url)?;
      writeln!(f, "RELEASE_COMMIT={}", self.release_commit.as_ref())?;
      writeln!(f, "RELEASE_CHANNEL={}", self.release_channel.as_ref())?;
      if let Some(discord_webhook) = self.discord_webhook.as_ref() {
        writeln!(f, "DISCORD_WEBHOOK_ID={}", discord_webhook.id.as_ref())?;
        writeln!(f, "DISCORD_WEBHOOK_TOKEN={}", discord_webhook.token.as_ref())?;
      }
      if let Some(logs_webhook) = self.logs_webhook.as_ref() {
        writeln!(f, "DISCORD_LOGS_WEBHOOK_ID={}", logs_webhook.id.as_ref())?;
        writeln!(f, "DISCORD_LOGS_WEBHOOK_TOKEN={}", logs_webhook.token.as_ref())?;
      }
      if let Some(rankup_webhook) = self.rankup_webhook.as_ref() {
        writeln!(f, "DISCORD_RANKUP_WEBHOOK_ID={}", rankup_webhook.id.as_ref())?;
        writeln!(f, "DISCORD_RANKUP_WEBHOOK_TOKEN={}", rankup_webhook.token.as_ref())?;
      }
      if let Some(release_webhook) = self.release_webhook.as_ref() {
        writeln!(f, "DISCORD_RELEASES_WEBHOOK_ID={}", release_webhook.id.as_ref())?;
        writeln!(f, "DISCORD_RELEASES_WEBHOOK_TOKEN={}", release_webhook.token.as_ref())?;
      }
      if let Some(known_issues_webhook) = self.known_issues_webhook.as_ref() {
        writeln!(
          f,
          "DISCORD_KNOWN_ISSUES_WEBHOOK_ID={}",
          known_issues_webhook.id.as_ref()
        )?;
        writeln!(
          f,
          "DISCORD_KNOWN_ISSUES_WEBHOOK_TOKEN={}",
          known_issues_webhook.token.as_ref()
        )?;
      }
      writeln!(f, "CORS_REGEX={}", self.cors_regex.as_ref())?;
      writeln!(f, "COOKIE_SECRET={}", self.cookie_secret.as_ref())?;
      writeln!(f, "CSRF_SECRET={}", self.csrf_secret.as_ref())?;
      Ok(())
    }
  }
}

pub fn get_nginx_config<S: AsRef<str>>(channel: &BruteChannel<impl LinuxUser, S>) -> String {
  let upstream_name = channel.full_name.as_ref().to_string().replace('.', "_");
  let access_log = channel.paths.logs.join("main.nginx.access.log");
  let error_log = channel.paths.logs.join("main.nginx.error.log");
  let static_dir = channel.paths.home.join("active/repo/client/build");

  let fallback_server = format!(
    r#"# Fallback server
server {{
  # HTTP port
  listen {fallback_port};
  listen [::]:{fallback_port};

  # Hide nginx version in `Server` header.
  server_tokens off;
  add_header Referrer-Policy same-origin;
  add_header X-Content-Type-Options nosniff;
  add_header X-XSS-Protection "1; mode=block";

  index index.html;

  root {fallback_dir};
}}
"#,
    fallback_port = channel.fallback_server.port,
    fallback_dir = channel.fallback_server.dir.to_str().unwrap(),
  );

  let upstream_server = format!(
    r#"# Define target of the reverse-proxy
upstream {upstream_name} {{
  server [::1]:{port};
  server [::1]:{fallback_port} backup;
  keepalive 8;
}}
"#,
    upstream_name = upstream_name,
    port = channel.main_port,
    fallback_port = channel.fallback_server.port,
  );

  let main_directives = format!(
    r#"  # Space-separated hostnames (wildcard * is accepted)
  server_name {domain} {subdomains};

  access_log {access_log};
  error_log {error_log};
  client_max_body_size 5M;

  root {static_dir};

  location ~* ^/(?:api)(?:/|$) {{
    # Upstream reference
    proxy_pass http://{upstream_name};

    # Configuration of the proxy
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection 'upgrade';
    proxy_set_header Host $host;
    proxy_set_header X-NginX-Proxy true;
    proxy_cache_bypass $http_upgrade;
    proxy_redirect off;
  }}

  location / {{
    try_files $uri /index.html;
    gzip_static on;
  }}
"#,
    domain = channel.domain.main.as_ref(),
    subdomains = channel
      .domain
      .alt
      .iter()
      .map(|s| s.as_ref().to_string())
      .collect::<Vec<String>>()
      .join(" "),
    access_log = access_log.to_str().unwrap(),
    error_log = error_log.to_str().unwrap(),
    static_dir = static_dir.to_str().unwrap(),
    upstream_name = upstream_name,
  );

  match channel.domain.cert.as_ref() {
    Some(https) => format!(
      r#"# This configuration sets a reverse proxy for a specific domain

{fallback_server}

{upstream_server}

# HTTPS
server {{
  # HTTPS port
  listen 443 ssl;
  listen [::]:443 ssl;

  ssl_certificate {cert};
  ssl_certificate_key {cert_key};

  # # HSTS (ngx_http_headers_module is required) (63072000 seconds)
  # add_header Strict-Transport-Security "max-age=63072000" always;

  {main_directives}
}}

# HTTP
server {{
  # HTTP port
  listen 80;
  listen [::]:80;
  server_name {domain};
  return 301 https://$host$request_uri;
}}

# www.
server {{
  listen 80;
  listen 443 ssl;
  listen [::]:80;
  listen [::]:443 ssl;

  server_name www.{domain};

  ssl_certificate {cert};
  ssl_certificate_key {cert_key};

  return 307 https://{domain}$request_uri;
}}

"#,
      fallback_server = fallback_server,
      upstream_server = upstream_server,
      main_directives = main_directives,
      domain = channel.domain.main.as_ref(),
      cert = https.fullchain_cert_path.to_str().unwrap(),
      cert_key = https.cert_key_path.to_str().unwrap(),
    ),
    None => format!(
      r#"# This configuration sets a reverse proxy for a specific domain

{fallback_server}

{upstream_server}

# HTTP
server {{
  # HTTP port
  listen 80;
  listen [::]:80;

  {main_directives}
}}

# www.
server {{
  listen 80;
  listen [::]:80;

  server_name www.{domain};

  return 307 http://{domain}$request_uri;
}}

"#,
      fallback_server = fallback_server,
      upstream_server = upstream_server,
      main_directives = main_directives,
      domain = channel.domain.main.as_ref(),
    ),
  }
}
