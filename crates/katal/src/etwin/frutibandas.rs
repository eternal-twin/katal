use crate::etwin::channel::{ChannelDomain, ChannelFallbackServer, ChannelPaths, ChannelTarget};
use crate::task::fs::{EnsureDir, EnsureDirError, EnsureDirSymlink, EnsureFile};
use crate::task::git::GitCheckout;
use crate::task::systemd::{SystemdState, SystemdUnit};
use crate::task::witness::{FsWitness, FsWitnessError};
use crate::task::{exec_as, AsyncFn, ExecAsError, NamedTask, TaskName, TaskRef, TaskResult, TaskSuccess};
use crate::utils::display_error_chain::DisplayErrorChainExt;
use async_trait::async_trait;
use asys::common::fs::FsHost;
use asys::linux::fs::{FileMode, LinuxFsHost, LinuxMetadata, ResolveError};
use asys::linux::user::{GroupRef, LinuxUser, LinuxUserHost, Uid, UserRef};
use asys::local_linux::LocalLinux;
use asys::{Command, ExecHost};
use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};
use std::fmt;
use std::fmt::Debug;
use std::path::PathBuf;

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct FrutibandasReleaseTarget<S: AsRef<str> = String> {
  pub channel: FrutibandasChannelTarget,
  pub repository: S,
  pub git_ref: String,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct FrutibandasChannelTarget<S: AsRef<str> = String> {
  pub inner: ChannelTarget<S>,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct FrutibandasChannel<User: LinuxUser, S: AsRef<str> = String> {
  full_name: S,
  short_name: S,
  user: User,
  domain: ChannelDomain<S>,
  paths: ChannelPaths,
  fallback_server: ChannelFallbackServer,
  main_port: u16,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct FrutibandasRelease<User: LinuxUser, S: AsRef<str> = String> {
  release_dir: PathBuf,
  repository: S,
  repo_dir: PathBuf,
  channel: FrutibandasChannel<User, S>,
  git_ref: String,
}

#[derive(Serialize, Deserialize)]
pub struct DeployFrutibandas<S: AsRef<str> = String> {
  pub pass: S,
  pub target: FrutibandasReleaseTarget,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum DeployFrutibandasError {
  #[error("failed to exec frutibandas deployment task")]
  Exec(#[source] ExecAsError),
  #[error("failed to deploy frutibandas release")]
  Deploy(#[source] DeployFrutibandasAsRootError),
}

#[async_trait]
impl<'h, H: ExecHost, S: AsRef<str> + Send + Sync> AsyncFn<&'h H> for DeployFrutibandas<S> {
  type Output = TaskResult<(), DeployFrutibandasError>;

  async fn run(&self, _host: &'h H) -> Self::Output {
    let input = DeployFrutibandasAsRoot {
      target: self.target.clone(),
    };
    exec_as::<LocalLinux, DeployFrutibandasAsRoot>(UserRef::ROOT, Some(self.pass.as_ref()), &input)
      .await
      .map_err(DeployFrutibandasError::Exec)?
      .map_err(DeployFrutibandasError::Deploy)
  }
}

impl<'h, H: 'h + ExecHost> NamedTask<&'h H> for DeployFrutibandas {
  const NAME: TaskName = TaskName::new("frutibandas::DeployFrutibandas");
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DeployFrutibandasAsRoot {
  target: FrutibandasReleaseTarget,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum DeployFrutibandasAsRootError {
  #[error("failed to use witness file")]
  Witness(#[source] FsWitnessError),
  #[error("failed to run task `FrutibandasReleaseTarget`")]
  Task(#[source] FrutibandasReleaseTargetError),
}

#[async_trait]
impl<'h, H: ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for DeployFrutibandasAsRoot
where
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Debug + Clone + Send + Sync + Serialize + DeserializeOwned,
  H::Group: Debug + Send + Sync,
{
  type Output = TaskResult<(), DeployFrutibandasAsRootError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let paths: ChannelPaths = self.target.channel.inner.paths();
    let witness = paths.home.join("frutibandas_release.witness");
    let mut changed = false;
    let res = FsWitness::new(witness, TaskRef(&self.target))
      .run(host)
      .await
      .map_err(DeployFrutibandasAsRootError::Witness)?;
    changed = res.changed || changed;
    let res = res.output.map_err(DeployFrutibandasAsRootError::Task)?;
    changed = res.changed || changed;
    Ok(TaskSuccess { changed, output: () })
  }
}

impl<'h, H> NamedTask<&'h H> for DeployFrutibandasAsRoot
where
  H: 'h + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Debug + Clone + Send + Sync + Serialize + DeserializeOwned,
  H::Group: Debug + Send + Sync,
{
  const NAME: TaskName = TaskName::new("frutibandas::DeployFrutibandasAsRoot");
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum FrutibandasReleaseTargetError {
  #[error("failed to ensure release directory")]
  ReleaseDir(#[source] EnsureDirError),
  #[error("failed to execute build task")]
  ExecBuild(#[source] ExecAsError),
  #[error("unexpected error: {0}")]
  Other(String),
}

impl From<anyhow::Error> for FrutibandasReleaseTargetError {
  fn from(value: anyhow::Error) -> Self {
    Self::Other(value.display_chain().to_string())
  }
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for FrutibandasReleaseTarget
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Debug + Clone + Send + Sync,
  H::Group: Debug + Send + Sync,
{
  type Output = TaskResult<FrutibandasRelease<H::User>, FrutibandasReleaseTargetError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let res = self.channel.run(host).await?;
    let channel = res.output;
    let mut changed = res.changed;

    let release_dir = channel.paths.releases.join(&self.git_ref);

    eprintln!("Create directory: {}", release_dir.display());
    let dir_created = EnsureDir::new(release_dir.clone())
      .owner(UserRef::Id(channel.user.uid()))
      .group(GroupRef::Id(channel.user.gid()))
      .mode(FileMode::OWNER_ALL | FileMode::ALL_READ | FileMode::ALL_EXEC)
      .run(host)
      .await
      .map_err(FrutibandasReleaseTargetError::ReleaseDir)?
      .changed;

    changed = dir_created || changed;

    let repo_dir = release_dir.join("repo");

    let release = FrutibandasRelease {
      release_dir,
      repository: self.repository.clone(),
      repo_dir: repo_dir.clone(),
      channel: channel.clone(),
      git_ref: self.git_ref.clone(),
    };

    if dir_created {
      let br = BuildFrutibandasRelease {
        conan_bin: channel.paths.home.join(".local/bin/conan"),
        repo_dir: repo_dir.clone(),
        remote: self.repository.clone(),
        git_ref: self.git_ref.clone(),
      };

      changed = exec_as::<LocalLinux, _>(UserRef::Id(channel.user.uid()), None, &br)
        .await
        .map_err(FrutibandasReleaseTargetError::ExecBuild)?
        .unwrap()
        .changed
        || changed;

      changed = (UpdateActiveFrutibandasRelease {
        release: release.clone(),
      })
      .run(host)
      .await?
      .changed
        || changed;
    } else {
      eprintln!("Skipping build (already exists)");
    }

    Ok(TaskSuccess {
      changed,
      output: release,
    })
  }
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for FrutibandasChannelTarget
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  H::Group: Debug + Send + Sync,
  H::User: Debug + Clone + Send + Sync,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<FrutibandasChannel<H::User>, anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    eprintln!("Frutibandas: Create channel");
    let res = self.inner.run(host).await?;

    eprintln!("Channel ready");
    let output = FrutibandasChannel {
      full_name: res.output.full_name,
      short_name: res.output.short_name,
      user: res.output.user,
      domain: res.output.domain.expect("Expected associated domain name"),
      paths: res.output.paths,
      fallback_server: res.output.fallback_server.expect("Expected fallback server"),
      main_port: res.output.main_port.expect("Expected main port"),
    };

    Ok(TaskSuccess {
      changed: res.changed,
      output,
    })
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct BuildFrutibandasRelease {
  conan_bin: PathBuf,
  repo_dir: PathBuf,
  remote: String,
  git_ref: String,
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for BuildFrutibandasRelease
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Send + Sync,
{
  type Output = Result<TaskSuccess<()>, ()>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let gc = GitCheckout {
      path: self.repo_dir.clone(),
      remote: self.remote.clone(),
      r#ref: self.git_ref.clone(),
    };

    eprintln!("Checkout repository: {:?} {:?}", self.remote, self.git_ref);
    let res = gc.run(host).await.unwrap();
    let changed = res.changed;

    eprintln!("Install conan");
    let cmd = Command::new("pip")
      .arg("install")
      .arg("conan")
      .current_dir(self.repo_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    eprintln!("Install the dependencies");
    let cmd = Command::new(self.conan_bin.display().to_string())
      .arg("install")
      .arg(".")
      .arg("-s")
      .arg("build_type=Release")
      .arg("--build")
      .arg("missing")
      .arg("--install-folder")
      .arg("./build")
      .current_dir(self.repo_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    eprintln!("Generate build system from CMake");
    let cmd = Command::new("cmake")
      .arg("-S")
      .arg(".")
      .arg("-B")
      .arg("./build")
      .current_dir(self.repo_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    eprintln!("Build server binary");
    let cmd = Command::new("cmake")
      .arg("--build")
      .arg("./build")
      .current_dir(self.repo_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    Ok(TaskSuccess { changed, output: () })
  }
}

impl<'h, H> NamedTask<&'h H> for BuildFrutibandasRelease
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Send + Sync,
{
  const NAME: TaskName = TaskName::new("frutibandas::BuildFrutibandasRelease");
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize)]
pub struct UpdateActiveFrutibandasRelease<User: LinuxUser, S: AsRef<str> = String> {
  release: FrutibandasRelease<User, S>,
}

#[async_trait]
impl<'h, H, S> AsyncFn<&'h H> for UpdateActiveFrutibandasRelease<H::User, S>
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Send + Sync,
  S: AsRef<str> + Send + Sync,
{
  type Output = TaskResult<(), anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let service_name = format!("{}.service", self.release.channel.full_name.as_ref());

    let mut changed = false;
    let active_release_link = self.release.channel.paths.home.join("active");
    let previous_release_link = self.release.channel.paths.home.join("previous");
    let old_release_dir = {
      match host.resolve(&active_release_link) {
        Ok(old) => Some(old),
        Err(ResolveError::NotFound) => None,
        Err(e) => return Err(e.into()),
      }
    };
    let new_release_dir = host.resolve(&self.release.release_dir)?;

    if let Some(old_release_dir) = old_release_dir {
      if old_release_dir != new_release_dir {
        eprintln!("Start to disable old release");
        // There is already an active release but it does not match the target
        // release: stop the old version before proceeding to free the channel
        // resources (database, port)
        EnsureDirSymlink::new(previous_release_link.clone())
          .points_to(old_release_dir.clone())
          .owner(self.release.channel.user.uid())
          .group(self.release.channel.user.gid())
          .mode(FileMode::ALL_READ | FileMode::ALL_EXEC)
          .run(host)
          .await?;

        SystemdUnit::new(&service_name)
          .state(SystemdState::Inactive)
          .run(host)
          .await?;

        eprintln!("Old release disabled");
        changed = true;
      }
    }

    eprintln!("Generating Systemd unit");
    let repo_dir = self.release.channel.paths.home.join("active/repo");
    let service = FrutibandasService {
      channel_name: self.release.channel.full_name.as_ref().to_string(),
      repo_dir,
      user_name: self.release.channel.user.name().to_string(),
      server_port: self.release.channel.main_port,
    };
    eprintln!("Writing Systemd unit");
    changed = EnsureFile::new(PathBuf::from("/etc/systemd/system").join(&service_name))
      .content(service.to_string())
      .owner(Uid::ROOT)
      .mode(FileMode::ALL_READ)
      .run(host)
      .await?
      .changed
      || changed;

    eprintln!("Marking release as active");
    EnsureDirSymlink::new(active_release_link.clone())
      .points_to(new_release_dir.clone())
      .owner(self.release.channel.user.uid())
      .group(self.release.channel.user.gid())
      .mode(FileMode::ALL_READ | FileMode::ALL_EXEC)
      .run(host)
      .await?;

    eprintln!("Starting service");
    changed = SystemdUnit::new(&service_name)
      .enabled(true)
      .state(SystemdState::Active)
      .run(host)
      .await?
      .changed
      || changed;

    Ok(TaskSuccess { changed, output: () })
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
struct FrutibandasService<S: AsRef<str> = String> {
  channel_name: S,
  repo_dir: PathBuf,
  user_name: S,
  server_port: u16,
}

impl<S: AsRef<str>> fmt::Display for FrutibandasService<S> {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    let server_bin = self.repo_dir.join("build/bin/serverFrutibandas");
    write!(
      f,
      r#"[Unit]
Description=Frutibandas ({channel_name})
After=network.target

[Service]
Type=simple
User={user_name}
LimitNOFILE=infinity
LimitNPROC=infinity
LimitCORE=infinity
Environment=PATH=/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl:/usr/bin:/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin
Restart=on-failure

WorkingDirectory={working_directory}
ExecStart={server_bin} {server_port}

[Install]
WantedBy=multi-user.target
"#,
      channel_name = self.channel_name.as_ref(),
      user_name = self.user_name.as_ref(),
      working_directory = self.repo_dir.display(),
      server_bin = server_bin.display(),
      server_port = self.server_port,
    )
  }
}
