use crate::etwin::channel::{
  ChannelDomain, ChannelFallbackServer, ChannelPaths, ChannelPhp, ChannelPostgres, ChannelTarget, PostgresCredentials,
};
use crate::task::fs::{EnsureDir, EnsureDirError, EnsureDirSymlink, EnsureFile};
use crate::task::git::GitCheckout;
use crate::task::nginx::{NginxAvailableSite, NginxEnableSite};
use crate::task::witness::{FsWitness, FsWitnessError};
use crate::task::{exec_as, AsyncFn, ExecAsError, NamedTask, TaskName, TaskRef, TaskResult, TaskSuccess};
use crate::utils::display_error_chain::DisplayErrorChainExt;
use async_trait::async_trait;
use asys::common::fs::FsHost;
use asys::linux::fs::{FileMode, LinuxFsHost, LinuxMetadata, ResolveError};
use asys::linux::user::{GroupRef, LinuxUser, LinuxUserHost, UserRef};
use asys::local_linux::LocalLinux;
use asys::{Command, ExecHost};
use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};
use std::fmt;
use std::fmt::Debug;
use std::path::PathBuf;
use url::Url;

const SKIP_BUILD: bool = false;
const PG_VERSION: u8 = 14;

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct KingdomChannelTarget<S: AsRef<str> = String> {
  pub inner: ChannelTarget<S>,
  pub vault: KingdomVault,
  pub etwin_uri: Url,
  pub etwin_client_id: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct KingdomVault {
  pub etwin_client_secret: String,
  pub app_secret: String,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct KingdomChannel<User: LinuxUser, S: AsRef<str> = String> {
  full_name: S,
  short_name: S,
  user: User,
  domain: ChannelDomain<S>,
  paths: ChannelPaths,
  fallback_server: ChannelFallbackServer,
  postgres: ChannelPostgres,
  php: ChannelPhp,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct KingdomReleaseTarget<S: AsRef<str> = String> {
  pub channel: KingdomChannelTarget,
  pub repository: S,
  pub git_ref: String,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct KingdomRelease<User: LinuxUser, S: AsRef<str> = String> {
  release_dir: PathBuf,
  repository: S,
  repo_dir: PathBuf,
  channel: KingdomChannel<User, S>,
  git_ref: String,
}

#[derive(Serialize, Deserialize)]
pub struct DeployKingdom<S: AsRef<str> = String> {
  pub pass: S,
  pub target: KingdomReleaseTarget,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum DeployKingdomError {
  #[error("failed to exec kingdom deployment task")]
  Exec(#[source] ExecAsError),
  #[error("failed to deploy kingdom release")]
  Deploy(#[source] DeployKingdomAsRootError),
}

#[async_trait]
impl<'h, H, S> AsyncFn<&'h H> for DeployKingdom<S>
where
  H: ExecHost,
  S: AsRef<str> + Send + Sync,
{
  type Output = TaskResult<(), DeployKingdomError>;

  async fn run(&self, _host: &'h H) -> Self::Output {
    let input = DeployKingdomAsRoot {
      target: self.target.clone(),
    };
    exec_as::<LocalLinux, DeployKingdomAsRoot>(UserRef::ROOT, Some(self.pass.as_ref()), &input)
      .await
      .map_err(DeployKingdomError::Exec)?
      .map_err(DeployKingdomError::Deploy)
  }
}

impl<'h, H: 'h + ExecHost> NamedTask<&'h H> for DeployKingdom {
  const NAME: TaskName = TaskName::new("kingdom::DeployKingdom");
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DeployKingdomAsRoot {
  target: KingdomReleaseTarget,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum DeployKingdomAsRootError {
  #[error("failed to use witness file")]
  Witness(#[source] FsWitnessError),
  #[error("failed to run task `KingdomReleaseTarget`")]
  Task(#[source] KingdomReleaseTargetError),
}

#[async_trait]
impl<'h, H: ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for DeployKingdomAsRoot
where
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Debug + Clone + Send + Sync + Serialize + DeserializeOwned,
  H::Group: Debug + Send + Sync,
{
  type Output = TaskResult<(), DeployKingdomAsRootError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let paths: ChannelPaths = self.target.channel.inner.paths();
    let witness = paths.home.join("kingdom_release.witness");
    let mut changed = false;
    let res = FsWitness::new(witness, TaskRef(&self.target))
      .run(host)
      .await
      .map_err(DeployKingdomAsRootError::Witness)?;
    changed = res.changed || changed;
    let res = res.output.map_err(DeployKingdomAsRootError::Task)?;
    changed = res.changed || changed;
    Ok(TaskSuccess { changed, output: () })
  }
}

impl<'h, H> NamedTask<&'h H> for DeployKingdomAsRoot
where
  H: 'h + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Debug + Clone + Send + Sync + Serialize + DeserializeOwned,
  H::Group: Debug + Send + Sync,
{
  const NAME: TaskName = TaskName::new("kingdom::DeployKingdomAsRoot");
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum KingdomReleaseTargetError {
  #[error("failed to ensure release directory")]
  ReleaseDir(#[source] EnsureDirError),
  #[error("failed to execute build task")]
  ExecBuild(#[source] ExecAsError),
  #[error("unexpected error: {0}")]
  Other(String),
}

impl From<anyhow::Error> for KingdomReleaseTargetError {
  fn from(value: anyhow::Error) -> Self {
    Self::Other(value.display_chain().to_string())
  }
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for KingdomReleaseTarget
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Debug + Clone + Send + Sync,
  H::Group: Debug + Send + Sync,
{
  type Output = TaskResult<KingdomRelease<H::User>, KingdomReleaseTargetError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let res = self.channel.run(host).await?;
    let channel = res.output;
    let mut changed = res.changed;

    let release_dir = channel.paths.releases.join(&self.git_ref);

    eprintln!("Create directory: {}", release_dir.display());
    changed = EnsureDir::new(release_dir.clone())
      .owner(UserRef::Id(channel.user.uid()))
      .group(GroupRef::Id(channel.user.gid()))
      .mode(FileMode::OWNER_ALL | FileMode::ALL_READ | FileMode::ALL_EXEC)
      .run(host)
      .await
      .map_err(KingdomReleaseTargetError::ReleaseDir)?
      .changed
      || changed;

    let repo_dir = release_dir.join("repo");

    let protocol = if channel.domain.cert.is_some() { "https" } else { "http" };
    let uri = format!("{}://{}", protocol, channel.domain.main.as_str());

    let br = BuildKingdomRelease {
      repo_dir: repo_dir.clone(),
      remote: self.repository.clone(),
      git_ref: self.git_ref.clone(),
      backend_config: BackendConfig {
        app_env: if self.channel.inner.short_name.as_str() == "staging" {
          "dev".to_string()
        } else {
          "prod".to_string()
        },
        app_secret: self.channel.vault.app_secret.to_string(),
        db_credentials: channel.postgres.main.clone(),
        db_name: channel.postgres.name.clone(),
        db_version: PG_VERSION,
        identity_server: self.channel.etwin_uri.clone(),
        oauth_callback: Url::parse(format!("{}/oauth/callback", uri.as_str()).as_str()).expect("uri is well formed"),
        oauth_client_id: self.channel.etwin_client_id.clone(),
        oauth_client_secret: self.channel.vault.etwin_client_secret.clone(),
      },
    };

    if !SKIP_BUILD {
      changed = exec_as::<LocalLinux, _>(UserRef::Id(channel.user.uid()), None, &br)
        .await
        .map_err(KingdomReleaseTargetError::ExecBuild)?
        .unwrap()
        .changed
        || changed;
    }

    let release = KingdomRelease {
      release_dir,
      repository: self.repository.clone(),
      repo_dir,
      channel,
      git_ref: self.git_ref.clone(),
    };

    changed = (UpdateActiveKingdomRelease {
      release: release.clone(),
    })
    .run(host)
    .await?
    .changed
      || changed;

    // eprintln!("Reapply Postgres permissions");
    // changed = PostgresDbPermissions::new(
    //   &release.channel.postgres.name,
    //   release.channel.postgres.admin.name.clone(),
    //   release.channel.postgres.main.name.clone(),
    //   release.channel.postgres.read.name.clone(),
    // )
    // .run(host)
    // .await?
    // .changed
    //   || changed;

    Ok(TaskSuccess {
      changed,
      output: release,
    })
  }
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for KingdomChannelTarget
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  H::Group: Debug + Send + Sync,
  H::User: Debug + Clone + Send + Sync,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<KingdomChannel<H::User>, anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    eprintln!("Kingdom: Create channel");
    let res = self.inner.run(host).await?;

    eprintln!("Channel ready");
    let output = KingdomChannel {
      full_name: res.output.full_name,
      short_name: res.output.short_name,
      user: res.output.user,
      domain: res.output.domain.expect("Expected associated domain name"),
      paths: res.output.paths,
      fallback_server: res.output.fallback_server.expect("Expected fallback server"),
      postgres: res.output.postgres.expect("Expected Postgres database"),
      php: res.output.php.expect("Expected PHP environment"),
    };

    Ok(TaskSuccess {
      changed: res.changed,
      output,
    })
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct BuildKingdomRelease {
  repo_dir: PathBuf,
  remote: String,
  git_ref: String,
  backend_config: BackendConfig,
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for BuildKingdomRelease
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Send + Sync,
{
  type Output = Result<TaskSuccess<()>, ()>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let gc = GitCheckout {
      path: self.repo_dir.clone(),
      remote: self.remote.clone(),
      r#ref: self.git_ref.clone(),
    };

    eprintln!("Checkout repository: {:?} {:?}", self.remote, self.git_ref);
    let res = gc.run(host).await.unwrap();
    let changed = res.changed;

    let game_dir = self.repo_dir.join("EternalKingdom");

    eprintln!("Apply backend config");
    EnsureFile::new(game_dir.join(".env.local"))
      .content(get_backend_config(&self.backend_config))
      .mode(FileMode::OWNER_READ | FileMode::OWNER_WRITE)
      .run(host)
      .await
      .unwrap();

    eprintln!("Install PHP dependencies");
    let cmd = Command::new("composer")
      .arg("install")
      .arg("--no-interaction")
      .arg("--no-ansi")
      .current_dir(game_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    Ok(TaskSuccess { changed, output: () })
  }
}

impl<'h, H> NamedTask<&'h H> for BuildKingdomRelease
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Send + Sync,
{
  const NAME: TaskName = TaskName::new("kingdom::BuildKingdomRelease");
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize)]
pub struct UpdateActiveKingdomRelease<User: LinuxUser, S: AsRef<str> = String> {
  release: KingdomRelease<User, S>,
}

#[async_trait]
impl<'h, H, S> AsyncFn<&'h H> for UpdateActiveKingdomRelease<H::User, S>
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Send + Sync,
  S: AsRef<str> + Send + Sync,
{
  type Output = TaskResult<(), anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let mut changed = false;
    let active_release_link = self.release.channel.paths.home.join("active");
    let previous_release_link = self.release.channel.paths.home.join("previous");
    let old_release_dir = {
      match host.resolve(&active_release_link) {
        Ok(old) => Some(old),
        Err(ResolveError::NotFound) => None,
        Err(e) => return Err(e.into()),
      }
    };
    let new_release_dir = host.resolve(&self.release.release_dir)?;

    if let Some(old_release_dir) = old_release_dir {
      if old_release_dir != new_release_dir {
        eprintln!("Start to disable old release");
        // There is already an active release but it does not match the target
        // release: stop the old version before proceeding to free the channel
        // resources (database, port)
        EnsureDirSymlink::new(previous_release_link.clone())
          .points_to(old_release_dir.clone())
          .owner(self.release.channel.user.uid())
          .group(self.release.channel.user.gid())
          .mode(FileMode::ALL_READ | FileMode::ALL_EXEC)
          .run(host)
          .await?;

        eprintln!("Old release disabled");
        changed = true;
      }
    }

    eprintln!("UpgradingDatabase");
    changed = exec_as::<LocalLinux, _>(
      UserRef::Id(self.release.channel.user.uid()),
      None,
      &UpgradeKingdomDatabase {
        repo_dir: self.release.repo_dir.clone(),
        db_name: self.release.channel.postgres.name.clone(),
        db_admin: self.release.channel.postgres.admin.clone(),
      },
    )
    .await?
    .unwrap()
    .changed
      || changed;

    eprintln!("Generating nginx config");
    let nginx_config = get_nginx_config(&self.release.channel);
    eprintln!("Writing nginx config");
    NginxAvailableSite::new(self.release.channel.full_name.as_ref(), nginx_config)
      .run(host)
      .await?;
    eprintln!("Enabling nginx config");
    NginxEnableSite::new(self.release.channel.full_name.as_ref())
      .run(host)
      .await?;

    eprintln!("Marking release as active");
    EnsureDirSymlink::new(active_release_link.clone())
      .points_to(new_release_dir.clone())
      .owner(self.release.channel.user.uid())
      .group(self.release.channel.user.gid())
      .mode(FileMode::ALL_READ | FileMode::ALL_EXEC)
      .run(host)
      .await?;

    Ok(TaskSuccess { changed, output: () })
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct UpgradeKingdomDatabase {
  repo_dir: PathBuf,
  db_name: String,
  db_admin: PostgresCredentials,
}

#[async_trait]
impl<'h, H: ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for UpgradeKingdomDatabase
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = Result<TaskSuccess<()>, ()>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let game_dir = self.repo_dir.join("EternalKingdom");
    let console_bin = game_dir.join("bin/console");

    let admin_db_url = PostgresUrl::new(
      "localhost",
      5432,
      self.db_name.as_str(),
      self.db_admin.name.as_str(),
      self.db_admin.password.as_str(),
      PG_VERSION,
    )
    .unwrap()
    .to_string();

    let cmd = Command::new(console_bin.to_str().unwrap())
      .arg("doctrine:migrations:migrate")
      .arg("--no-interaction")
      .env("DATABASE_URL", admin_db_url)
      .current_dir(game_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    Ok(TaskSuccess {
      changed: true,
      output: (),
    })
  }
}

impl<'h, H: 'h + ExecHost + LinuxUserHost + LinuxFsHost> NamedTask<&'h H> for UpgradeKingdomDatabase
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  const NAME: TaskName = TaskName::new("kingdom::UpgradeKingdomDatabase");
}

pub fn get_nginx_config<U: LinuxUser, S: AsRef<str>>(channel: &KingdomChannel<U, S>) -> String {
  let access_log = channel.paths.logs.join("main.nginx.access.log");
  let error_log = channel.paths.logs.join("main.nginx.error.log");
  let web_root = channel.paths.home.join("active/repo/EternalKingdom/public");

  let main_directives = format!(
    r#"  # Space-separated hostnames (wildcard * is accepted)
  server_name {domain};

  access_log {access_log};
  error_log {error_log};
  client_max_body_size 5M;

  root {web_root};

  location / {{
    # try to serve file directly, fallback to index.php
    try_files $uri /index.php$is_args$args;
  }}

  location ~ ^/index\.php(/|$) {{
    fastcgi_pass unix:{php_fpm_socket};
    fastcgi_split_path_info ^(.+\.php)(/.*)$;
    include fastcgi_params;

    # optionally set the value of the environment variables used in the application
    # fastcgi_param APP_ENV prod;
    # fastcgi_param APP_SECRET <app-secret-id>;
    # fastcgi_param DATABASE_URL "mysql://db_user:db_pass@host:3306/db_name";

    # When you are using symlinks to link the document root to the
    # current version of your application, you should pass the real
    # application path instead of the path to the symlink to PHP
    # FPM.
    # Otherwise, PHP's OPcache may not properly detect changes to
    # your PHP files (see https://github.com/zendtech/ZendOptimizerPlus/issues/126
    # for more information).
    fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
    fastcgi_param DOCUMENT_ROOT $realpath_root;
    # Prevents URIs that include the front controller. This will 404:
    # http://domain.tld/index.php/some-path
    # Remove the internal directive to allow URIs like this
    internal;
  }}

  # return 404 for all other php files not matching the front controller
  # this prevents access to other php files you don't want to be accessible.
  location ~ \.php$ {{
    return 404;
  }}
"#,
    domain = channel.domain.main.as_ref(),
    access_log = access_log.to_str().unwrap(),
    error_log = error_log.to_str().unwrap(),
    web_root = web_root.to_str().unwrap(),
    php_fpm_socket = channel.php.socket.to_str().unwrap(),
  );

  match channel.domain.cert.as_ref() {
    Some(https) => format!(
      r#"# HTTPS
server {{
  # HTTPS port
  listen 443 ssl;
  listen [::]:443 ssl;

  ssl_certificate {cert};
  ssl_certificate_key {cert_key};

  # # HSTS (ngx_http_headers_module is required) (63072000 seconds)
  # add_header Strict-Transport-Security "max-age=63072000" always;

  {main_directives}
}}

# HTTP
server {{
  # HTTP port
  listen 80;
  listen [::]:80;
  server_name {domain};
  return 301 https://$host$request_uri;
}}

# www.
server {{
  listen 80;
  listen 443 ssl;
  listen [::]:80;
  listen [::]:443 ssl;

  server_name www.{domain};

  ssl_certificate {cert};
  ssl_certificate_key {cert_key};

  return 307 https://{domain}$request_uri;
}}

"#,
      main_directives = main_directives,
      domain = channel.domain.main.as_ref(),
      cert = https.fullchain_cert_path.to_str().unwrap(),
      cert_key = https.cert_key_path.to_str().unwrap(),
    ),
    None => format!(
      r#"# HTTP
server {{
  # HTTP port
  listen 80;
  listen [::]:80;

  {main_directives}
}}

# www.
server {{
  listen 80;
  listen [::]:80;

  server_name www.{domain};

  return 307 http://{domain}$request_uri;
}}

"#,
      main_directives = main_directives,
      domain = channel.domain.main.as_ref(),
    ),
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
struct BackendConfig<S: AsRef<str> = String> {
  app_env: S,
  app_secret: S,
  db_credentials: PostgresCredentials,
  db_name: S,
  db_version: u8,
  identity_server: Url,
  oauth_callback: Url,
  oauth_client_id: S,
  oauth_client_secret: S,
}

fn get_backend_config(config: &BackendConfig) -> String {
  format!(
    r##"# In all environments, the following files are loaded if they exist,
# the latter taking precedence over the former:
#
#  * .env                contains default values for the environment variables needed by the app
#  * .env.local          uncommitted file with local overrides
#  * .env.$APP_ENV       committed environment-specific defaults
#  * .env.$APP_ENV.local uncommitted environment-specific overrides
#
# Real environment variables win over .env files.

###> symfony/framework-bundle ###
APP_ENV={app_env}
APP_SECRET={app_secret}
###< symfony/framework-bundle ###

###> doctrine/doctrine-bundle ###
# Format described at https://www.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/configuration.html#connecting-using-a-url
# For an SQLite database, use: "sqlite:///%kernel.project_dir%/var/data.db"
# For a PostgreSQL database, use: "postgresql://db_user:db_password@127.0.0.1:5432/db_name?serverVersion=11&charset=utf8"
# IMPORTANT: You MUST configure your server version, either here or in config/packages/doctrine.yaml
DATABASE_URL={db_url}
###< doctrine/doctrine-bundle ###

IDENTITY_SERVER_URI="{identity_server}"
OAUTH_CALLBACK="{oauth_callback}"
OAUTH_AUTHORIZATION_URI="{oauth_authorization_uri}"
OAUTH_TOKEN_URI="{oauth_token_uri}"
OAUTH_CLIENT_ID="{oauth_client_id}"
OAUTH_SECRET_ID="{oauth_client_secret}"
"##,
    app_env = config.app_env,
    app_secret = config.app_secret,
    db_url = PostgresUrl::new(
      "localhost",
      5432,
      config.db_name.as_str(),
      config.db_credentials.name.as_str(),
      config.db_credentials.password.as_str(),
      config.db_version
    )
    .unwrap(),
    identity_server = &config.identity_server,
    oauth_callback = config.oauth_callback,
    oauth_authorization_uri = {
      let mut u = config.identity_server.clone();
      {
        let mut s = u.path_segments_mut().unwrap();
        s.push("oauth");
        s.push("authorize");
      }
      u
    },
    oauth_token_uri = {
      let mut u = config.identity_server.clone();
      {
        let mut s = u.path_segments_mut().unwrap();
        s.push("oauth");
        s.push("token");
      }
      u
    },
    oauth_client_id = config.oauth_client_id,
    oauth_client_secret = config.oauth_client_secret,
  )
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
struct FrontendConfig<S: AsRef<str> = String> {
  vue_app_i18n_locale: S,
  vue_app_i18n_fallback_locale: S,
  vue_app_uri: Url,
  vue_app_api_uri: Url,
  vue_app_oauth_uri: Url,
}

impl<S: AsRef<str>> fmt::Display for FrontendConfig<S> {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    write!(
      f,
      r#"VUE_APP_I18N_LOCALE={i18n_locale}
VUE_APP_I18N_FALLBACK_LOCALE={i18n_fallback_locale}
VUE_APP_URL={url}
VUE_APP_API_URL={api_url}
VUE_APP_OAUTH_URL={oauth_url}
"#,
      i18n_locale = self.vue_app_i18n_locale.as_ref(),
      i18n_fallback_locale = self.vue_app_i18n_fallback_locale.as_ref(),
      url = self.vue_app_uri,
      api_url = self.vue_app_api_uri,
      oauth_url = self.vue_app_oauth_uri,
    )?;
    Ok(())
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
struct PostgresUrl(Url);

impl PostgresUrl {
  pub fn new(
    host: impl AsRef<str>,
    port: u16,
    name: impl AsRef<str>,
    user: impl AsRef<str>,
    password: impl AsRef<str>,
    version: u8,
  ) -> Result<Self, url::ParseError> {
    let mut url = Url::parse("postgresql://localhost").expect("Expected default Postgres URI to be valid");
    url.set_host(Some(host.as_ref()))?;
    url
      .set_port(Some(port))
      .expect("Setting the port should always succeed");
    {
      let mut segments = url
        .path_segments_mut()
        .expect("Getting the path segments should always succeed");
      segments.push(name.as_ref());
    }
    url
      .set_username(user.as_ref())
      .expect("Setting the user should always succeed");
    url
      .set_password(Some(password.as_ref()))
      .expect("Setting the password should always succeed");
    {
      let mut query = url.query_pairs_mut();
      query.append_pair("version", version.to_string().as_str());
      query.append_pair("charset", "utf8");
    }
    Ok(Self(url))
  }
}

impl fmt::Display for PostgresUrl {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    fmt::Display::fmt(&self.0, f)
  }
}
