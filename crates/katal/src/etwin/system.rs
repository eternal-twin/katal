use crate::config::{
  ChannelConfig, RawAppConfig, RawAutoChannelConfig, RawAutoVault, RawBruteChannelConfig, RawBruteVault, RawConfig,
  RawDinorpgChannelConfig, RawDinorpgVault, RawEmushChannelConfig, RawEmushVault, RawEpopotamoChannelConfig,
  RawEpopotamoVault, RawEternalfestChannelConfig, RawEternalfestVault, RawEternaltwinChannelConfig,
  RawEternaltwinVault, RawFrutibandasChannelConfig, RawKingdomChannelConfig, RawKingdomVault, RawNeoparcChannelConfig,
  RawNeoparcVault, RawStateConfig, RawUptraceChannelConfig, RawUptraceVault, RawVaultConfig, StateAction,
};
use crate::discord_webhook::DiscordWebhook;
use crate::etwin::auto::{AutoChannelTarget, AutoReleaseTarget, AutoVault, DeployAuto, DeployAutoError};
use crate::etwin::brute::{BruteChannelTarget, BruteReleaseTarget, BruteVault, DeployBrute, DeployBruteError};
use crate::etwin::channel::{
  BorgTarget, CertTarget, ChannelClickhouseTarget, ChannelDomainTarget, ChannelPostgresTarget, ChannelTarget,
};
use crate::etwin::dinorpg::{
  DeployDinorpg, DeployDinorpgError, DinorpgChannelTarget, DinorpgReleaseTarget, DinorpgVault,
};
use crate::etwin::emush::{DeployEmush, DeployEmushError, EmushChannelTarget, EmushReleaseTarget, EmushVault};
use crate::etwin::epopotamo::{
  DeployEpopotamo, DeployEpopotamoError, EpopotamoChannelTarget, EpopotamoReleaseTarget, EpopotamoVault,
};
use crate::etwin::eternalfest::{
  DeployEternalfest, DeployEternalfestError, EternalfestChannelTarget, EternalfestReleaseTarget, EternalfestVault,
};
use crate::etwin::etwin::{
  DeployEternaltwin, DeployEternaltwinError, EternaltwinChannelTarget, EternaltwinOpentelemetry,
  EternaltwinReleaseTarget, EternaltwinVault,
};
use crate::etwin::frutibandas::{
  DeployFrutibandas, DeployFrutibandasError, FrutibandasChannelTarget, FrutibandasReleaseTarget,
};
use crate::etwin::kingdom::{
  DeployKingdom, DeployKingdomError, KingdomChannelTarget, KingdomReleaseTarget, KingdomVault,
};
use crate::etwin::neoparc::{
  DeployNeoparc, DeployNeoparcError, NeoparcChannelTarget, NeoparcReleaseTarget, NeoparcVault,
};
use crate::job_runner::opaque_data::BincodeBuf;
use crate::task::fused::{AcquireIdempotencyToken, StateAndHost, StatefulHost};
use crate::task::php::PhpAvailable;
use crate::task::uptrace::{
  DeployUptrace, DeployUptraceError, UptraceChannelTarget, UptraceReleaseTarget, UptraceVault,
};
use crate::task::{exec_as_rec, AsyncFn, NamedTask, TaskName, TaskResult, TaskRunnerRegistry, TaskSuccess};
use crate::utils::display_error_chain::DisplayErrorChainExt;
use async_trait::async_trait;
use asys::common::fs::FsHost;
use asys::linux::fs::{LinuxFsHost, LinuxMetadata};
use asys::linux::user::{LinuxUserHost, UserRef};
use asys::local_linux::LocalLinux;
use asys::ExecHost;
use katal_state::{AcquireIdempotencyTokenError, IdempotencyKey, TryAcquireIdempotencyTokenError};
use serde::{Deserialize, Serialize};
use std::fmt::Debug;
use std::time::Duration;
use thiserror::Error;
use url::Url;

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct SyncSystem {
  pub config: RawConfig,
}

#[async_trait]
impl<'h, H: StatefulHost + ExecHost + Send + Sync> AsyncFn<&'h H> for SyncSystem {
  type Output = TaskResult<(), String>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let registry: TaskRunnerRegistry<'h, BincodeBuf, &'h H, BincodeBuf> = {
      let mut r = TaskRunnerRegistry::new();
      r.register::<AcquireIdempotencyToken>();
      r
    };

    let registry = Box::new(registry.bind(host));

    let input = SyncSystemAsRoot {
      config: self.config.clone(),
    };
    exec_as_rec::<StateAndHost<LocalLinux, ()>, SyncSystemAsRoot>(
      UserRef::ROOT,
      Some(
        self
          .config
          .vault
          .as_ref()
          .ok_or_else(|| "missing vault".to_string())?
          .sudo_password
          .as_deref()
          .ok_or_else(|| "missing sudo_password".to_string())?,
      ),
      &input,
      registry,
    )
    .await
    .unwrap()
  }
}

impl<'h, H: 'h + StatefulHost + ExecHost> NamedTask<&'h H> for SyncSystem {
  const NAME: TaskName = TaskName::new("system::SyncSystem");
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SyncSystemAsRoot {
  pub config: RawConfig,
}

pub enum SyncSystemAsRootError {}

#[async_trait]
impl<'h, H: StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for SyncSystemAsRoot
where
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Debug + Clone + Send + Sync,
  H::Group: Debug + Send + Sync,
{
  type Output = TaskResult<(), String>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let changed = InitializeSystem.run(host).await.map_err(|e| e.to_string())?.changed;

    let (apps, vault) = match (self.config.apps.as_ref(), self.config.vault.as_ref()) {
      (Some(apps), Some(vault)) => (apps, vault),
      _ => {
        eprintln!("no apps or vault config");
        return Ok(TaskSuccess { changed, output: () });
      }
    };
    let sudo_password = vault.sudo_password.as_deref().expect("missing sudo_password");
    let ch_sys_password = vault.ch_sys_password.as_deref();

    if let Some(app_config) = apps.brute.as_ref() {
      for (channel_name, channel_config) in &app_config.channels {
        let vault = vault
          .brute
          .get(channel_name)
          .unwrap_or_else(|| panic!("Missing vault for {}", channel_name));
        let task = SyncBrute {
          sudo_password,
          channel_name,
          app_config,
          channel_config,
          vault,
        };
        match task.run(host).await {
          Ok(_) => eprintln!("Ok"),
          Err(e) => eprintln!("Error: {}", e.display_chain()),
        };
      }
    }

    if let Some(app_config) = apps.dinorpg.as_ref() {
      for (channel_name, channel_config) in &app_config.channels {
        let vault = vault
          .dinorpg
          .get(channel_name)
          .unwrap_or_else(|| panic!("Missing vault for {}", channel_name));
        let task = SyncDinorpg {
          borgbase_api_key: self.config.vault.as_ref().and_then(|v| v.borgbase.as_deref()),
          sudo_password,
          channel_name,
          app_config,
          channel_config,
          vault,
        };
        match task.run(host).await {
          Ok(_) => eprintln!("Ok"),
          Err(e) => eprintln!("Error: {}", e.display_chain()),
        };
      }
    }

    if let Some(app_config) = apps.emush.as_ref() {
      for (channel_name, channel_config) in &app_config.channels {
        let vault = vault
          .emush
          .get(channel_name)
          .unwrap_or_else(|| panic!("Missing vault for {}", channel_name));
        let task = SyncEmush {
          sudo_password,
          channel_name,
          app_config,
          channel_config,
          vault,
        };
        match task.run(host).await {
          Ok(_) => eprintln!("Ok"),
          Err(e) => eprintln!("Error: {}", e.display_chain()),
        };
      }
    }

    if let Some(app_config) = apps.epopotamo.as_ref() {
      for (channel_name, channel_config) in &app_config.channels {
        let vault = vault
          .epopotamo
          .get(channel_name)
          .unwrap_or_else(|| panic!("Missing vault for epopotamo.{channel_name}"));
        let task = SyncEpopotamo {
          sudo_password,
          channel_name,
          app_config,
          channel_config,
          vault,
        };
        match task.run(host).await {
          Ok(_) => eprintln!("Ok"),
          Err(e) => eprintln!("Error: {}", e.display_chain()),
        };
      }
    }

    if let Some(app_config) = apps.eternalfest.as_ref() {
      for (channel_name, channel_config) in &app_config.channels {
        let vault = vault
          .eternalfest
          .get(channel_name)
          .unwrap_or_else(|| panic!("Missing vault for {}", channel_name));
        let task = SyncEternalfest {
          sudo_password,
          borgbase_api_key: self.config.vault.as_ref().and_then(|v| v.borgbase.as_deref()),
          channel_name,
          app_config,
          channel_config,
          vault,
        };
        match task.run(host).await {
          Ok(_) => eprintln!("Ok"),
          Err(e) => eprintln!("Error: {}", e.display_chain()),
        };
      }
    }

    if let Some(app_config) = apps.eternaltwin.as_ref() {
      for (channel_name, channel_config) in &app_config.channels {
        let vault = vault
          .eternaltwin
          .get(channel_name)
          .unwrap_or_else(|| panic!("Missing vault for {}", channel_name));
        let task = SyncEternaltwin {
          sudo_password,
          borgbase_api_key: self.config.vault.as_ref().and_then(|v| v.borgbase.as_deref()),
          channel_name,
          app_config,
          channel_config,
          vault,
        };
        match task.run(host).await {
          Ok(_) => eprintln!("Ok"),
          Err(e) => eprintln!("Error: {}", e.display_chain()),
        };
      }
    }

    if let Some(app_config) = apps.frutibandas.as_ref() {
      for (channel_name, channel_config) in &app_config.channels {
        let task = SyncFrutibandas {
          sudo_password,
          channel_name,
          app_config,
          channel_config,
        };
        match task.run(host).await {
          Ok(_) => eprintln!("Ok"),
          Err(e) => eprintln!("Error: {}", e.display_chain()),
        };
      }
    }

    let auto_targets: [(
      _,
      _,
      for<'a> fn(&'a RawVaultConfig, &'a str) -> Option<&'a RawAutoVault>,
    ); 2] = [
      (
        "dinocard",
        apps.dinocard.as_ref(),
        |vault: &RawVaultConfig, channel: &str| vault.dinocard.get(channel),
      ),
      (
        "kadokadeo",
        apps.kadokadeo.as_ref(),
        |vault: &RawVaultConfig, channel: &str| vault.kadokadeo.get(channel),
      ),
    ];

    for (app_name, app_config, get_vault) in auto_targets {
      let app_config = match app_config {
        Some(app_config) => app_config,
        None => continue,
      };
      for (channel_name, channel_config) in &app_config.channels {
        let vault = get_vault(vault, channel_name.as_str())
          .unwrap_or_else(|| panic!("Missing vault for {app_name}.{channel_name}"));
        let task = SyncAuto {
          app_name,
          sudo_password,
          channel_name,
          app_config,
          channel_config,
          vault,
        };
        match task.run(host).await {
          Ok(_) => eprintln!("Ok"),
          Err(e) => eprintln!("Error: {}", e.display_chain()),
        };
      }
    }

    if let Some(app_config) = apps.kingdom.as_ref() {
      for (channel_name, channel_config) in &app_config.channels {
        let vault = vault
          .kingdom
          .get(channel_name)
          .unwrap_or_else(|| panic!("Missing vault for {}", channel_name));
        let task = SyncKingdom {
          sudo_password,
          channel_name,
          app_config,
          channel_config,
          vault,
        };
        match task.run(host).await {
          Ok(_) => eprintln!("Ok"),
          Err(e) => eprintln!("Error: {}", e.display_chain()),
        };
      }
    }

    if let Some(app_config) = apps.neoparc.as_ref() {
      for (channel_name, channel_config) in &app_config.channels {
        let vault = vault
          .neoparc
          .get(channel_name)
          .unwrap_or_else(|| panic!("Missing vault for {}", channel_name));
        let task = SyncNeoparc {
          sudo_password,
          channel_name,
          app_config,
          channel_config,
          vault,
        };
        match task.run(host).await {
          Ok(_) => eprintln!("Ok"),
          Err(e) => eprintln!("Error: {}", e.display_chain()),
        };
      }
    }

    if let Some(app_config) = apps.uptrace.as_ref() {
      for (channel_name, channel_config) in &app_config.channels {
        let vault = vault
          .uptrace
          .get(channel_name)
          .unwrap_or_else(|| panic!("Missing vault for {}", channel_name));
        let task = SyncUptrace {
          sudo_password,
          ch_sys_password: ch_sys_password.expect("ch_sys_password must be present"),
          channel_name,
          app_config,
          channel_config,
          vault,
        };
        match task.run(host).await {
          Ok(_) => eprintln!("Ok"),
          Err(e) => eprintln!("Error: {}", e.display_chain()),
        };
      }
    }

    Ok(TaskSuccess { changed, output: () })
  }
}

impl<'h, H> NamedTask<&'h H> for SyncSystemAsRoot
where
  H: 'h + StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Clone + Debug + Sync,
  H::Group: Debug + Sync + Send,
{
  const NAME: TaskName = TaskName::new("system::SyncSystemAsRoot");
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct InitializeSystem;

#[derive(Debug, Clone, PartialEq, Eq, Error, Serialize, Deserialize)]
pub enum InitializeSystemError {
  #[error("failed to acquire idempotency token")]
  AcquireIdempotencyToken(#[from] AcquireIdempotencyTokenError),
  #[error("pacman upgrade failed")]
  Pacman(String),
}

#[async_trait]
impl<'h, H: StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for InitializeSystem
where
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Debug + Clone + Sync,
{
  type Output = TaskResult<(), InitializeSystemError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let key = IdempotencyKey {
      name: "system::InitializeSystem".to_string(),
      expiry: Some(Duration::from_secs(3600)),
    };

    match host.acquire_idempotency_token(key).await {
      Ok(_token) => {
        eprintln!("acquired token for system upgrade");
      }
      Err(AcquireIdempotencyTokenError::Burned(_, _)) => {
        eprintln!("system upgrade token already burned, skipping");
        return Ok(TaskSuccess {
          changed: false,
          output: (),
        });
      }
      Err(e) => {
        return Err(e)?;
      }
    };

    eprintln!("system upgrade: start");
    eprintln!("system upgrade skipped (commented out until we figure how to handle Postgres updates)");
    // PacmanSync::new()
    //   .refresh(true)
    //   .sys_upgrade(true)
    //   .needed(true)
    //   .run(host)
    //   .await
    //   .map_err(|e| InitializeSystemError::Pacman(e.to_string()))?;
    eprintln!("system upgrade: done");

    Ok(TaskSuccess {
      changed: true,
      output: (),
    })
  }
}

impl<'h, H> NamedTask<&'h H> for InitializeSystem
where
  H: 'h + StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Clone + Debug + Sync,
{
  const NAME: TaskName = TaskName::new("system::InitializeSystem");
}

#[derive(Debug, Clone)]
pub struct SyncAuto<'a> {
  pub app_name: &'a str,
  pub sudo_password: &'a str,
  pub channel_name: &'a str,
  pub app_config: &'a RawAppConfig<RawAutoChannelConfig>,
  pub channel_config: &'a RawAutoChannelConfig,
  pub vault: &'a RawAutoVault,
}

#[derive(Debug, Error)]
pub enum SyncAutoError {
  #[error("failed auto deployment")]
  Deploy(#[source] DeployAutoError),
}

#[async_trait]
impl<'a, 'h, H> AsyncFn<&'h H> for SyncAuto<'a>
where
  H: 'h + StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Clone + Debug + Sync,
{
  type Output = TaskResult<(), SyncAutoError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let app_name = self.app_name;
    let sudo_password = self.sudo_password;
    let channel_name = self.channel_name;
    let app_config = self.app_config;
    let channel_config = self.channel_config;
    let vault = self.vault;

    let full_name = format!("{}.{}", app_name, channel_name);
    eprintln!("Syncing: {full_name}");

    let is_reset = is_reset(host, full_name.as_str(), channel_config.state())
      .await
      .expect("check `is_reset`");
    if is_reset {
      eprintln!("State will be reset");
    }

    let channel_target = {
      let mut target = ChannelTarget::new(full_name.clone(), channel_name.to_string());
      target.domain = Some(ChannelDomainTarget {
        main: channel_config.domain.clone(),
        www: true,
        legacy: Vec::new(),
        alt: Vec::new(),
        cert: channel_config.https.as_ref().map(|conf| CertTarget {
          name: full_name.clone(),
          email: conf.email.clone(),
        }),
      });
      target.main_port = Some(channel_config.main_port);
      target.fallback_server = Some(channel_config.fallback_port);
      target.postgres = Some(ChannelPostgresTarget {
        admin_password: vault.db_admin_password.clone(),
        main_password: vault.db_main_password.clone(),
        read_password: vault.db_read_password.clone(),
        reset: is_reset,
      });
      target.node = Some("^20.11.0".parse().expect("valid version version requirement"));
      target
    };
    let channel_target = AutoChannelTarget {
      inner: channel_target,
      vault: AutoVault {
        secret: vault.secret.clone(),
        etwin_client_secret: vault.etwin_client_secret.clone(),
      },
      etwin_uri: Url::parse(channel_config.etwin_uri.as_str()).expect("invalid etwin_uri"),
      etwin_client_id: channel_config.etwin_client_id.clone(),
    };
    let release_target = AutoReleaseTarget {
      channel: channel_target,
      repository: app_config.repository.clone(),
      git_ref: channel_config.release.clone(),
    };
    let deploy_task = DeployAuto {
      pass: sudo_password,
      target: release_target,
    };
    deploy_task.run(host).await.map_err(SyncAutoError::Deploy)
  }
}

impl<'a, 'h, H> NamedTask<&'h H> for SyncAuto<'a>
where
  H: 'h + StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Clone + Debug + Sync,
{
  const NAME: TaskName = TaskName::new("system::SyncAuto");
}

#[derive(Debug, Clone)]
pub struct SyncBrute<'a> {
  pub sudo_password: &'a str,
  pub channel_name: &'a str,
  pub app_config: &'a RawAppConfig<RawBruteChannelConfig>,
  pub channel_config: &'a RawBruteChannelConfig,
  pub vault: &'a RawBruteVault,
}

#[derive(Debug, Error)]
pub enum SyncBruteError {
  #[error("failed brute deployment")]
  Deploy(#[source] DeployBruteError),
}

#[async_trait]
impl<'a, 'h, H: StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for SyncBrute<'a>
where
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Debug + Clone + Sync,
{
  type Output = TaskResult<(), SyncBruteError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let app_name = "brute";
    let sudo_password = self.sudo_password;
    let channel_name = self.channel_name;
    let app_config = self.app_config;
    let channel_config = self.channel_config;
    let vault = self.vault;

    let full_name = format!("{}.{}", app_name, channel_name);
    eprintln!("Syncing: {full_name}");

    let is_reset = is_reset(host, full_name.as_str(), channel_config.state())
      .await
      .expect("check `is_reset`");
    if is_reset {
      eprintln!("State will be reset");
    }

    let channel_target = {
      let mut target = ChannelTarget::new(full_name.clone(), channel_name.to_string());
      target.domain = Some(ChannelDomainTarget {
        main: channel_config.domain.clone(),
        www: true,
        legacy: Vec::new(),
        alt: channel_config.subdomains.iter().cloned().collect(),
        cert: channel_config.https.as_ref().map(|conf| CertTarget {
          name: full_name.clone(),
          email: conf.email.clone(),
        }),
      });
      target.main_port = Some(channel_config.main_port);
      target.fallback_server = Some(channel_config.fallback_port);
      target.postgres = Some(ChannelPostgresTarget {
        admin_password: vault.db_admin_password.clone(),
        main_password: vault.db_main_password.clone(),
        read_password: vault.db_read_password.clone(),
        reset: is_reset,
      });
      target.node = Some("^22.12.0".parse().expect("valid version version requirement"));
      target.yarn = true;
      target
    };
    let discord_webhook = if let Some(id) = channel_config.discord_webhook_id.clone() {
      let token = vault
        .discord_webhook_token
        .clone()
        .expect("missing discord_webhook_token");
      Some(DiscordWebhook { id, token })
    } else {
      None
    };
    let logs_webhook = if let Some(id) = channel_config.logs_webhook_id.clone() {
      let token = vault.logs_webhook_token.clone().expect("missing logs_webhook_token");
      Some(DiscordWebhook { id, token })
    } else {
      None
    };
    let rankup_webhook = if let Some(id) = channel_config.rankup_webhook_id.clone() {
      let token = vault
        .rankup_webhook_token
        .clone()
        .expect("missing rankup_webhook_token");
      Some(DiscordWebhook { id, token })
    } else {
      None
    };
    let release_webhook = if let Some(id) = channel_config.release_webhook_id.clone() {
      let token = vault
        .release_webhook_token
        .clone()
        .expect("missing release_webhook_token");
      Some(DiscordWebhook { id, token })
    } else {
      None
    };
    let known_issues_webhook = if let Some(id) = channel_config.known_issues_webhook_id.clone() {
      let token = vault
        .known_issues_webhook_token
        .clone()
        .expect("missing release_webhook_token");
      Some(DiscordWebhook { id, token })
    } else {
      None
    };
    let brute_channel_target = BruteChannelTarget {
      inner: channel_target,
      cors_regex: channel_config.cors_regex.clone(),
      vault: BruteVault {
        etwin_client_secret: vault.etwin_client_secret.clone(),
        discord_webhook,
        logs_webhook,
        rankup_webhook,
        release_webhook,
        known_issues_webhook,
        cookie_secret: vault.cookie_secret.clone(),
        csrf_secret: vault.csrf_secret.clone(),
      },
      etwin_uri: channel_config.etwin_uri.parse().expect("invalid etwin URI"),
      etwin_client_id: channel_config.etwin_client_id.clone(),
    };
    let release_target = BruteReleaseTarget {
      channel: brute_channel_target,
      repository: app_config.repository.clone(),
      git_ref: channel_config.release.clone(),
    };
    let deploy_task = DeployBrute {
      pass: sudo_password,
      target: release_target,
    };
    deploy_task.run(host).await.map_err(SyncBruteError::Deploy)
  }
}

impl<'a, 'h, H> NamedTask<&'h H> for SyncBrute<'a>
where
  H: 'h + StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Clone + Debug + Sync,
{
  const NAME: TaskName = TaskName::new("system::SyncBrute");
}

#[derive(Debug, Clone)]
pub struct SyncDinorpg<'a> {
  pub sudo_password: &'a str,
  pub borgbase_api_key: Option<&'a str>,
  pub channel_name: &'a str,
  pub app_config: &'a RawAppConfig<RawDinorpgChannelConfig>,
  pub channel_config: &'a RawDinorpgChannelConfig,
  pub vault: &'a RawDinorpgVault,
}

#[derive(Debug, Error)]
pub enum SyncDinorpgError {
  #[error("failed dinorpg deployment")]
  Deploy(#[source] DeployDinorpgError),
}

#[async_trait]
impl<'a, 'h, H> AsyncFn<&'h H> for SyncDinorpg<'a>
where
  H: 'h + StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Clone + Debug + Sync,
{
  type Output = TaskResult<(), SyncDinorpgError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let app_name = "dinorpg";
    let sudo_password = self.sudo_password;
    let channel_name = self.channel_name;
    let app_config = self.app_config;
    let channel_config = self.channel_config;
    let vault = self.vault;

    let full_name = format!("{}.{}", app_name, channel_name);
    eprintln!("Syncing: {full_name}");

    let is_reset = is_reset(host, full_name.as_str(), channel_config.state())
      .await
      .expect("check `is_reset`");
    if is_reset {
      eprintln!("State will be reset");
    }

    let channel_target = {
      let mut target = ChannelTarget::new(full_name.clone(), channel_name.to_string());
      target.domain = Some(ChannelDomainTarget {
        main: channel_config.domain.clone(),
        www: true,
        legacy: Vec::new(),
        alt: Vec::new(),
        cert: channel_config.https.as_ref().map(|conf| CertTarget {
          name: full_name.clone(),
          email: conf.email.clone(),
        }),
      });
      target.main_port = Some(channel_config.main_port);
      target.fallback_server = Some(channel_config.fallback_port);
      target.postgres = Some(ChannelPostgresTarget {
        admin_password: vault.db_admin_password.clone(),
        main_password: vault.db_main_password.clone(),
        read_password: vault.db_read_password.clone(),
        reset: is_reset,
      });
      target.node = Some("^20.11.0".parse().expect("valid version version requirement"));
      target.yarn = true;
      if let Some(borgbase_api_key) = self.borgbase_api_key {
        target.borg = Some(BorgTarget {
          borgbase_api_key: borgbase_api_key.to_string(),
          repo_password: vault.borgbase_repo.clone(),
        });
      }
      target
    };
    let discord_webhook = if let Some(id) = channel_config.discord_webhook_id.clone() {
      let token = vault
        .discord_webhook_token
        .clone()
        .expect("missing discord_webhook_token");
      Some(DiscordWebhook { id, token })
    } else {
      None
    };
    let logs_webhook = if let Some(id) = channel_config.logs_webhook_id.clone() {
      let token = vault.logs_webhook_token.clone().expect("missing logs_webhook_token");
      Some(DiscordWebhook { id, token })
    } else {
      None
    };
    let channel_target = DinorpgChannelTarget {
      inner: channel_target,
      vault: DinorpgVault {
        secret_key: vault.secret_key.clone(),
        etwin_client_secret: vault.etwin_client_secret.clone(),
        discord_webhook,
        logs_webhook,
        salt: vault.salt.clone(),
      },
      etwin_uri: channel_config.etwin_uri.parse().expect("invalid etwin URI"),
      etwin_client_id: channel_config.etwin_client_id.clone(),
      administrator: channel_config.administrator.clone(),
      wss_port: channel_config.wss_port,
    };
    let release_target = DinorpgReleaseTarget {
      channel: channel_target,
      repository: app_config.repository.clone(),
      git_ref: channel_config.release.clone(),
    };
    let deploy_task = DeployDinorpg {
      pass: sudo_password,
      target: release_target,
    };
    deploy_task.run(host).await.map_err(SyncDinorpgError::Deploy)
  }
}

impl<'a, 'h, H> NamedTask<&'h H> for SyncDinorpg<'a>
where
  H: 'h + StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Clone + Debug + Sync,
{
  const NAME: TaskName = TaskName::new("system::SyncDinorpg");
}

#[derive(Debug, Clone)]
pub struct SyncEmush<'a> {
  pub sudo_password: &'a str,
  pub channel_name: &'a str,
  pub app_config: &'a RawAppConfig<RawEmushChannelConfig>,
  pub channel_config: &'a RawEmushChannelConfig,
  pub vault: &'a RawEmushVault,
}

#[derive(Debug, Error)]
pub enum SyncEmushError {
  #[error("failed emush deployment")]
  Deploy(#[source] DeployEmushError),
}

#[async_trait]
impl<'a, 'h, H> AsyncFn<&'h H> for SyncEmush<'a>
where
  H: 'h + StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Clone + Debug + Sync,
{
  type Output = TaskResult<(), SyncEmushError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let app_name = "emush";
    let sudo_password = self.sudo_password;
    let channel_name = self.channel_name;
    let app_config = self.app_config;
    let channel_config = self.channel_config;
    let vault = self.vault;

    let full_name = format!("{}.{}", app_name, channel_name);
    eprintln!("Syncing: {full_name}");

    let is_reset = is_reset(host, full_name.as_str(), channel_config.state())
      .await
      .expect("check `is_reset`");
    if is_reset {
      eprintln!("State will be reset");
    }

    let channel_target = {
      let mut target = ChannelTarget::new(full_name.clone(), channel_name.to_string());
      target.domain = Some(ChannelDomainTarget {
        main: channel_config.domain.clone(),
        www: true,
        legacy: Vec::new(),
        alt: Vec::new(),
        cert: channel_config.https.as_ref().map(|conf| CertTarget {
          name: full_name.clone(),
          email: conf.email.clone(),
        }),
      });
      target.main_port = Some(channel_config.main_port);
      target.fallback_server = Some(channel_config.fallback_port);
      target.postgres = Some(ChannelPostgresTarget {
        admin_password: vault.db_admin_password.clone(),
        main_password: vault.db_main_password.clone(),
        read_password: vault.db_read_password.clone(),
        reset: is_reset,
      });
      target.node = Some("^18.13.0".parse().expect("valid version version requirement"));
      target.yarn = true;
      target.php = Some(PhpAvailable {
        ext_curl: true,
        ext_ctype: false,
        ext_iconv: true,
        ext_intl: true,
        ext_opentelemetry: true,
        ext_pdo_pgsql: true,
        ext_pgsql: true,
        ext_protobuf: true,
        ext_sodium: true,
        ext_zip: true,
      });
      target
    };
    let channel_target = EmushChannelTarget {
      inner: channel_target,
      vault: EmushVault {
        etwin_client_secret: vault.etwin_client_secret.clone(),
        app_secret: vault.app_secret.clone(),
        access_passphrase: vault.access_passphrase.clone(),
        discord_webhook: vault.discord_webhook.clone(),
      },
      etwin_uri: Url::parse(&channel_config.etwin_uri).expect("invalid etwin_uri"),
      etwin_client_id: channel_config.etwin_client_id.clone(),
      admin_user_id: channel_config.admin_user_id.clone(),
      app_name: channel_config.app_name.clone(),
      otel_endpoint: channel_config.otel_endpoint.clone(),
    };
    let release_target = EmushReleaseTarget {
      channel: channel_target,
      repository: app_config.repository.clone(),
      git_ref: channel_config.release.clone(),
      is_after_db_reset: is_reset,
    };
    let deploy_task = DeployEmush {
      pass: sudo_password,
      target: release_target,
    };
    deploy_task.run(host).await.map_err(SyncEmushError::Deploy)
  }
}

impl<'a, 'h, H> NamedTask<&'h H> for SyncEmush<'a>
where
  H: 'h + StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Clone + Debug + Sync,
{
  const NAME: TaskName = TaskName::new("system::SyncEmush");
}

#[derive(Debug, Clone)]
pub struct SyncEpopotamo<'a> {
  pub sudo_password: &'a str,
  pub channel_name: &'a str,
  pub app_config: &'a RawAppConfig<RawEpopotamoChannelConfig>,
  pub channel_config: &'a RawEpopotamoChannelConfig,
  pub vault: &'a RawEpopotamoVault,
}

#[derive(Debug, Error)]
pub enum SyncEpopotamoError {
  #[error("failed epopotamo deployment")]
  Deploy(#[source] DeployEpopotamoError),
}

#[async_trait]
impl<'a, 'h, H> AsyncFn<&'h H> for SyncEpopotamo<'a>
where
  H: 'h + StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Clone + Debug + Sync,
{
  type Output = TaskResult<(), SyncEpopotamoError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let app_name = "epopotamo";
    let sudo_password = self.sudo_password;
    let channel_name = self.channel_name;
    let app_config = self.app_config;
    let channel_config = self.channel_config;
    let vault = self.vault;

    let full_name = format!("{}.{}", app_name, channel_name);
    eprintln!("Syncing: {full_name}");

    let is_reset = is_reset(host, full_name.as_str(), channel_config.state())
      .await
      .expect("check `is_reset`");
    if is_reset {
      eprintln!("State will be reset");
    }

    let channel_target = {
      let mut target = ChannelTarget::new(full_name.clone(), channel_name.to_string());
      target.domain = Some(ChannelDomainTarget {
        alt: Vec::new(),
        main: channel_config.domain.clone(),
        www: true,
        legacy: Vec::new(),
        cert: channel_config.https.as_ref().map(|conf| CertTarget {
          name: full_name.clone(),
          email: conf.email.clone(),
        }),
      });
      target.main_port = Some(channel_config.main_port);
      target.fallback_server = Some(channel_config.fallback_port);
      target.postgres = Some(ChannelPostgresTarget {
        admin_password: vault.db_admin_password.clone(),
        main_password: vault.db_main_password.clone(),
        read_password: vault.db_read_password.clone(),
        reset: is_reset,
      });
      target.php = Some(PhpAvailable {
        ext_curl: false,
        ext_ctype: false,
        ext_iconv: false,
        ext_intl: false,
        ext_opentelemetry: true,
        ext_pdo_pgsql: true,
        ext_pgsql: true,
        ext_protobuf: true,
        ext_sodium: false,
        ext_zip: false,
      });
      target.node = Some("^22.12.0".parse().expect("valid version version requirement"));
      target.yarn = true;
      target
    };
    let channel_target = EpopotamoChannelTarget {
      inner: channel_target,
      vault: EpopotamoVault {
        etwin_client_secret: vault.etwin_client_secret.clone(),
      },
      etwin_uri: Url::parse(channel_config.etwin_uri.as_str()).expect("invalid etwin_uri"),
      etwin_client_id: channel_config.etwin_client_id.clone(),
      permanent_admin: channel_config.permanent_admin.clone(),
      socket_port: channel_config.socket_port,
      profiling: channel_config.profiling,
    };
    let release_target = EpopotamoReleaseTarget {
      channel: channel_target,
      repository: app_config.repository.clone(),
      git_ref: channel_config.release.clone(),
    };
    let deploy_task = DeployEpopotamo {
      pass: sudo_password,
      target: release_target,
    };
    deploy_task.run(host).await.map_err(SyncEpopotamoError::Deploy)
  }
}

impl<'a, 'h, H> NamedTask<&'h H> for SyncEpopotamo<'a>
where
  H: 'h + StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Clone + Debug + Sync,
{
  const NAME: TaskName = TaskName::new("system::SyncEpopotamo");
}

#[derive(Debug, Clone)]
pub struct SyncEternalfest<'a> {
  pub sudo_password: &'a str,
  pub borgbase_api_key: Option<&'a str>,
  pub channel_name: &'a str,
  pub app_config: &'a RawAppConfig<RawEternalfestChannelConfig>,
  pub channel_config: &'a RawEternalfestChannelConfig,
  pub vault: &'a RawEternalfestVault,
}

#[derive(Debug, Error)]
pub enum SyncEternalfestError {
  #[error("failed eternalfest deployment")]
  Deploy(#[source] DeployEternalfestError),
}

#[async_trait]
impl<'a, 'h, H> AsyncFn<&'h H> for SyncEternalfest<'a>
where
  H: 'h + StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Clone + Debug + Sync,
{
  type Output = TaskResult<(), SyncEternalfestError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let app_name = "eternalfest";
    let sudo_password = self.sudo_password;
    let channel_name = self.channel_name;
    let app_config = self.app_config;
    let channel_config = self.channel_config;
    let vault = self.vault;

    let full_name = format!("{}.{}", app_name, channel_name);
    eprintln!("Syncing: {full_name}");

    let is_reset = is_reset(host, full_name.as_str(), channel_config.state())
      .await
      .expect("check `is_reset`");
    if is_reset {
      eprintln!("State will be reset");
    }

    let channel_target = {
      let mut target = ChannelTarget::new(full_name.clone(), channel_name.to_string());
      target.data_dir = true;
      target.domain = Some(ChannelDomainTarget {
        main: channel_config.domain.clone(),
        www: true,
        legacy: Vec::new(),
        alt: Vec::new(),
        cert: channel_config.https.as_ref().map(|conf| CertTarget {
          name: full_name.clone(),
          email: conf.email.clone(),
        }),
      });
      target.main_port = Some(channel_config.main_port);
      target.fallback_server = Some(channel_config.fallback_port);
      target.postgres = Some(ChannelPostgresTarget {
        admin_password: vault.db_admin_password.clone(),
        main_password: vault.db_main_password.clone(),
        read_password: vault.db_read_password.clone(),
        reset: is_reset,
      });
      target.node = Some("^22.12.0".parse().expect("valid version version requirement"));
      target.rust = true;
      target.yarn = true;

      if let Some(borgbase_api_key) = self.borgbase_api_key {
        target.borg = Some(BorgTarget {
          borgbase_api_key: borgbase_api_key.to_string(),
          repo_password: vault.borgbase_repo.clone(),
        });
      }
      target
    };
    let channel_target = EternalfestChannelTarget {
      inner: channel_target,
      backend_port: channel_config.backend_port,
      vault: EternalfestVault {
        secret_key: vault.secret_key.clone(),
        cookie_key: vault.cookie_key.clone(),
        eternaltwin_client_secret: vault.etwin_client_secret.clone(),
      },
      eternaltwin_uri: channel_config.etwin_uri.clone(),
      eternaltwin_client_id: channel_config.etwin_client_id.clone(),
    };
    let release_target = EternalfestReleaseTarget {
      channel: channel_target,
      repository: app_config.repository.clone(),
      git_ref: channel_config.release.clone(),
    };
    let deploy_task = DeployEternalfest {
      pass: sudo_password,
      target: release_target,
    };
    deploy_task.run(host).await.map_err(SyncEternalfestError::Deploy)
  }
}

impl<'a, 'h, H> NamedTask<&'h H> for SyncEternalfest<'a>
where
  H: 'h + StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Clone + Debug + Sync,
{
  const NAME: TaskName = TaskName::new("system::SyncEternalfest");
}

#[derive(Debug, Clone)]
pub struct SyncEternaltwin<'a> {
  pub sudo_password: &'a str,
  pub borgbase_api_key: Option<&'a str>,
  pub channel_name: &'a str,
  pub app_config: &'a RawAppConfig<RawEternaltwinChannelConfig>,
  pub channel_config: &'a RawEternaltwinChannelConfig,
  pub vault: &'a RawEternaltwinVault,
}

#[derive(Debug, Error)]
pub enum SyncEternaltwinError {
  #[error("failed eternaltwin deployment")]
  Deploy(#[source] DeployEternaltwinError),
}

#[async_trait]
impl<'a, 'h, H> AsyncFn<&'h H> for SyncEternaltwin<'a>
where
  H: 'h + StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Clone + Debug + Sync,
{
  type Output = TaskResult<(), SyncEternaltwinError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let app_name = "eternaltwin";
    let sudo_password = self.sudo_password;
    let channel_name = self.channel_name;
    let app_config = self.app_config;
    let channel_config = self.channel_config;
    let vault = self.vault;

    let full_name = format!("{}.{}", app_name, channel_name);
    eprintln!("Syncing: {full_name}");

    let is_reset = is_reset(host, full_name.as_str(), channel_config.state())
      .await
      .expect("check `is_reset`");
    if is_reset {
      eprintln!("State will be reset");
    }

    let channel_target = {
      let mut target = ChannelTarget::new(full_name.clone(), channel_name.to_string());
      target.domain = Some(ChannelDomainTarget {
        main: channel_config.domain.clone(),
        www: true,
        legacy: match channel_config.domain.as_str() {
          "eternaltwin.org" => vec!["eternal-twin.net".to_string(), "www.eternal-twin.net".to_string()],
          _ => Vec::new(),
        },
        alt: Vec::new(),
        cert: channel_config.https.as_ref().map(|conf| CertTarget {
          name: full_name.clone(),
          email: conf.email.clone(),
        }),
      });
      target.main_port = Some(channel_config.main_port);
      target.fallback_server = Some(channel_config.fallback_port);
      target.postgres = Some(ChannelPostgresTarget {
        admin_password: vault.db_admin_password.clone(),
        main_password: vault.db_main_password.clone(),
        read_password: vault.db_read_password.clone(),
        reset: is_reset,
      });
      target.node = Some("^22.12.0".parse().expect("valid version version requirement"));
      target.rust = true;
      target.yarn = true;

      if let Some(borgbase_api_key) = self.borgbase_api_key {
        target.borg = Some(BorgTarget {
          borgbase_api_key: borgbase_api_key.to_string(),
          repo_password: vault.borgbase_repo.clone(),
        });
      }
      target
    };
    let channel_target = EternaltwinChannelTarget {
      inner: channel_target,
      backend_port: channel_config.backend_port,
      opentelemetry: match (channel_config.opentelemetry_endpoint.clone(), vault.uptrace_dsn.clone()) {
        (Some(endpoint), Some(uptrace_dsn)) => Some(EternaltwinOpentelemetry { endpoint, uptrace_dsn }),
        (None, None) => None,
        _ => panic!("both `opentelemetry_endpoint` and `uptrace_dsn` must be set or unset together"),
      },
      vault: EternaltwinVault {
        secret_key: vault.secret_key.clone(),
        mailer: vault.mailer.clone(),
        brute_production_secret: vault.brute_production_secret.clone(),
        brute_staging_secret: vault.brute_staging_secret.clone(),
        dinocard_production_secret: vault.dinocard_production_secret.clone(),
        dinocard_staging_secret: vault.dinocard_staging_secret.clone(),
        dinorpg_production_secret: vault.dinorpg_production_secret.clone(),
        dinorpg_staging_secret: vault.dinorpg_staging_secret.clone(),
        directquiz_secret: vault.directquiz_secret.clone(),
        emush_production_secret: vault.emush_production_secret.clone(),
        emush_staging_secret: vault.emush_staging_secret.clone(),
        epopotamo_production_secret: vault.epopotamo_production_secret.clone(),
        epopotamo_staging_secret: vault.epopotamo_staging_secret.clone(),
        eternalfest_production_secret: vault.eternalfest_production_secret.clone(),
        eternalfest_staging_secret: vault.eternalfest_staging_secret.clone(),
        kadokadeo_production_secret: vault.kadokadeo_production_secret.clone(),
        kadokadeo_staging_secret: vault.kadokadeo_staging_secret.clone(),
        kingdom_production_secret: vault.kingdom_production_secret.clone(),
        kingdom_staging_secret: vault.kingdom_staging_secret.clone(),
        myhordes_secret: vault.myhordes_secret.clone(),
        neoparc_production_secret: vault.neoparc_production_secret.clone(),
        neoparc_staging_secret: vault.neoparc_staging_secret.clone(),
        mjrt_secret: vault.mjrt_secret.clone(),
        wiki_secret: vault.wiki_secret.clone(),
        twinoid_client_id: vault.twinoid_client_id.clone(),
        twinoid_client_key: vault.twinoid_client_key.clone(),
      },
    };
    let release_target = EternaltwinReleaseTarget {
      channel: channel_target,
      repository: app_config.repository.clone(),
      git_ref: channel_config.release.clone(),
    };
    let deploy_task = DeployEternaltwin {
      pass: sudo_password,
      target: release_target,
    };
    deploy_task.run(host).await.map_err(SyncEternaltwinError::Deploy)
  }
}

impl<'a, 'h, H> NamedTask<&'h H> for SyncEternaltwin<'a>
where
  H: 'h + StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Clone + Debug + Sync,
{
  const NAME: TaskName = TaskName::new("system::SyncEternaltwin");
}

#[derive(Debug, Clone)]
pub struct SyncFrutibandas<'a> {
  pub sudo_password: &'a str,
  pub channel_name: &'a str,
  pub app_config: &'a RawAppConfig<RawFrutibandasChannelConfig>,
  pub channel_config: &'a RawFrutibandasChannelConfig,
  // pub vault: &'a RawFrutibandasVault,
}

#[derive(Debug, Error)]
pub enum SyncFrutibandasError {
  #[error("failed frutibandas deployment")]
  Deploy(#[source] DeployFrutibandasError),
}

#[async_trait]
impl<'a, 'h, H: StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for SyncFrutibandas<'a>
where
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Debug + Clone + Sync,
{
  type Output = TaskResult<(), SyncFrutibandasError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let app_name = "frutibandas";
    let sudo_password = self.sudo_password;
    let channel_name = self.channel_name;
    let app_config = self.app_config;
    let channel_config = self.channel_config;
    // let vault = self.vault;

    let full_name = format!("{}.{}", app_name, channel_name);
    eprintln!("Syncing: {full_name}");

    let is_reset = is_reset(host, full_name.as_str(), channel_config.state())
      .await
      .expect("check `is_reset`");
    if is_reset {
      eprintln!("State will be reset");
    }

    let channel_target = {
      let mut target = ChannelTarget::new(full_name.clone(), channel_name.to_string());
      target.domain = Some(ChannelDomainTarget {
        main: channel_config.domain.clone(),
        www: true,
        legacy: Vec::new(),
        alt: Vec::new(),
        cert: channel_config.https.as_ref().map(|conf| CertTarget {
          name: full_name.clone(),
          email: conf.email.clone(),
        }),
      });
      target.main_port = Some(channel_config.main_port);
      target.fallback_server = Some(channel_config.fallback_port);
      target
    };
    let frutibandas_channel_target = FrutibandasChannelTarget { inner: channel_target };
    let release_target = FrutibandasReleaseTarget {
      channel: frutibandas_channel_target,
      repository: app_config.repository.clone(),
      git_ref: channel_config.release.clone(),
    };
    let deploy_task = DeployFrutibandas {
      pass: sudo_password,
      target: release_target,
    };
    deploy_task.run(host).await.map_err(SyncFrutibandasError::Deploy)
  }
}

impl<'a, 'h, H> NamedTask<&'h H> for SyncFrutibandas<'a>
where
  H: 'h + StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Clone + Debug + Sync,
{
  const NAME: TaskName = TaskName::new("system::SyncFrutibandas");
}

#[derive(Debug, Clone)]
pub struct SyncKingdom<'a> {
  pub sudo_password: &'a str,
  pub channel_name: &'a str,
  pub app_config: &'a RawAppConfig<RawKingdomChannelConfig>,
  pub channel_config: &'a RawKingdomChannelConfig,
  pub vault: &'a RawKingdomVault,
}

#[derive(Debug, Error)]
pub enum SyncKingdomError {
  #[error("failed kingdom deployment")]
  Deploy(#[source] DeployKingdomError),
}

#[async_trait]
impl<'a, 'h, H> AsyncFn<&'h H> for SyncKingdom<'a>
where
  H: 'h + StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Clone + Debug + Sync,
{
  type Output = TaskResult<(), SyncKingdomError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let app_name = "kingdom";
    let sudo_password = self.sudo_password;
    let channel_name = self.channel_name;
    let app_config = self.app_config;
    let channel_config = self.channel_config;
    let vault = self.vault;

    let full_name = format!("{}.{}", app_name, channel_name);
    eprintln!("Syncing: {full_name}");

    let is_reset = is_reset(host, full_name.as_str(), channel_config.state())
      .await
      .expect("check `is_reset`");
    if is_reset {
      eprintln!("State will be reset");
    }

    let channel_target = {
      let mut target = ChannelTarget::new(full_name.clone(), channel_name.to_string());
      target.domain = Some(ChannelDomainTarget {
        main: channel_config.domain.clone(),
        www: true,
        legacy: Vec::new(),
        alt: Vec::new(),
        cert: channel_config.https.as_ref().map(|conf| CertTarget {
          name: full_name.clone(),
          email: conf.email.clone(),
        }),
      });
      target.main_port = Some(channel_config.main_port);
      target.fallback_server = Some(channel_config.fallback_port);
      target.postgres = Some(ChannelPostgresTarget {
        admin_password: vault.db_admin_password.clone(),
        main_password: vault.db_main_password.clone(),
        read_password: vault.db_read_password.clone(),
        reset: is_reset,
      });
      target.php = Some(PhpAvailable {
        ext_curl: false,
        ext_ctype: true,
        ext_iconv: true,
        ext_intl: false,
        ext_opentelemetry: true,
        ext_pdo_pgsql: true,
        ext_pgsql: false,
        ext_protobuf: true,
        ext_sodium: false,
        ext_zip: false,
      });
      target
    };
    let channel_target = KingdomChannelTarget {
      inner: channel_target,
      vault: KingdomVault {
        etwin_client_secret: vault.etwin_client_secret.clone(),
        app_secret: vault.app_secret.clone(),
      },
      etwin_uri: Url::parse(&channel_config.etwin_uri).expect("invalid etwin_uri"),
      etwin_client_id: channel_config.etwin_client_id.clone(),
    };
    let release_target = KingdomReleaseTarget {
      channel: channel_target,
      repository: app_config.repository.clone(),
      git_ref: channel_config.release.clone(),
    };
    let deploy_task = DeployKingdom {
      pass: sudo_password,
      target: release_target,
    };
    deploy_task.run(host).await.map_err(SyncKingdomError::Deploy)
  }
}

impl<'a, 'h, H> NamedTask<&'h H> for SyncKingdom<'a>
where
  H: 'h + StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Clone + Debug + Sync,
{
  const NAME: TaskName = TaskName::new("system::SyncKingdom");
}

#[derive(Debug, Clone)]
pub struct SyncNeoparc<'a> {
  pub sudo_password: &'a str,
  pub channel_name: &'a str,
  pub app_config: &'a RawAppConfig<RawNeoparcChannelConfig>,
  pub channel_config: &'a RawNeoparcChannelConfig,
  pub vault: &'a RawNeoparcVault,
}

#[derive(Debug, Error)]
pub enum SyncNeoparcError {
  #[error("failed neoparc deployment")]
  Deploy(#[source] DeployNeoparcError),
}

#[async_trait]
impl<'a, 'h, H: StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for SyncNeoparc<'a>
where
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Debug + Clone + Sync,
{
  type Output = TaskResult<(), SyncNeoparcError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let app_name = "neoparc";
    let sudo_password = self.sudo_password;
    let channel_name = self.channel_name;
    let app_config = self.app_config;
    let channel_config = self.channel_config;
    let vault = self.vault;

    let full_name = format!("{}.{}", app_name, channel_name);
    eprintln!("Syncing: {full_name}");

    let is_reset = is_reset(host, full_name.as_str(), channel_config.state())
      .await
      .expect("check `is_reset`");
    if is_reset {
      eprintln!("State will be reset");
    }

    let channel_target = {
      let mut target = ChannelTarget::new(full_name.clone(), channel_name.to_string());
      target.domain = Some(ChannelDomainTarget {
        main: channel_config.domain.clone(),
        www: true,
        legacy: match channel_config.domain.as_str() {
          "neoparc.eternaltwin.org" => vec![
            "neoparc.eternal-twin.net".to_string(),
            "www.neoparc.eternal-twin.net".to_string(),
          ],
          _ => Vec::new(),
        },
        alt: Vec::new(),
        cert: channel_config.https.as_ref().map(|conf| CertTarget {
          name: full_name.clone(),
          email: conf.email.clone(),
        }),
      });
      target.main_port = Some(channel_config.main_port);
      target.fallback_server = Some(channel_config.fallback_port);
      target.postgres = Some(ChannelPostgresTarget {
        admin_password: vault.db_admin_password.clone(),
        main_password: vault.db_main_password.clone(),
        read_password: vault.db_read_password.clone(),
        reset: is_reset,
      });
      target.node = Some("^20.11.0".parse().expect("valid version version requirement"));
      target.java = true;
      target.yarn = true;
      target
    };
    let neoparc_channel_target = NeoparcChannelTarget {
      inner: channel_target,
      vault: NeoparcVault {
        secret: vault.secret.clone(),
        discord_token: vault.discord_token.clone(),
        salt: vault.salt.clone(),
        seed: vault.seed.clone(),
        test: vault.test.clone(),
        etwin_client_secret: vault.etwin_client_secret.clone(),
      },
      etwin_uri: channel_config.etwin_uri.clone(),
      etwin_client_id: channel_config.etwin_client_id.clone(),
    };
    let release_target = NeoparcReleaseTarget {
      channel: neoparc_channel_target,
      repository: app_config.repository.clone(),
      git_ref: channel_config.release.clone(),
    };
    let deploy_task = DeployNeoparc {
      pass: sudo_password,
      target: release_target,
    };
    deploy_task.run(host).await.map_err(SyncNeoparcError::Deploy)
  }
}

impl<'a, 'h, H> NamedTask<&'h H> for SyncNeoparc<'a>
where
  H: 'h + StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Clone + Debug + Sync,
{
  const NAME: TaskName = TaskName::new("system::SyncNeoparc");
}

#[derive(Debug, Clone)]
pub struct SyncUptrace<'a> {
  pub sudo_password: &'a str,
  pub ch_sys_password: &'a str,
  pub channel_name: &'a str,
  pub app_config: &'a RawAppConfig<RawUptraceChannelConfig>,
  pub channel_config: &'a RawUptraceChannelConfig,
  pub vault: &'a RawUptraceVault,
}

#[derive(Debug, Error)]
pub enum SyncUptraceError {
  #[error("failed auto deployment")]
  Deploy(#[source] DeployUptraceError),
}

#[async_trait]
impl<'a, 'h, H> AsyncFn<&'h H> for SyncUptrace<'a>
where
  H: 'h + StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Clone + Debug + Sync,
{
  type Output = TaskResult<(), SyncUptraceError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let app_name = "uptrace";
    let sudo_password = self.sudo_password;
    let channel_name = self.channel_name;
    let channel_config = self.channel_config;
    let vault = self.vault;

    let full_name = format!("{}.{}", app_name, channel_name);
    eprintln!("Syncing: {full_name}");

    let is_reset = is_reset(host, full_name.as_str(), channel_config.state())
      .await
      .expect("check `is_reset`");
    if is_reset {
      eprintln!("State will be reset");
    }

    let channel_target = {
      let mut target = ChannelTarget::new(full_name.clone(), channel_name.to_string());
      target.data_dir = true;
      target.domain = Some(ChannelDomainTarget {
        main: channel_config.domain.clone(),
        www: true,
        legacy: Vec::new(),
        alt: Vec::new(),
        cert: channel_config.https.as_ref().map(|conf| CertTarget {
          name: full_name.clone(),
          email: conf.email.clone(),
        }),
      });
      target.main_port = Some(channel_config.main_port);
      target.fallback_server = Some(channel_config.fallback_port);
      target.postgres = Some(ChannelPostgresTarget {
        admin_password: vault.pg_admin_password.clone(),
        main_password: vault.pg_main_password.clone(),
        read_password: vault.pg_read_password.clone(),
        reset: is_reset,
      });
      target.clickhouse = Some(ChannelClickhouseTarget {
        sys_password: self.ch_sys_password.to_string(),
        main_password: vault.ch_main_password.clone(),
        read_password: vault.ch_read_password.clone(),
        reset: is_reset,
      });
      target
    };
    let channel_target = UptraceChannelTarget {
      inner: channel_target,
      grpc_port: channel_config.grpc_port,
      uptrace_projects: channel_config.projects.clone(),
      uptrace_users: channel_config.users.clone(),
      vault: UptraceVault {
        pg_admin_password: vault.pg_admin_password.clone(),
        pg_main_password: vault.pg_main_password.clone(),
        pg_read_password: vault.pg_read_password.clone(),
        ch_main_password: vault.ch_main_password.clone(),
        ch_read_password: vault.ch_read_password.clone(),
        secret: vault.secret.clone(),
        uptrace_system_project_token: vault.uptrace_system_project_token.clone(),
        uptrace_project_token: vault.uptrace_project_token.clone(),
        uptrace_user_password: vault.uptrace_user_password.clone(),
      },
    };
    let release_target = UptraceReleaseTarget {
      channel: channel_target,
      version: channel_config.version.clone(),
      sha256_digest: channel_config.sha256_digest.clone(),
    };
    let deploy_task = DeployUptrace {
      pass: sudo_password,
      target: release_target,
    };
    deploy_task.run(host).await.map_err(SyncUptraceError::Deploy)
  }
}

impl<'a, 'h, H> NamedTask<&'h H> for SyncUptrace<'a>
where
  H: 'h + StatefulHost + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Clone + Debug + Sync,
{
  const NAME: TaskName = TaskName::new("system::SyncUptrace");
}

async fn is_reset<H: StatefulHost>(
  host: &H,
  full_name: &str,
  state: Option<&RawStateConfig>,
) -> Result<bool, TryAcquireIdempotencyTokenError> {
  let state_token = match state {
    Some(state) => {
      let state_token = IdempotencyKey {
        name: format!("{}.state.{}", full_name, &state.token),
        expiry: None,
      };
      host.try_acquire_idempotency_token(state_token).await?
    }
    None => None,
  };

  Ok(state.map(|s| s.action == StateAction::Reset).unwrap_or(false) && state_token.is_some())
}
