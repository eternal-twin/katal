use crate::etwin::channel::ResolvedHttps;
use std::fmt;

pub struct NginxRedirectionServer<'a, Source, Target>
where
  Source: fmt::Display,
  Target: fmt::Display,
{
  /// Source domains, those will be redirected to `target`
  pub sources: &'a [Source],
  /// Target domain (where to redirect)
  pub target: Target,
  /// SSL certificates for HTTPS support.
  pub cert: Option<&'a ResolvedHttps>,
}

impl<'a, Source, Target> fmt::Display for NginxRedirectionServer<'a, Source, Target>
where
  Source: fmt::Display,
  Target: fmt::Display,
{
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    assert!(!self.sources.is_empty());
    write!(f, "server {{\n  server_name")?;
    for src in self.sources.iter() {
      write!(f, r#" {}"#, src)?;
    }

    if let Some(cert) = self.cert {
      write!(
        f,
        r#"

  listen 80;
  listen 443 ssl;
  listen [::]:80;
  listen [::]:443 ssl;

  ssl_certificate {cert};
  ssl_certificate_key {cert_key};

  return 307 https://{target}$request_uri;
}}"#,
        target = self.target,
        cert = cert.fullchain_cert_path.to_str().unwrap(),
        cert_key = cert.cert_key_path.to_str().unwrap(),
      )
    } else {
      write!(
        f,
        r#"

  listen 80;
  listen [::]:80;

  return 307 http://{target}$request_uri;
}}"#,
        target = self.target
      )
    }
  }
}

#[cfg(test)]
mod test {
  use super::NginxRedirectionServer;

  #[test]
  fn redirect_http_www() {
    let input = NginxRedirectionServer {
      sources: &["www.example.com"],
      target: "example.com",
      cert: None,
    };
    let actual = input.to_string();
    let expected = r#"server {
  server_name www.example.com

  listen 80;
  listen [::]:80;

  return 307 http://example.com$request_uri;
}"#;
    assert_eq!(actual, expected)
  }

  #[test]
  fn redirect_http_many() {
    let input = NginxRedirectionServer {
      sources: &["www.example.com", "example.com", "www.example.org"],
      target: "example.org",
      cert: None,
    };
    let actual = input.to_string();
    let expected = r#"server {
  server_name www.example.com example.com www.example.org

  listen 80;
  listen [::]:80;

  return 307 http://example.org$request_uri;
}"#;
    assert_eq!(actual, expected)
  }
}
