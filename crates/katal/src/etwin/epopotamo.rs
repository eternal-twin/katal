use crate::etwin::channel::{
  ChannelDomain, ChannelFallbackServer, ChannelPaths, ChannelPhp, ChannelPostgres, ChannelTarget, ChannelTargetError,
  NodeEnv, PostgresCredentials,
};
use crate::task::fs::{EnsureDir, EnsureDirError, EnsureDirSymlink, EnsureDirSymlinkError, EnsureFile};
use crate::task::git::{GitCheckout, GitCheckoutError};
use crate::task::nginx::{NginxAvailableSite, NginxAvailableSiteError, NginxEnableSite, NginxEnableSiteError};
use crate::task::witness::{FsWitness, FsWitnessError};
use crate::task::yarn::YarnEnv;
use crate::task::{exec_as, AsyncFn, ExecAsError, NamedTask, TaskName, TaskRef, TaskResult, TaskSuccess};
use async_trait::async_trait;
use asys::common::fs::FsHost;
use asys::linux::fs::{FileMode, LinuxFsHost, LinuxMetadata, ResolveError};
use asys::linux::user::{GroupRef, LinuxUser, LinuxUserHost, UserRef};
use asys::local_linux::LocalLinux;
use asys::{Command, ExecHost};
use katal_nginx::{LocationDirective, LocationPattern, RawFragment};
use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};
use std::fmt;
use std::fmt::Debug;
use std::path::PathBuf;
use url::Url;

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct EpopotamoChannelTarget<S: AsRef<str> = String> {
  pub inner: ChannelTarget<S>,
  pub vault: EpopotamoVault,
  pub etwin_uri: Url,
  pub etwin_client_id: String,
  pub socket_port: u16,
  pub permanent_admin: String,
  pub profiling: bool,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct EpopotamoVault {
  pub etwin_client_secret: String,
  // pub app_secret: String,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct EpopotamoChannel<User: LinuxUser, S: AsRef<str> = String> {
  full_name: S,
  short_name: S,
  user: User,
  domain: ChannelDomain<S>,
  paths: ChannelPaths,
  fallback_server: ChannelFallbackServer,
  postgres: ChannelPostgres,
  vault: EpopotamoVault,
  php: ChannelPhp,
  node: NodeEnv,
  yarn: YarnEnv,
  etwin_uri: Url,
  etwin_client_id: String,
  socket_port: u16,
  permanent_admin: String,
  profiling: bool,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct EpopotamoReleaseTarget<S: AsRef<str> = String> {
  pub channel: EpopotamoChannelTarget,
  pub repository: S,
  pub git_ref: String,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct EpopotamoRelease<User: LinuxUser, S: AsRef<str> = String> {
  release_dir: PathBuf,
  repository: S,
  repo_dir: PathBuf,
  channel: EpopotamoChannel<User, S>,
  git_ref: String,
}

#[derive(Serialize, Deserialize)]
pub struct DeployEpopotamo<S: AsRef<str> = String> {
  pub pass: S,
  pub target: EpopotamoReleaseTarget,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum DeployEpopotamoError {
  #[error("failed to exec epopotamo deployment task")]
  Exec(#[source] ExecAsError),
  #[error("failed to deploy epopotamo release")]
  Deploy(#[source] DeployEpopotamoAsRootError),
}

#[async_trait]
impl<'h, H, S> AsyncFn<&'h H> for DeployEpopotamo<S>
where
  H: ExecHost,
  S: AsRef<str> + Send + Sync,
{
  type Output = TaskResult<(), DeployEpopotamoError>;

  async fn run(&self, _host: &'h H) -> Self::Output {
    let input = DeployEpopotamoAsRoot {
      target: self.target.clone(),
    };
    exec_as::<LocalLinux, DeployEpopotamoAsRoot>(UserRef::ROOT, Some(self.pass.as_ref()), &input)
      .await
      .map_err(DeployEpopotamoError::Exec)?
      .map_err(DeployEpopotamoError::Deploy)
  }
}

impl<'h, H: 'h + ExecHost> NamedTask<&'h H> for DeployEpopotamo {
  const NAME: TaskName = TaskName::new("epopotamo::DeployEpopotamo");
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DeployEpopotamoAsRoot {
  target: EpopotamoReleaseTarget,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum DeployEpopotamoAsRootError {
  #[error("failed to use witness file")]
  Witness(#[source] FsWitnessError),
  #[error("failed to run task `EpopotamoReleaseTarget`")]
  Task(#[source] EpopotamoReleaseTargetError),
}

#[async_trait]
impl<'h, H: ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for DeployEpopotamoAsRoot
where
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Debug + Clone + Send + Sync + Serialize + DeserializeOwned,
  H::Group: Debug + Send + Sync,
{
  type Output = TaskResult<(), DeployEpopotamoAsRootError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let paths: ChannelPaths = self.target.channel.inner.paths();
    let witness = paths.home.join("epopotamo_release.witness");
    let mut changed = false;
    let res = FsWitness::new(witness, TaskRef(&self.target))
      .run(host)
      .await
      .map_err(DeployEpopotamoAsRootError::Witness)?;
    changed = res.changed || changed;
    let res = res.output.map_err(DeployEpopotamoAsRootError::Task)?;
    changed = res.changed || changed;
    Ok(TaskSuccess { changed, output: () })
  }
}

impl<'h, H> NamedTask<&'h H> for DeployEpopotamoAsRoot
where
  H: 'h + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Debug + Clone + Send + Sync + Serialize + DeserializeOwned,
  H::Group: Debug + Send + Sync,
{
  const NAME: TaskName = TaskName::new("epopotamo::DeployEpopotamoAsRoot");
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum EpopotamoReleaseTargetError {
  #[error("failed to initialize epopotamo channel")]
  Channel(#[source] EpopotamoChannelTargetError),
  #[error("failed to ensure release directory")]
  ReleaseDir(#[source] EnsureDirError),
  #[error("failed to execute build task")]
  ExecBuild(#[source] ExecAsError),
  #[error("failed to build epopotamo release")]
  Build(#[source] BuildEpopotamoReleaseError),
  #[error("failed to update active epopotamo release")]
  UpdateActive(#[source] UpdateActiveEpopotamoReleaseError),
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for EpopotamoReleaseTarget
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Debug + Clone + Send + Sync,
  H::Group: Debug + Send + Sync,
{
  type Output = TaskResult<EpopotamoRelease<H::User>, EpopotamoReleaseTargetError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let res = self
      .channel
      .run(host)
      .await
      .map_err(EpopotamoReleaseTargetError::Channel)?;
    let channel = res.output;
    let mut changed = res.changed;

    let release_dir = channel.paths.releases.join(&self.git_ref);

    eprintln!("Create directory: {}", release_dir.display());
    changed = EnsureDir::new(release_dir.clone())
      .owner(UserRef::Id(channel.user.uid()))
      .group(GroupRef::Id(channel.user.gid()))
      .mode(FileMode::OWNER_ALL | FileMode::ALL_READ | FileMode::ALL_EXEC)
      .run(host)
      .await
      .map_err(EpopotamoReleaseTargetError::ReleaseDir)?
      .changed
      || changed;

    let repo_dir = release_dir.join("repo");

    let br = BuildEpopotamoRelease {
      repo_dir: repo_dir.clone(),
      remote: self.repository.clone(),
      git_ref: self.git_ref.clone(),
      backend_config: config::BackendConfig::from_channel(&channel, true, self.git_ref.clone()),
      node: channel.node.clone(),
      yarn: channel.yarn.clone(),
    };

    changed = exec_as::<LocalLinux, _>(UserRef::Id(channel.user.uid()), None, &br)
      .await
      .map_err(EpopotamoReleaseTargetError::ExecBuild)?
      .map_err(EpopotamoReleaseTargetError::Build)?
      .changed
      || changed;

    let release = EpopotamoRelease {
      release_dir,
      repository: self.repository.clone(),
      repo_dir,
      channel,
      git_ref: self.git_ref.clone(),
    };

    changed = (UpdateActiveEpopotamoRelease {
      release: release.clone(),
    })
    .run(host)
    .await
    .map_err(EpopotamoReleaseTargetError::UpdateActive)?
    .changed
      || changed;

    // eprintln!("Reapply Postgres permissions");
    // changed = PostgresDbPermissions::new(
    //   &release.channel.postgres.name,
    //   release.channel.postgres.admin.name.clone(),
    //   release.channel.postgres.main.name.clone(),
    //   release.channel.postgres.read.name.clone(),
    // )
    // .run(host)
    // .await?
    // .changed
    //   || changed;

    Ok(TaskSuccess {
      changed,
      output: release,
    })
  }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum EpopotamoChannelTargetError {
  #[error("base channel creation failed")]
  Base(#[source] ChannelTargetError),
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for EpopotamoChannelTarget
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  H::Group: Debug + Send + Sync,
  H::User: Debug + Clone + Send + Sync,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<EpopotamoChannel<H::User>, EpopotamoChannelTargetError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    eprintln!("Epopotamo: Create channel");
    let res = self.inner.run(host).await.map_err(EpopotamoChannelTargetError::Base)?;

    eprintln!("Channel ready");
    let output = EpopotamoChannel {
      full_name: res.output.full_name,
      short_name: res.output.short_name,
      user: res.output.user,
      domain: res.output.domain.expect("Expected associated domain name"),
      paths: res.output.paths,
      fallback_server: res.output.fallback_server.expect("Expected fallback server"),
      postgres: res.output.postgres.expect("Expected Postgres database"),
      vault: EpopotamoVault {
        etwin_client_secret: self.vault.etwin_client_secret.clone(),
        // app_secret: self.vault.app_secret.clone(),
      },
      php: res.output.php.expect("Expected PHP environment"),
      node: res.output.node.expect("Expected Node environment"),
      yarn: res.output.yarn.expect("Expected Yarn environment"),
      etwin_uri: self.etwin_uri.clone(),
      etwin_client_id: self.etwin_client_id.clone(),
      socket_port: self.socket_port,
      permanent_admin: self.permanent_admin.clone(),
      profiling: self.profiling,
    };

    Ok(TaskSuccess {
      changed: res.changed,
      output,
    })
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct BuildEpopotamoRelease {
  repo_dir: PathBuf,
  remote: String,
  git_ref: String,
  backend_config: config::BackendConfig,
  node: NodeEnv,
  yarn: YarnEnv,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize, thiserror::Error)]
pub enum BuildEpopotamoReleaseError {
  #[error("failed to checkout Git repository")]
  GitCheckout(#[from] GitCheckoutError),
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for BuildEpopotamoRelease
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Send + Sync,
{
  type Output = TaskResult<(), BuildEpopotamoReleaseError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let gc = GitCheckout {
      path: self.repo_dir.clone(),
      remote: self.remote.clone(),
      r#ref: self.git_ref.clone(),
    };

    eprintln!("Checkout repository: {:?} {:?}", self.remote, self.git_ref);
    let res = gc.run(host).await?;
    let mut changed = res.changed;

    let config_dir = self.repo_dir.join("_CONFIG");

    eprintln!("Create backend config dir");
    changed = EnsureDir::new(config_dir.clone())
      // .owner(UserRef::Id(channel.user.uid()))
      // .group(GroupRef::Id(channel.user.gid()))
      .mode(FileMode::OWNER_ALL)
      .run(host)
      .await
      .expect("failed to create dir")
      .changed
      || changed;

    eprintln!("Apply backend config (server)");
    EnsureFile::new(config_dir.join("config-server.json"))
      .content(self.backend_config.server.to_string())
      .mode(FileMode::OWNER_READ | FileMode::OWNER_WRITE)
      .run(host)
      .await
      .unwrap();

    eprintln!("Apply backend config (database)");
    EnsureFile::new(config_dir.join("database-logins.json"))
      .content(self.backend_config.db.to_string())
      .mode(FileMode::OWNER_READ | FileMode::OWNER_WRITE)
      .run(host)
      .await
      .unwrap();

    eprintln!("Install PHP dependencies");
    let cmd = Command::new("composer")
      .arg("install")
      .arg("--no-interaction")
      .arg("--no-ansi")
      .current_dir(self.repo_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    eprintln!("Install Node dependencies");
    let cmd = Command::new("yarn")
      .arg("install")
      .arg("--immutable")
      .prepend_path(self.node.bin_dir.display().to_string())
      .prepend_path(self.yarn.bin_dir.display().to_string())
      .current_dir(self.repo_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    eprintln!("Build the website");
    let cmd = Command::new("composer")
      .arg("run-script")
      .arg("--")
      .arg("build")
      .arg("--no-confirm")
      .prepend_path(self.node.bin_dir.display().to_string())
      .prepend_path(self.yarn.bin_dir.display().to_string())
      .current_dir(self.repo_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    Ok(TaskSuccess { changed, output: () })
  }
}

impl<'h, H> NamedTask<&'h H> for BuildEpopotamoRelease
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Send + Sync,
{
  const NAME: TaskName = TaskName::new("epopotamo::BuildEpopotamoRelease");
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize)]
pub struct UpdateActiveEpopotamoRelease<User: LinuxUser, S: AsRef<str> = String> {
  release: EpopotamoRelease<User, S>,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum UpdateActiveEpopotamoReleaseError {
  #[error("failed to resolve link to the active release")]
  ResolveActive(#[source] ResolveError),
  #[error("failed to resolve new release")]
  ResolveNew(#[source] ResolveError),
  #[error("failed to symlink previous release")]
  LinkPrevious(#[source] EnsureDirSymlinkError),
  #[error("failed to configure nginx (available)")]
  NginxAvailable(#[source] NginxAvailableSiteError),
  #[error("failed to configure nginx (enable)")]
  NginxEnable(#[source] NginxEnableSiteError),
  #[error("failed to execute DB sync task")]
  ExecDbSync(#[source] ExecAsError),
  #[error("failed to DB sync")]
  DbSync(#[source] UpgradeEpopotamoDatabaseError),
  #[error("failed to symlink active release")]
  LinkActive(#[source] EnsureDirSymlinkError),
}

#[async_trait]
impl<'h, H, S> AsyncFn<&'h H> for UpdateActiveEpopotamoRelease<H::User, S>
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Send + Sync,
  S: AsRef<str> + Send + Sync,
{
  type Output = TaskResult<(), UpdateActiveEpopotamoReleaseError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let mut changed = false;
    let active_release_link = self.release.channel.paths.home.join("active");
    let previous_release_link = self.release.channel.paths.home.join("previous");
    let old_release_dir = {
      match host.resolve(&active_release_link) {
        Ok(old) => Some(old),
        Err(ResolveError::NotFound) => None,
        Err(e) => return Err(UpdateActiveEpopotamoReleaseError::ResolveActive(e)),
      }
    };
    let new_release_dir = host
      .resolve(&self.release.release_dir)
      .map_err(UpdateActiveEpopotamoReleaseError::ResolveNew)?;

    if let Some(old_release_dir) = old_release_dir {
      if old_release_dir != new_release_dir {
        eprintln!("Start to disable old release");
        // There is already an active release but it does not match the target
        // release: stop the old version before proceeding to free the channel
        // resources (database, port)
        EnsureDirSymlink::new(previous_release_link.clone())
          .points_to(old_release_dir.clone())
          .owner(self.release.channel.user.uid())
          .group(self.release.channel.user.gid())
          .mode(FileMode::ALL_READ | FileMode::ALL_EXEC)
          .run(host)
          .await
          .map_err(UpdateActiveEpopotamoReleaseError::LinkPrevious)?;

        eprintln!("Old release disabled");
        changed = true;
      }
    }

    eprintln!("UpgradingDatabase");
    changed = exec_as::<LocalLinux, _>(
      UserRef::Id(self.release.channel.user.uid()),
      None,
      &UpgradeEpopotamoDatabase {
        repo_dir: self.release.repo_dir.clone(),
        db_name: self.release.channel.postgres.name.clone(),
        db_admin: self.release.channel.postgres.admin.clone(),
      },
    )
    .await
    .map_err(UpdateActiveEpopotamoReleaseError::ExecDbSync)?
    .map_err(UpdateActiveEpopotamoReleaseError::DbSync)?
    .changed
      || changed;

    eprintln!("Generating nginx config");
    let nginx_config = get_nginx_config(&self.release.channel);
    eprintln!("Writing nginx config");
    NginxAvailableSite::new(self.release.channel.full_name.as_ref(), nginx_config)
      .run(host)
      .await
      .map_err(UpdateActiveEpopotamoReleaseError::NginxAvailable)?;
    eprintln!("Enabling nginx config");
    NginxEnableSite::new(self.release.channel.full_name.as_ref())
      .run(host)
      .await
      .map_err(UpdateActiveEpopotamoReleaseError::NginxEnable)?;

    eprintln!("Marking release as active");
    EnsureDirSymlink::new(active_release_link.clone())
      .points_to(new_release_dir.clone())
      .owner(self.release.channel.user.uid())
      .group(self.release.channel.user.gid())
      .mode(FileMode::ALL_READ | FileMode::ALL_EXEC)
      .run(host)
      .await
      .map_err(UpdateActiveEpopotamoReleaseError::LinkActive)?;

    Ok(TaskSuccess { changed, output: () })
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct UpgradeEpopotamoDatabase {
  repo_dir: PathBuf,
  db_name: String,
  db_admin: PostgresCredentials,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum UpgradeEpopotamoDatabaseError {}

#[async_trait]
impl<'h, H: ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for UpgradeEpopotamoDatabase
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = Result<TaskSuccess<()>, UpgradeEpopotamoDatabaseError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let cmd = Command::new("composer")
      .arg("run-script")
      .arg("--")
      .arg("db:sync")
      .arg("--no-confirm")
      .current_dir(self.repo_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    Ok(TaskSuccess {
      changed: true,
      output: (),
    })
  }
}

impl<'h, H: 'h + ExecHost + LinuxUserHost + LinuxFsHost> NamedTask<&'h H> for UpgradeEpopotamoDatabase
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  const NAME: TaskName = TaskName::new("epopotamo::UpgradeEpopotamoDatabase");
}

pub fn get_nginx_config<U: LinuxUser, S: AsRef<str>>(channel: &EpopotamoChannel<U, S>) -> String {
  let access_log = channel.paths.logs.join("main.nginx.access.log");
  let error_log = channel.paths.logs.join("main.nginx.error.log");
  let web_root = channel.paths.home.join("active/repo/public");

  let location_root_php = LocationDirective {
    pattern: LocationPattern::Exact("/index.php"),
    body: RawFragment {
      text: &format!(
        r#"
    # Indicate that this can only reached through indirect redirect (here try_files)
    internal;
    fastcgi_pass unix:{php_fpm_socket};
    fastcgi_split_path_info ^(.+\.php)(/.*)$;
    include fastcgi_params;

    # Use `$realpath_root` to avoid bad interractions between symlinks and PHP's OPcache
    # See https://github.com/zendtech/ZendOptimizerPlus/issues/126
    fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
    fastcgi_param DOCUMENT_ROOT $realpath_root;
    # Prevent buffering since Server Sent Events (SSE) are basically an infinte stream
    fastcgi_buffering off;
    "#,
        php_fpm_socket = channel.php.socket.to_str().unwrap(),
      ),
    },
    indent: None,
  };

  let location_default = LocationDirective {
    pattern: LocationPattern::WeakPrefix("/"),
    body: RawFragment {
      text: r#"
    try_files $uri /index.php$is_args$args;
    "#,
    },
    indent: None,
  };

  let main_directives = format!(
    r#"  # Space-separated hostnames (wildcard * is accepted)
  server_name {domain};

  access_log {access_log};
  error_log {error_log};
  client_max_body_size 5M;

  root {web_root};

  {location_root_php}

  {location_default}
"#,
    domain = channel.domain.main.as_ref(),
    access_log = access_log.to_str().unwrap(),
    error_log = error_log.to_str().unwrap(),
    web_root = web_root.to_str().unwrap(),
  );

  match channel.domain.cert.as_ref() {
    Some(https) => format!(
      r#"# HTTPS
server {{
  # HTTPS port
  listen 443 ssl;
  listen [::]:443 ssl;

  ssl_certificate {cert};
  ssl_certificate_key {cert_key};

  # # HSTS (ngx_http_headers_module is required) (63072000 seconds)
  # add_header Strict-Transport-Security "max-age=63072000" always;

  {main_directives}
}}

# HTTP
server {{
  # HTTP port
  listen 80;
  listen [::]:80;
  server_name {domain};
  return 301 https://$host$request_uri;
}}

# www.
server {{
  listen 80;
  listen 443 ssl;
  listen [::]:80;
  listen [::]:443 ssl;

  server_name www.{domain};

  ssl_certificate {cert};
  ssl_certificate_key {cert_key};

  return 307 https://{domain}$request_uri;
}}

"#,
      main_directives = main_directives,
      domain = channel.domain.main.as_ref(),
      cert = https.fullchain_cert_path.to_str().unwrap(),
      cert_key = https.cert_key_path.to_str().unwrap(),
    ),
    None => format!(
      r#"# HTTP
server {{
  # HTTP port
  listen 80;
  listen [::]:80;

  {main_directives}
}}

# www.
server {{
  listen 80;
  listen [::]:80;

  server_name www.{domain};

  return 307 http://{domain}$request_uri;
}}

"#,
      main_directives = main_directives,
      domain = channel.domain.main.as_ref(),
    ),
  }
}

pub(crate) mod config {
  use crate::etwin::epopotamo::EpopotamoChannel;
  use asys::linux::user::LinuxUser;
  use serde::{Deserialize, Serialize};
  use std::fmt;

  #[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
  pub(crate) struct BackendConfig<S: AsRef<str> = String> {
    pub(crate) db: DbConfig<S>,
    pub(crate) server: ServerConfig<S>,
  }

  impl BackendConfig<String> {
    pub fn from_channel(channel: &EpopotamoChannel<impl LinuxUser>, admin_db_role: bool, git_revision: String) -> Self {
      let db_role = if admin_db_role {
        &channel.postgres.admin
      } else {
        &channel.postgres.main
      };
      let protocol = if channel.domain.cert.is_some() { "https" } else { "http" };
      let uri = format!("{}://{}/", protocol, channel.domain.main.as_str());
      let deployment = DeploymentInfo {
        app: String::from("epopotamo"),
        channel_short: channel.short_name.clone(),
        channel_full: channel.full_name.clone(),
        git_revision,
        profiling: channel.profiling,
      };
      Self {
        server: ServerConfig {
          etwin_uri: channel.etwin_uri.as_str().to_string(),
          popotamo_uri: uri,
          etwin_client_id: channel.etwin_client_id.clone(),
          etwin_client_secret: channel.vault.etwin_client_secret.clone(),
          socket_port: channel.socket_port,
          domain_name: channel.domain.main.clone(),
          permanent_admin: channel.permanent_admin.clone(),
          deployment,
        },
        db: DbConfig {
          host: channel.postgres.host.clone(),
          port: channel.postgres.port,
          dbname: channel.postgres.name.clone(),
          login: db_role.name.as_str().to_string(),
          password: db_role.password.clone(),
        },
      }
    }
  }

  #[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
  pub(crate) struct ServerConfig<S: AsRef<str> = String> {
    #[serde(rename = "etwinUri")]
    etwin_uri: S,
    #[serde(rename = "popotamoUri")]
    popotamo_uri: S,
    #[serde(rename = "etwinClientId")]
    etwin_client_id: S,
    #[serde(rename = "etwinClientSecret")]
    etwin_client_secret: S,
    #[serde(rename = "socketPort")]
    socket_port: u16,
    #[serde(rename = "popotamoDomain")]
    domain_name: S,
    #[serde(rename = "permanentAdmins")]
    permanent_admin: S,
    #[serde(rename = "deployment")]
    deployment: DeploymentInfo,
  }

  impl<S> fmt::Display for ServerConfig<S>
  where
    Self: Serialize,
    S: AsRef<str>,
  {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
      writeln!(f, "{}", serde_json::to_string_pretty(self).unwrap())
    }
  }

  #[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
  pub(crate) struct DeploymentInfo<S: AsRef<str> = String> {
    #[serde(rename = "app")]
    app: S,
    #[serde(rename = "channelShort")]
    channel_short: S,
    #[serde(rename = "channelFull")]
    channel_full: S,
    #[serde(rename = "gitRevision")]
    git_revision: S,
    #[serde(rename = "profiling")]
    profiling: bool,
  }

  impl<S> fmt::Display for DeploymentInfo<S>
  where
    Self: Serialize,
    S: AsRef<str>,
  {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
      writeln!(f, "{}", serde_json::to_string_pretty(self).unwrap())
    }
  }

  #[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
  pub(crate) struct DbConfig<S: AsRef<str> = String> {
    host: S,
    port: u16,
    dbname: S,
    login: S,
    password: S,
  }

  impl<S> fmt::Display for DbConfig<S>
  where
    Self: Serialize,
    S: AsRef<str>,
  {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
      writeln!(f, "{}", serde_json::to_string_pretty(self).unwrap())
    }
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
struct PostgresUrl(Url);

impl PostgresUrl {
  #[allow(unused)]
  pub fn new(
    host: impl AsRef<str>,
    port: u16,
    name: impl AsRef<str>,
    user: impl AsRef<str>,
    password: impl AsRef<str>,
    version: u8,
  ) -> Result<Self, url::ParseError> {
    let mut url = Url::parse("postgresql://localhost").expect("Expected default Postgres URI to be valid");
    url.set_host(Some(host.as_ref()))?;
    url
      .set_port(Some(port))
      .expect("Setting the port should always succeed");
    {
      let mut segments = url
        .path_segments_mut()
        .expect("Getting the path segments should always succeed");
      segments.push(name.as_ref());
    }
    url
      .set_username(user.as_ref())
      .expect("Setting the user should always succeed");
    url
      .set_password(Some(password.as_ref()))
      .expect("Setting the password should always succeed");
    {
      let mut query = url.query_pairs_mut();
      query.append_pair("version", version.to_string().as_str());
      query.append_pair("charset", "utf8");
    }
    Ok(Self(url))
  }
}

impl fmt::Display for PostgresUrl {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    fmt::Display::fmt(&self.0, f)
  }
}
