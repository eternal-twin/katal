use crate::etwin::channel::{
  ChannelDomain, ChannelFallbackServer, ChannelPaths, ChannelPhp, ChannelPostgres, ChannelTarget, ChannelTargetError,
  NodeEnv, PostgresCredentials,
};
use crate::task::composer::ComposerJson;
use crate::task::fs::{EnsureDir, EnsureDirError, EnsureDirSymlink, EnsureFile};
use crate::task::git::GitCheckout;
use crate::task::nginx::{NginxAvailableSite, NginxEnableSite};
use crate::task::php::PhpAvailable;
use crate::task::postgres::PostgresUrl;
use crate::task::witness::{FsWitness, FsWitnessError};
use crate::task::{exec_as, AsyncFn, ExecAsError, NamedTask, TaskName, TaskRef, TaskResult, TaskSuccess};
use crate::utils::display_error_chain::DisplayErrorChainExt;
use async_trait::async_trait;
use asys::common::fs::{FsHost, ReadFileError};
use asys::linux::fs::{FileMode, LinuxFsHost, LinuxMetadata, ResolveError};
use asys::linux::user::{GroupRef, LinuxUser, LinuxUserHost, UserRef};
use asys::local_linux::LocalLinux;
use asys::{Command, ExecHost};
use core::fmt::Debug;
use indexmap::IndexMap;
use katal_loader::tree::{LocalFs, ReadError, ReadFsx};
use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};
use std::path::PathBuf;
use url::Url;

const PG_VERSION: u8 = 15;

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct AutoChannelTarget<S: AsRef<str> = String> {
  pub inner: ChannelTarget<S>,
  pub vault: AutoVault,
  pub etwin_uri: Url,
  pub etwin_client_id: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct AutoVault {
  pub secret: String,
  pub etwin_client_secret: String,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct AutoChannel<User: LinuxUser, S: AsRef<str> = String> {
  full_name: S,
  short_name: S,
  user: User,
  domain: ChannelDomain<S>,
  paths: ChannelPaths,
  fallback_server: ChannelFallbackServer,
  postgres: ChannelPostgres,
  node: NodeEnv,
  php: Option<ChannelPhp>,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct AutoReleaseTarget<S: AsRef<str> = String> {
  pub channel: AutoChannelTarget,
  pub repository: S,
  pub git_ref: String,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct AutoRelease<User: LinuxUser, S: AsRef<str> = String> {
  release_dir: PathBuf,
  /// Git pull URL for the repository
  repository: S,
  /// Git reference for the commit to use for this release (e.g. commit SHA-1)
  git_ref: String,
  repo_dir: PathBuf,
  channel: AutoChannel<User, S>,
}

#[derive(Serialize, Deserialize)]
pub struct DeployAuto<S: AsRef<str> = String> {
  /// sudo password (to become root)
  pub pass: S,
  /// Wanted release to deploy
  pub target: AutoReleaseTarget,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum DeployAutoError {
  #[error("failed to exec auto deployment task")]
  Exec(#[source] ExecAsError),
  #[error("failed to deploy auto release")]
  Deploy(#[source] DeployAutoAsRootError),
}

#[async_trait]
impl<'h, H: ExecHost, S: AsRef<str> + Send + Sync> AsyncFn<&'h H> for DeployAuto<S> {
  type Output = TaskResult<(), DeployAutoError>;

  async fn run(&self, _host: &'h H) -> Self::Output {
    let input = DeployAutoAsRoot {
      target: self.target.clone(),
    };
    exec_as::<LocalLinux, DeployAutoAsRoot>(UserRef::ROOT, Some(self.pass.as_ref()), &input)
      .await
      .map_err(DeployAutoError::Exec)?
      .map_err(DeployAutoError::Deploy)
  }
}

impl<'h, H: 'h + ExecHost> NamedTask<&'h H> for DeployAuto {
  const NAME: TaskName = TaskName::new("emush::DeployAuto");
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DeployAutoAsRoot {
  target: AutoReleaseTarget,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum DeployAutoAsRootError {
  #[error("failed to use witness file")]
  Witness(#[source] FsWitnessError),
  #[error("failed to run task `AutoReleaseTarget`")]
  Task(#[source] AutoReleaseTargetError),
}

#[async_trait]
impl<'h, H: ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for DeployAutoAsRoot
where
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Debug + Clone + Send + Sync + Serialize + DeserializeOwned,
  H::Group: Debug + Send + Sync,
{
  type Output = TaskResult<(), DeployAutoAsRootError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let paths: ChannelPaths = self.target.channel.inner.paths();
    let witness = paths.home.join("auto_release.witness");
    let mut changed = false;
    let res = FsWitness::new(witness, TaskRef(&self.target))
      .run(host)
      .await
      .map_err(DeployAutoAsRootError::Witness)?;
    changed = res.changed || changed;
    let res = res.output.map_err(DeployAutoAsRootError::Task)?;
    changed = res.changed || changed;
    Ok(TaskSuccess { changed, output: () })
  }
}

impl<'h, H> NamedTask<&'h H> for DeployAutoAsRoot
where
  H: 'h + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Debug + Clone + Send + Sync + Serialize + DeserializeOwned,
  H::Group: Debug + Send + Sync,
{
  const NAME: TaskName = TaskName::new("emush::DeployAutoAsRoot");
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum AutoReleaseTargetError {
  #[error("failed to ensure commit directory")]
  CommitDir(#[source] EnsureDirError),
  #[error("failed to execute git checkout task")]
  ExecGitCheckout(#[source] ExecAsError),
  #[error("failed to initialize auto channel")]
  Channel(#[source] ChannelTargetError),
  #[error("failed to ensure release directory")]
  ReleaseDir(#[source] EnsureDirError),
  #[error("failed to execute build task")]
  ExecBuild(#[source] ExecAsError),
  #[error("unexpected error: {0}")]
  Other(String),
}

impl From<anyhow::Error> for AutoReleaseTargetError {
  fn from(value: anyhow::Error) -> Self {
    Self::Other(value.display_chain().to_string())
  }
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for AutoReleaseTarget
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Debug + Clone + Send + Sync,
  H::Group: Debug + Send + Sync,
{
  type Output = TaskResult<AutoRelease<H::User>, AutoReleaseTargetError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let channel = self.channel.run(host).await?;
    let channel = channel.output;
    eprintln!("clone repository");
    let commit_dir = channel.paths.commits.join(self.git_ref.as_str());
    let mut changed = EnsureDir::new(commit_dir.clone())
      .owner(UserRef::Id(channel.user.uid()))
      .group(GroupRef::Id(channel.user.gid()))
      .mode(FileMode::OWNER_ALL | FileMode::ALL_READ | FileMode::ALL_EXEC)
      .run(host)
      .await
      .map_err(AutoReleaseTargetError::CommitDir)?
      .changed;
    let release_dir = channel.paths.releases.join(&self.git_ref);

    let witness = release_dir.join("ok");

    let witness_exists = match host.read_file(witness.as_path()) {
      Ok(_) => true,
      Err(ReadFileError::NotFound) => false,
      Err(e) => {
        panic!("failed to check witness file: {e:?}")
      }
    };

    let repo_dir = release_dir.join("repo");

    let mut release = AutoRelease {
      release_dir: release_dir.clone(),
      repository: self.repository.clone(),
      git_ref: self.git_ref.clone(),
      repo_dir: repo_dir.clone(),
      channel,
    };

    if witness_exists {
      eprintln!(
        "bailing-out, commit already downloaded (assuming the release already completed) (todo: better tracking)"
      );
      return Ok(TaskSuccess {
        changed,
        output: release,
      });
    }

    eprintln!("checkout repository: {:?} {:?}", self.repository, self.git_ref);
    let gc = GitCheckout {
      path: commit_dir.clone(),
      remote: self.repository.clone(),
      r#ref: self.git_ref.clone(),
    };

    changed = exec_as::<LocalLinux, _>(UserRef::Id(release.channel.user.uid()), None, &gc)
      .await
      .map_err(AutoReleaseTargetError::ExecGitCheckout)?
      .map_err(|e| anyhow::Error::new(e).context("failed to checkout commit"))?
      .changed
      || changed;

    let tree = LocalFs::new(commit_dir);
    let mut composer_install = false;
    let mut build_script: Option<String> = None;
    let mut config_script: Option<String> = None;
    let mut db_script: Option<String> = None;
    let php_target: Option<PhpAvailable> =
      match <LocalFs as ReadFsx<[String]>>::read::<[String; 1]>(&tree, ["composer.json".to_string()]).await {
        Ok(composer_json) => {
          composer_install = true;
          let composer_json = String::from_utf8(composer_json).expect("composer.json is not UTF-8");
          let composer_json =
            serde_json::from_str::<ComposerJson>(composer_json.as_str()).expect("invalid `composer.json`");
          let mut php_target = PhpAvailable::default();
          // TODO: Read it from the lockfile instead
          for req in composer_json.require.keys().chain(composer_json.require.keys()) {
            match req.as_str() {
              "ext-pdo_pgsql" => {
                php_target.ext_pgsql = true;
                php_target.ext_pdo_pgsql = true;
              }
              "ext-sodium" => {
                php_target.ext_sodium = true;
              }
              "ext-iconv" => {
                php_target.ext_iconv = true;
              }
              _ => {}
            }
          }
          for script in ["katal:config", "config:katal", "config"] {
            if composer_json.scripts.contains_key(script) {
              config_script = Some(script.to_string());
              break;
            }
          }
          for script in ["build:production", "build"] {
            if composer_json.scripts.contains_key(script) {
              build_script = Some(script.to_string());
              break;
            }
          }
          for script in ["db:sync:production", "db:sync"] {
            if composer_json.scripts.contains_key(script) {
              db_script = Some(script.to_string());
              break;
            }
          }
          Some(php_target)
        }
        Err(ReadError::NotFound) => None,
        Err(error) => panic!("unexpected error when reading `composer.json`: {error:?}"),
      };

    {
      let mut auto_channel = self.channel.inner.clone();
      auto_channel.php = php_target;
      let channel_env = auto_channel.run(host).await.map_err(AutoReleaseTargetError::Channel)?;
      changed = channel_env.changed || changed;
      release.channel.php = channel_env.output.php;
    }

    eprintln!("create release directory: {}", release_dir.display());
    changed = EnsureDir::new(release_dir.clone())
      .owner(UserRef::Id(release.channel.user.uid()))
      .group(GroupRef::Id(release.channel.user.gid()))
      .mode(FileMode::OWNER_ALL | FileMode::ALL_READ | FileMode::ALL_EXEC)
      .run(host)
      .await
      .map_err(AutoReleaseTargetError::ReleaseDir)?
      .changed
      || changed;

    let protocol = if release.channel.domain.cert.is_some() {
      "https"
    } else {
      "http"
    };
    let uri = format!("{}://{}", protocol, release.channel.domain.main.as_str());

    let br = BuildAutoRelease {
      node: release.channel.node.clone(),
      channel: self.channel.inner.full_name.clone(),
      secret: self.channel.vault.secret.clone(),
      external_uri: Url::parse(&uri).unwrap(),
      composer_install,
      config_script,
      build_script,
      repo_dir: repo_dir.clone(),
      remote: self.repository.clone(),
      git_ref: self.git_ref.clone(),
      db_name: release.channel.postgres.name.clone(),
      db_admin: release.channel.postgres.admin.clone(),
      db_main: release.channel.postgres.main.clone(),
      eternaltwin_uri: self.channel.etwin_uri.clone(),
      oauth_client_id: self.channel.etwin_client_id.clone(),
      oauth_client_secret: self.channel.vault.etwin_client_secret.clone(),
    };

    changed = exec_as::<LocalLinux, _>(UserRef::Id(release.channel.user.uid()), None, &br)
      .await
      .map_err(AutoReleaseTargetError::ExecBuild)?
      .unwrap()
      .changed
      || changed;

    changed = (UpdateActiveAutoRelease {
      release: release.clone(),
      db_script,
    })
    .run(host)
    .await?
    .changed
      || changed;

    // TODO: Delete commit dir?

    changed = EnsureFile::new(witness).content([]).run(host).await.unwrap().changed || changed;

    Ok(TaskSuccess {
      changed,
      output: release,
    })
  }
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for AutoChannelTarget
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  H::User: Debug + Clone + Send + Sync,
  H::Group: Debug + Send + Sync,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<AutoChannel<H::User>, anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    eprintln!("eMush: Create channel");
    let res = self.inner.run(host).await?;

    eprintln!("Channel ready");
    let output = AutoChannel {
      full_name: res.output.full_name,
      short_name: res.output.short_name,
      user: res.output.user,
      domain: res.output.domain.expect("Expected associated domain name"),
      paths: res.output.paths,
      fallback_server: res.output.fallback_server.expect("Expected fallback server"),
      postgres: res.output.postgres.expect("Expected Postgres database"),
      node: res.output.node.expect("Expected Node environment"),
      php: None,
    };

    Ok(TaskSuccess {
      changed: res.changed,
      output,
    })
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct BuildAutoRelease {
  node: NodeEnv,
  channel: String,
  secret: String,
  external_uri: Url,
  composer_install: bool,
  /// Composer config script to run (if any)
  config_script: Option<String>,
  /// Composer build script to run (if any)
  build_script: Option<String>,
  repo_dir: PathBuf,
  remote: String,
  git_ref: String,
  db_name: String,
  db_admin: PostgresCredentials,
  db_main: PostgresCredentials,
  eternaltwin_uri: Url,
  oauth_client_id: String,
  oauth_client_secret: String,
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for BuildAutoRelease
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Send + Sync,
{
  type Output = Result<TaskSuccess<()>, ()>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let gc = GitCheckout {
      path: self.repo_dir.clone(),
      remote: self.remote.clone(),
      r#ref: self.git_ref.clone(),
    };

    eprintln!("Checkout repository: {:?} {:?}", self.remote, self.git_ref);
    let res = gc.run(host).await.unwrap();
    let mut changed = res.changed;

    if self.composer_install {
      eprintln!("installing composer dependencies");
      let cmd = Command::new("composer")
        .arg("install")
        .arg("--no-interaction")
        .arg("--no-ansi")
        .current_dir(self.repo_dir.to_str().unwrap());
      host.exec(&cmd).unwrap();
    }

    match self.config_script.as_deref() {
      None => {
        eprintln!("no config script")
      }
      Some(config_script) => {
        eprintln!("running config: {config_script}");
        let config = AppConfig {
          channel: self.channel.clone(),
          revision: self.git_ref.clone(),
          secret: self.secret.clone(),
          external_uri: self.external_uri.to_string(),
          database: AppDatabaseConfig {
            version: PG_VERSION,
            host: "localhost".to_string(),
            port: 5432,
            name: self.db_name.clone(),
            roles: AppDatabaseRolesConfig {
              admin: AppDatabaseRoleConfig {
                uri: PostgresUrl::new(
                  "localhost",
                  5432,
                  self.db_name.as_str(),
                  self.db_admin.name.as_str(),
                  self.db_admin.password.as_str(),
                  Some(PG_VERSION),
                  None,
                )
                .unwrap()
                .to_string(),
                name: self.db_admin.name.to_string(),
                password: self.db_admin.name.to_string(),
              },
              main: AppDatabaseRoleConfig {
                uri: PostgresUrl::new(
                  "localhost",
                  5432,
                  self.db_name.as_str(),
                  self.db_main.name.as_str(),
                  self.db_main.password.as_str(),
                  Some(PG_VERSION),
                  None,
                )
                .unwrap()
                .to_string(),
                name: self.db_main.name.to_string(),
                password: self.db_main.password.to_string(),
              },
            },
          },
          eternaltwin: AppEternaltwinConfig {
            uri: self.eternaltwin_uri.to_string(),
            oauth: AppEternaltwinOauthConfig {
              id: self.oauth_client_id.clone(),
              secret: self.oauth_client_secret.clone(),
            },
          },
          extra: IndexMap::new(),
        };

        let config = serde_json::to_string(&config).unwrap();

        let cmd = Command::new("composer")
          .arg("run-script")
          .arg("--")
          .arg(config_script)
          .stdin(config.into_bytes())
          .current_dir(self.repo_dir.to_str().unwrap());
        let out = host.exec(&cmd).unwrap();
        let out = String::from_utf8(out.stdout).expect("output must be UTF-8");
        for line in out.lines() {
          let line = line.trim();
          if line.is_empty() {
            continue;
          }
          let out: AppConfigOutput = serde_json::from_str(line).expect("invalid config output");
          let out_file = self.repo_dir.join(out.file);
          eprintln!("write config: {}", out_file.display());
          let file_out = EnsureFile::new(out_file)
            .content(out.data.as_bytes())
            .run(host)
            .await
            .expect("failed to write config");
          changed = file_out.changed || changed;
        }
      }
    }

    match self.build_script.as_deref() {
      None => {
        eprintln!("no build script")
      }
      Some(build_script) => {
        eprintln!("running build: {build_script}");

        let cmd = Command::new("composer")
          .arg("run-script")
          .arg("--")
          .arg(build_script)
          .current_dir(self.repo_dir.to_str().unwrap());
        host.exec(&cmd).unwrap();
      }
    }

    Ok(TaskSuccess { changed, output: () })
  }
}

impl<'h, H> NamedTask<&'h H> for BuildAutoRelease
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Send + Sync,
{
  const NAME: TaskName = TaskName::new("emush::BuildAutoRelease");
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize)]
pub struct UpdateActiveAutoRelease<User: LinuxUser, S: AsRef<str> = String> {
  release: AutoRelease<User, S>,
  db_script: Option<String>,
}

#[async_trait]
impl<'h, H, S> AsyncFn<&'h H> for UpdateActiveAutoRelease<H::User, S>
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Send + Sync,
  S: AsRef<str> + Send + Sync,
{
  type Output = TaskResult<(), anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let mut changed = false;
    let active_release_link = self.release.channel.paths.home.join("active");
    let previous_release_link = self.release.channel.paths.home.join("previous");
    let old_release_dir = {
      match host.resolve(&active_release_link) {
        Ok(old) => Some(old),
        Err(ResolveError::NotFound) => None,
        Err(e) => return Err(e.into()),
      }
    };
    let new_release_dir = host.resolve(&self.release.release_dir)?;

    if let Some(old_release_dir) = old_release_dir {
      if old_release_dir != new_release_dir {
        eprintln!("Start to disable old release");
        // There is already an active release but it does not match the target
        // release: stop the old version before proceeding to free the channel
        // resources (database, port)
        EnsureDirSymlink::new(previous_release_link.clone())
          .points_to(old_release_dir.clone())
          .owner(self.release.channel.user.uid())
          .group(self.release.channel.user.gid())
          .mode(FileMode::ALL_READ | FileMode::ALL_EXEC)
          .run(host)
          .await?;

        eprintln!("Old release disabled");
        changed = true;
      }
    }

    eprintln!("UpgradingDatabase");
    changed = exec_as::<LocalLinux, _>(
      UserRef::Id(self.release.channel.user.uid()),
      None,
      &UpgradeAutoDatabase {
        repo_dir: self.release.repo_dir.clone(),
        db_name: self.release.channel.postgres.name.clone(),
        db_admin: self.release.channel.postgres.admin.clone(),
        db_script: self.db_script.clone(),
      },
    )
    .await?
    .unwrap()
    .changed
      || changed;

    eprintln!("Generating nginx config");
    let nginx_config = get_nginx_config(&self.release.channel);
    eprintln!("Writing nginx config");
    NginxAvailableSite::new(self.release.channel.full_name.as_ref(), nginx_config)
      .run(host)
      .await?;
    eprintln!("Enabling nginx config");
    NginxEnableSite::new(self.release.channel.full_name.as_ref())
      .run(host)
      .await?;

    eprintln!("Marking release as active");
    EnsureDirSymlink::new(active_release_link.clone())
      .points_to(new_release_dir.clone())
      .owner(self.release.channel.user.uid())
      .group(self.release.channel.user.gid())
      .mode(FileMode::ALL_READ | FileMode::ALL_EXEC)
      .run(host)
      .await?;

    Ok(TaskSuccess { changed, output: () })
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct UpgradeAutoDatabase {
  repo_dir: PathBuf,
  db_name: String,
  db_admin: PostgresCredentials,
  db_script: Option<String>,
}

#[async_trait]
impl<'h, H: ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for UpgradeAutoDatabase
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = Result<TaskSuccess<()>, ()>;

  async fn run(&self, host: &'h H) -> Self::Output {
    match self.db_script.as_deref() {
      None => {
        eprintln!("no db migration script")
      }
      Some(db_script) => {
        eprintln!("running db migration: {db_script}");
        let admin_db_url = PostgresUrl::new(
          "localhost",
          5432,
          self.db_name.as_str(),
          self.db_admin.name.as_str(),
          self.db_admin.password.as_str(),
          Some(PG_VERSION),
          None,
        )
        .unwrap()
        .to_string();

        let cmd = Command::new("composer")
          .arg(db_script)
          .env("DATABASE_URL", admin_db_url)
          .current_dir(self.repo_dir.to_str().unwrap());
        host.exec(&cmd).unwrap();
      }
    }

    Ok(TaskSuccess {
      changed: true,
      output: (),
    })
  }
}

impl<'h, H: 'h + ExecHost + LinuxUserHost + LinuxFsHost> NamedTask<&'h H> for UpgradeAutoDatabase
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  const NAME: TaskName = TaskName::new("emush::UpgradeAutoDatabase");
}

pub fn get_nginx_config<U: LinuxUser, S: AsRef<str>>(channel: &AutoChannel<U, S>) -> String {
  let access_log = channel.paths.logs.join("main.nginx.access.log");
  let error_log = channel.paths.logs.join("main.nginx.error.log");
  let web_root = channel.paths.home.join("active/repo/public");

  let main_directives = format!(
    r#"  # Space-separated hostnames (wildcard * is accepted)
    server_name {domain};

    access_log {access_log};
    error_log {error_log};
    client_max_body_size 5M;

    root {web_root};

    location ~* ^/(?:api/v1|oauth)(?:/|$) {{
      # try to serve file directly, fallback to index.php
      try_files $uri /index.php$is_args$args;
    }}

    location ~ ^/index\.php(/|$) {{
      fastcgi_pass unix:{php_fpm_socket};
      fastcgi_split_path_info ^(.+\.php)(/.*)$;
      include fastcgi_params;

      # optionally set the value of the environment variables used in the application
      # fastcgi_param APP_ENV prod;
      # fastcgi_param APP_SECRET <app-secret-id>;
      # fastcgi_param DATABASE_URL "mysql://db_user:db_pass@host:3306/db_name";

      # When you are using symlinks to link the document root to the
      # current version of your application, you should pass the real
      # application path instead of the path to the symlink to PHP
      # FPM.
      # Otherwise, PHP's OPcache may not properly detect changes to
      # your PHP files (see https://github.com/zendtech/ZendOptimizerPlus/issues/126
      # for more information).
      fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
      fastcgi_param DOCUMENT_ROOT $realpath_root;
      # Prevents URIs that include the front controller. This will 404:
      # http://domain.tld/index.php/some-path
      # Remove the internal directive to allow URIs like this
      internal;
    }}

    # return 404 for all other php files not matching the front controller
    # this prevents access to other php files you don't want to be accessible.
    location ~ \.php$ {{
      return 404;
    }}

    location / {{
      # try to serve file directly, fallback to index.php
      try_files $uri /index.php$is_args$args;
      # root ...;
      # index index.html;
      # try_files $uri /index.html;
    }}
  "#,
    domain = channel.domain.main.as_ref(),
    access_log = access_log.to_str().unwrap(),
    error_log = error_log.to_str().unwrap(),
    web_root = web_root.to_str().unwrap(),
    php_fpm_socket = channel
      .php
      .as_ref()
      .expect("`auto` requires PHP at the moment")
      .socket
      .to_str()
      .unwrap(),
  );

  match channel.domain.cert.as_ref() {
    Some(https) => format!(
      r#"# HTTPS
  server {{
    # HTTPS port
    listen 443 ssl;
    listen [::]:443 ssl;

    ssl_certificate {cert};
    ssl_certificate_key {cert_key};

    # # HSTS (ngx_http_headers_module is required) (63072000 seconds)
    # add_header Strict-Transport-Security "max-age=63072000" always;

    {main_directives}
  }}

  # HTTP
  server {{
    # HTTP port
    listen 80;
    listen [::]:80;
    server_name {domain};
    return 301 https://$host$request_uri;
  }}

  # www.
  server {{
    listen 80;
    listen 443 ssl;
    listen [::]:80;
    listen [::]:443 ssl;

    server_name www.{domain};

    ssl_certificate {cert};
    ssl_certificate_key {cert_key};

    return 307 https://{domain}$request_uri;
  }}

  "#,
      main_directives = main_directives,
      domain = channel.domain.main.as_ref(),
      cert = https.fullchain_cert_path.to_str().unwrap(),
      cert_key = https.cert_key_path.to_str().unwrap(),
    ),
    None => format!(
      r#"# HTTP
  server {{
    # HTTP port
    listen 80;
    listen [::]:80;

    {main_directives}
  }}

  # www.
  server {{
    listen 80;
    listen [::]:80;

    server_name www.{domain};

    return 307 http://{domain}$request_uri;
  }}

  "#,
      main_directives = main_directives,
      domain = channel.domain.main.as_ref(),
    ),
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
struct AppConfig<S: AsRef<str> = String> {
  channel: S,
  revision: S,
  secret: S,
  external_uri: S,
  database: AppDatabaseConfig<S>,
  eternaltwin: AppEternaltwinConfig<S>,
  extra: IndexMap<String, S>,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
struct AppDatabaseConfig<S: AsRef<str> = String> {
  version: u8,
  host: S,
  port: u16,
  name: S,
  roles: AppDatabaseRolesConfig<S>,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
struct AppDatabaseRolesConfig<S: AsRef<str> = String> {
  admin: AppDatabaseRoleConfig<S>,
  main: AppDatabaseRoleConfig<S>,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
struct AppDatabaseRoleConfig<S: AsRef<str> = String> {
  uri: S,
  name: S,
  password: S,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
struct AppEternaltwinConfig<S: AsRef<str> = String> {
  uri: S,
  oauth: AppEternaltwinOauthConfig<S>,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
struct AppEternaltwinOauthConfig<S: AsRef<str> = String> {
  id: S,
  secret: S,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
struct AppConfigOutput {
  file: PathBuf,
  data: String,
}
