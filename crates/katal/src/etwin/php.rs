use crate::etwin::channel::ResolvedHttps;
use crate::etwin::nginx::NginxRedirectionServer;
use std::fmt;

/// Directives used by the main server, regardless of HTTP or HTTPS
#[allow(unused)]
struct NginxMainDirectivesDisplay<'a, Str: fmt::Display + 'a, Path: fmt::Display + 'a> {
  /// Main domain name (without trailing dot)
  /// Example: `kingdom.eternaltwin.org`.
  domain: &'a Str,
  /// Path to the access log
  access_log: &'a Path,
  /// Path to the error log
  error_log: &'a Path,
  web_root: &'a Path,
  static_root: &'a Path,
  /// Path to the PHP-FPM socket
  php_fpm_socket: &'a Path,
}

impl<'a, Str: fmt::Display + 'a, Path: fmt::Display + 'a> fmt::Display for NginxMainDirectivesDisplay<'a, Str, Path> {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    write!(
      f,
      r#"  # Main domain name
  server_name {domain};

  access_log {access_log};
  error_log {error_log};
  client_max_body_size 5M;

  root {web_root};

  location ~* ^/(?:api/v1|oauth)(?:/|$) {{
    # try to serve file directly, fallback to index.php
    try_files $uri /index.php$is_args$args;
  }}

  location ~ ^/index\.php(/|$) {{
    fastcgi_pass unix:{php_fpm_socket};
    fastcgi_split_path_info ^(.+\.php)(/.*)$;
    include fastcgi_params;

    # optionally set the value of the environment variables used in the application
    # fastcgi_param APP_ENV prod;
    # fastcgi_param APP_SECRET <app-secret-id>;
    # fastcgi_param DATABASE_URL "mysql://db_user:db_pass@host:3306/db_name";

    # When you are using symlinks to link the document root to the
    # current version of your application, you should pass the real
    # application path instead of the path to the symlink to PHP
    # FPM.
    # Otherwise, PHP's OPcache may not properly detect changes to
    # your PHP files (see https://github.com/zendtech/ZendOptimizerPlus/issues/126
    # for more information).
    fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
    fastcgi_param DOCUMENT_ROOT $realpath_root;
    # Prevents URIs that include the front controller. This will 404:
    # http://domain.tld/index.php/some-path
    # Remove the internal directive to allow URIs like this
    internal;
  }}

  # return 404 for all other php files not matching the front controller
  # this prevents access to other php files you don't want to be accessible.
  location ~ \.php$ {{
    return 404;
  }}

  location / {{
    root {static_root};
    index index.html;
    try_files $uri /index.html;
  }}
"#,
      domain = self.domain,
      access_log = self.access_log,
      error_log = self.error_log,
      web_root = self.web_root,
      static_root = self.static_root,
      php_fpm_socket = self.php_fpm_socket,
    )
  }
}

#[allow(unused)]
struct NginxConfigDisplay<'a, Str: fmt::Display + 'a, Path: fmt::Display + 'a, AltDomain: fmt::Display> {
  pub main: NginxMainDirectivesDisplay<'a, Str, Path>,
  pub cert: Option<&'a ResolvedHttps>,
  pub alt_domains: &'a [AltDomain],
}

impl<'a, Str: fmt::Display + 'a, Path: fmt::Display + 'a, AltDomain: fmt::Display> fmt::Display
  for NginxConfigDisplay<'a, Str, Path, AltDomain>
{
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    if let Some(cert) = self.cert {
      write!(
        f,
        r#"# HTTPS
server {{
  # HTTPS port
  listen 443 ssl;
  listen [::]:443 ssl;

  ssl_certificate {cert};
  ssl_certificate_key {cert_key};

  # # HSTS (ngx_http_headers_module is required) (63072000 seconds)
  # add_header Strict-Transport-Security "max-age=63072000" always;

  {main_directives}
}}

# HTTP
server {{
  # HTTP port
  listen 80;
  listen [::]:80;
  server_name {domain};
  return 301 https://$host$request_uri;
}}
"#,
        main_directives = &self.main,
        domain = &self.main.domain,
        cert = cert.fullchain_cert_path.to_str().unwrap(),
        cert_key = cert.cert_key_path.to_str().unwrap(),
      )?;
    } else {
      write!(
        f,
        r#"# HTTP
server {{
  listen 80;
  listen [::]:80;

  {main_directives}
}}
"#,
        main_directives = &self.main,
      )?;
    }

    if !self.alt_domains.is_empty() {
      let redirection_server = NginxRedirectionServer {
        sources: self.alt_domains,
        target: self.main.domain,
        cert: self.cert,
      };
      write!(f, "{}", redirection_server)?;
    }

    Ok(())
  }
}
