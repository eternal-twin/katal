#[derive(
  Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize, thiserror::Error,
)]
pub enum GetOneTextError {
  #[error("expected exactly one text node, none found")]
  Empty,
  #[error("expected exactly one text node, found two or more")]
  Many,
}

pub fn get_one_text(node: ElementRef) -> Result<&str, GetOneTextError> {
  let mut it = node.text();
  match (it.next(), it.next()) {
    (None, None) => Err(GetOneTextError::Empty),
    (Some(t), None) => Ok(t),
    (_, Some(_)) => Err(GetOneTextError::Many),
  }
}

macro_rules! selector {
  ($selector:literal $(,)?) => {{
    static SELECTOR: ::once_cell::race::OnceBox<::scraper::Selector> = ::once_cell::race::OnceBox::new();
    SELECTOR.get_or_init(|| match ::scraper::Selector::parse($selector) {
      Ok(selector) => Box::new(selector),
      Err(e) => {
        panic!("invalid selector {:?}: {:?}", $selector, e)
      }
    })
  }};
}

use scraper::ElementRef;
pub(crate) use selector;
