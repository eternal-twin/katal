use core::fmt;

pub trait DisplayErrorChainExt: std::error::Error {
  fn display_chain(&self) -> DisplayErrorChain<'_, Self> {
    DisplayErrorChain(self)
  }
}

impl<E> DisplayErrorChainExt for E where E: std::error::Error + ?Sized {}

/// Helper struct to print errors with their source chain.
pub struct DisplayErrorChain<'e, E>(pub &'e E)
where
  E: std::error::Error + ?Sized;

impl<'e, E> fmt::Display for DisplayErrorChain<'e, E>
where
  E: std::error::Error + ?Sized,
{
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    fmt::Display::fmt(&self.0, f)?;
    for e in core::iter::successors(self.0.source(), |e| e.source()) {
      f.write_str(": ")?;
      fmt::Display::fmt(e, f)?;
    }
    Ok(())
  }
}
