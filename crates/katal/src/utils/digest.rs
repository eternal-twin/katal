use arrayvec::ArrayString;
use core::fmt;
use std::str::FromStr;
use thiserror::Error;

struct StringVisitorFn<'a, V, E> {
  expected: &'a str,
  visitor: &'a (dyn Fn(&str) -> Result<V, E> + 'a),
}

impl<'de, V, E: std::error::Error> serde::de::Visitor<'de> for StringVisitorFn<'_, V, E> {
  type Value = V;

  #[inline(always)]
  fn expecting(&self, f: &mut fmt::Formatter) -> fmt::Result {
    f.write_str(self.expected)
  }

  #[inline(always)]
  fn visit_borrowed_str<EE: serde::de::Error>(self, v: &'de str) -> Result<Self::Value, EE> {
    (self.visitor)(v).map_err(|e| EE::custom(e))
  }

  #[inline(always)]
  fn visit_str<EE: serde::de::Error>(self, v: &str) -> Result<Self::Value, EE> {
    (self.visitor)(v).map_err(|e| EE::custom(e))
  }

  #[inline(always)]
  fn visit_string<EE: serde::de::Error>(self, v: String) -> Result<Self::Value, EE> {
    (self.visitor)(v.as_str()).map_err(|e| EE::custom(e))
  }
}

macro_rules! define_hex_digest {
  (
    name = $name:literal,
    ty = $ty:ident,
    ty_byte_error = $ty_byte_error:ident,
    ty_parse_error = $ty_parse_error:ident,
    byte_len = $byte_len:literal,
    hex_len = $hex_len:literal,
    EMPTY = $empty:expr,
  ) => {
    const _: () = assert!($byte_len * 2 == $hex_len);

    #[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub struct $ty([u8; $byte_len]);

    #[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Error)]
    #[error("invalid {} digest size, expected = {}, actual = {actual}", $name, $byte_len, actual = .0)]
    pub struct $ty_byte_error(pub usize);

    #[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Error)]
    pub enum $ty_parse_error {
      #[error("wrong length, expected {} hex characters as input, actual = {actual}", $hex_len, actual = .0)]
      Length(usize),
      #[error("input is not a lowercase hex string: found {character:?} at offset {offset}")]
      InvalidCharacter { character: char, offset: usize },
    }

    impl $ty {
      /// Digest for the empty array
      pub const EMPTY: Self = Self($empty);

      pub fn as_slice(&self) -> &[u8] {
        &self.0
      }

      pub const fn from_array(bytes: [u8; $byte_len]) -> Self {
        Self(bytes)
      }

      pub fn from_bytes(bytes: &[u8]) -> Result<Self, $ty_byte_error> {
        match <[u8; $byte_len]>::try_from(bytes) {
          Ok(a) => Ok(Self(a)),
          Err(_) => Err($ty_byte_error(bytes.len())),
        }
      }

      pub fn from_hex(input: &str) -> Result<Self, $ty_parse_error> {
        let mut bytes = [0u8; $byte_len];
        match hex::decode_to_slice(input, &mut bytes) {
          Ok(()) => Ok(Self(bytes)),
          Err(hex::FromHexError::InvalidHexCharacter { c, index }) => Err($ty_parse_error::InvalidCharacter {
            character: c,
            offset: index,
          }),
          Err(hex::FromHexError::OddLength | hex::FromHexError::InvalidStringLength) => {
            Err($ty_parse_error::Length(input.len()))
          }
        }
      }

      pub fn hex(&self) -> ArrayString<$hex_len> {
        let mut out = [0u8; $hex_len];
        hex::encode_to_slice(self.0, &mut out).expect("encoding to hex always succeeds");
        ArrayString::from_byte_string(&out).expect("converting the hex byte string to an array string always succeeds")
      }
    }

    impl TryFrom<&[u8]> for $ty {
      type Error = $ty_byte_error;

      fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
        Self::from_bytes(value)
      }
    }

    impl TryFrom<&str> for $ty {
      type Error = $ty_parse_error;

      fn try_from(value: &str) -> Result<Self, Self::Error> {
        Self::from_hex(value)
      }
    }

    impl From<$ty> for ArrayString<$hex_len> {
      fn from(value: $ty) -> Self {
        value.hex()
      }
    }

    impl FromStr for $ty {
      type Err = $ty_parse_error;

      fn from_str(input: &str) -> Result<Self, Self::Err> {
        Self::from_hex(input)
      }
    }

    impl fmt::Debug for $ty {
      fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_tuple(stringify!($ty)).field(&self.hex()).finish()
      }
    }

    impl fmt::Display for $ty {
      fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Display::fmt(&self.hex(), f)
      }
    }

    impl serde::Serialize for $ty {
      fn serialize<S: serde::Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
        self.hex().serialize(serializer)
      }
    }

    impl<'de> serde::Deserialize<'de> for $ty {
      fn deserialize<D: serde::Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
        deserializer.deserialize_str(StringVisitorFn {
          expected: concat!("a ", $name, " digest"),
          visitor: &Self::from_hex,
        })
      }
    }
  };
}

#[rustfmt::skip]
define_hex_digest! {
  name = "sha2",
  ty = DigestSha2_256,
  ty_byte_error = DigestSha2FromBytesError,
  ty_parse_error = ParseDigestSha2Error,
  byte_len = 32,
  hex_len = 64,
  EMPTY = [
    0xe3, 0xb0, 0xc4, 0x42, 0x98, 0xfc, 0x1c, 0x14,
    0x9a, 0xfb, 0xf4, 0xc8, 0x99, 0x6f, 0xb9, 0x24,
    0x27, 0xae, 0x41, 0xe4, 0x64, 0x9b, 0x93, 0x4c,
    0xa4, 0x95, 0x99, 0x1b, 0x78, 0x52, 0xb8, 0x55,
  ],
}

impl DigestSha2_256 {
  pub fn digest(data: &[u8]) -> Self {
    use sha2::{Digest, Sha256};
    Self(Sha256::digest(data).into())
  }
}

#[rustfmt::skip]
define_hex_digest! {
  name = "sha3",
  ty = DigestSha3_256,
  ty_byte_error = DigestSha3FromBytesError,
  ty_parse_error = ParseDigestSha3Error,
  byte_len = 32,
  hex_len = 64,
  EMPTY = [
    0xa7, 0xff, 0xc6, 0xf8, 0xbf, 0x1e, 0xd7, 0x66,
    0x51, 0xc1, 0x47, 0x56, 0xa0, 0x61, 0xd6, 0x62,
    0xf5, 0x80, 0xff, 0x4d, 0xe4, 0x3b, 0x49, 0xfa,
    0x82, 0xd8, 0x0a, 0x4b, 0x80, 0xf8, 0x43, 0x4a,
  ],
}

impl DigestSha3_256 {
  pub fn digest(data: &[u8]) -> Self {
    use sha3::{Digest, Sha3_256};
    Self(Sha3_256::digest(data).into())
  }
}

#[cfg(test)]
mod test {
  use super::*;

  mod digest_sha2_256 {
    use super::*;

    #[test]
    fn empty_digest() {
      let actual = DigestSha2_256::digest(&[]);
      let expected = DigestSha2_256::EMPTY;
      assert_eq!(actual, expected);
    }

    #[test]
    fn from_hex_ok() {
      let actual = DigestSha2_256::from_hex("e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855");
      #[rustfmt::skip]
      let expected = Ok(DigestSha2_256([
        0xe3, 0xb0, 0xc4, 0x42, 0x98, 0xfc, 0x1c, 0x14,
        0x9a, 0xfb, 0xf4, 0xc8, 0x99, 0x6f, 0xb9, 0x24,
        0x27, 0xae, 0x41, 0xe4, 0x64, 0x9b, 0x93, 0x4c,
        0xa4, 0x95, 0x99, 0x1b, 0x78, 0x52, 0xb8, 0x55,
      ]));
      assert_eq!(actual, expected);
    }

    #[test]
    fn from_hex_empty() {
      let actual = DigestSha2_256::from_hex("");
      let expected = Err(ParseDigestSha2Error::Length(0));
      assert_eq!(actual, expected);
    }

    #[test]
    fn from_hex_too_long() {
      let actual = DigestSha2_256::from_hex("e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b85500");
      let expected = Err(ParseDigestSha2Error::Length(66));
      assert_eq!(actual, expected);
    }

    #[test]
    fn from_hex_odd_length() {
      let actual = DigestSha2_256::from_hex("e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b85");
      let expected = Err(ParseDigestSha2Error::Length(63));
      assert_eq!(actual, expected);
    }

    #[test]
    fn from_hex_invalid_char() {
      let actual = DigestSha2_256::from_hex("e3!0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855");
      let expected = Err(ParseDigestSha2Error::InvalidCharacter {
        character: '!',
        offset: 2,
      });
      assert_eq!(actual, expected);
    }
  }

  mod digest_sha3_256 {
    use super::*;

    #[test]
    fn empty_digest() {
      let actual = DigestSha3_256::digest(&[]);
      let expected = DigestSha3_256::EMPTY;
      assert_eq!(actual, expected);
    }

    #[test]
    fn from_hex_ok() {
      let actual = DigestSha3_256::from_hex("a7ffc6f8bf1ed76651c14756a061d662f580ff4de43b49fa82d80a4b80f8434a");
      #[rustfmt::skip]
        let expected = Ok(DigestSha3_256([
        0xa7, 0xff, 0xc6, 0xf8, 0xbf, 0x1e, 0xd7, 0x66,
        0x51, 0xc1, 0x47, 0x56, 0xa0, 0x61, 0xd6, 0x62,
        0xf5, 0x80, 0xff, 0x4d, 0xe4, 0x3b, 0x49, 0xfa,
        0x82, 0xd8, 0x0a, 0x4b, 0x80, 0xf8, 0x43, 0x4a,
      ]));
      assert_eq!(actual, expected);
    }
  }
}
