macro_rules! regex {
  ($regex:literal $(,)?) => {{
    static REGEX: ::once_cell::race::OnceBox<::regex::Regex> = ::once_cell::race::OnceBox::new();
    REGEX.get_or_init(|| match ::regex::Regex::new($regex) {
      Ok(regex) => Box::new(regex),
      Err(e) => {
        panic!("invalid regex {:?}: {:?}", $regex, e)
      }
    })
  }};
}

pub(crate) use regex;
