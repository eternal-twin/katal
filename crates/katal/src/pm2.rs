use serde::Serialize;
use std::collections::BTreeMap;
use std::path::{Path, PathBuf};

#[derive(Clone, Debug, PartialEq, Eq, Serialize)]
pub struct Pm2Ecosystem {
  pub apps: Vec<Pm2App>,
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize)]
pub struct Pm2App {
  pub name: String,
  pub script: PathBuf,
  pub watch: bool,
  pub env: BTreeMap<String, String>,
  pub cwd: Option<PathBuf>,
  pub node_args: Option<String>,
  pub interpreter: Option<String>,
}

impl Pm2App {
  pub fn new(name: String, script: PathBuf) -> Self {
    Self {
      name,
      script,
      watch: false,
      env: BTreeMap::new(),
      cwd: None,
      node_args: None,
      interpreter: None,
    }
  }

  pub fn env(mut self, name: impl AsRef<str>, value: impl AsRef<str>) -> Self {
    self.env.insert(name.as_ref().to_string(), value.as_ref().to_string());
    self
  }

  pub fn cwd(mut self, dir: impl AsRef<Path>) -> Self {
    self.cwd = Some(dir.as_ref().to_path_buf());
    self
  }

  pub fn node_args(mut self, args: impl AsRef<str>) -> Self {
    self.node_args = Some(args.as_ref().to_string());
    self
  }

  pub fn interpreter(mut self, spec: impl AsRef<str>) -> Self {
    self.interpreter = Some(spec.as_ref().to_string());
    self
  }
}
