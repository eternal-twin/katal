use vergen_gix::{Emitter, GixBuilder};

fn main() -> Result<(), Box<dyn std::error::Error>> {
  let gix = GixBuilder::all_git()?;

  Emitter::default().add_instructions(&gix)?.emit()?;

  Ok(())
}
