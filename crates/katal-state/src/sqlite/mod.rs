use crate::{AcquireIdempotencyTokenError, IdempotencyKey, IdempotencyToken, StateStore};
use async_trait::async_trait;
use chrono::{DateTime, Utc};
use rand::RngCore;
use sqlx::sqlite::{SqliteConnectOptions, SqlitePoolOptions};
use sqlx::{Pool, Sqlite, Transaction};
use std::path::Path;
use uuid::Uuid;

pub struct SqliteStore {
  pool: Pool<Sqlite>,
  // TODO: Clock, uuid generator
}

impl SqliteStore {
  pub async fn new<P: AsRef<Path>>(p: P, reset: bool) -> Self {
    let p = p.as_ref();
    if reset {
      std::fs::File::create(p).expect("failed to create Sqlite DB");
    }

    let pool = SqlitePoolOptions::new()
      .connect_with(SqliteConnectOptions::new().filename(p))
      .await
      .unwrap();
    if reset {
      let q = include_str!("./db.sql");
      sqlx::query(q).execute(&pool).await.unwrap();
    }
    let store = Self { pool };
    store.burn_all_tokens().await.unwrap();
    store
  }

  async fn burn_all_tokens(&self) -> Result<(), String> {
    // language=SQLite
    sqlx::query(r#"UPDATE idempotency_tokens SET state = 'Burned';"#)
      .execute(&self.pool)
      .await
      .map(drop)
      .map_err(|e| e.to_string())
  }

  async fn acquire_idempotency_token_inner(
    &self,
    tx: &mut Transaction<'_, Sqlite>,
    key: &IdempotencyKey,
  ) -> Result<IdempotencyToken, AcquireIdempotencyTokenError> {
    let now = Utc::now();

    {
      let expiry_time_limit = key
        .expiry
        .map(|d| now - chrono::Duration::from_std(d).expect("invalid expiry duration"));

      #[derive(Debug, sqlx::FromRow)]
      struct LockedRow {
        // token_id: Uuid,
        creation_time: DateTime<Utc>,
        // refresh_time: DateTime<Utc>,
        state: String,
      }

      // language=SQLite
      let res = sqlx::query_as::<_, LockedRow>(
        r#"
        SELECT token_id, creation_time, refresh_time, state
        FROM idempotency_tokens
        WHERE name = ? AND (state = 'Locked' OR coalesce(refresh_time > ?, TRUE))
        ORDER BY state = 'Locked' DESC, refresh_time DESC
        LIMIT 1;"#,
      )
      .bind(key.name.as_str())
      .bind(expiry_time_limit)
      .fetch_optional(tx.as_mut())
      .await
      .map_err(|e| e.to_string())
      .map_err(AcquireIdempotencyTokenError::Other)?;

      if let Some(old) = res {
        return match old.state.as_str() {
          "Locked" => Err(AcquireIdempotencyTokenError::Locked(key.clone(), old.creation_time)),
          "Burned" => Err(AcquireIdempotencyTokenError::Burned(key.clone(), old.creation_time)),
          s => panic!("unexpected state {s:?}"),
        };
      }
    }

    let token_id = Uuid::new_v4();
    let mut secret = [0u8; 32];
    rand::thread_rng().fill_bytes(&mut secret);
    assert_ne!(
      secret, [0u8; 32],
      "failed to generate secret key for the idempotency token"
    ); // TODO: don't panic?
    let secret = secret;

    // #[derive(Debug, sqlx::FromRow)]
    // struct Row {
    //   token_id: Uuid,
    // }

    // language=SQLite
    let res = sqlx::query(
      r#"INSERT INTO idempotency_tokens(token_id, secret, creation_time, refresh_time, name, state) VALUES (?, ?, ?, ?, ?, 'Locked');"#,
    )
      .bind(token_id)
      .bind(secret.as_slice())
      .bind(now)
      .bind(now)
      .bind(key.name.as_str())
      .execute(tx.as_mut()).await.map_err(|e| e.to_string()).map_err(AcquireIdempotencyTokenError::Other)?;
    assert_eq!(res.rows_affected(), 1);

    Ok(IdempotencyToken {
      secret,
      creation_time: now,
    })
  }
}

#[async_trait]
impl StateStore for SqliteStore {
  async fn acquire_idempotency_token(&self, key: &IdempotencyKey) -> Result<IdempotencyToken, AcquireIdempotencyTokenError> {
    let mut tx = self.pool.begin().await.map_err(|e| e.to_string()).map_err(AcquireIdempotencyTokenError::Other)?;
    let res = self.acquire_idempotency_token_inner(&mut tx, key).await;
    tx.commit().await.map_err(|e| e.to_string()).map_err(AcquireIdempotencyTokenError::Other)?;
    res
  }
}
