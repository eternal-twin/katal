-- DROP TABLE IF EXISTS main.katal_process_config;
-- DROP TABLE IF EXISTS main.host;
-- DROP TABLE IF EXISTS main.target;
-- DROP TABLE IF EXISTS main.katal_process;
-- DROP TABLE IF EXISTS main.load_target_job;
-- DROP TABLE IF EXISTS main.sync_host_job;
-- DROP TABLE IF EXISTS main.idempotency_tokens;

CREATE TABLE main.katal_process_config(
  katal_process_config_id BLOB PRIMARY KEY NOT NULL,
  config TEXT NOT NULL
);

CREATE TABLE main.host(
  host_id BLOB PRIMARY KEY NOT NULL,
  hostname TEXT NOT NULL,
  start_time INTEGER NOT NULL
);

CREATE TABLE main.target(
  target_id BLOB PRIMARY KEY NOT NULL,
  creation_time INTEGER NOT NULL,
  agent_version TEXT NOT NULL,
  config TEXT NOT NULL,
  vault BLOB NOT NULL
);

CREATE TABLE main.katal_process(
  katal_process_id BLOB PRIMARY KEY NOT NULL,
  start_time INTEGER NOT NULL,
  end_time INTEGER NULL,
  host_id BLOB NOT NULL,
-- Process metadata
  katal_version TEXT NOT NULL,
  pid INTEGER NOT NULL,
  uid INTEGER NOT NULL
);

CREATE TABLE main.load_target_job(
  load_target_job_id BLOB PRIMARY KEY NOT NULL,
  start_time INTEGER NOT NULL,
  end_time INTEGER NULL,
  katal_process_id BLOB NOT NULL,
  target_id BLOB NOT NULL,
  agent_pid INTEGER NOT NULL
);

CREATE TABLE main.sync_host_job(
  sync_host_job_id BLOB PRIMARY KEY NOT NULL,
  start_time INTEGER NOT NULL,
  end_time INTEGER NULL,
  katal_process_id BLOB NOT NULL,
  target_id BLOB NOT NULL,
  agent_pid INTEGER NOT NULL
);

CREATE TABLE main.idempotency_tokens(
  token_id BLOB PRIMARY KEY NOT NULL,
  secret BLOB NOT NULL,
  creation_time INTEGER NOT NULL,
  refresh_time INTEGER NOT NULL,
  name TEXT NOT NULL,
  state TEXT NOT NULL
);
