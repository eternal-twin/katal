use async_trait::async_trait;
use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use std::time::Duration;

pub mod sqlite;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize, thiserror::Error)]
pub enum AcquireIdempotencyTokenError {
  #[error("the token {0:?} is locked since {1}")]
  Locked(IdempotencyKey, DateTime<Utc>),
  #[error("the token {0:?} created at {1} is burned")]
  Burned(IdempotencyKey, DateTime<Utc>),
  #[error("{0}")]
  Other(String),
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize, thiserror::Error)]
pub enum TryAcquireIdempotencyTokenError {
  #[error("the token {0:?} is locked since {1}")]
  Locked(IdempotencyKey, DateTime<Utc>),
  #[error("{0}")]
  Other(String),
}

/// A key may be exchanged for tokens
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct IdempotencyKey {
  /// Unique name identifying this key
  pub name: String,
  /// Min duration between two successful token creations for this key.
  ///
  /// `None` means "infinite" (never reuse a key).
  pub expiry: Option<Duration>,
}

/// A token granting the right to run an action.
///
/// The token ensures the action is only executed once.
///
/// This also acts like a lock guard.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct IdempotencyToken {
  pub secret: [u8; 32],
  pub creation_time: DateTime<Utc>,
}

pub mod command {
  use crate::IdempotencyKey;
  use serde::{Deserialize, Serialize};

  #[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
  pub struct AcquireIdempotencyToken {
    key: IdempotencyKey,
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub enum StateCommand {
  AcquireIdempotencyToken(command::AcquireIdempotencyToken),
}

#[async_trait]
pub trait StateStore {
  /// Acquire an idempotency token ("at most once" semantics).
  ///
  /// An idempotency token works in two steps:
  /// 1. Create it in a locked state, the first consumer gets it.
  /// 2. Perform the action, then "burn" the token to mark the action as completed.
  ///
  /// If the token is already locked, it means that a concurrent action is using it already.
  /// This first action will either complete and burn the token. Or it will crash and the token
  /// will be stuck in a locked state. In this situation, you can call `burn_all_tokens` on the
  /// store to forcibly burn the existing tokens.
  ///
  /// If the `IdempotencyKey`, burned tokens are deleted after the expiration period, allowing
  /// to reuse the token.
  ///
  /// Result:
  /// - If it's the first access, `Ok(token)`
  /// - If it's a later access: error indicating if the token is locked/burned; or some internal error.
  async fn acquire_idempotency_token(&self, key: &IdempotencyKey) -> Result<IdempotencyToken, AcquireIdempotencyTokenError>;

  /// Like `acquire_idempotency_token`, but return `None` instead of `Err` in case of burned token.
  async fn try_acquire_idempotency_token(&self, key: &IdempotencyKey) -> Result<Option<IdempotencyToken>, TryAcquireIdempotencyTokenError> {
    try_idempotency_token_result(self.acquire_idempotency_token(key).await)
  }
}

pub fn try_idempotency_token_result(value: Result<IdempotencyToken, AcquireIdempotencyTokenError>) -> Result<Option<IdempotencyToken>, TryAcquireIdempotencyTokenError> {
  match value {
    Ok(token) => Ok(Some(token)),
    Err(AcquireIdempotencyTokenError::Burned(..)) => Ok(None),
    Err(AcquireIdempotencyTokenError::Locked(key, time)) => Err(TryAcquireIdempotencyTokenError::Locked(key, time)),
    Err(AcquireIdempotencyTokenError::Other(msg)) => Err(TryAcquireIdempotencyTokenError::Other(msg)),
  }
}

#[async_trait]
impl StateStore for () {
  async fn acquire_idempotency_token(&self, _key: &IdempotencyKey) -> Result<IdempotencyToken, AcquireIdempotencyTokenError> {
    panic!("()::acquire_idempotency_token is only a stub")
  }
}
