pub mod dir;
pub mod git;
pub mod tree;
use katal_vault::eval::EvalVaultError;
use serde_json::Value as JsonValue;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum ResolveVaultError {
  #[error("failed to evaluate vault")]
  Eval(#[from] EvalVaultError),
}

pub fn resolve_vault(key: &[u8], public_config: JsonValue) -> Result<JsonValue, ResolveVaultError> {
  match public_config {
    JsonValue::Object(mut config) => {
      if let Some(vault) = config.remove("vault") {
        let new_vault = match vault {
          JsonValue::Object(v) => JsonValue::Object(katal_vault::eval::eval_vault(key, v)?),
          v => v,
        };
        config.insert("vault".to_string(), new_vault);
      }
      Ok(JsonValue::Object(config))
    }
    v => Ok(v),
  }
}
