use crate::dir::DirLoader;
use crate::tree::LoadTargetError;
use git2::{FetchOptions, Oid, Repository, RepositoryInitOptions, ResetType};
use serde_json::Value as JsonValue;
use tempfile::TempDir;

pub struct GitLoader {
  remote: String,
  dir: Option<String>,
  r#ref: String,
}

#[derive(Debug)]
pub enum GitLoaderLoadError {
  TempDir(std::io::Error),
  InitRepository(git2::Error),
  AddRemote(git2::Error),
  FetchRemote(git2::Error),
  NoObjectFound,
  ResolveOneFetched,
  CommitNotFound(git2::Error),
  IterateFetched(git2::Error),
  ResetRepository(git2::Error),
  Inner(LoadTargetError),
}

#[derive(Debug)]
pub enum ResolveRefError {
  ResolveShortName(git2::Error),
  NoTarget,
  FindCommit(git2::Error),
}

impl GitLoader {
  pub fn new(repo: String, dir: Option<String>, r#ref: String) -> Self {
    Self {
      remote: repo,
      dir,
      r#ref,
    }
  }

  pub async fn load(&self) -> Result<JsonValue, GitLoaderLoadError> {
    let tmp_dir = TempDir::new().map_err(GitLoaderLoadError::TempDir)?;
    let tmp_dir = tmp_dir.path();

    let mut init_opts = RepositoryInitOptions::new();
    init_opts.mkpath(false);
    init_opts.mkdir(false);
    init_opts.external_template(false);
    let repository = Repository::init_opts(tmp_dir, &init_opts).map_err(GitLoaderLoadError::InitRepository)?;

    let mut remote = repository
      .remote_anonymous(&self.remote)
      .map_err(GitLoaderLoadError::AddRemote)?;
    {
      let mut fetch_opts = FetchOptions::new();
      let all_refs: [&str; 1] = [&self.r#ref];
      remote
        .fetch(&all_refs, Some(&mut fetch_opts), None)
        .map_err(GitLoaderLoadError::FetchRemote)?;
    }

    // TODO: Find some cleaner way to resolve the commit from the remote ref
    let mut fetched: Vec<Oid> = Vec::new();
    repository
      .fetchhead_foreach(|_git_ref, _remote_url, oid, _is_from_merge| {
        fetched.push(*oid);
        let should_stop_iteration: bool = false;
        should_stop_iteration
      })
      .map_err(GitLoaderLoadError::IterateFetched)?;
    let fetched: [Oid; 1] = fetched.try_into().map_err(|_| GitLoaderLoadError::ResolveOneFetched)?;
    let fetched = fetched[0];
    let commit = repository
      .find_commit(fetched)
      .map_err(GitLoaderLoadError::CommitNotFound)?;

    repository
      .reset(&commit.into_object(), ResetType::Hard, None)
      .map_err(GitLoaderLoadError::ResetRepository)?;

    let path = match self.dir.as_deref() {
      None => tmp_dir.to_path_buf(),
      Some(dir) => tmp_dir.join(dir),
    };

    DirLoader::new(path).load().await.map_err(GitLoaderLoadError::Inner)
  }
}

#[allow(unused)]
fn resolve_ref<'r>(repository: &'r Repository, git_ref: &'r str) -> Result<git2::Object<'r>, ResolveRefError> {
  let git_ref = repository
    .resolve_reference_from_short_name(git_ref)
    .map_err(ResolveRefError::ResolveShortName)?;
  let commit_oid = git_ref.target().ok_or(ResolveRefError::NoTarget)?;
  let commit = repository
    .find_commit(commit_oid)
    .map_err(ResolveRefError::FindCommit)?;
  Ok(commit.into_object())
}

// pub async fn load() -> String {
//   eprintln!("done start");
//   let odb: Odb<'static> = Odb::new().unwrap();
//   eprintln!("done odb");
//   let repo = git2::Repository::from_odb(odb).unwrap();
//   let odb2 = repo.odb().unwrap();
//   let mp = odb2.add_new_mempack_backend(999).unwrap(); // The ODB must be `>2`, the documentation recommends `1000`.
//                                                        // let _ = std::fs::remove_dir_all("etconf");
//                                                        // eprintln!("done rmdir");
//                                                        // let repo = git2::Repository::init("etconf").unwrap();
//   eprintln!("done init");
//   let mut remote = repo
//     .remote_anonymous("https://gitlab.com/eternaltwin/config.git")
//     .unwrap();
//   eprintln!("done remote");
//   let mut out = git2::Buf::new();
//   let dump = mp.dump(&repo, &mut out).unwrap();
//   eprintln!("done dump");
//   remote.fetch(&["main"], None, None).unwrap();
//   eprintln!("done fetch");
//   let thing = repo
//     .revparse_single("refs/commit/19af4ffc8d92213b1b587f327882e18e906b97a9")
//     .unwrap();
//   mp.reset().unwrap();
//   println!("{}", thing.id());
//   "".into()
// }
