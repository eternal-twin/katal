use crate::tree::{load_config, LoadTargetError, LocalFs};
use serde_json::Value as JsonValue;
use std::path::PathBuf;

pub struct DirLoader {
  fs: LocalFs,
}

impl DirLoader {
  pub fn new<P: Into<PathBuf>>(path: P) -> Self {
    Self {
      fs: LocalFs::new(path.into()),
    }
  }

  pub async fn load(&self) -> Result<JsonValue, LoadTargetError> {
    load_config(&self.fs).await
  }
}

#[cfg(test)]
mod test {
  use crate::dir::DirLoader;
  use crate::resolve_vault;
  use serde_json::Value as JsonValue;
  use std::path::PathBuf;

  #[tokio::test]
  async fn test_load_complete() {
    let input_path = PathBuf::from(env!("CARGO_MANIFEST_DIR")).join("../../test-resources/complete/input");
    let expected_path = PathBuf::from(env!("CARGO_MANIFEST_DIR")).join("../../test-resources/complete/expected.json");
    let loader = DirLoader::new(input_path);
    let actual: JsonValue = loader.load().await.unwrap();
    let expected = std::fs::read_to_string(expected_path.as_path()).unwrap();
    let expected: JsonValue = serde_json::from_str(&expected).unwrap();
    assert_eq!(actual, expected);
  }

  #[tokio::test]
  async fn test_load_complete_and_eval() {
    let input_path = PathBuf::from(env!("CARGO_MANIFEST_DIR")).join("../../test-resources/complete/input");
    let expected_path =
      PathBuf::from(env!("CARGO_MANIFEST_DIR")).join("../../test-resources/complete/expected-evaluated.json");
    let loader = DirLoader::new(input_path);
    let actual: JsonValue = loader.load().await.unwrap();
    let actual = resolve_vault(b"test", actual).unwrap();
    let expected = std::fs::read_to_string(expected_path.as_path()).unwrap();
    let expected: JsonValue = serde_json::from_str(&expected).unwrap();
    assert_eq!(actual, expected);
  }
}
