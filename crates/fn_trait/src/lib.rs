#![no_std]
#[cfg(feature = "alloc")]
extern crate alloc;

/// A function that can be called by value, like the standard library [`FnOnce`](`ops::FnOnce`) trait.
pub trait FnOnce<Args> {
  /// The return type of the function.
  type Output;

  /// Calls the function.
  fn call_once(self, args: Args) -> Self::Output;
}

/// This trait represents any type that can act as a unary asynchronous function.
///
/// This trait is based off the [`Fn`] trait and may become a simple alias
/// once Fn-traits are stabilized.
///
/// This represents a function taking a single input `Input` and returning a
/// future for the lifetime `Self::Output`. The output value may borrow from
/// the input.
///
/// Borrowing from `Self` is not yet possible: it requires Generic Associated
/// Types (GAT) which are not stable yet.
///
/// This trait definition is compatible with `async_trait`.
#[cfg(feature = "alloc")]
pub trait AsyncFn<Input> {
  type Output;

  #[must_use]
  fn run<'this, 'fut>(
    &'this self,
    input: Input,
  ) -> ::core::pin::Pin<::alloc::boxed::Box<dyn ::core::future::Future<Output = Self::Output> + Send + 'fut>>
  where
    'this: 'fut,
    Input: 'fut,
    Self: 'fut;
}
