# `fn_trait` - stable traits for Rust functions

This crate provide Rust traits for functions that may be used and implemented
in stable Rust today. Its goal is to provide a shared definition for these
traits, and to be compatible with the standard library.

In particular, it addresses use-cases depending on the following currently
unstable Rust features:
- [`fn_traits`](https://doc.rust-lang.org/stable/unstable-book/library-features/fn-traits.html):
  allow manual implementation of `Fn*` traits on user-defined typed.
- [`unboxed_closure`](https://doc.rust-lang.org/stable/unstable-book/language-features/unboxed-closures.html)
- Async function traits
