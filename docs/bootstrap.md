# Bootstrap

This document describes how to configure a new machine from scratch using Katal.
This is based on the configuration of the Eternaltwin server during 2024-12.

## Server imaging

Here are the server specs:

> ### Hetzner AX102 2024
> 
> - 1556.52€/year (129.71€/month)
> - CPU: AMD Ryzen 9 7950X3D 16c/32t - 4.2GHz
>     - [Geekbench v6: 2929/19702](https://browser.geekbench.com/processors/amd-ryzen-9-7950x3d)
>     - [PassMark: 4148/62496](https://www.cpubenchmark.net/cpu.php?cpu=AMD+Ryzen+9+7950X3D&id=5234)
> - RAM: 128GiB
> - Storage: 2×1.92TB SSD NVMe Soft RAID

While ordering, we generate an `ED25519 256` key pair and sent the public key
to Hetzner. This grants initial root access. A few days later we received an
email with the IP address and SSH fingerprint.

| Field                  | value                                                                                 |
|------------------------|---------------------------------------------------------------------------------------|
| IPv4 Address           | 162.55.107.242                                                                        |
| IPv6 Address           | 2a01:4f8:2191:302a::2                                                                 |
| Username               | root                                                                                  |
| Public key             | contact@eternaltwin.org e1:7a:55:f1:c2:2f:98:34:95:4f:c9:38:32:77:41:f2 (ED25519 256) |
| Host key (ECDSA 256)   | kNKjZqkB1jnfcdGT7pzmwn4wgHhM925WY+TQf/zHdp8 (ECDSA 256)                               |
| Host key (ED25519 256) | tEMkNDyCCeDoS8QmzJG1EIq32ekJaCBGNrGadIH3HME (ED25519 256)                             |
| Host key (RSA 3072)    | ymkLgJ4BAOj8D3GT9yoNDVq8IZ+cw6BpaeNJKBlMbvo (RSA 3072)                                |

We'll use the codename "Pousty" for this server. It's the name of the penguin
from the game "Fever".

- [Hetzner | Robot](https://robot.hetzner.com/)
- [Docs | Robot](https://docs.hetzner.com/robot/general/overview/)
- [Hetzner | Docs | Robot | Dedicated Server | Standard images](https://docs.hetzner.com/robot/dedicated-server/operating-systems/standard-images/)
- [Hetzner | Docs | Robot | Dedicated Server | Hetzner Rescue System](https://docs.hetzner.com/robot/dedicated-server/troubleshooting/hetzner-rescue-system/)
- [Hetzner | Docs | Robot | Dedicated Server | Installimage](https://docs.hetzner.com/robot/dedicated-server/operating-systems/installimage/)

Hetzner provides tooling to automatically some standard images.

We use "Arch Linux latest" as this distribution minimizes package customization
and keeps up-to-date.

### DNS entry

Add a DNS entry to identify the server, we'll use the following records:
1. IPv4:
   - Type: A
   - Name: pousty.eternaltwin.org.
   - Target: 162.55.107.242
2. IPv6:
   - Type: AAAA
   - Name: pousty.eternaltwin.org.
   - Target: 2a01:4f8:2191:302a::2

### SSH configuration

The SSH key pair is stored in the Bitwarden vault for Eternaltwin. It is
accessible to administrators.

1. Download the keys from Bitwarden, place them in `~/.ssh/`:
   ```
   $ ls -al ~/.ssh/eternaltwin*
   -rw------- 1 demurgos demurgos  464 Nov 22 00:11 eternaltwin_id_ed25519
   -rw-r--r-- 1 demurgos demurgos  105 Nov 22 00:11 eternaltwin_id_ed25519.pub
   ```
2. Edit `~/.ssh/config` (create it if needed, with permission `600`) to add
   the following entry:
   ```
   Host eternaltwin
    HostName pousty.eternaltwin.org.
    IdentityFile ~/.ssh/eternaltwin_id_ed25519
    IdentitiesOnly yes
    StrictHostKeyChecking yes
    User root
    Port 22
   ```
   [SSH config manual](https://man.archlinux.org/man/ssh_config.5)
3. Edit `~/.ssh/known_hosts`, add the following entry:
   ```
   pousty.eternaltwin.org. ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICUiwx/cOrbJdpgVgSfEIA/twxXvyNmj87DUjibfu4ko
   ```
   [File format](https://man.archlinux.org/man/core/openssh/sshd.8.en#SSH_KNOWN_HOSTS_FILE_FORMAT)
4. Check the connection:
   ```
   ssh eternaltwin
   ```
   The password is stored in Bitwarden with the key.

Once connected to the rescue system, you should be greeted with a message
similar to the following:

```
Linux rescue 6.11.3 #1 SMP Mon Oct 14 07:32:00 UTC 2024 x86_64

-------------------------------------------------------------------------------------------------------------------------

Welcome to the Hetzner Rescue System.

This Rescue System is based on Debian GNU/Linux 12 (bookworm) with a custom kernel.
You can install software like you would in a normal system.

To install a new operating system from one of our prebuilt images, run 'installimage' and follow the instructions.

Important note: Any data that was not written to the disks will be lost during a reboot.

For additional information, check the following resources:
Rescue System:           https://docs.hetzner.com/robot/dedicated-server/troubleshooting/hetzner-rescue-system
Installimage:            https://docs.hetzner.com/robot/dedicated-server/operating-systems/installimage
Install custom software: https://docs.hetzner.com/robot/dedicated-server/operating-systems/installing-custom-images
other articles:          https://docs.hetzner.com/robot

-------------------------------------------------------------------------------------------------------------------------

Rescue System (via EFI) up since 2024-11-22 14:36 +01:00

Hardware data:

CPU1: AMD Ryzen 9 7950X3D 16-Core Processor (Cores 32)
Memory:  127935 MB
Disk /dev/nvme0n1: 1920 GB (=> 1788 GiB) doesn't contain a valid partition table
Disk /dev/nvme1n1: 1920 GB (=> 1788 GiB) doesn't contain a valid partition table
Total capacity 3576 GiB with 2 Disks

Network data:
eth0  LINK: yes
MAC:  9c:6b:00:55:dc:02
IP:   162.55.107.242
IPv6: 2a01:4f8:2191:302a::2/64
Intel(R) Gigabit Ethernet Network Driver

```

### Installimage

Once conected to the rescue system, execute the command `installimage`

1. Choose o/s
   - `Arch Linux`
2. Choose image
   - `archlinux-latest-64-minimal`
3. Config file
   - Original in `hetzner-install.conf.original`
   - Final in `hetzner-install.conf`
   - Explanations:
     - We keep software RAID enabled. With 2 SSD drives, we use RAID level 1
       (mirroring) for reliability.
     - The hostname is set to `pousty`, the codename for this server.
     - We leave `ipv4only` disabled, we _want_ IPv6 support
     - We leave kernel mode setting enabled, as it's a headless system
     - Partitions are left with their default value. In particular, we use
       `ext4` for the root partition as [it was found to have a low overhead
       for Postgres compared to more recent filesystems such as btrfs or
       bcachefs](https://www.phoronix.com/review/linux-611-filesystems/3)
     - Image left as-is
4. Confirm that existing data will be deleted.

The setup will then run automatically:

```
                Hetzner Online GmbH - installimage

  Your server will be installed now, this will take some minutes
             You can abort at any time with CTRL+C ...

         :  Reading configuration                           done 
         :  Loading image file variables                    done 
         :  Loading archlinux specific functions            done 
   1/16  :  Deleting partitions                             done 
   2/16  :  Test partition size                             done 
   3/16  :  Creating partitions and /etc/fstab              done 
   4/16  :  Creating software RAID level 1                  done 
   5/16  :  Formatting partitions
         :    formatting /dev/md/0 with vfat                done 
         :    formatting /dev/md/1 with swap                done 
         :    formatting /dev/md/2 with ext3                done 
         :    formatting /dev/md/3 with ext4                done 
   6/16  :  Mounting partitions                             done 
   7/16  :  Sync time via ntp                               done 
         :  Importing public key for image validation       done 
   8/16  :  Validating image before starting extraction     done 
   9/16  :  Extracting image (local)                        done 
  10/16  :  Setting up network config                       done 
  11/16  :  Executing additional commands
         :    Setting hostname                              done 
         :    Generating new SSH keys                       done 
         :    Generating mdadm config                       done 
         :    Generating ramdisk                            done 
         :    Generating ntp config                         done 
  12/16  :  Setting up miscellaneous files                  done 
  13/16  :  Configuring authentication
         :    Fetching SSH keys                             done 
         :    Disabling root password                       done 
         :    Disabling SSH root login with password        done 
         :    Copying SSH keys                              done 
  14/16  :  Installing bootloader grub                      done 
  15/16  :  Running some archlinux specific functions       done 
  16/16  :  Clearing log files                              done 

                  INSTALLATION COMPLETE
   You can now reboot and log in to your new system with the
 same credentials that you used to log into the rescue system.
```

Once the config is complete, reboot the server: `systemctl reboot`.

## Server early configuration

Once the server reboots, you can connect to it with the same SSH credentials
after updating the fingerprint. The new `~/.ssh/known_hosts` entry is:

```
pousty.eternaltwin.org. ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIbT4tseU1xcMI8q4pNTrdGwR91jSQuYanqqTNPWSXWU
```

Once you connect, you can check the system config:

```
$ ssh eternaltwin
Last login: Sun Dec  8 10:48:34 2024 from <redacted_ip_addr>
[root@pousty ~]# ls -alR
.:
total 20
drwxr-x---  4 root root 4096 Dec  8 02:37 .
drwxr-xr-x 17 root root 4096 Dec  8 02:38 ..
-rw-r--r--  1 root root  149 Dec  8 02:37 .bash_profile
drwx------  3 root root 4096 Dec  8 02:37 .gnupg
drwx------  2 root root 4096 Dec  8 02:37 .ssh

./.gnupg:
total 12
drwx------ 3 root root 4096 Dec  8 02:37 .
drwxr-x--- 4 root root 4096 Dec  8 02:37 ..
drwx------ 2 root root 4096 Dec  8 02:37 crls.d

./.gnupg/crls.d:
total 12
drwx------ 2 root root 4096 Dec  8 02:37 .
drwx------ 3 root root 4096 Dec  8 02:37 ..
-rw-r--r-- 1 root root    5 Dec  8 02:37 DIR.txt

./.ssh:
total 12
drwx------ 2 root root 4096 Dec  8 02:37 .
drwxr-x--- 4 root root 4096 Dec  8 02:37 ..
-rw-r--r-- 1 root root  105 Dec  8 02:37 authorized_keys
```

### Original files

Original content of some files

#### /root/.bash_profile

```
### Hetzner Online GmbH installimage

alias ls='ls --color=auto'
alias ll='ls -l'
alias l='ls -A'
HISTCONTROL=ignoreboth
HISTFILESIZE=-1
HISTSIZE=-1
```

#### /root/.gnupg/crls.d/DIR.txt

```
v:1:
```

#### /root/.ssh/authorized_keys

```
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBpP07EgP+jm8Td0NpRuu42H6YWaBRZtrWZ9+mUhQfQi contact@eternaltwin.org
```

#### Pacman packages

```
$ pacman -Qe
base 3-2
btrfs-progs 6.12-1
cronie 1.7.2-1
cryptsetup 2.7.5-1
gptfdisk 1.0.10-1
grub 2:2.12-3
haveged 1.9.19-1
inetutils 2.5-1
linux 6.12.3.arch1-1
linux-firmware 20241111.b5885ec5-1
lvm2 2.03.28-1
mdadm 4.3-2
net-tools 2.10-3
openssh 9.9p1-2
python 3.12.7-1
rsync 3.3.0-2
vim 9.1.0866-1
wget 1.25.0-1
xfsprogs 6.12.0-1
```

## Katal initialization

### User creation

1. `timedatectl set-timezone UTC`
2. `pacman -S base-devel git htop make nano net-tools rust sudo`
3. Edit the `sudo` config: `EDITOR=vim visudo`
4. Uncomment the line `%wheel ALL=(ALL:ALL) ALL`
5. Save and exit (`:wq`)
6. `useradd --home-dir /opt/katal --create-home --user-group --groups wheel --shell /bin/bash --system katal`
7. `passwd katal`, configure a new random password

### User configuration

The following commands are executed as the `katal` user (`su katal` to switch,
check with `whoami`).

Original home content:

```
$ ls -alR /opt/katal
/opt/katal:
total 20
drwx------ 2 katal katal 4096 Dec  8 03:38 .
drwxr-xr-x 3 root  root  4096 Dec  8 03:38 ..
-rw-r--r-- 1 katal katal   21 Sep 24 21:46 .bash_logout
-rw-r--r-- 1 katal katal   57 Sep 24 21:46 .bash_profile
-rw-r--r-- 1 katal katal  172 Sep 24 21:46 .bashrc
```

With the following files:

```
[katal@pousty root]$ cat /opt/katal/.bash_logout 
#
# ~/.bash_logout
#
[katal@pousty root]$ cat /opt/katal/.bash_profile 
#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc
[katal@pousty root]$ cat /opt/katal/.bashrc 
#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias grep='grep --color=auto'
PS1='[\u@\h \W]\$ '
```

### Katal initialization

1. `git clone https://gitlab.com/eternaltwin/katal.git /opt/katal/repo`
2. Generate a random secret key value
3. Write the file `/opt/katal/repo/katal.toml` with the content below (set the
   key to the generate value)
4. `chmod 600 /opt/katal/repo/katal.toml`
5. `chmod 755 /opt/katal`
6. Edit the config repo, commit there. (e.g. set the sudo password, vault, apps, etc.)
7. Test manually with `cargo run --package katal -- sync`

```toml
[loader]
type = "Git"
repo = "https://gitlab.com/eternaltwin/config.git"
dir = "pousty"
ref = "pousty"

[state]
path = "./katald.sqlite"

[daemon]
key = "..."
```

### Refresh script

Add the file `/opt/katal/refresh.sh` and make it executable.

```
#!/usr/bin/env bash
set -eux
cd ~/repo && git fetch origin && git reset --hard origin/master && cargo clean && cargo run --package katal -- sync
```

### Enable Katal services



```
# ~/.bashrc: executed by bash(1) for non-login shells.

#export PS1='\h:\w\$ '
export PS1='\[\033[01;31m\]\u\[\033[01;33m\]@\[\033[01;32m\]\h \[\033[01;33m\]\w \[\033[01;35m\]\$ \[\033[00m\]'
umask 022

export LS_OPTIONS='--color=auto -h'
eval "`dircolors`"

alias ls='ls $LS_OPTIONS'
alias ll='ls $LS_OPTIONS -l'
alias l='ls $LS_OPTIONS -lA'
alias ..='cd ..'
alias ...='cd ../..'
source /root/.oldroot/nfs/bash_aliases
source /root/.oldroot/nfs/hwc/generic.functions
```

### Enable sync

```
# cat /etc/systemd/system/katal.service
[Unit]
Description=Synchronize the server

[Service]
Type=oneshot
User=katal
WorkingDirectory=/opt/katal/repo
ExecStart=cargo run --package katal -- sync

[Install]
WantedBy=multi-user.target
```

```
# cat /etc/systemd/system/katal.timer
[Unit]
Description=Timer for katal synchronization

[Timer]
# Try to sync every five minutes, with 2 minutes of randomness
OnCalendar=*-*-* *:*:00/5
RandomizedDelaySec=120
# Run immediately if the previous start time was missed (e.g. due to the server being powered off)
Persistent=true

[Install]
WantedBy=timers.target
```
