pub mod fetch_upstream;

use clap::Parser;
use once_cell::sync::Lazy;
use std::path::PathBuf;

pub static REPO_DIR: Lazy<PathBuf> = Lazy::new(|| {
  PathBuf::from(env!("CARGO_MANIFEST_DIR"))
    .join("..")
    .canonicalize()
    .expect("REPO_DIR exists")
});

#[derive(Debug, Parser)]
#[clap(author = "Eternaltwin")]
pub struct Args {
  #[clap(subcommand)]
  command: CliCommand,
}

#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
pub enum CliError {
  #[error(transparent)]
  FetchUpstream(#[from] fetch_upstream::FetchUpstreamError),
}

#[derive(Debug, Parser)]
pub enum CliCommand {
  /// Run the `fetch-upstream` subcommand to download upstream indexes
  #[clap(name = "fetch-upstream")]
  FetchUpstream(fetch_upstream::Args),
}

pub async fn run(args: &Args) -> Result<(), CliError> {
  match &args.command {
    CliCommand::FetchUpstream(ref args) => fetch_upstream::run(args).await?,
  }
  Ok(())
}
