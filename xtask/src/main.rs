use clap::Parser;
use xtask::Args;

#[tokio::main]
async fn main() {
  let args: Args = Args::parse();

  let res = xtask::run(&args).await;

  match res {
    Ok(_) => std::process::exit(0),
    Err(_) => res.unwrap(),
  }
}
