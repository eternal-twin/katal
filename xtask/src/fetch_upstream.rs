use crate::REPO_DIR;
use clap::Parser;
use katal::task::clickhouse::index::{ClickhouseIndex, ClickhouseIndexFetchEntriesError};
use reqwest::Client;

/// Arguments to the `fetch-upstream` task.
#[derive(Debug, Parser)]
pub struct Args {}

#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
pub enum FetchUpstreamError {
  #[error("failed to send node index request: {0}")]
  NodeIndexRequest(String),
  #[error("failed to read node index response: {0}")]
  NodeIndexResponse(String),
  #[error("failed to write node index result: {0}")]
  NodeIndexWrite(String),
  #[error("failed to fetch clickhouse index")]
  ClickhouseFetch(#[from] ClickhouseIndexFetchEntriesError),
  #[error("failed to serialize clickhouse index: {0}")]
  ClickhouseSerialize(String),
  #[error("failed to write clickhouse index result: {0}")]
  ClickhouseIndexWrite(String),
}

pub async fn run(_args: &Args) -> Result<(), FetchUpstreamError> {
  let client = Client::builder()
    .user_agent("katal_xtask")
    .build()
    .expect("building the client succeeds");

  {
    eprintln!("node index: start");
    let req = client
      .get("https://nodejs.org/download/release/index.json")
      .build()
      .expect("request is valid");
    let res = client
      .execute(req)
      .await
      .map_err(|e| FetchUpstreamError::NodeIndexRequest(e.to_string()))?;
    let body = res
      .bytes()
      .await
      .map_err(|e| FetchUpstreamError::NodeIndexResponse(e.to_string()))?;
    let out = REPO_DIR.join("./crates/katal/files/node/index.json");

    tokio::fs::write(out, body)
      .await
      .map_err(|e| FetchUpstreamError::NodeIndexWrite(e.to_string()))?;
    eprintln!("node index: done");
  }

  {
    eprintln!("clickhouse index: start");
    let index = ClickhouseIndex::fetch_entries("katal_xtask").await?;
    let out = REPO_DIR.join("./crates/katal/files/clickhouse/index.json");
    let index =
      serde_json::to_string_pretty(&index).map_err(|e| FetchUpstreamError::ClickhouseSerialize(e.to_string()))?;
    tokio::fs::write(out, index)
      .await
      .map_err(|e| FetchUpstreamError::ClickhouseIndexWrite(e.to_string()))?;
    eprintln!("clickhouse index: done");
  }

  Ok(())
}
